SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
Author: Mdewaard
date: ??
Notes:
  built to prevent long running linked queries to the Epicor server.
  Can either run against the Epicor_integration table or actually use the linked server to query the data.
Revision:
3/8/05   MDW  Added fields on EPICOR_INTEGRATION.DBO.EPICOR_AR_INVOICE_SUMMARY table and exposed them on this view

*/
create  view [dbo].[EPICOR_ARIN1PST_VW]
as 
select   	
    EPICOR_AR_INVOICE_SUMMARY_ID
  , [batch_code]
  , [doc_ctrl_num]
  , [Paid_flag]
  , [settled_status]
  , [amt_paid_to_date]
  , [unpaid_balance]
  , Customer_Code = Customer_Code
  , trx_type = trx_type
  , trx_desc = trx_desc

from EPICOR_INTEGRATION.DBO.EPICOR_AR_INVOICE_SUMMARY








GO
