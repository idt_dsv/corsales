SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*--Table: STD_PRICE
Author: MDW
Date: 11/15/05
Notes: View created for backwards compatiblity
  Table moved to reference database for replication

  Table moved to reference database for replication

Revisions
*/

CREATE view [dbo].[STD_PRICE]
 as 
Select 
STD_PRICE_ID
, PROD_COMP_ID
, FROM_DT
, STD_PRICE
, MIN_PRICE
, THRU_DT
, PRICE_LIST_ID
from reference.dbo.STD_PRICE

GO
