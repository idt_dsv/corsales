SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*--Table: PRICE_GRP
Author: MDW
Date: 11/15/05
Notes: View created for backwards compatiblity
  Table moved to reference database for replication
  Table moved to reference database for replication
Revisions
*/

CREATE view [dbo].[PRICE_GRP]
 as 
Select 
PRICE_GRP_ID
, PRICE_GRP_CD
, GRP_NM
, GRP_DESC
, SHIPPING
, ACTIVE
, CREATE_DATE
, PRICE_LIST_ID
from reference.dbo.PRICE_GRP

GO
