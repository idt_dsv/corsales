SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Date: 11/19/07
Author: MDW
Notes:
  Built to review the Sales Hierarchy
  
Example:
select * from [SalesTerritoryHierarchyVerboseVw] where territoryId = 27 and territoryType = 'Territory' order by SalesTerritoryIDPath

*/

CREATE VIEW [dbo].[SalesTerritoryHierarchyVerboseVw]
AS
  WITH  SalesManagerSecurity ( 
           SalesTerritoryID,ParentSalesTerritoryID
          ,ancestorSalesTerritoryId,TerritoryType,TerritoryId
          ,LEVEL,SalesTerritoryIDPath 
          )
  AS (
       SELECT
        SalesTerritoryID
      , ParentSalesTerritoryID
      , ancestorSalesTerritoryId = SalesTerritoryID
      , TerritoryType
      , TerritoryId
      , LEVEL = 0
      , CONVERT(VARCHAR(max) ,ISNULL(SalesTerritoryID ,'')) AS SalesTerritoryIDPath
       FROM
        salesTerritory
       WHERE ParentSalesTerritoryId IS null
       UNION ALL
       SELECT
        st.SalesTerritoryID
      , st.ParentSalesTerritoryID
      , sms.ancestorSalesTerritoryId
      , st.TerritoryType
      , st.TerritoryId
      , LEVEL = LEVEL + 1
      , CAST(SalesTerritoryIDPath + '.' + RTRIM(st.SalesTerritoryID) AS VARCHAR(MAX)) SalesTerritoryIDPath
       FROM
        salesTerritory st
        JOIN SalesManagerSecurity sms ON st.ParentSalesTerritoryID = sms.SalesTerritoryID
     )
  SELECT
    sms.SalesTerritoryID
  , sms.ParentSalesTerritoryID
  , sms.ancestorSalesTerritoryId
  , sms.TerritoryType
  , sms.TerritoryId
  , sms.LEVEL
  , REPLICATE(' | ' ,LEVEL) + st.[NAME] AS TerritoryHierarchy
  , st.NAME AS Territory
  , st.Description
  , sms.SalesTerritoryIDPath
  , Network_login_nm_list = dbo.getSalesTerritoryManagerNetworkNames(st.SalesTerritoryID)
  FROM
    SalesManagerSecurity sms
    JOIN salesTerritory st ON st.SalesTerritoryID = sms.SalesTerritoryID


GO
