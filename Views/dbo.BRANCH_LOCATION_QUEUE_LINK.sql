SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: Mark DeWaard
Date Created: Unknown
Purpose:

  This view was created to allow access to the table with the same name in the reference database.

Modifications:
Who     Date      Modification  Note
--------------------------------------------------------------
MDQ     ??/??/??  Created
CAS     10/09/08  Chris Sailor  Rebuilt and documented to reflect changes in the table in reference
*/
CREATE View [dbo].[BRANCH_LOCATION_QUEUE_LINK]
as
	select		*
	from		Reference.dbo.BRANCH_LOCATION_QUEUE_LINK
GO
