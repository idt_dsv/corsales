SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- View

/*
Author: MDW
Date: 6/25/07
Notes:
  Built to supply reporting needs

*/

CREATE VIEW [dbo].[InvoiceHeaderWithSubTotalVw]
AS
SELECT
  BATCH_ORD_ID
, BATCH_ID
, SALES_ORD_NBR
, INV_NBR
, CURR_STAT
, BILL_ACCT_ID
, ORG_NBR
, ACCT_NBR
, ENDUSER_NBR
, INV_LINES
, SHIP_DT
, ORD_DT
, INV_DT
, PAY_AUTH_NBR
, PAYMENT_TYPE
, PRICE_GRP_CD
, SHIP_METH
, SHIP_NM
, SHIP_ADDR1
, SHIP_ADDR2
, SHIP_CITY
, SHIP_ST
, SHIP_ZIP
, SHIP_CNTRY
, SHIP_PH
, BILL_NM
, BILL_ADDR1
, BILL_ADDR2
, BILL_CITY
, BILL_ST
, BILL_ZIP
, BILL_CNTRY
, BILL_PH
, SHIP_ENDUSER_NM
, ORD_METH
, CURRENCY_ID
, EXCHANGE_RATE
, BILL_FAX
, DELIVERY_METHOD_ID
, INV_TOTAL
, INVOICED_SHIP_PRICE = coalesce((
                                   select
                                    sum(IILI.amount)
                                   from
                                    sales.dbo.Inv_Line_Items IILI ( nolock )
                                   where
                                    IILI.batch_ord_id = Invoice_Header.batch_ord_id
                                    and IILI.Line_item_type = 2 -- Shipping

                                 ) ,0.0)
, INVOICED_SALES_TAX = coalesce((
                                  select
                                    sum(IILI.amount)
                                  from
                                    sales.dbo.Inv_Line_Items IILI ( nolock )
                                  where
                                    IILI.batch_ord_id = Invoice_Header.batch_ord_id
                                    and IILI.Line_item_type = 3 -- Sales Tax

                                ) ,0.0)
, INVOICED_CREDIT_APPLIED = coalesce((
                                       select
                                        sum(IILI.amount)
                                       from
                                        sales.dbo.Inv_Line_Items IILI ( nolock )
                                       where
                                        IILI.batch_ord_id = Invoice_Header.batch_ord_id
                                        and IILI.Line_item_type = 4 -- Credit applied
                                     ) ,0)
from
  production.dbo.inv_hdr_vw Invoice_Header ( nolock )
--WHERE sales_ord_nbr BETWEEN 4440000 AND 4440010





GO
