SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: MDW
Date: 5/30/08
Notes:
  Built for Portal Distributor reports. 
  Returns dataset of org_ids and distibutors for current date and time

example:
  select DISTRIBUTOR, 
    ORG_ID,
    ORG_NBR,
    ORG_NM
  from DistributorVw
  where par_party_id NOT in (240592,296890)


Permissions:
  grant select on DistributorVw to idt_reporter

Modifications:
Candace Schebel - 8-13-08 - Cleaned up column names, eliminated pr.par_party_id as it is
							the same as DistOrgID
Candace Schebel - 8-14-08 - Added is NULL on the thru_dt (some dates are NULL--others 9999)
*/


CREATE view [dbo].[DistributorVw]
as

select distinct
  DistributorName = dist.org_nm 
, DistOrgID = dist.party_id
, DistOrgNbr = dist.org_nbr
, OrgID = org.party_id
, OrgNbr = org.org_nbr
, OrgName = org.org_nm
from sales.dbo.party_rel pr
  join sales.dbo.org org on (org.party_id = pr.child_party_id
	or org.party_id = pr.par_party_id)
	and pr.from_dt < getdate()
	and (pr.thru_dt is NULL or pr.thru_dt > getdate())
	and pr.party_rel_type = 8
  join sales.dbo.org dist on pr.par_party_id = dist.party_id
GO
GRANT SELECT ON  [dbo].[DistributorVw] TO [IDT_Reporter]
GO
