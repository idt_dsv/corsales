SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[Sales_REGION_NEW]
AS 
select SALES_REGION_ID = vw.TerritoryId
, SALES_REGION_NM = vw.Name
, SALES_REGION_DESC = vw.Description
, IDT_GUID = vw.IDT_GUID
from SalesTerritoryManagerVw vw
where vw.territoryType = 'region'
GO
