SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
Author: MDW
Date: 12/30/03
Notes:
 Created to simplify getting address information for a person.

select *
from [ADDRESS_INFO_VW]
where enduser_nbr = 143432

Modifications:
12/31/03 MDW added org info
8/10/04  JL  added country info
4/10/06  JL  added geographic boundary info
11/6/07  MDW Added Ship_address_1 and bill_address_1
01/14/08 CRS Added additional information--ph ext, added first & last names, address city, state &
			 zip broken out.  No changes to existing fields.
04/16/08 CRS Added account party_id
09/27/12 TDT Added Distinct.

*/
CREATE view [dbo].[ADDRESS_INFO_VW]
as
select distinct
    Person_Party_id = person.Party_id   
  , person.Enduser_nbr
  , [Name] = (person.first_nm + ' ' + person.last_nm)
  , Last_Name = person.last_nm
  , First_Name = person.first_nm
  , cm.ph_nbr
  , cm.fax_nbr
  , cm.ph_ext
  , cm.email
  , ACC_Party_ID = account.party_id
  , account.acc_nbr
  , account.acc_nm
  , ship_addr_1 = ship_addr.addr1
  , ship_addr_2 = ship_addr.addr2
  , ship_addr_3 = ship_addr.addr3
  , Ship_CSZ = (ship_addr.city_nm + ', ' + ship_addr.state_cd + '  ' + ship_addr.post_cd)
  , Ship_City = ship_addr.city_nm
  , Ship_State = ship_addr.state_cd
  , Ship_Zip = ship_addr.post_cd
  , ship_country = ship_addr.cntry_nm
  , geo_boundary = gb.name
  , bill_addr_1 = bill_addr.addr1
  , bill_addr_2 = bill_addr.addr2
  , bill_addr_3 = bill_addr.addr3
  , Bill_City = bill_addr.city_nm
  , Bill_State = bill_addr.state_cd
  , Bill_Zip = bill_addr.post_cd
  , Bill_CSZ = (bill_addr.city_nm + ', ' + bill_addr.state_cd + '  ' + bill_addr.post_cd)
  , bill_country = bill_addr.cntry_nm
  , Org.org_nm
  , org.org_nbr
  , ORG_Party_ID = Org.party_id
from
  sales.dbo.person (nolock)
  join sales.dbo.cont_mech cm (nolock) on cm.party_id = person.party_id
    and cont_role_type_id = 1
  join sales.dbo.account (nolock) on account.party_id = person.acc_id
  join sales.dbo.org (nolock) on org.party_id = person.org_id
  left join sales.dbo.party_addr_rl shipping_rl(nolock) on shipping_rl.party_id = person.party_id
    and shipping_rl.party_addr_rl_id = 1
    and prim_addr = 1
    and shipping_rl.thru_date is null
  left join sales.dbo.addr ship_addr (nolock) on ship_addr.addr_id = shipping_rl.addr_id
  left join sales.dbo.GEOGRAPHIC_BOUNDARY gb on gb.GEOGRAPHIC_BOUNDARY_ID = ship_addr.GEOGRAPHIC_BOUNDARY_ID
  left join sales.dbo.party_addr_rl billing_rl (nolock) on billing_rl.party_id = person.party_id
    and billing_rl.party_addr_rl_id = 2
    and billing_rl.thru_date is null
  left join sales.dbo.addr bill_addr (nolock) on bill_addr.addr_id = billing_rl.addr_id


GO
