SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[SalesTerritoryHierarchyVw]
AS
  WITH SalesManagerSecurity (SalesTerritoryID, ParentSalesTerritoryID, ancestorSalesTerritoryId, TerritoryType, TerritoryId, LEVEL)
  AS
  (SELECT SalesTerritoryID, ParentSalesTerritoryID, ancestorSalesTerritoryId = SalesTerritoryID, TerritoryType, TerritoryId, LEVEL = 0
  FROM salesTerritory
  UNION ALL
  SELECT st.SalesTerritoryID, st.ParentSalesTerritoryID, sms.ancestorSalesTerritoryId, st.TerritoryType, st.TerritoryId, LEVEL = LEVEL +1
  FROM salesTerritory st
  JOIN SalesManagerSecurity sms ON st.ParentSalesTerritoryID = sms.SalesTerritoryID
  )
  SELECT SalesTerritoryID, ParentSalesTerritoryID, ancestorSalesTerritoryId, TerritoryType, TerritoryId, level 
  FROM SalesManagerSecurity sms 

GO
