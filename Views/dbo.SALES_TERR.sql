SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SALES_TERR]
AS 
select --*,
  SALES_TERR_ID = st.TerritoryId
, SALES_TERR_NM      = st.Name
, SALES_TERR_DESC    = st.Description
, SALES_TERR_ORD     = null
, SALES_REGION_ID    = (SELECT TerritoryID FROM dbo.SalesTerritory st2 WHERE st2.SalesTerritoryId = st.ParentSalesTerritoryId)
, NETWORK_LOGIN_NM   = dbo.getSalesTerritoryManagerNetworkNames(salesterritoryId)
, IDT_GUID           = st.IDT_GUID     
from SalesTerritory st
where st.territoryType = 'territory'


GO
