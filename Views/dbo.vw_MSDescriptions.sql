SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [dbo].[vw_MSDescriptions]
as
select TableColumns.TableName,
 TableColumns.columnName,
 sp.value as descr,
 TableColumns.ColumnOrder
from (select TableName = t.name , columnName = c.name , ColumnOrder = isnull(c.column_Id, 0) , c.object_id 
      FROM sys.tables t
        join sys.columns c on t.object_id = c.object_id
      where type IN ('S','V', 'U') 
      union all
      select s1.name TableName, null columnName, 0 ColumnOrder, s1.object_id 
      from sys.views s1
      where type IN ('S','V', 'U')
     ) TableColumns
 left join sys.extended_properties sp on TableColumns.object_id = sp.major_id
   AND  TableColumns.ColumnOrder = sp.minor_id
   and sp.name = 'MS_DESCRIPTION'
--order by TableColumns.TableName , TableColumns.ColumnOrder




GO
