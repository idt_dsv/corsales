SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/* 
author: MDW
Date: 6/20/07
Notes:
 Created to load the sales data Mart item detail. It gets the ord_line_item products with pricing (std and actual)

Example: -- only get items that are details-not high level items
select distinct vw.*
, product.prod_nm, component_name = p2.prod_nm
from SalesDataMartItemDtlFact vw (nolock)
join product (nolock) on product.prod_id = vw.productId
join product p2 on p2.prod_id = vw.Prod_comp_id
where (vw.prod_comp_id is not null) and 
vw.salesOrderId in (
  2629648, --trifecta kit
  2629649, -- normal tube
  2646184,  -- plate - multiple 96 well plates
  2646185,  -- plate - multiple 96 well plates
  2646186,  -- plate - multiple 96 well plates
  2646187,  -- plate - multiple 96 well plates
  2646188,  -- plate - multiple 96 well plates
  2646189,  -- plate - multiple 96 well plates
  2646190,  -- plate - multiple 96 well plates
  2646191,  -- plate - multiple 96 well plates
  2646192  -- plate - multiple 96 well plates
  )
order by vw.SalesOrderNbr, vw.line_item_id  

Modifications:
1/18/08 MDW Changed to include all ord_line_items

*/
CREATE VIEW [dbo].[SalesDataMartItemDtlFact]
AS
  select
    SalesOrderId = ord.ord_id
  , SalesOrderNbr = ord.sales_ord_nbr
  , Ref_id = dbo.Get_Ancestor_REF_ID(oli.line_item_id)
  , oli.par_line_item
  , oli.line_item_id
  , ProductID = pcd.prod_id
  /* Prod_comp_id is not stored. It will only have a Prod_Comp_ID if it is a detailed component connected to the prod_id 
     in the parent line_item. In other words high level products will not have prod_comp_id
  */
  , Prod_comp_id = ISNULL(pcd.comp_id,0)
  , oli.Quantity
  , ActualUnitPrice = oli.unit_price
  , ActualUnitPriceCurrency = 'USD'
   /* trick to get around plan deficincy of coalesce */
  , StandardUnitPrice = 
        ISNULL(
            isnull(lipd.wholesale_std_price_home, (
                      SELECT MAX(sp.std_price)
                      FROM reference.dbo.std_price sp (NOLOCK) 
                      where pcd.prod_comp_id = sp.prod_comp_id
                        AND sp.from_dt < ord.ord_date 
                        AND (sp.thru_dt IS NULL OR sp.thru_Dt > ord.Ord_date)
                    )
              )
         , 0.0) /* Wholesale price is our retail, where retail is for oem model */
  , StandardUnitPriceCurrency = 'USD'
  , Ord.Ord_date
  , ActualNaturalCurrencyID = ord.CURRENCY_ID
  , ActualNaturalCurrency = (SELECT Epicor_Currency_code FROM currency c WHERE c.currency_id = ord.currency_id)
  , ActualNaturalUnitPrice = UNIT_PRICE_NATURAL
  FROM
    ord (NOLOCK)
    JOIN ord_line_item oli (NOLOCK) ON ord.ord_id = oli.ord_id
      AND oli.oli_prod_id <> 1130 /* not shipping */
    LEFT JOIN reference.dbo.Prod_comp_det pcd (NOLOCK) ON 
                      pcd.prod_id = (
                                  SELECT
                                    oli2.oli_prod_id
                                  FROM
                                    ord_line_item oli2
                                  WHERE
                                    oli2.line_item_id = oli.par_line_item
                                )
                      AND pcd.comp_id = oli.oli_prod_id
                      AND pcd.from_dt <= ord.ord_date AND (Thru_dt IS NULL OR Thru_dt > ord.ord_date)

    LEFT JOIN line_item_price_detail lipd (NOLOCK) on oli.line_item_id = lipd.line_item_id


GO
