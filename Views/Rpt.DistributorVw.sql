SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
Author: MDW
Date: 5/30/08
Notes:
  Built for Portal Distributor reports. 
  Returns dataset of org_ids and distibutors for current date and time

example:
  select DISTRIBUTOR, 
    ORG_ID,
    ORG_NBR,
    ORG_NM
  from DistributorVw
  where par_party_id NOT in (240592,296890)


Permissions:
  grant select on DistributorVw to idt_reporter

Modifications:
Candace Schebel - 8-13-08 - Cleaned up column names, eliminated pr.par_party_id as it is
							the same as DistOrgID
Candace Schebel - 8-14-08 - Added is NULL on the thru_dt (some dates are NULL--others 9999)
*/


CREATE view [Rpt].[DistributorVw]
as

select 
  DistOrgID = org.party_id
, DistOrgNbr = org.org_nbr
, DistributorName = org.org_nm
, OrgID = org.party_id
, OrgNbr = org.org_nbr
, OrgName = org.org_nm
from sales.dbo.org
where class_type_id in (3)
  and org.party_id <> 264165

union

select 
  DistOrgID = pr.par_party_id
, DistOrgNbr = porg.org_nbr
, DIST_ORG_NM = porg.org_nm
, DistributorName = pr.child_party_id
, OrgNbr = org.org_nbr
, OrgName = org.org_nm
from sales.dbo.party_rel pr
  join sales.dbo.org on org.party_id = pr.child_party_id
  join sales.dbo.org porg on porg.party_id = pr.par_party_id
where party_rel_type = 8
  and pr.par_party_id not in (264165, 375379)
  and (pr.thru_dt > getdate()
	or pr.thru_dt is NULL)
	
GO
