SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*--Table: BRANCH_LOCATION
Author: MDW
Date: 11/15/05
Notes: View created for backwards compatiblity
  Table moved to reference database for replication

  Table moved to reference database for replication

Revisions
05/15/07 JLB Changed to a select * query

*/

CREATE view [dbo].[BRANCH_LOCATION]
 as 
Select *
from reference.dbo.BRANCH_LOCATION

GO
