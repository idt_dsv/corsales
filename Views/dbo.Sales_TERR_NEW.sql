SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[Sales_TERR_NEW]
AS 
select --*,
  SALES_TERR_ID = vw.TerritoryId
, SALES_TERR_NM      = vw.Name
, SALES_TERR_DESC    = vw.Description
, SALES_TERR_ORD     = null
, SALES_REGION_ID    = (SELECT TerritoryID FROM dbo.SalesTerritory st2 WHERE st2.SalesTerritoryId = vw.ParentSalesTerritoryId)
, NETWORK_LOGIN_NM   = vw.NETWORK_LOGIN_NM
, IDT_GUID           = vw.IDT_GUID     
from SalesTerritoryManagerVw vw
where vw.territoryType = 'territory'

GO
