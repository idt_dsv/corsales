SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Notes:
select * 
from SalesTerritoryManagerVw
where territoryType = 'territory'
*/
CREATE VIEW [dbo].[SalesTerritoryManagerVw]
AS 
SELECT st.TerritoryType, st.Name, st.Description, EmployeeName = Emp.First_name + ' ' + Emp.last_name, emp.NETWORK_LOGIN_NM, st.TerritoryId, EMP.HR_EMPLOYEE_ID, st.SalesTerritoryId, St.ParentSalesTerritoryId, ST.IDT_GUID
FROM SalesTerritory st 
LEFT JOIN dbo.SalesTerritoryManager stm ON stm.SalesTerritoryID = st.SalesTerritoryID
LEFT JOIN reference.dbo.employee emp ON emp.HR_Employee_id = stm.Hr_employee_id
GO
