SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*--Table: QUOTE
Author: MDW
Date: 11/15/05
Notes: View created for backwards compatiblity
  Table moved to reference database for replication

  Table moved to reference database for replication

Revisions
*/

CREATE view [dbo].[QUOTE]
 as 
Select QUOTE_ID
, QUOTE_STATUS_ID
, NEGOTIATED_BY_ID
, QUOTE_CATEGORY_ID
, PI_PARTY_ID
, OE_INSTRUCTION_NOTE
, QUOTE_HISTORY_NOTE
, FROM_DT
, THRU_DT
, IS_ONE_TIME_USE
, IS_AUTOMATED_PRICING
, IS_FREE_SHIPPING
, SHIP_GRP_ID
, CREATE_USER_ID
, CREATE_DTM
, MODIFY_USER_ID
, MODIFY_DTM
, M_NEGOTIATED_BY
, M_KEYWORDS
, M_PARTY
, M_CREATE_USER
, PRICE_LIST_ID
from reference.dbo.QUOTE

GO
