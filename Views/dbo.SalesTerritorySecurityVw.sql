SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Example:
select distinct TerritoryID
from [SalesTerritorySecurityVw]
WHERE last_name LIKE 'berrios%'
and TerritoryType = 'territory'
order by TerritoryID
*/
CREATE VIEW [dbo].[SalesTerritorySecurityVw]
AS
SELECT st.*, emp.hr_employee_id, emp.last_name, emp.first_name, emp.network_Login_nm 
FROM dbo.SalesTerritoryManager stm 
JOIN reference.dbo.employee emp ON emp.HR_Employee_id = stm.Hr_employee_id
JOIN SalesTerritoryHierarchyVw sth ON stm.SalesTerritoryID = sth.AncestorSalesTerritoryID
JOIN dbo.SalesTerritory st ON st.SalesTerritoryID = sth.SalesTerritoryID


GO
