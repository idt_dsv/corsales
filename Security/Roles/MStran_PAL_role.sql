CREATE ROLE [MStran_PAL_role]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'MStran_PAL_role', N'MSReplPAL_55_1'
GO
EXEC sp_addrolemember N'MStran_PAL_role', N'MSReplPAL_55_2'
GO
EXEC sp_addrolemember N'MStran_PAL_role', N'MSReplPAL_55_3'
GO
EXEC sp_addrolemember N'MStran_PAL_role', N'MSReplPAL_55_4'
GO
EXEC sp_addrolemember N'MStran_PAL_role', N'MSReplPAL_55_5'
GO
EXEC sp_addrolemember N'MStran_PAL_role', N'MSReplPAL_55_6'
GO
