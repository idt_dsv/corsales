CREATE ROLE [IDT_MfgSynthAnalysis]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'IDT_MfgSynthAnalysis', N'MfgSynthAnalysis'
GO
