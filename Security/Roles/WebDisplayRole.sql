CREATE ROLE [WebDisplayRole]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'WebDisplayRole', N'IDT-CORALVILLE\IUSR_IDT'
GO
EXEC sp_addrolemember N'WebDisplayRole', N'WebDisplay'
GO
EXEC sp_addrolemember N'WebDisplayRole', N'WebQC'
GO
