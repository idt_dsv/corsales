EXEC sp_addrolemember N'db_datawriter', N'coliseum'
GO
EXEC sp_addrolemember N'db_datawriter', N'IDT_APPS'
GO
EXEC sp_addrolemember N'db_datawriter', N'IDT_DbDeveloper'
GO
EXEC sp_addrolemember N'db_datawriter', N'IDT_GRANT_INFO_USER'
GO
EXEC sp_addrolemember N'db_datawriter', N'IDT_SUPPORT'
GO
