EXEC sp_addrolemember N'db_datareader', N'coliseum'
GO
EXEC sp_addrolemember N'db_datareader', N'epicor'
GO
EXEC sp_addrolemember N'db_datareader', N'Epicor_user'
GO
EXEC sp_addrolemember N'db_datareader', N'ETL_MFGSQL'
GO
EXEC sp_addrolemember N'db_datareader', N'IDT_APPS'
GO
EXEC sp_addrolemember N'db_datareader', N'IDT_DbDeveloper'
GO
EXEC sp_addrolemember N'db_datareader', N'IDT_DOCUMENTER_APP'
GO
EXEC sp_addrolemember N'db_datareader', N'IDT_ERP_APPS'
GO
EXEC sp_addrolemember N'db_datareader', N'IDT_GRANT_INFO_USER'
GO
EXEC sp_addrolemember N'db_datareader', N'IDT_LAB_ACCESS'
GO
EXEC sp_addrolemember N'db_datareader', N'IDT_Reporter'
GO
EXEC sp_addrolemember N'db_datareader', N'IDT_USER'
GO
EXEC sp_addrolemember N'db_datareader', N'IDT_WebApps'
GO
EXEC sp_addrolemember N'db_datareader', N'IDT-CORALVILLE\SSRS_Coralville'
GO
EXEC sp_addrolemember N'db_datareader', N'LS_WNSQL_Website'
GO
EXEC sp_addrolemember N'db_datareader', N'MfgSynthAnalysis'
GO
EXEC sp_addrolemember N'db_datareader', N'PLATE_ORDER_APP'
GO
EXEC sp_addrolemember N'db_datareader', N'RESEARCH_LAB_USER'
GO
EXEC sp_addrolemember N'db_datareader', N'SQLSharePoint'
GO
EXEC sp_addrolemember N'db_datareader', N'TLShipping'
GO
