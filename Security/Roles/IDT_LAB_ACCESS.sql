CREATE ROLE [IDT_LAB_ACCESS]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'IDT_LAB_ACCESS', N'Analytical_Services_User'
GO
EXEC sp_addrolemember N'IDT_LAB_ACCESS', N'DLP_APP_USER'
GO
EXEC sp_addrolemember N'IDT_LAB_ACCESS', N'IDT_LAB_REPORTS'
GO
EXEC sp_addrolemember N'IDT_LAB_ACCESS', N'IDT_MSQuery'
GO
EXEC sp_addrolemember N'IDT_LAB_ACCESS', N'IDT_SPECIALTY_APP'
GO
EXEC sp_addrolemember N'IDT_LAB_ACCESS', N'Lab'
GO
EXEC sp_addrolemember N'IDT_LAB_ACCESS', N'PROD_ANA_DEV'
GO
EXEC sp_addrolemember N'IDT_LAB_ACCESS', N'RESEARCH_LAB_USER'
GO
