CREATE ROLE [IDT_WebApps]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'IDT_WebApps', N'IDT-CORALVILLE\IUSR_IDT'
GO
EXEC sp_addrolemember N'IDT_WebApps', N'Webdev'
GO
EXEC sp_addrolemember N'IDT_WebApps', N'WebDisplayRole'
GO
