CREATE ROLE [IDT_APPS]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'IDT_APPS', N'coliseum'
GO
EXEC sp_addrolemember N'IDT_APPS', N'Epicor_LinkServer'
GO
EXEC sp_addrolemember N'IDT_APPS', N'IDT_DbDeveloper'
GO
EXEC sp_addrolemember N'IDT_APPS', N'idt-coralville\wladmin'
GO
EXEC sp_addrolemember N'IDT_APPS', N'Webdev'
GO
