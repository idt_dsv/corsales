CREATE ROLE [IDT_Reporter]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'IDT_Reporter', N'CcmAppId'
GO
EXEC sp_addrolemember N'IDT_Reporter', N'coliseum'
GO
EXEC sp_addrolemember N'IDT_Reporter', N'dmillen'
GO
EXEC sp_addrolemember N'IDT_Reporter', N'GeneApp'
GO
EXEC sp_addrolemember N'IDT_Reporter', N'IDT_ACCTG_REPORTS'
GO
EXEC sp_addrolemember N'IDT_Reporter', N'IDT_LAB_ACCESS'
GO
EXEC sp_addrolemember N'IDT_Reporter', N'IDT_LAB_REPORTS'
GO
EXEC sp_addrolemember N'IDT_Reporter', N'IDT-CORALVILLE\IUSR_IDT'
GO
EXEC sp_addrolemember N'IDT_Reporter', N'idt-coralville\wladmin'
GO
EXEC sp_addrolemember N'IDT_Reporter', N'PLATE_ORDER_APP'
GO
EXEC sp_addrolemember N'IDT_Reporter', N'PROD_ANA_DEV'
GO
EXEC sp_addrolemember N'IDT_Reporter', N'ROYALTY_RPT_APP'
GO
