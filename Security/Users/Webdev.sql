IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Webdev')
CREATE LOGIN [Webdev] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [Webdev] FOR LOGIN [Webdev] WITH DEFAULT_SCHEMA=[Webdev]
GO
