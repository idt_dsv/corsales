IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Epicor_LinkServer')
CREATE LOGIN [Epicor_LinkServer] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [Epicor_LinkServer] FOR LOGIN [Epicor_LinkServer] WITH DEFAULT_SCHEMA=[Epicor_LinkServer]
GO
