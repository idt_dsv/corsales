IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'epicor')
CREATE LOGIN [epicor] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [epicor] FOR LOGIN [epicor] WITH DEFAULT_SCHEMA=[epicor]
GO
