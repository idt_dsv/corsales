IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'WebQC')
CREATE LOGIN [WebQC] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [WebQC] FOR LOGIN [WebQC]
GO
