IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'SQLSharePoint')
CREATE LOGIN [SQLSharePoint] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [SQLSharePoint] FOR LOGIN [SQLSharePoint]
GO
