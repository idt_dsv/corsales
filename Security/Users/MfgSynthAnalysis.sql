IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'MfgSynthAnalysis')
CREATE LOGIN [MfgSynthAnalysis] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [MfgSynthAnalysis] FOR LOGIN [MfgSynthAnalysis] WITH DEFAULT_SCHEMA=[MfgSynthAnalysis]
GO
