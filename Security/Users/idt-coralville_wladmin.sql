IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'IDT-CORALVILLE\wladmin')
CREATE LOGIN [IDT-CORALVILLE\wladmin] FROM WINDOWS
GO
CREATE USER [idt-coralville\wladmin] FOR LOGIN [IDT-CORALVILLE\wladmin] WITH DEFAULT_SCHEMA=[idt-coralville\wladmin]
GO
