IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'GeneApp')
CREATE LOGIN [GeneApp] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [GeneApp] FOR LOGIN [GeneApp]
GO
