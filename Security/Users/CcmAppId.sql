IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'CcmAppId')
CREATE LOGIN [CcmAppId] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [CcmAppId] FOR LOGIN [CcmAppId]
GO
