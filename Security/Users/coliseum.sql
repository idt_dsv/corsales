IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'coliseum')
CREATE LOGIN [coliseum] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [coliseum] FOR LOGIN [coliseum] WITH DEFAULT_SCHEMA=[coliseum]
GO
GRANT CONNECT REPLICATION TO [coliseum]
