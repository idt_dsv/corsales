IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Epicor_user')
CREATE LOGIN [Epicor_user] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [Epicor_user] FOR LOGIN [Epicor_user] WITH DEFAULT_SCHEMA=[Epicor_user]
GO
