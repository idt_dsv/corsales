IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'LS_WNSQL_Website')
CREATE LOGIN [LS_WNSQL_Website] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [LS_WNSQL_Website] FOR LOGIN [LS_WNSQL_Website]
GO
