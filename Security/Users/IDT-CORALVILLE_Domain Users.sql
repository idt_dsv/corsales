IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'IDT-CORALVILLE\Domain Users')
CREATE LOGIN [IDT-CORALVILLE\Domain Users] FROM WINDOWS
GO
CREATE USER [IDT-CORALVILLE\Domain Users] FOR LOGIN [IDT-CORALVILLE\Domain Users]
GO
