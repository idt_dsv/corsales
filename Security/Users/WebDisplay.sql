IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'WebDisplay')
CREATE LOGIN [WebDisplay] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [WebDisplay] FOR LOGIN [WebDisplay] WITH DEFAULT_SCHEMA=[WebDisplay]
GO
