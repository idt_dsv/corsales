IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'IDT-CORALVILLE\SSRS_Coralville')
CREATE LOGIN [IDT-CORALVILLE\SSRS_Coralville] FROM WINDOWS
GO
CREATE USER [IDT-CORALVILLE\SSRS_Coralville] FOR LOGIN [IDT-CORALVILLE\SSRS_Coralville]
GO
