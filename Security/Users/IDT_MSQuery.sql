IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'IDT_MSQuery')
CREATE LOGIN [IDT_MSQuery] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [IDT_MSQuery] FOR LOGIN [IDT_MSQuery]
GO
