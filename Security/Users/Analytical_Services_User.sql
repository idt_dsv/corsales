IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Analytical_Services_User')
CREATE LOGIN [Analytical_Services_User] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [Analytical_Services_User] FOR LOGIN [Analytical_Services_User] WITH DEFAULT_SCHEMA=[Analytical_Services_User]
GO
