SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: Chris A. Sailor
Date Created: April 22, 2004
Purpose:

  This procedure is intended to retrieve the inputs to
the yield guarantee engine for a specified reference ID

Modifications:
Who     Date      Modification  Note
--------------------------------------------------------------
CAS     04/22/04  Chris Sailor  Created
CAS     08/10/04  Chris Sailor  Modified to return MFold info
CAS     08/19/04  Chris Sailor  Converted Seq to text to retrieve longer sequences
CAS     02/09/05  Chris Sailor  Added the Fold_TM to returned data.
CAS     07/18/05  Chris Sailor  Added Purity_Guar_Explanation_ID for use with MFold Kludge
CAS     10/27/05  Chris Sailor  Added Branch_Location_ID and Ordered_By_Cutoff
CAS     03/20/07  Chris Sailor  Moved out of Manufacturing database
CAS     01/30/08  Chris Sailor  Updated to also retrieve data for duplexes and plates to support changes to Ship Date Calculator
*/
CREATE Procedure [dbo].[YieldGuar_2_Aux_GetInfoForRefID]
	@Ref_ID Int
As
	set nocount on

	-- Pseudo-constants
	Declare @IgnorePurifID Int	Set @IgnorePurifID = 0
	Declare @UnknownTypeID Int	Set @UnknownTypeID = 0
	Declare @OligoTypeID Int	Set @OligoTypeID = 1
	Declare @PlateTypeID Int	Set @PlateTypeID = 2
	Declare @DuplexTypeID Int	Set @DuplexTypeID = 3
	-- Working variables
	Declare @ItemTypeID Int
	Declare @Purif_ID Int

	Declare @Results Table (
		Ref_ID Int Primary Key Clustered,
		Seq Text NULL,
		LIRN_Prod_ID Int,
		Purif_ID Int NULL,
		Yield_Guar_Agreement_ID Int,
		Ord_Date DateTime,
		Yield_Guar_Override Int Null,
		Purity_Guar_Explanation_ID Int Null,
		CanUseDeltaG bit,
		Delta_G Numeric ( 10, 2 ),
		Fold_TM Numeric ( 10, 2 ),
		Branch_Location_ID Int,
		Ordered_By_Cutoff bit,
		ParentRefID Int,
		ItemType Int
		)

	-- populate the @Results table with the data that is common to all of the different types
	Insert Into	@Results
	select		@Ref_ID,
			NULL,
			LIRN.LIRN_Prod_ID,
			-1,
			A.Yield_GUAR_AGREEMENT_ID,
			O.Ord_Date,
			0,
			0,
			0,
			0,
			0,
			LIRN.Branch_Location_ID,
			LIRN.Ordered_By_Cutoff,
			ParentRefID = (
				select		Parent_LIRN.Ref_ID
				from		Ord_Line_Item OLI_Item (nolock)
				join		Line_Item_Ref_Nbr Parent_LIRN (nolock)	On	OLI_Item.Par_Line_Item = Parent_LIRN.Line_Item_ID
				where		OLI_Item.Line_Item_ID = LIRN.Line_Item_ID
				),
			@UnknownTypeID
	from 		dbo.Line_item_ref_nbr LIRN  (nolock)
	join 		dbo.Ord O (nolock)			on	( O.Ord_ID = LIRN.FK_Ord_ID )
	left join	dbo.Agreement A (nolock)		on	( A.Agreement_ID = LIRN.Agreement_ID ) and
									( Convert ( Varchar ( 20 ), O.Ord_Date, 101 ) between A.BEGIN_DT and A.END_DT )
	where		LIRN.REF_ID = @REF_ID


	if ( exists ( select * from OLIGO_SPEC where Ref_ID = @Ref_ID )) begin
		-- print 'Ref_ID is an Oligo'

		update		R
			set	R.ItemType = @OligoTypeID,
				R.Seq = Convert ( Text, OS.seq ),
				R.Purif_ID = OS.Purif_ID,
				R.Yield_Guar_Override = OS.YIELD_GUAR_OVERRIDE,
				R.Purity_Guar_Explanation_ID = OS.Purity_Guar_Explanation_ID,
				R.CanUseDeltaG = Case	When ( MFC.Ref_ID Is Null ) then Convert ( bit, 0 )
							Else Convert ( bit, 1 )
						End,
				R.Delta_G = Convert ( numeric ( 10, 2 ), Coalesce ( MFC.Delta_G, 0 )),
				R.Fold_TM = Convert ( numeric ( 10, 2 ), Coalesce ( MFC.Fold_TM, 0 ))
		from 		@Results R
		join		dbo.Oligo_Spec OS (nolock) 		on	R.Ref_ID = OS.Ref_ID
		left join	dbo.MFold_Calculation MFC (nolock)	on	( OS.Ref_ID = MFC.Ref_ID )

	end
	else if ( exists ( select * from PLATE_SPEC where Ref_ID = @Ref_ID )) begin
		-- print 'Ref_ID is a Plate'
		update		R
			set	R.ItemType = @PlateTypeID,
				R.Purif_ID = @IgnorePurifID
		from 		@Results R
	end
	else begin
		-- DO NOT look for the purification if the container is full of containers of oligos.  (A kit of duplexes.)
		-- The Delphi code doesn't, so we shouldn't here either.
		select top 1	@Purif_ID = OS.Purif_ID
		from		Line_Item_Ref_Nbr LIRN_Container (nolock)
		join		Ord_Line_Item OLI_Oligo (nolock)		on	( LIRN_Container.Line_Item_ID = OLI_Oligo.Par_Line_Item )
		join		Line_Item_Ref_Nbr LIRN_Oligo (nolock)		on	( OLI_Oligo.Line_Item_ID = LIRN_Oligo.Line_Item_ID )
		join		Oligo_Spec OS (nolock)				on	( LIRN_Oligo.Ref_ID = OS.Ref_ID )
		where		LIRN_Container.Ref_ID = @Ref_ID
		order by	OLI_Oligo.Line_Item_ID

		set @Purif_ID = Coalesce ( @Purif_ID, -1 )

		-- Figure out what kind of item we are handling
		if ( exists ( select * from DUPLEX_OLIGO_SPEC where Ref_ID = @Ref_ID )) begin
			-- print 'Ref_ID is a Duplex'
			set @ItemTypeID = @DuplexTypeID
		end
		else begin
			-- print 'Ref_ID is not an Oligo, Plate, or Duplex'
			set @ItemTypeID = @UnknownTypeID
		end

		-- update the data accordingly
		update		R
			set	R.ItemType = @ItemTypeID,
				R.Purif_ID = @Purif_ID
		from 		@Results R
	end

	-- Get the information
	select * from @Results where Ref_ID = @Ref_ID

	set nocount off
GO
GRANT EXECUTE ON  [dbo].[YieldGuar_2_Aux_GetInfoForRefID] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[YieldGuar_2_Aux_GetInfoForRefID] TO [IDT_WebApps]
GO
