SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[IDT_GET_TABLE_SIZE] 
as 
/*
Author: Mdewaard
Date: 8/8/02
Description:
Create list of tables and their size for the current database
*/


set nocount on

create table #tbl_cnt 
 (Server varchar(20) null,
  [Database] varchar(20) null, 
  --[Schema] varchar(8) null,
  [Table] varchar(50) null,
  RecordCnt int null,
  ReservedSizeKb int null,
  DataSizeKb int null,
  IndexSizeKb int null,
  UnusedKb int null,
  EntryDt varchar(8) null)

create table #tbl_cnt_tmp
 (tableName varchar(128) null,
  RecordCnt int null,
  reservedSize varchar(50) null,
  dataSize varchar(50) null,
  indexSize varchar(50) null,
  unused varchar(50) null)

declare @table_schema varchar(128), @table_name varchar(128), @SQLStr nvarchar (512)

declare table_cursor INSENSITIVE cursor  for
select table_schema, table_name
  from information_schema.tables
 where table_type = 'base table'
   and table_name not like 'dtproperties%'

open table_cursor
Fetch next from table_cursor into @table_schema, @table_name

while @@Fetch_Status = 0
begin
        -- For each table get size statistics
        set @SQLStr = N'insert #tbl_cnt_tmp (  tableName, RecordCnt, reservedSize, DataSize, indexSize, unused )' 
                      + char(10) + 'execute sp_spaceused ''' + @table_schema + '.' + @table_name + ''''

        exec sp_executesql @SQLStr

        -- For use above tmp table + other added information to result table
   
          set @SQLStr = N'insert #tbl_cnt ' 
                      + char(10) 
                      + 'select @@servername, db_name(),''' 
                      + @table_schema +'.' + @table_name +
                      + ''', RecordCnt, ' 
                      + 'convert(int,substring(reservedSize,1,patindex(''% KB'', reservedSize) -1)), '
                      + 'convert(int,substring(dataSize,1,patindex(''% KB'', dataSize) -1)), '
                      + 'convert(int,substring(indexSize,1,patindex(''% KB'', indexSize) -1)), '
                      + 'convert(int,substring(unused,1,patindex(''% KB'', unused) -1)), '
                      + 'convert(varchar(8), getdate(), 1) '
                      + 'from #tbl_cnt_tmp'

        exec sp_executesql @SQLStr

        -- get rid of results in temp table
        truncate table #tbl_cnt_tmp

        Fetch next from table_cursor into @table_schema, @table_name 
end
CLOSE table_cursor
DEALLOCATE table_cursor

INSERT development.ServerInventory.dbo.TABLE_SIZE              
      ([SERVER_ID], [DATABASE_NM],
      [TABLE_NM], [RECORD_CNT], [RESERVED_SIZE], 
      [DATA_SIZE], [INDEX_SIZE], [UNUSED_SPACE], [ENTRY_DT])
SELECT 2, [DATABASE], [TABLE], RECORDCNT, RESERVEDSIZEKB, DATASIZEKB, INDEXSIZEKB,
        UNUSEDKB, ENTRYDT 
from #tbl_cnt

set nocount off







GO
GRANT EXECUTE ON  [dbo].[IDT_GET_TABLE_SIZE] TO [IDT_APPS]
GO
