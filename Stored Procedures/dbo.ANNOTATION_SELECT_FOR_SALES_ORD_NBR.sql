SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
Author: Eric Borman
Date: 6/30/2005
Notes:  
  Returns the shippping annotations for a given sales order number
Usage:
  Execute dbo.ANNOTATION_SELECT_FOR_SALES_ORD_NBR 5000003
Permissions
  grant execute on dbo.ANNOTATION_SELECT_FOR_SALES_ORD_NBR to idt_apps 
Revisions:
Date   Who  Notes
-----------------------------------------
*/
Create      procedure [dbo].[ANNOTATION_SELECT_FOR_SALES_ORD_NBR] (@SalesOrdNbr int) 
AS 
BEGIN

declare @DTM datetime
Set @DTM = getDate()

SELECT
  ANNOTATION_ID,
  convert(text, ANNOTATION) as Annotation,
  AUTHOR_ID as AuthorID,
  CANCEL_BY_ID as CancelByID,
  CANCEL_DTM as CancelDate,
  CREATE_DTM as CreateDate,
  MACHINE_STAMP as MachineStamp,
  NOTE_CATEGORY as NoteCategory,
  BF_BOBJECT_CID as TopicClassID,
  BF_BOBJECT_OID as TopicInstanceID,
  WORKSTATION_ID as WorkStationID
FROM DBO.GET_SHIPPING_ANNOTATIONS_FOR_SALES_ORD_NBR(@SalesOrdNbr, @DTM)

END






GO
GRANT EXECUTE ON  [dbo].[ANNOTATION_SELECT_FOR_SALES_ORD_NBR] TO [IDT_APPS]
GO
