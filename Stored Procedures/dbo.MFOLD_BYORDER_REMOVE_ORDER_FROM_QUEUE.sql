SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
Author: Chris A. Sailor
Date Created: August 1, 2005
Purpose:

  This procedure is intended to remove a single order from the
MFold queue, and add the appropriate record(s) to the order
transform queue.

  As of the 12/19 update, it no longer calls
DBO.MFOLD_COMPLETE_QUEUE_ITEM  or
DBO.MFold_ByOrder_Update_OrderConversionQs_For_Order.

grant execute on DBO.MFold_ByOrder_Remove_Order_From_Queue to IDT_Apps

Modifications:
Who     Date      Modification  Note
--------------------------------------------------------------
CAS     08/01/05  Created
CAS     12/19/05  Removed called routines, and improved transaction code
CAS     01/26/06  Modified to generate an error if an invalid Branch Location ID is stored in Line_Item_Ref_Nbr
CAS     03/20/07  Chris Sailor  Moved out of Manufacturing database
CAS     07/18/07  Chris Sailor  Fixed issue calling Key_Field_Get_Next_X_Values asking for 0 keys
CAS     10/09/08  Chris Sailor  Modified to support product specific order conversion queues
EDJ		03/08/10  Eric Johnson  Add check to make sure Order Status is Posted Order (7) before proceeding (or has been Posted at some point)
*/
CREATE Procedure [dbo].[MFOLD_BYORDER_REMOVE_ORDER_FROM_QUEUE]
	(
	@Queue_Item_ID Int
	)
As
Begin
	set nocount on

	DECLARE @MFOLD_QUEUE_DEFINITION_ID int	SET @MFOLD_QUEUE_DEFINITION_ID	= 13
	DECLARE @TORDER_SPEC_CID int		SET @TORDER_SPEC_CID		= 63
	Declare @Now DateTime			Set @Now 			= GetDate ( )
	Declare @OrderID Int
	Declare @Priority Int
	Declare @Today DateTime			Set @Today			= Convert ( DateTime, Convert ( Varchar ( 10 ), @Now, 101 ))

	Declare @ErrCode Int
	Declare @NumKeys Int
	Declare @LowKey Int
	Declare @HighKey Int
	DECLARE @OrdStatHistResult INT

	Declare @Current_Branch_Location_IDs_For_Order Table ( Branch_Location_ID Int Not NULL, Queue_Definition_ID Int Not NULL )
	Declare @Queue_IDs_For_Order Table
		(
		TmpIdx Int Identity ( 1, 1 ) Not Null Primary Key Clustered,
		Queue_ID Int Not NULL Unique
		)




	-- Get the Order ID and the Priority for the given MFold Queue record
	Select	@OrderID = BF_BObject_OID,
		@Priority = Priority
	From	DBO.Queue_Item_MFold
	Where	Queue_Item_ID = @Queue_Item_ID

	Set @ErrCode = @@Error




	If ( @ErrCode = 0 ) Begin
		-- Find all of the Branch_Location_IDs currently assigned to the order
		Insert Into @Current_Branch_Location_IDs_For_Order
			select distinct	LIRN.Branch_Location_ID,
					BLQL.Queue_Definition_ID
			from		dbo.Line_Item_Ref_Nbr LIRN
			join		dbo.Branch_Location_Queue_Link BLQL
				on	( LIRN.Branch_Location_ID = BLQL.Branch_Location_ID ) and 
					(( LIRN.LIRN_Prod_ID = BLQL.Prod_ID ) or 
					 (( BLQL.Prod_ID is Null ) and
					  ( not exists
						(
						select		*
						from		dbo.Branch_Location_Queue_Link BLQL2
						where		( BLQL2.Branch_Location_ID = LIRN.Branch_Location_ID ) and
								( BLQL2.Prod_ID = LIRN.LIRN_Prod_ID ) and
								( BLQL2.From_DTM <= @Today ) and
								(( BLQL2.Thru_DTM Is Null ) or ( @Today <= BLQL2.Thru_DTM ))
						)
					  )
					 )
					)
			where		( LIRN.FK_Ord_ID = @OrderID ) and
					( BLQL.From_DTM <= @Today ) and
					(( BLQL.Thru_DTM Is Null ) or ( @Today <= BLQL.Thru_DTM ))

		Set @ErrCode = @@Error
	End

	If ( @ErrCode = 0 ) Begin
		-- Insert all of the valid Branch_Location_IDs associated with the order into the
		-- Ord_Branch_Location_Hist table, if they are not already there.
		Insert Into DBO.Ord_Branch_Location_Hist ( Ord_ID, Branch_Location_ID, Queue_Definition_ID )
			Select		@OrderID,
					CBL.Branch_Location_ID,
					CBL.Queue_Definition_ID
			From		@Current_Branch_Location_IDs_For_Order CBL
			Left Join	DBO.Ord_Branch_Location_Hist BLH
				on	( @OrderID = BLH.Ord_ID ) and
					( CBL.Branch_Location_ID = BLH.Branch_Location_ID ) and
					( CBL.Queue_Definition_ID = BLH.Queue_Definition_ID )
			Where		( BLH.Ord_Branch_Location_Hist_ID is Null )

		Set @ErrCode = @@Error
	End

	If ( @ErrCode = 0 ) Begin
		-- We need to add new queue items for each of the branch locations related to this order.
		-- We are excluding queue items for those queues that already have an active queue item
		-- for the order.  In these cases, the original priority is sufficient.
		insert into @Queue_IDs_For_Order ( Queue_ID )
			select		OBLH.Queue_Definition_ID
			from		DBO.Ord_Branch_Location_Hist OBLH
			join		DBO.Queue_Definition QD
				on	( OBLH.Queue_Definition_ID = QD.Queue_Definition_ID )
			where		( OBLH.Ord_ID = @OrderID ) and
					( QD.From_DTM <= @Today ) and
					(( QD.Thru_DTM is Null ) or ( @Today <= QD.Thru_DTM )) and
					( OBLH.Queue_Definition_ID not in
						(
						select	Queue_Definition_ID
						from	DBO.Queue_Item_Order_Transform
						where	( BF_BObject_OID = @OrderID ) and
							( Claimed_Workstation_ID is Null ) and
							( Claimed_DTM is Null ) and
							( Completed_DTM is Null ) and
							( Active = 1 )
						)
					)

		Set @ErrCode = @@Error
	End

	If ( @ErrCode = 0 ) Begin
		-- Get the queue IDs
		Select @NumKeys = count (*) from @Queue_IDs_For_Order
		If ( @NumKeys > 0 ) Begin
			Exec @ErrCode = Key_Field_Get_Next_X_Values 'Queue_Item_ID', @NumKeys, @LowKey Output, @HighKey Output
		End
		Else Begin
			Set @ErrCode = 0
		End
	End

	If ( @ErrCode = 0 ) Begin

		Begin Transaction

		-- Complete the MFold Queue Item
		Update	dbo.Queue_Item_MFold
		Set	Completed_DTM = @Now
		Where	Queue_Item_ID = @Queue_Item_ID

		Set @ErrCode = @@Error


		If ( @ErrCode = 0 ) Begin
			
			--Check ORD_STAT_HIST to see if the order has been posted
			 Select @OrdStatHistResult = ORD_ID
		    
			From		dbo.ORD_STAT_HIST
			Where		ORD_ID = @OrderID
						AND
						ORD_STAT_TYPE_ID = 7
		    
			Set @ErrCode = @@Error
				
		END
	

		-- Added @OrdStatHistResult to only insert records in order transform if the order has been posted 
		-- It will also insert records if it has been held, then posted and then held again
		If (( @ErrCode = 0 ) and ( @NumKeys > 0 ) AND (@OrdStatHistResult IS NOT null)) Begin
			-- Add the records to the order transform queue
			Insert Into DBO.Queue_Item_Order_Transform
				Select	Tmp.TmpIdx + @LowKey - 1,
					@Now,
					@Now,
					@TORDER_SPEC_CID,
					@OrderID,
					Tmp.Queue_ID,
					NULL,
					NULL,
					NULL,
					@Priority,
					1,
					0
				From	@Queue_IDs_For_Order Tmp
			Set @ErrCode = @@Error
		End

		If ( @ErrCode = 0 ) Begin
			Commit Transaction
		End
		Else Begin
			Rollback Transaction
		End

	End

	Return @ErrCode

	set nocount off
End






GO
GRANT EXECUTE ON  [dbo].[MFOLD_BYORDER_REMOVE_ORDER_FROM_QUEUE] TO [IDT_APPS]
GO
