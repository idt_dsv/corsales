SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
Author: MDW
Date: 11/30/05  
Notes: 
  -- writes data to temporary local Epicor Server tables to 
     avoid problems of linked server select, running full table scans
  -- nightly job will remove records
  -- Called from EPICOR.IDT_CONTROL_PROD.IDT_IM_POPULATE

example:
  exec EPICOR_IM_PRE_POPULATE 14340

permissions:
grant exec on EPICOR_IM_PRE_POPULATE to idt_apps

Modifications:

Date		BL		WHO			Descr

07/20/07			TTL			Changed * call from Epicor.IDT_OPERATIONS_INTEGRATION.dbo.inv_hdr and inv_line_items to specified columns
									to account for changes on Sales.inv_hdr and inv_line_items and for BVBA OBI changes

7/28/08		2083	Bob			Filter Internal Invoices
*/


CREATE   procedure [dbo].[EPICOR_IM_PRE_POPULATE]
(@Batch_id int)
as
begin
  declare @err int, @rowcount int

  exec epicor.IDT_OPERATIONS_INTEGRATION.dbo.INV_DELETE_BY_BATCH @Batch_id

  insert epicor.IDT_OPERATIONS_INTEGRATION.dbo.inv_hdr 
  select --ih.*			--MOD#1
	  [BATCH_ORD_ID]
      ,[BATCH_ID]
      ,[SALES_ORD_NBR]
      ,[INV_NBR]
      ,[CURR_STAT]
      ,[BILL_ACCT_ID]
      ,[ORG_NBR]
      ,[ACCT_NBR]
      ,[ENDUSER_NBR]
      ,[INV_TOTAL]
      ,[INV_LINES]
      ,[SHIP_DT]
      ,[ORD_DT]
      ,[INV_DT]
      ,[PAY_AUTH_NBR]
      ,[PAYMENT_TYPE]
      ,[PRICE_GRP_CD]
      ,[SHIP_METH]
      ,[SHIP_NM]
      ,[SHIP_ADDR1]
      ,[SHIP_ADDR2]
      ,[SHIP_CITY]
      ,[SHIP_ST]
      ,[SHIP_ZIP]
      ,[SHIP_ZIP4]
      ,[SHIP_CNTRY]
      ,[SHIP_PH]
      ,[BILL_NM]
      ,[BILL_ADDR1]
      ,[BILL_ADDR2]
      ,[BILL_CITY]
      ,[BILL_ST]
      ,[BILL_ZIP]
      ,[BILL_ZIP4]
      ,[BILL_CNTRY]
      ,[BILL_PH]
      ,[SHIP_ENDUSER_NM]
      ,[ORD_METH]
      ,ih.[CURRENCY_ID]
      ,[EXCHANGE_RATE]
      ,[BILL_FAX]
      ,[DELIVERY_METHOD_ID]
	  ,CURRENCY.EPICOR_CURRENCY_CODE
      ,[INV_TOTAL_NATURAL]	--Part of Mark's changes to Sales.inv_hdr?
	  ,[COMPANY_ID]	--Part of Neal's changes for BVBA?
  from dbo.inv_hdr ih
  JOIN dbo.CURRENCY CURRENCY ON CURRENCY.CURRENCY_ID = ih.CURRENCY_ID
  where batch_id = @Batch_id AND
	ih.bill_acct_id <> '10720' --Filter internal Orders

  select @err = @@error, @rowCount = @@rowCount
  if @err <> 0 or @rowCount = 0
    goto Error_Handling 

  insert epicor.IDT_OPERATIONS_INTEGRATION.dbo.inv_line_items
  select --ili.*		--MOD#1
	  [INV_LINE_ITEM_ID]
      ,ih.[BATCH_ORD_ID]
      ,[ACCT_ID]
      ,[QTY]
      ,[REF_NBR]
      ,[PROD_DESC]
      ,[UNIT_PRICE]
      ,[SEQ_DESC]
      ,[PAR_GRP]
      ,[SHIPPING]
      ,[GROUP_ID]
      ,[LINE_ITEM_TYPE]
      ,[PCApplyID]
      ,[AMOUNT]
      ,[UNIT_PRICE_NATURAL]
      ,[AMOUNT_NATURAL]
  from inv_hdr ih
  join inv_line_items ili on ih.batch_ord_id = ili.batch_ord_id
  where batch_id = @Batch_id
    and UNIT_PRICE <> 0 AND
	ih.bill_acct_id <> '10720' --Filter internal Orders

  select @err = @@error, @rowCount = @@rowCount
  if @err <> 0 or @rowCount = 0
    goto Error_Handling 

  return 0

  Error_Handling:
    raiserror ('Error writing invoicing batch to Epicor. Error_code: %i; Rowcount: %i',16,1,@err, @rowCount) 
    return -1

end
GO
GRANT EXECUTE ON  [dbo].[EPICOR_IM_PRE_POPULATE] TO [IDT_APPS]
GO
