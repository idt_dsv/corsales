SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


create  procedure [dbo].[SALES_TERRITORY_UPDATE_BY_ORG_NBR](@OrgList varchar(8000), @TERR_ID INT)

/**********************************************
 SALES_TERR_UPDATE_BY_ORG_NBR
 Author:NLaikhter
 Date:10/24/03
 Description: Updates the Organization territory
              (or comma separeted list of Orgs)  
              for Sales_Terr_ID in Party table 
 How to call Stored Procedure: pass either one or comma separated list of org_nbr's and sales_terr_id

 Exec SALES_TERRITORY_UPDATE_BY_ORG_NBR '@ORGS,@ORGS,@ORGS,@ORGS...',@TERR_ID 
-- testing  outputs - distinct sales_terr values before and after
declare  @OrgNbrList varchar(512)
set  @OrgNbrList = '1109'
 select distinct 'org before', sales_terr_id from party where party_id in (select party_id from org where org_nbr = @OrgNbrList)
 select distinct 'party before', sales_terr_id from party where party_id in (select party_id from account where org_id = (select party_id from org where org_nbr = @OrgNbrList))
 select distinct 'person before', sales_terr_id  from party where party_id in  (select party_id from person where org_id = (select party_id from org where org_nbr = @OrgNbrList))
 begin transaction
 Exec sales.dbo.SALES_TERRITORY_UPDATE_BY_ORG_NBR @OrgNbrList,5
 select distinct 'org after sp', sales_terr_id from party where party_id in (select party_id from org where org_nbr = @OrgNbrList)
 select distinct 'party after sp', sales_terr_id from party where party_id in (select party_id from account where org_id = (select party_id from org where org_nbr = @OrgNbrList))
 select distinct 'person after sp', sales_terr_id  from party where party_id in  (select party_id from person where org_id = (select party_id from org where org_nbr = @OrgNbrList))
 rollback
 select distinct 'org after rollback', sales_terr_id from party where party_id in (select party_id from org where org_nbr = @OrgNbrList)
 select distinct 'party after rollback', sales_terr_id from party where party_id in (select party_id from account where org_id = (select party_id from org where org_nbr = @OrgNbrList))
 select distinct 'person after rollback', sales_terr_id  from party where party_id in  (select party_id from person where org_id = (select party_id from org where org_nbr = @OrgNbrList))

grant execute on SALES_TERRITORY_UPDATE_BY_ORG_NBR to idt_reporter

 Revision:
 10/29/03 MDW+NL  Updated joins
 4/1/08   MDW modified to include associated Accounts and endusers
**********************************************/

AS
BEGIN
DECLARE @RowCount INT

BEGIN TRANSACTION

--SELECT *
UPDATE PARTY
SET SALES_TERR_ID = @TERR_ID
FROM dbo.org 
  join DBO.BreakApartStr(@OrgList) OrgList on org.org_nbr = convert(int,OrgList.StrValue)
  join dbo.PARTY ON PARTY.PARTY_ID = org.PARTY_ID

SET  @RowCount =  @@ROWCOUNT
IF  @RowCount = 0
  BEGIN
   PRINT 'NO ORG ROWS WERE UPDATED'
  END
ELSE
  BEGIN 
   PRINT 'SALES TERRITORY HAS BEEN UPDATED for Orgs'
  END

--SELECT *
UPDATE PARTY
SET SALES_TERR_ID = @TERR_ID
FROM dbo.org 
  join DBO.BreakApartStr(@OrgList) OrgList on org.org_nbr = convert(int,OrgList.StrValue)
  join Account on account.org_id = org.party_id
  join dbo.PARTY ON PARTY.PARTY_ID = account.PARTY_ID

SET  @RowCount =  @@ROWCOUNT
IF  @RowCount = 0
  BEGIN
   PRINT 'NO ACCOUNT ROWS WERE UPDATED'
  END
ELSE
  BEGIN 
   PRINT 'SALES TERRITORY HAS BEEN UPDATED for associated accounts'
  END


--SELECT *
UPDATE PARTY
SET SALES_TERR_ID = @TERR_ID
FROM dbo.org
  join DBO.BreakApartStr(@OrgList) OrgList on org.org_nbr = convert(int,OrgList.StrValue)
  join person on person.org_id = org.party_id
  join dbo.PARTY ON PARTY.PARTY_ID = person.PARTY_ID

SET  @RowCount =  @@ROWCOUNT
IF  @RowCount = 0
  BEGIN
   PRINT 'NO Person ROWS WERE UPDATED'
  END
ELSE
  BEGIN 
   PRINT 'SALES TERRITORY HAS BEEN UPDATED for associated endusers'
  END
  
COMMIT
  
END






GO
GRANT EXECUTE ON  [dbo].[SALES_TERRITORY_UPDATE_BY_ORG_NBR] TO [IDT_Reporter]
GO
