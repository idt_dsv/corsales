SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
Author: Bill Sorensen
Date: 1/18/2008
Notes:  
  Returns the shipping annotations for a given sales order number.
  
  ANNOTATION_SELECT_FOR_SALES_ORD_NBR is for Framework 3 applications;
  this is for .NET applications (columns are not aliased).
Usage:
  Execute dbo.ANNOTATION_SELECT_SHIPPING_FOR_SALES_ORDER_NUMBER_NET 4528595
Permissions:
  grant execute on dbo.ANNOTATION_SELECT_SHIPPING_FOR_SALES_ORDER_NUMBER_NET to idt_apps 
Revisions:
Date   Who  Notes
-----------------------------------------
*/
Create procedure [dbo].[ANNOTATION_SELECT_SHIPPING_FOR_SALES_ORDER_NUMBER_NET] (@SalesOrderNumber int) 
AS 
BEGIN

SELECT [ANNOTATION_ID]
      ,[BF_BOBJECT_CID]
      ,[BF_BOBJECT_OID]
      ,[ANNOTATION]
      ,[AUTHOR_ID]
      ,[CREATE_DTM]
      ,[MACHINE_STAMP]
      ,[WORKSTATION_ID]
      ,[CANCEL_BY_ID]
      ,[NOTE_CATEGORY]
      ,[CANCEL_DTM]
FROM DBO.GET_SHIPPING_ANNOTATIONS_FOR_SALES_ORD_NBR(@SalesOrderNumber, getDate())

END
GO
GRANT EXECUTE ON  [dbo].[ANNOTATION_SELECT_SHIPPING_FOR_SALES_ORDER_NUMBER_NET] TO [IDT_APPS]
GO
