SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO


create        PROCEDURE [dbo].[APP_VERSION_CHECK] 
--with execute as 'idt-coralville\mdewaard'
/*
Author: MDEWAARD
DATE: 6/19/02
Purpose:
To provide Application developers a report that shows all versions of all applications currently running

permissions:
grant execute on [APP_VERSION_CHECK] to idt_apps

Modifications:
DATE	AUTHOR	Description
---------------------------------------------------------
8/25/06 MDW   Updated to use the sys.sysprocesses for SQL 2005. 
              have to grant 'View server state' priveliges on server for it to see all sessions
*/
AS

select distinct count(*) user_count, rtrim(program_name)  unique_app_name
from master.sys.dm_exec_sessions --sys.sysprocesses 
where program_name <> '' 
  and host_name not like 'Trident%' 
  and  host_name not like 'pete%' 
  and program_name not in ('SQL Profiler','SQL Query Analyzer'
        , 'SQL Query Analyzer - Object Browser'
        , 'Microsoft(R) Windows (R) 2000 Operating System'
        , 'SQLDMO_9'
        , 'MS SQLEM' )
group by rtrim(program_name)
order by rtrim(program_name)

select rtrim(program_name) , case rtrim(host_name) when '' then 'null' else rtrim(host_name) end host_name
from master.sys.dm_exec_sessions --sys.sysprocesses 
where program_name <> '' 
  and host_name not like 'Trident%' 
  and  host_name not like 'pete%' 
  and program_name not in ('SQL Profiler','SQL Query Analyzer'
        , 'SQL Query Analyzer - Object Browser'
        , 'Microsoft(R) Windows (R) 2000 Operating System'
        , 'SQLDMO_9'
        , 'MS SQLEM' )
order by rtrim(program_name) , host_name







GO
GRANT EXECUTE ON  [dbo].[APP_VERSION_CHECK] TO [IDT_APPS]
GO
