
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
GRANT  EXECUTE  ON [dbo].[UpdateProcessPreferenceExpanded] TO [Coliseum]

Author: Phil Shaff
Date Created: April 14th, 2014
Purpose:

	Updates PROCESS_PREFERENCE_EXPANDED record

Modifications:
Who	Date	Modification	Note
---------------------------------------------------------------------------------------
PS  7/11/14 Added output to the keystore key allocation query
PS  8/12/14 Added location to ID selection query
*/

CREATE PROCEDURE [dbo].[UpdateProcessPreferenceExpanded]
    @BusinessObjectId INT
  , @GroupId INT
  , @OptionTypeId INT
  , @Value VARCHAR(1000)
  , @TypeId INT
  , @OwnerId INT
  , @LocationId INT
  , @OverrideLevel INT
  , @OverrideTypeId INT
  , @OverrideOwnerId INT
AS
    DECLARE @id INT
    SET @id = ( SELECT  TOP 1 PROCESS_PREFERENCE_EXPANDED_ID
                FROM    SALES.DBO.PROCESS_PREFERENCE_EXPANDED WITH ( NOLOCK )
                WHERE   OPTION_OWNER_CID = @TypeId
                        AND OPTION_OWNER_OID = @OwnerId
                        AND PROC_PREF_GRP_ID = @GroupId
                        AND PROC_PREF_DEF_ID = @OptionTypeId
						AND LOCATION_ID = @LocationId
				ORDER BY VERSION_DTM DESC
              )

    IF @id IS NULL
        OR @id = 0
        BEGIN
            DECLARE @NEXT_KEY_VAL INT
            EXEC KEY_FIELD_GET_NEXT_VALUE 'PROCESS_PREFERENCE_EXPANDED_ID',
                @NEXT_KEY_VAL OUTPUT

            INSERT  INTO SALES.DBO.PROCESS_PREFERENCE_EXPANDED
                    (
                      PROCESS_PREFERENCE_EXPANDED_ID
                    , PROCESS_PREFERENCE_CID
                    , PROC_PREF_GRP_ID
                    , PROC_PREF_DEF_ID
                    , OPTION_VALUE
                    , OPTION_OWNER_CID
                    , OPTION_OWNER_OID
                    , LOCATION_ID
                    , OVERRIDE_TYPE
                    , OVERRIDE_SOURCE_CID
                    , OVERRIDE_SOURCE_OID
                    , VERSION_NBR
                    , VERSION_DTM
                    )
            VALUES  (
                      @NEXT_KEY_VAL
                    , @BusinessObjectId
                    , @GroupId
                    , @OptionTypeId
                    , @Value
                    , @TypeId
                    , @OwnerId
                    , @LocationId
                    , @OverrideLevel
                    , @OverrideTypeId
                    , @OverrideOwnerId
                    , 1
                    , GETDATE()
                    )
        END
    ELSE
        BEGIN
            UPDATE  SALES.DBO.PROCESS_PREFERENCE_EXPANDED
            SET     PROCESS_PREFERENCE_CID = @BusinessObjectId
                  , PROC_PREF_GRP_ID = @GroupId
                  , PROC_PREF_DEF_ID = @OptionTypeId
                  , OPTION_VALUE = @Value
                  , OPTION_OWNER_CID = @TypeId
                  , OPTION_OWNER_OID = @OwnerId
                  , LOCATION_ID = @LocationId
                  , OVERRIDE_TYPE = @OverrideLevel
                  , OVERRIDE_SOURCE_CID = @OverrideTypeId
                  , OVERRIDE_SOURCE_OID = @OverrideOwnerId
                  , VERSION_NBR = VERSION_NBR + 1
                  , VERSION_DTM = GETDATE()
            WHERE   PROCESS_PREFERENCE_EXPANDED_ID = @id
        END



GO



GRANT EXECUTE ON  [dbo].[UpdateProcessPreferenceExpanded] TO [coliseum]
GO
