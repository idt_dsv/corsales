SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [admin].[MissingIndexesGet]
( @dbName nvarchar(128) = null
, @TableName nvarchar(128) = '%'  -- filter to see only specific indexex for a table
, @FieldName nvarchar(128) ='%' -- filter to see only specific indexex for a field
, @avg_user_impact decimal(5,1) = 20  
, @last_user_seek datetime  = null  -- filter to prevent old indexes from showing up. The index could have been used since this date
                               -- in code defaults to 24 hours
, @avg_total_user_cost decimal(5,1) = 1.0
)
 AS
/*
Suggested Index SQL
-- see where clause for suggested options

--get last reboot time
select create_date from master.sys.databases where name = 'tempdb'
declare @dbName nvarchar(128), @TableName nvarchar(128), @FieldName nvarchar(128), @last_user_seek datetime, @avg_total_user_cost decimal(5,1)
select @dbName = '%sales%'
  , @TableName= '%ord_line_item%' --%
  , @FieldName = '%' --%'
  , @last_user_seek = getdate()-5
exec admin.MissingIndexesGet @dbName = @dbName
, @TableName  = @TableName  -- filter to see only specific indexex for a table
, @FieldName  =@FieldName -- filter to see only specific indexex for a field
, @avg_user_impact = 1.0  
, @last_user_seek  = @last_user_seek  
, @avg_total_user_cost = 1.0
*/
BEGIN
--  if @dbName is null 
--    set @dbName = db_name()
  if @last_user_seek is null 
    set @last_user_seek = dateadd(hour,-24, getdate())
  select 
        compiles = s.unique_compiles
        -- Made up composite to combine importance by show impact and usage
      , composite = round(((avg_total_user_cost)+(avg_user_impact))+((user_seeks+(user_scans))/100.0), 2)
      , seeks = s.user_seeks
      , scans = s.user_scans
      , last_seek = replace(substring(convert(varchar(25),s.last_user_seek,121), 6, 14), '-','/')
      , cost = round(s.avg_total_user_cost,2)
      , impact = s.avg_user_impact
      , d.equality_columns
      , d.inequality_columns
      , d.included_columns
      , tableName =d.statement
      , Index_Statement = 'create nonclustered index ncix_'+
          replace(replace(replace(replace(d.statement,']',''),'[',''),db_Name(database_id)+'.', ''), 'dbo.', '') +
            '_'+replace(replace(isnull(d.equality_columns,',add_fieldname>'), '[',''),']','')+' on ' + statement + ' (' + isnull(equality_columns,' ') + ' ' + 
          case 
            when ISNULL(inequality_columns, '-1') <> '-1' 
              then ',' + ISNULL(inequality_columns, ' ') 
            else '' 
            end 
            + ') ' + 
            CASE 
              WHEN included_columns IS NOT NULL 
                THEN 'include (' + RTRIM(included_columns) + ')' 
              ELSE '' 
            end
     , db_Name(database_id)    
  from sys.dm_db_missing_index_group_stats s
		  ,sys.dm_db_missing_index_groups g
		  ,sys.dm_db_missing_index_details d
  where s.group_handle = g.index_group_handle
  and d.index_handle = g.index_handle
  AND db_Name(database_id) like @dbname 
  AND statement LIKE @TableName
   and (equality_columns like @FieldName or inequality_columns like  @FieldName or included_columns like  @FieldName)
      --May want to Increase the number to filter out more to filter out low usage suggested indexes
  and (user_seeks +user_scans) > 1
      --Occassionally change this value to show all options -- Increase the number to filter out more and show impact of indexes with biggest impact 
  and avg_user_impact > @avg_user_impact
      --Only show indexages that where have been used at least once in last so much time - to filter out old suggested indexes
  and last_user_seek > @last_user_seek
      --Occassionally change this value to show all options -- Increase the number to filter out more and show impact of indexes with biggest impact 
  and avg_total_user_cost > @avg_total_user_cost
  order by round(((avg_total_user_cost)+(avg_user_impact))+((user_seeks+(user_scans))/100.0), 2)desc ,CONVERT(INT,s.avg_user_impact) DESC, s.user_seeks desc
end
GO
