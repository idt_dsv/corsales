SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
  Author: MDEWAARD                            
  Date: 1/15/00                                 
  Notes: Update Order ship status when all items have been shipped   
       
  Modifications:
   9/4/03 MDW Updated nolocks and added coalesce on date to prevent nulls on insert
   2/16/05 MDW removed exclusion for Line_item_ref_nbr of work_spec_obj_type = 'TPromoItem' 
               -- changed default @Begin_@dt date to 3 days from 5 days
               -- Modified a few comments for clarity
   11/16/05 JLB When a TUFTS order fully is shipped place a record into confirmation
   11/30/05 WES Changed TUFTS confirmation to set HOLD_ID column to 0
   12/13/05 JLB Took out TUFTS conformation. It has been made into TUFTS_POST_SHIPPED_CONFIRMATION(SP)
 		and it call by NIGHTLY_UPDATES(SP) now.
   3/08/06  MDW rewrote to make update more reliable
   3/10/06  MDW Add clause to prevent current days oligos clause begins with "if datepart(hour, GETDATE()) < 20)"
   8/29/07  MDW moved to Sales database - updated to current information
   5/31/12	DAP Updated to set an order and all non-deliverable line items to shipped once all deliverable line items have shipped.
 */
 

CREATE  PROCEDURE [dbo].[UPD_ORD_SHIP_STATUS] 
(@BEGIN_DT dateTime = null)
 
 AS 
  set nocount on 
--drop table  #Tmp_Ord_Not_shipped
--drop table #TMP_shipped_ord

  CREATE TABLE #Tmp_Ord_Not_shipped  
     (ord_id int not NULL, 
      Sales_ord_nbr INT, 
      Ord_dtm datetime)
      
  CREATE TABLE #TMP_shipped_ord 
     (ord_id int)
     
  CREATE TABLE #Tmp_Shipped_Ref_Id 
     (ref_id INT)
---------------------------------------------

  DECLARE @cancelledOrHeldStatuses TABLE 
     (ord_stat_type_id INT PRIMARY key)
     
---------------------------------------------
  
  INSERT @cancelledOrHeldStatuses
  SELECT ord_stat_type_id 
  FROM SALES.dbo.ord_stat_type with (nolock)
  WHERE ord_stat_type_int_nm LIKE '%held%' 
  OR ord_stat_type_int_nm = 'Back Ordered'
  OR ord_stat_type_id IN (14,15,16,17,19)--invoiced, cancelled, shipped, etc

  
---------------------------------------------
  
--begin
--   declare @BEGIN_DT dateTime
	SET @BEGIN_DT = dateadd(d,-180,convert(varchar(10),getdate(),101))

	-- Get all orders that haven't shipped, are cancelled, or are in hold stat. store into #TMP_ORD_ID\
  -- For performance
    INSERT  #Tmp_Ord_Not_shipped
	SELECT DISTINCT ord.ord_ID, 
	                ord.sales_ord_nbr, 
	                ord.ord_date --oli.ord_ID, 
	FROM SALES.dbo.ord AS ord with (nolock)
	WHERE ord.ord_date >= @BEGIN_DT 
    AND ord.curr_stat NOT IN (SELECT ord_stat_type_id 
                              FROM @cancelledOrHeldStatuses)
                              
----------------------------------------------                               

	-- Update this to only check the status of line items that are deliverable.
	INSERT #TMP_shipped_ord
	SELECT TMP_Not_Shipped.ord_id
	FROM #Tmp_Ord_Not_shipped AS TMP_Not_Shipped
    JOIN SALES.dbo.ord AS ord with (nolock)
       ON ord.ORD_ID = TMP_Not_Shipped.ord_id
	WHERE ord.items_on_ord > 0
    AND NOT EXISTS (	SELECT * 
						FROM sales.dbo.LINE_ITEM_REF_NBR AS lirn with (nolock) 
						WHERE	TMP_Not_Shipped.ORD_ID = lirn.FK_ORD_ID
						AND  lirn.IS_DELIVERABLE = 1 
						AND  lirn.curr_stat not in (4, 5)
					) 
				
	--remove items from todays processing if run before 10 PM
    IF (datepart(hour, GETDATE()) < 22)
    BEGIN
    
	DELETE TMP_shipped_ord
	FROM #TMP_shipped_ord TMP_shipped_ord
	JOIN SALES.dbo.ord AS ord with (nolock)
	   ON ord.ORD_ID = TMP_shipped_ord.ord_id
	WHERE EXISTS 
	   ( -- items that were shipped from 10 PM last night to current time
		SELECT * 
		FROM sales.dbo.LINE_ITEM_REF_NBR AS lirn with (nolock) 
		JOIN sales.dbo.ref_nbr_stat_hist AS rnsh with (nolock) 
		   ON rnsh.REF_ID = lirn.ref_id
		WHERE lirn.FK_ORD_ID = TMP_shipped_ord.ord_id 
		AND rnsh.STAT_DT > DATEADD(hour, 22, floor(convert(float,GETDATE()-1))) -- 10 PM of current day
		AND rnsh.STAT_ID in (4,5)
		)
		
    END

-----------------------------------------------

	-- Add temp table for ref ids that need to be marked as shipped.
	INSERT #Tmp_Shipped_Ref_Id
	SELECT lirn.REF_ID 
	FROM #TMP_shipped_ord AS tso 
	JOIN sales.dbo.LINE_ITEM_REF_NBR AS lirn with (nolock) 
	   ON tso.ord_id = lirn.fk_ord_id
	WHERE tso.ORD_ID=lirn.FK_ORD_ID 
	AND	lirn.IS_DELIVERABLE = 0 
	AND lirn.curr_stat not in (4, 5)
	
-----------------------------------------------			
			
	INSERT INTO [dbo].[ORD_STAT_HIST]
			 ([ORD_ID]
			 ,[ORD_STAT_DT]
			 ,[ORD_STAT_TYPE_ID]
			 ,[USER_NM]
			 ,[REASON])
	SELECT ord.ORD_ID, 
	       ORD_STAT_DT = GETDATE(), 
	       ORD_STAT_TYPE_ID = 14, 
	       USER_NM = 'UPD_ORD_SHIP_STATUS SP', 
	       REASON = 'Shipped complete'
	FROM #TMP_shipped_ord AS TMP_shipped_ord 
	JOIN SALES.dbo.ord AS ord 
	   ON ord.ORD_ID = TMP_shipped_ord.ord_id
	WHERE NOT EXISTS (SELECT * 
	                  FROM sales.dbo.inv_hdr AS ih with (nolock)
	                  WHERE ih.sales_ord_nbr = ord.sales_ord_nbr)
	ORDER BY ord.ord_date 
	
-----------------------------------------------

	-- Add code to insert into ref_nbr_stat_hist for the ref ids that need to be marked as shipped.
	INSERT INTO dbo.REF_NBR_STAT_HIST
	        ( REF_ID ,
	          STAT_ID ,
	          STAT_DT ,
	          REASON ,
	          USER_NM
	        )
	SELECT ref.Ref_Id, 
	       STAT_ID = 4, 
	       STAT_DT = GETDATE(), 
	       REASON = 'Shipped complete', 
	       USER_NM = 'UPD_ORD_SHIP_STATUS SP'
	FROM #Tmp_Shipped_Ref_Id ref

 set nocount off


GO
