SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
--GRANT  EXECUTE  ON [dbo].[UpdateConfirmationSetEmailSend] TO [IDT-Coralville\iusr_idt]

Author: Phil Shaff
Date Created: January 27th, 2014
Purpose:

	Updates confirmation e-mail to be sent, Sets e-mail sent status

Modifications:
Who	Date		Modification	Note
---------------------------------------------------------------------------------------
PES 6/16/2014	Clearing out ERR_MSG for a successful send.
*/

CREATE PROCEDURE [dbo].[UpdateConfirmationSetEmailSend]
@ConfirmationId int,
@WorkStationName varchar(64)

AS

DECLARE @WORKSTATION_ID INT

SET @WORKSTATION_ID = (SELECT workstation_id
                       FROM   framework_system.dbo.workstation WITH (nolock)
                       WHERE  name = @WorkStationName)

UPDATE sales.dbo.confirmation
SET    status = 1,
       sent_dtm = Getdate(),
	   ERR_MSG = ''
WHERE  COALESCE(status, 0) = 2
       AND con_id = @ConfirmationId
       AND claimed_workstation_id = @WORKSTATION_ID  


GO
GRANT EXECUTE ON  [dbo].[UpdateConfirmationSetEmailSend] TO [coliseum]
GRANT EXECUTE ON  [dbo].[UpdateConfirmationSetEmailSend] TO [IDT-CORALVILLE\IUSR_IDT]
GO
