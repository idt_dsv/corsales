SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[KEY_FIELD_GET_NEXT_X_VALUES]
	  @KEY_NM					  VARCHAR(128)--BDE TQuery  fieldname  limits to 30 chars
	, @NBR_OF_KEYS				  INT
	, @NEXT_LOW_KEY_VAL			  INT = -1 OUTPUT
	, @NEXT_HIGH_KEY_VAL		  INT = -1 OUTPUT
AS 

SET NOCOUNT ON

/*--------------------------------------------------------------------------------------
Author: Mark DeWaard
Date Created: 3/21/03
Purpose:
	Gets unique next key value for a key field from the next_key_val table
	Took GET_NEXT_X_KEY_VALUES SP to meet naming standards
	and renamed it and added error logic

Permissions:
	GRANT EXECUTE ON  [dbo].[KEY_FIELD_GET_NEXT_X_VALUES] TO [IDT_APPS]

Example: (note it is different than GET_NEXT_X_KEY_VALUES
	DECLARE @RC				 INT
		, @KEY_NM			 VARCHAR(128)
		, @NBR_OF_KEYS		 INT
		, @NEXT_LOW_KEY_VAL  INT
		, @NEXT_HIGH_KEY_VAL INT

	select @KEY_NM = 'acc_nbr'
		, @NBR_OF_KEYS = -1

	EXEC @RC = dbo.KEY_FIELD_GET_NEXT_X_VALUES @KEY_NM
		, @NBR_OF_KEYS
		, @NEXT_LOW_KEY_VAL OUTPUT
		, @NEXT_HIGH_KEY_VAL OUTPUT 

	IF @RC = 0
		SELECT @KEY_NM
			, @NBR_OF_KEYS
			, @NEXT_LOW_KEY_VAL
			, @NEXT_HIGH_KEY_VAL
	ELSE PRINT 'error ' + CONVERT(VARCHAR(10), @RC)

Modifications:
Who     Date      Modification  Note
----------------------------------------------------------------------------------------
MDW     07/29/05	Added call to KEY_FIELD_GET_NEXT_ALLOCATION
MDW     06/18/07	add Transaction control
MDW     06/21/07	refactored to Transaction control, used BEGIN try/except, added raiserror
JLB		03/05/08	Review/cleanup
JLB		03/07/08	Refactored for allocation range checks
JLB		03/11/08	Change to handle the next_key_val directly
JLB		03/13/08	Pass @DATABASE_NM to allocation request also
JLB		04/26/08	Added @SERVER_NM to the allocation request
APG		05/14/13	Combined the six different versions of this proc into one that can handle all cases.
--------------------------------------------------------------------------------------*/

DECLARE @Count					  INT
	, @Error					  INT
	, @RC						  INT 
	, @KEY_VAL_END				  INT
	, @DATABASE_NM				  SYSNAME
	, @SERVER_NM				  SYSNAME
	, @BranchID					  INT
	, @RVAL						  INT
	, @EXT_KEY_NM				  SYSNAME
	, @NEXT_KEY_VAL_ALLOCATION_ID INT
	, @KEY_VAL_START_NBR		  INT
	, @KEY_VAL_END_NBR			  INT
	, @ErrorMessage				  NVARCHAR(4000)
	, @ErrorSeverity			  INT
	, @ErrorState				  INT

SELECT @SERVER_NM				  = @@SERVERNAME
	, @DATABASE_NM				  = DB_NAME()

SELECT @BranchID = BRANCH_LOCATION_ID
FROM Reference.dbo.BRANCH_CONFIGURATION WITH (NOLOCK)

BEGIN TRY
	/* negative number causes issue */ 
	IF @NBR_OF_KEYS <= 0
	BEGIN
		RAISERROR ( N'Invalid "@NBR_OF_KEYS" parameter. Must be an integer greater than 0', 16 , 1 )
	END

	/* no blank key name */ 
    IF @KEY_NM = '' 
	BEGIN
		RAISERROR ( N'Invalid "@KEY_NM" parameter. Key name can not be NULL or Blank', 16 , 1 )
	END

	-- Check for NEXT_KEY_VAL table in the current database.
	-- If it doesn't exist, call KEY_FIELD_GET_NEXT_X_VALUES from the Production\Manufacturing database
	-- based on the branch location and then exit the code.
	IF NOT EXISTS ( SELECT name FROM SYS.SYSOBJECTS WITH (NOLOCK) WHERE name = 'NEXT_KEY_VAL' )
	BEGIN
		IF @BranchID = 1
		BEGIN
			EXEC Production.dbo.KEY_FIELD_GET_NEXT_X_VALUES @KEY_NM
				, @NBR_OF_KEYS
				, @NEXT_LOW_KEY_VAL OUTPUT
				, @NEXT_HIGH_KEY_VAL OUTPUT 
		END
		ELSE
		BEGIN
			EXEC Manufacturing.dbo.KEY_FIELD_GET_NEXT_X_VALUES @KEY_NM
				, @NBR_OF_KEYS
				, @NEXT_LOW_KEY_VAL OUTPUT
				, @NEXT_HIGH_KEY_VAL OUTPUT 
		END

		RETURN
	END

	-- Check for GetPrimaryKeyNameAlias stored proc in the current database.
	-- If it exists, execute it to get the alias for the key name.
	IF EXISTS ( SELECT name FROM SYS.SYSOBJECTS WITH (NOLOCK) WHERE name = 'GetPrimaryKeyNameAlias' )
	BEGIN
		EXEC @RVAL = dbo.GetPrimaryKeyNameAlias @KEY_NM
			, @EXT_KEY_NM OUTPUT
		IF @RVAL = 0 
		BEGIN
			SELECT  @KEY_NM = @EXT_KEY_NM
		END
		ELSE 
		BEGIN
			RAISERROR ( N'No entry found in the PrimaryKeyAlias table for key: %s.', 16, 1, @KEY_NM )
		END  
	END
    
	-- For speed get the allocation end value from the local NEXT_KEY_VAL table
	-- It should ONLY be set and updated during the keystore request for a new allocation block
	SELECT @KEY_VAL_END = ISNULL(NEXT_KEY_VAL_ALLOCATION_END_VAL,0)
	FROM dbo.NEXT_KEY_VAL WITH (NOLOCK)
	WHERE KEY_NM = @KEY_NM
    
    BEGIN TRANSACTION    
    /* Try to optimistically update -- assumption is that it will work */ 
    UPDATE dbo.NEXT_KEY_VAL WITH (HOLDLOCK)
    SET @NEXT_HIGH_KEY_VAL = KEY_VAL = KEY_VAL + @NBR_OF_KEYS
    WHERE KEY_NM = @KEY_NM
		AND KEY_VAL < (@KEY_VAL_END - @NBR_OF_KEYS)

    SELECT @Count = @@ROWCOUNT
		, @Error = @@ERROR

	/* Update was successful, but no records ; request is beyond max range*/
    IF (@Error = 0 AND @Count = 0)  
	BEGIN
		EXEC @RC = KEY_STORE.dbo.KEY_FIELD_GET_NEXT_ALLOCATION @KEY_NM
			, @NBR_OF_KEYS
			, @SERVER_NM
			, @DATABASE_NM
			, @NEXT_KEY_VAL_ALLOCATION_ID OUTPUT
			, @KEY_VAL_START_NBR OUTPUT
			, @KEY_VAL_END_NBR OUTPUT
		IF @RC <> 0 
		BEGIN
			RAISERROR ( N'Error in creating new key allocation block for Key: %s. The error returned from "KEY_FIELD_GET_NEXT_ALLOCATION" is: %i', 16, 1, @KEY_NM, @RC )
		END 
	  
		SELECT @NEXT_HIGH_KEY_VAL = @KEY_VAL_START_NBR + @NBR_OF_KEYS

		/* Update the NEXT_KEY_VAL table -- This assumes the KEY_NM is already in NEXT_KEY_VAL table */
		UPDATE dbo.NEXT_KEY_VAL
		SET KEY_VAL							  = @KEY_VAL_START_NBR + @NBR_OF_KEYS
			, NEXT_KEY_VAL_ALLOCATION_ID	  = @NEXT_KEY_VAL_ALLOCATION_ID
			, NEXT_KEY_VAL_ALLOCATION_END_VAL = @KEY_VAL_END_NBR
		FROM dbo.NEXT_KEY_VAL WITH (NOLOCK)
		WHERE KEY_NM = @KEY_NM

		SELECT @Count = @@ROWCOUNT
			, @Error = @@ERROR

		/* error in the update of next_key_val table */
		IF @Count <> 1
		BEGIN
			/* Add key if it is not there */
			IF NOT EXISTS ( SELECT KEY_NM FROM dbo.NEXT_KEY_VAL WHERE KEY_NM = @KEY_NM )
			BEGIN    
				INSERT INTO dbo.NEXT_KEY_VAL
				(
					KEY_NM
					, KEY_VAL
					, NEXT_KEY_VAL_ALLOCATION_ID
					, NEXT_KEY_VAL_ALLOCATION_END_VAL
				)
				VALUES
				(
					@KEY_NM
					, @KEY_VAL_START_NBR + @NBR_OF_KEYS
					, @NEXT_KEY_VAL_ALLOCATION_ID
					, @KEY_VAL_END_NBR
				)
						
				SELECT @Count = @@ROWCOUNT
					, @Error = @@ERROR
			END			
		END
	END 

	/* Update was successful */ 
	IF @Error = 0 and @Count = 1
	BEGIN
		SELECT @NEXT_LOW_KEY_VAL = @NEXT_HIGH_KEY_VAL - @NBR_OF_KEYS + 1

		COMMIT TRANSACTION

		RETURN 0    
	END
	ELSE 
	BEGIN
		SELECT @NEXT_HIGH_KEY_VAL = NULL
			, @NEXT_LOW_KEY_VAL = NULL
		IF @Error <> 0 
			RAISERROR ( N'Sql Server Error during update NEXT_KEY_VAL table: %i', 16, 1, @Error )
		ELSE IF @Count = 0
			RAISERROR ( N'Error during update NEXT_KEY_VAL table. No record was affected. The key is either not in the table and was unable to create it', 16, 1 )
		ELSE 
			RAISERROR ( N'Error during update NEXT_KEY_VAL table. More than one record affected. rolled back', 16, 1 )
	END
END TRY
BEGIN CATCH 
	IF @@TRANCOUNT > 0 
	BEGIN
		ROLLBACK 
	END 
		
	/* Return error information about the original error that caused execution to jump to the CATCH block. */      
	SELECT @ErrorMessage = ERROR_MESSAGE()
		, @ErrorSeverity = ERROR_SEVERITY()
		, @ErrorState	 = ERROR_STATE()

	RAISERROR (	@ErrorMessage, @ErrorSeverity, @ErrorState )

	RETURN -1
END CATCH

GO
GRANT EXECUTE ON  [dbo].[KEY_FIELD_GET_NEXT_X_VALUES] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[KEY_FIELD_GET_NEXT_X_VALUES] TO [IDT_WebApps]
GO
