SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: MDW
Date: 5/22/08
Notes:
  Built to replace dynamic sql from the Past Order Search 

example:
declare @EndUserNbr int, @referenceNbr int 
select top 1 @EndUserNbr= enduser_nbr, @referenceNbr = lirn.ref_id from dbo.Ord join person on person.party_id = ord.enduser_id   JOIN dbo.LINE_ITEM_REF_NBR LIRN with ( nolock ) ON LIRN.FK_ORD_ID = ORD.ORD_ID order by 1 desc
exec dbo.E_OrderHistoryRefSearch @EndUserNbr ,@referenceNbr 

Rights
grant execute on E_OrderHistoryRefSearch to idt_webapps, idt_reporter

Modifications:

*/
create procedure [dbo].[E_OrderHistoryRefSearch]
(
  @EndUserNbr int
, @ReferenceNbr int
)
as 
begin

set nocount on

SELECT
  SALES_ORD_NBR
, ORD_DATE
, ord.Curr_Stat
, ORD_ID
, ITEMS_ON_ORD
, ORD.PAY_METH_ID
, ORD.PAY_AUTH_NBR
, PERSON.FIRST_NM
, PERSON.LAST_NM
, ACCOUNT.ACC_NM
FROM   dbo.ORD with ( nolock )
  JOIN dbo.LINE_ITEM_REF_NBR LIRN with ( nolock )
         ON LIRN.FK_ORD_ID = ORD.ORD_ID
  LEFT JOIN dbo.PERSON with ( nolock )
         ON ORD.ENDUSER_ID = PERSON.PARTY_ID
  LEFT JOIN dbo.ACCOUNT with ( nolock )
         ON ORD.ACC_ID = ACCOUNT.ACC_NBR
WHERE
  PERSON.ENDUSER_NBR = @EndUserNbr
    AND LIRN.REF_ID = @ReferenceNbr
ORDER BY
  SALES_ORD_NBR DESC
, ORD_DATE DESC

set nocount off
end 
GO
GRANT EXECUTE ON  [dbo].[E_OrderHistoryRefSearch] TO [IDT_Reporter]
GRANT EXECUTE ON  [dbo].[E_OrderHistoryRefSearch] TO [IDT_WebApps]
GO
