SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO


CREATE   PROCEDURE [dbo].[QuotePartyAttributionsCurrent_SEL]
(@PersonID int, @AccID int, @OrgID int, @AsOf DateTime)

/*---------------------------------------------------------
QuotePartyAttribution_GET
Author:Bob Dawson 
Date:21 APR 03 
Notes:
-  Fetches QuotePartyAttributions connected to person, account, or Org. as of specified date.
Used by TEndUser, TAccount, and TOrganization classes.
In addition to AsOf params, TEndUser object supplies all three party params, 
TAccount only the acc and org params, and TOrganization only the OrgID.

Rights:
grant exec on [QuotePartyAttributionsCurrent_SEL] to IDT_APPS

Sample:
EXECUTE [SALES].[dbo].[QuotePartyAttributionsCurrent_SEL] 98426 ,Null ,Null ,'2006-09-25'

Revisions:
-----------------------------------------------------------
09/25/06	JLB Moved from Production domain to Sales
*/

AS
SELECT
  QUOTE_PARTY_ID,
  'Person' as TypeDesc,
  A.ACC_NBR as AcctNumber,
  A.ACC_NM as AcctName,
  O.ORG_NBR as OrgNumber,
  O.ORG_NM as OrgName,
  PS.ENDUSER_NBR as EndUserNumber,
  PS.FIRST_NM as FirstName,
  PS.LAST_NM as LastName,
  Q.FROM_DT as QuoteFromDate,
  Q.QUOTE_STATUS_ID as QuoteStatusID,
  Q.THRU_DT as QuoteThruDate,
  QP.CREATE_DTM as CreateDTM,
  QP.CREATE_USER_ID as CreateUserID,
  QP.FROM_DT as FromDate,
  QP.PARTY_ID as PartyID,
  QP.QUOTE_ID as QuoteID,
  QP.THRU_DT as ThruDate
FROM dbo.QUOTE_PARTY as QP
  join dbo.QUOTE as Q on QP.QUOTE_ID = Q.QUOTE_ID
  join dbo.PERSON as PS on PS.PARTY_ID = QP.PARTY_ID
  join dbo.ACCOUNT as A on A.PARTY_ID = PS.ACC_ID
  join dbo.ORG as O on O.PARTY_ID = PS.ORG_ID
WHERE (@PersonID > 0) and
  (QP.PARTY_ID = @PersonID
   and ((QP.From_DT is null) or (QP.From_DT < @AsOf))
   and ((QP.Thru_DT is null) or (QP.Thru_DT > @AsOf)))

UNION ALL
SELECT
  QUOTE_PARTY_ID,
  'Account' as TypeDesc,
  A.ACC_NBR as AcctNumber,
  A.ACC_NM as AcctName,
  O.ORG_NBR as OrgNumber,
  O.ORG_NM as OrgName,
  NULL as EndUserNumber,
  NULL as FirstName,
  NULL as LastName,
  Q.FROM_DT as QuoteFromDate,
  Q.QUOTE_STATUS_ID as QuoteStatusID,
  Q.THRU_DT as QuoteThruDate,
  QP.CREATE_DTM as CreateDTM,
  QP.CREATE_USER_ID as CreateUserID,
  QP.FROM_DT as FromDate,
  QP.PARTY_ID as PartyID,
  QP.QUOTE_ID as QuoteID,
  QP.THRU_DT as ThruDate
FROM dbo.QUOTE_PARTY as QP
  join dbo.QUOTE as Q on QP.QUOTE_ID = Q.QUOTE_ID
  join dbo.ACCOUNT as A on A.PARTY_ID = QP.PARTY_ID
  join dbo.ORG as O on O.PARTY_ID = A.ORG_ID
WHERE (@AccID > 0) and 
  (QP.PARTY_ID = @AccID
   and ((QP.From_DT is null) or (QP.From_DT < @AsOf))
   and ((QP.Thru_DT is null) or (QP.Thru_DT > @AsOf)))


UNION ALL
SELECT
  QUOTE_PARTY_ID,
  'Organization' as TypeDesc,
  NULL  as AcctNumber,
  NULL  as AcctName,
  O.ORG_NBR as OrgNumber,
  O.ORG_NM as OrgName,
  NULL as EndUserNumber,
  NULL as FirstName,
  NULL as LastName,
  Q.FROM_DT as QuoteFromDate,
  Q.QUOTE_STATUS_ID as QuoteStatusID,
  Q.THRU_DT as QuoteThruDate,
  QP.CREATE_DTM as CreateDTM,
  QP.CREATE_USER_ID as CreateUserID,
  QP.FROM_DT as FromDate,
  QP.PARTY_ID as PartyID,
  QP.QUOTE_ID as QuoteID,
  QP.THRU_DT as ThruDate
FROM dbo.QUOTE_PARTY as QP
  join dbo.QUOTE as Q on QP.QUOTE_ID = Q.QUOTE_ID
  join dbo.ORG as O on O.PARTY_ID = QP.PARTY_ID
WHERE (@OrgID > 0) and
  (QP.PARTY_ID =  @OrgID
   and ((QP.From_DT is null) or (QP.From_DT < @AsOf))
   and ((QP.Thru_DT is null) or (QP.Thru_DT > @AsOf)))


GO
GRANT EXECUTE ON  [dbo].[QuotePartyAttributionsCurrent_SEL] TO [IDT_APPS]
GO
