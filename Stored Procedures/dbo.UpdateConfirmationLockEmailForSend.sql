SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
GRANT  EXECUTE  ON [dbo].[UpdateConfirmationLockEmailForSend] TO [IDT-Coralville\iusr_idt]

Author: Phil Shaff
Date Created: January 27th, 2014
Purpose:

	Updates confirmation e-mail to be sent, Locks e-mail for send

Modifications:
Who	     Date	     Note
---------------------------------------------------------------------------------------
pshaff   5/29/2014   Added logic to set when the item was claimed and by who
*/

CREATE PROCEDURE [dbo].[UpdateConfirmationLockEmailForSend]
@ConfirmationId int,
@WorkStationName varchar(64)

AS

DECLARE @WORKSTATION_ID INT

SET @WORKSTATION_ID = (SELECT workstation_id
                       FROM   framework_system.dbo.workstation WITH (nolock)
                       WHERE  name = @WorkStationName)

UPDATE [dbo].confirmation
SET    status = 2,
       claimed_dtm = Getdate(),
       claimed_workstation_id = @WORKSTATION_ID
FROM   sales.dbo.confirmation AS c WITH (nolock)
       LEFT OUTER JOIN sales.dbo.ord AS r WITH (nolock)
                    ON r.ord_id = c.ord_id
WHERE  COALESCE(c.status, 0) = 0
       AND c.retries < 3
       AND (c.active_dtm <= Getdate() OR c.active_dtm IS NULL)
       AND c.con_id = @ConfirmationId
	   AND c.claimed_dtm is null
	   AND c.claimed_workstation_id is null
       AND ( r.curr_stat <> 16
              OR r.ord_id IS NULL OR c.ORD_ID = 0)  


GO
GRANT EXECUTE ON  [dbo].[UpdateConfirmationLockEmailForSend] TO [coliseum]
GRANT EXECUTE ON  [dbo].[UpdateConfirmationLockEmailForSend] TO [IDT-CORALVILLE\IUSR_IDT]
GO
