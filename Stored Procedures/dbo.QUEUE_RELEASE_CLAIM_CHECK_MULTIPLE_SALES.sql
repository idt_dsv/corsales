SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
Author: Neil McEntaggart
Date:   2/27/2004
Notes:  Takes a list of queue ids and releases them. List can be passed as 
        a comma separated list. Intended to be called from a selector for 
        the claim check object.        
Usage:  EXEC dbo.QUEUE_RELEASE_CLAIM_CHECK_MULTIPLE_SALES '10,11,13,14'
 
grant exec on QUEUE_RELEASE_CLAIM_CHECK_MULTIPLE_SALES to IDT_Apps
Revisions:
Date   Who  Notes
-----------------------------------------

*/

CREATE  PROCEDURE [dbo].[QUEUE_RELEASE_CLAIM_CHECK_MULTIPLE_SALES] ( 
   @QUEUE_ITEM_SALES_IDS varchar(8000))
AS 
BEGIN 
  DECLARE @ids table (id int)
  INSERT INTO @ids 
    SELECT Convert(int, StrValue ) 
      FROM dbo.BreakApartStr(@QUEUE_ITEM_SALES_IDS) 
  

  UPDATE QUEUE_ITEM_SALES
     SET CLAIMED_WORKSTATION_ID = null,
         APPLICATION_ID = null
    FROM @ids i
   WHERE QUEUE_ITEM_SALES_ID = i.ID  

  SELECT QUEUE_ITEM_SALES_ID,
         CLAIMED_WORKSTATION_ID as WorkStationClaimedID,         
         APPLICATION_ID         as ApplicationID,      
         CLAIMED_DTM            as ClaimedDateTime,      
         COMPLETED_DTM          as CompleatedDateTime   
    FROM dbo.QUEUE_ITEM_SALES Q (nolock)
         JOIN @ids i ON Q.QUEUE_ITEM_SALES_ID = i.ID 

   
END







GO
GRANT EXECUTE ON  [dbo].[QUEUE_RELEASE_CLAIM_CHECK_MULTIPLE_SALES] TO [IDT_APPS]
GO
