SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
GRANT  EXECUTE  ON [dbo].[UpdateProcessPreferenceExpandedSources] TO [Coliseum]

Author: Phil Shaff
Date Created: July 14th, 2014
Purpose:

	Updates PROCESS_PREFERENCE_EXPANDED sources records

Modifications:
Who	Date	Modification	Note
---------------------------------------------------------------------------------------

*/

CREATE PROCEDURE [dbo].[UpdateProcessPreferenceExpandedSources]
    @TypeId INT
  , @OwnerId INT
  , @SourceTypeId INT
  , @SourceOwnerId INT
AS
    DECLARE @id INT
    SET @id = ( SELECT  PROCESS_PREFERENCE_EXPANDED_SOURCES_ID
                FROM    SALES.DBO.PROCESS_PREFERENCE_EXPANDED_SOURCES WITH ( NOLOCK )
                WHERE   OPTION_OWNER_CID = @TypeId
                        AND OPTION_OWNER_OID = @OwnerId
                        AND SOURCE_OWNER_CID = @SourceTypeId
                        AND SOURCE_OWNER_OID = @SourceOwnerId
              )

    IF @id IS NULL
        OR @id = 0
        BEGIN
            INSERT  INTO SALES.DBO.PROCESS_PREFERENCE_EXPANDED_SOURCES
                    (
                      OPTION_OWNER_CID
                    , OPTION_OWNER_OID
                    , SOURCE_OWNER_CID
                    , SOURCE_OWNER_OID
                    )
            VALUES  (
                      @TypeId
                    , @OwnerId
                    , @SourceTypeId
                    , @SourceOwnerId
                    )
        END
    ELSE
        BEGIN
            UPDATE  SALES.DBO.PROCESS_PREFERENCE_EXPANDED_SOURCES
            SET     OPTION_OWNER_CID = @TypeId
                  , OPTION_OWNER_OID = @OwnerId
                  , SOURCE_OWNER_CID = @SourceTypeId
                  , SOURCE_OWNER_OID = @SourceOwnerId
            WHERE   PROCESS_PREFERENCE_EXPANDED_SOURCES_ID = @id
        END




GO
GRANT EXECUTE ON  [dbo].[UpdateProcessPreferenceExpandedSources] TO [coliseum]
GO
