SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO

/*
Author: ??
date: ??
Notes: Sets invoice numbers and inserts credit line items

Examples: This one inserts data, so no example

Permissions: IDT_APPS

Modifications:
date   Who           Notes
------------------------------------
??/??/??	??		Intial implementation
7/7/2007	Mark S.	Insert unit_price_natural for credits
8/2/2007    Mark S. Use key_field_get_next_x_values instead of old way
4/9/2008	Mark S.	Deal with total order credit scenario for natural currency
2/3/2009    Mark S. Round totals to 2 decimals when comparing order total to credit total for determining total order credit scenario
2/13/2009   Mark S. Cleanup for multiple credits totalling the entire order
3/13/2009   Mark S. Changed round to a truncate when looking at credits
*/

CREATE PROCEDURE [dbo].[InvoicePrepareNumbers]
@BATCH_ID INTEGER
AS
--set nocount on 
  
/* Insert Invoice numbers */
DECLARE @COUNT INTEGER, @INV_NBR INTEGER, @HIGH_VAL INTEGER

SELECT @COUNT = COUNT(BATCH_ORD_ID) FROM INV_HDR WHERE BATCH_ID = @BATCH_ID

EXEC [KEY_FIELD_GET_NEXT_X_VALUES] 'INV_NBR', @COUNT, @INV_NBR OUTPUT, @HIGH_VAL OUTPUT

--EXEC GET_NEXT_X_KEY_VALUES 'INV_NBR', @COUNT, @INV_NBR OUTPUT, @HIGH_VAL OUTPUT

DECLARE @BATCH_ORD_ID INTEGER

DECLARE CUR_INV_HDRS CURSOR FOR
SELECT BATCH_ORD_ID
FROM dbo.INV_HDR e
WHERE BATCH_ID = @BATCH_ID

OPEN CUR_INV_HDRS

FETCH NEXT FROM CUR_INV_HDRS
INTO @BATCH_ORD_ID

WHILE @@FETCH_STATUS = 0 BEGIN
	UPDATE INV_HDR 
	SET INV_NBR = @INV_NBR
	WHERE BATCH_ORD_ID = @BATCH_ORD_ID

	SET @INV_NBR = @INV_NBR + 1

	FETCH NEXT FROM CUR_INV_HDRS
	INTO @BATCH_ORD_ID
END

CLOSE CUR_INV_HDRS
DEALLOCATE CUR_INV_HDRS

/* Insert Credit Line Items */
INSERT INV_LINE_ITEMS
(BATCH_ORD_ID, ACCT_ID, QTY, PROD_DESC, UNIT_PRICE, 
	UNIT_PRICE_NATURAL, Group_ID, PAR_GRP, LINE_ITEM_TYPE, PCApplyID)
SELECT BATCH_ORD_ID, '4002-00-00', 1.0, Product_Description,  (INV_POOL_CREDIT_APPLY.PcApply * -1),
	CASE WHEN ROUND((INV_POOL_CREDIT_APPLY.[PcApply]), 2) = ROUND(INV_HDR.[INV_TOTAL], 2) THEN (INV_HDR.INV_TOTAL_NATURAL * -1)
	ELSE (DBO.CURRENCY_NATURAL_CONVERSION((INV_POOL_CREDIT_APPLY.[PcApply] * -1), INV_HDR.[EXCHANGE_RATE], 0))
	END
	, '2',10000000, 4, PCApplyEntryID
FROM dbo.INV_POOL_CREDIT_APPLY
JOIN dbo.INV_POOL_CREDITS ON INV_POOL_CREDIT_APPLY.PCTransID = INV_POOL_CREDITS.PCTransID
JOIN dbo.INV_HDR ON INV_POOL_CREDIT_APPLY.SALES_ORDER_NBR = INV_HDR.SALES_ORD_NBR
WHERE
INV_POOL_CREDIT_APPLY.SALES_ORDER_NBR IN (SELECT SALES_ORD_NBR FROM INV_HDR WHERE Batch_ID = @BATCH_ID)

/*Check if credits total entire order, but have differential on natural pricing; if so, grab last credit for each order and store in temp table*/
SELECT ih.batch_ord_id, MAX([pcapplyID]) AS pc_apply_id, inv_total_natural 
INTO #tmp
FROM inv_line_items ili (NOLOCK)
JOIN inv_hdr ih (NOLOCK) ON ih.[BATCH_ORD_ID] = ili.batch_ord_id
WHERE ih.[BATCH_ID] = @BATCH_ID
AND ROUND(ih.inv_total, 2, 1) = 0.00
AND ih.[INV_TOTAL_NATURAL] <> 0.00
AND ili.[LINE_ITEM_TYPE] = 4
GROUP BY ih.batch_ord_id, inv_total_natural

/*Update last credit to cleanup natural pricing*/
UPDATE [INV_LINE_ITEMS] set [UNIT_PRICE_NATURAL] = [UNIT_PRICE_NATURAL] - #tmp.inv_total_natural
FROM [#tmp]
JOIN [INV_LINE_ITEMS] ili (NOLOCK) ON ili.[BATCH_ORD_ID] = #tmp.batch_ord_id
WHERE ili.[PCApplyID] = #tmp.[pc_apply_id]

DROP TABLE #tmp

--Set Batch Status
INSERT INV_BATCH_STAT_HIST 
(BATCH_ID, STAT_TYPE_ID)
VALUES (@BATCH_ID,2)

--Set Invoice Status
INSERT INV_STAT_HIST
(BATCH_ORD_ID, STAT_TYPE_ID)
SELECT BATCH_ORD_ID, 3
FROM dbo.INV_HDR WHERE BATCH_ID = @BATCH_ID
GO
GRANT EXECUTE ON  [dbo].[InvoicePrepareNumbers] TO [IDT_APPS]
GO
