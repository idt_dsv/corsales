SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
Author: Eric Borman
Date:   10/14/2008
Notes:  Takes a list of queue ids and manipulates them in the selected way. 
Usage:  EXEC dbo.QueueOrderTransformController '10,11,13,14', 1
 
Permission: grant execute on DBO.QueueOrderTransformController to IDT_Apps 

Revisions:
Date   Who  Notes
-----------------------------------------
*/
Create  PROCEDURE [dbo].[QueueOrderTransformController] ( 
   @QueueItemIds varchar(8000), 
   @ManipulationType INT
   )
AS 
BEGIN 
	DECLARE @ManipulationTypeClaimRelease INT		SET @ManipulationTypeClaimRelease		= 1
	DECLARE @ManipulationTypeClaimInActivate INT	SET @ManipulationTypeClaimInActivate	= 2
	DECLARE @ManipulationTypeClaimComplete INT		SET @ManipulationTypeClaimComplete		= 3

	DECLARE @ids table (id int)
  
	INSERT INTO @ids 
		SELECT Convert(int, StrValue ) 
		FROM dbo.BreakApartStr(@QueueItemIds) 
		
	BEGIN TRANSACTION Manipulate
  
	IF @ManipulationType = @ManipulationTypeClaimRelease
	BEGIN	
		UPDATE QUEUE_ITEM_ORDER_TRANSFORM
			SET CLAIMED_WORKSTATION_ID = NULL,
				APPLICATION_ID = 0
		FROM @ids i
		WHERE QUEUE_ITEM_ID = i.ID  
	END

	IF @ManipulationType = @ManipulationTypeClaimInActivate
	BEGIN	
		UPDATE QUEUE_ITEM_ORDER_TRANSFORM
			SET ACTIVE = 0
		FROM @ids i
		WHERE QUEUE_ITEM_ID = i.ID  
	END
	
	IF @ManipulationType = @ManipulationTypeClaimComplete
	BEGIN	
		UPDATE QUEUE_ITEM_ORDER_TRANSFORM
			SET COMPLETED_DTM = GETDATE()
		FROM @ids i
		WHERE QUEUE_ITEM_ID = i.ID  
	END
	
	IF @@ERROR <> 0
	BEGIN
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION Manipulate
	END
	ELSE
		COMMIT TRANSACTION Manipulate

	SELECT QUEUE_ITEM_ID,
		CLAIMED_WORKSTATION_ID as WorkStationClaimedID,         
        APPLICATION_ID         as ApplicationID,      
        CLAIMED_DTM            as ClaimedDateTime,      
        COMPLETED_DTM          as CompleatedDateTime   
    FROM dbo.QUEUE_ITEM_ORDER_TRANSFORM Q (nolock)
        JOIN @ids i ON Q.QUEUE_ITEM_ID = i.ID    
END
GO
GRANT EXECUTE ON  [dbo].[QueueOrderTransformController] TO [IDT_APPS]
GO
