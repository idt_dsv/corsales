SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
AUTHOR: Jim Borland
DATE: 09/09/08
NOTES:
  Builds table for intranet to return the count of oligos sold by branch and the manufacturing site/department
	assigned to make for current day. Based on MDW sp DAILY_OLIGO_COUNT_BUILD.

EXAMPLE:
  EXEC DailyOligoCountSnapshotBuild
  EXEC DailyOligoCountSnapshotFetch

PERMISSION:
	GRANT EXECUTE ON [DailyOligoCountSnapshotBuild] TO IDT_WEBAPPS, IDT_REPORTER, IDT_DBDEVELOPER

REVISIONS:
09/09/08 JLB	SP restuctured and renamed to support more than one sales site.
*/

CREATE PROCEDURE [dbo].[DailyOligoCountSnapshotBuild] 
AS 
SET NOCOUNT ON

DECLARE @DT DATETIME
SET @DT = GETDATE()

DECLARE @ORDERS INT
DECLARE @BRANCH_NAME VARCHAR(60)

DECLARE @OLIGO_COUNT TABLE (
[SalesBranch] [varchar](60) NULL
,[MfgBranch] [varchar](60) NULL
,[Department] [varchar](60) NULL
,[Oligos] INT NULL
,[Bases] INT NULL
,[AveLen] INT NULL
,[OrderCnt] INT NULL
,[RecordType] INT NULL
,[DateUpdated] DATETIME NULL
)

SET @BRANCH_NAME = (
	SELECT BRANCH_NAME =	CASE WHEN BC.BRANCH_LOCATION_ID = 3 THEN 'Leuven'
								ELSE BL.NAME
							END
	FROM REFERENCE.DBO.BRANCH_CONFIGURATION BC WITH (NOLOCK)
	JOIN REFERENCE.DBO.BRANCH_LOCATION BL ON BL.BRANCH_LOCATION_ID = BC.BRANCH_LOCATION_ID
)


SELECT DEPART_ID, DEPART_NM = CASE WHEN DEPART_NM LIKE 'PROD%' THEN 'Main' ELSE DEPART_NM END
INTO #DEPT
FROM DEPART
WHERE DEPART.DEPART_ID NOT IN (3,6,12,13,14,15,17,18)

--SELECT * FROM #DEPT WITH (NOLOCK)

SELECT	ORD_ID, SALES_ORD_NBR
INTO #ORD
FROM DBO.ORD (NOLOCK) 
WHERE ORD.ORD_DATE BETWEEN CONVERT(DATETIME, CONVERT(VARCHAR(8),@DT,1)) AND CONVERT(DATETIME, CONVERT(VARCHAR(8),@DT+ 1,1))
	AND CURR_STAT <> 16 --CANCELLED
	AND ORD_METH_ID <> 9 --Filter out orders that came from other sites to be made
	
--SELECT * FROM #ORD WITH (NOLOCK)
SELECT @ORDERS = COUNT(*) FROM #ORD

IF  @ORDERS >= 1
BEGIN 
	PRINT @ORDERS
	
	INSERT @OLIGO_COUNT
	SELECT
		SalesBranch = @BRANCH_NAME
		,MfgBranch = CASE WHEN BL.BRANCH_LOCATION_ID = 3 THEN 'Leuven'
						ELSE BL.NAME
					END
		,Department = DEPART.DEPART_NM
		,Oligos = COUNT(*)
		,Bases = SUM(OS.BASES)
		,AveLen = CASE WHEN COUNT(*) = 0 THEN 0 ELSE SUM(OS.BASES)/COUNT(*) END
		,OrderCnt = NULL
		,RecordType = 1
		,DateUpdated = @DT	
	FROM #ORD ORD
	JOIN SALES.DBO.LINE_ITEM_REF_NBR AS LIRN (NOLOCK) ON LIRN.FK_ORD_ID = ORD.ORD_ID 
		AND WORK_SPEC_OBJ_TYPE LIKE 'TOLIGO%'
		AND LIRN.CURR_STAT <> 5
	JOIN #DEPT DEPART ON DEPART.DEPART_ID = LIRN.DEPT_ID
	JOIN SALES.DBO.OLIGO_SPEC OS (NOLOCK) ON OS.REF_ID = LIRN.REF_ID
	JOIN REFERENCE.DBO.BRANCH_LOCATION BL ON BL.BRANCH_LOCATION_ID = LIRN.BRANCH_LOCATION_ID
	GROUP BY	CASE WHEN BL.BRANCH_LOCATION_ID = 3 THEN 'Leuven'
				ELSE BL.NAME
				END
				,DEPART.DEPART_NM

END
ELSE
BEGIN
	PRINT @ORDERS
	
	INSERT @OLIGO_COUNT
	SELECT 
		SalesBranch = @BRANCH_NAME
		,MfgBranch = @BRANCH_NAME
		,Department = DEPART.DEPART_NM
		,Oligos = 0
		,Bases = 0
		,AveLen = 0
		,OrderCnt = NULL
		,RecordType = 1
		,DateUpdated = @DT
	FROM #DEPT DEPART
END

INSERT @OLIGO_COUNT
SELECT 
	SalesBranch = @BRANCH_NAME
	,MfgBranch = @BRANCH_NAME
	,Department = DEPART.DEPART_NM
	,Oligos = 0
	,Bases = 0
	,AveLen = 0
	,OrderCnt = NULL
	,RecordType = 1
	,DateUpdated = @DT
FROM #DEPT DEPART 
WHERE NOT EXISTS(SELECT * FROM @OLIGO_COUNT OC WHERE DEPART.DEPART_NM = OC.DEPARTMENT)

IF NOT EXISTS(SELECT * FROM  @OLIGO_COUNT OC WHERE OC.MfgBranch LIKE '%SAN DIEGO%')
BEGIN 
	INSERT @OLIGO_COUNT
	SELECT 
		SalesBranch = @BRANCH_NAME
		,MfgBranch = 'San Diego'
		,Department = 'Main'
		,Oligos = 0
		,Bases = 0
		,AveLen = 0
		,OrderCnt = NULL
		,RecordType = 1
		,DateUpdated = @DT
END
      

IF NOT EXISTS(SELECT * FROM  @OLIGO_COUNT OC WHERE OC.MfgBranch LIKE '%Leuven%')
BEGIN 
	INSERT @OLIGO_COUNT
	SELECT 
		SalesBranch = @BRANCH_NAME
		,MfgBranch = 'Leuven'
		,Department = 'Main'
		,Oligos = 0
		,Bases = 0
		,AveLen = 0
		,OrderCnt = NULL
		,RecordType = 1
		,DateUpdated = @DT
END

INSERT @OLIGO_COUNT
SELECT 
	SalesBranch = @BRANCH_NAME
	,MfgBranch = ''
	,Department = convert(varchar(60), 'Site Total Orders: ' + convert(varchar,@orders))
	,Oligos = sum(oligos)
	,Bases = sum(bases)
	,AveLen =	CASE
					WHEN SUM(OC.OLIGOS) = 0 THEN 0
					ELSE SUM(OC.BASES)/SUM(OC.OLIGOS)
				END
	,OrderCnt = @orders
	,RecordType = 2
	,DateUpdated = @DT
FROM @OLIGO_COUNT OC

TRUNCATE TABLE SALES.DBO.DailyOligoCountSnapshot

INSERT SALES.DBO.DailyOligoCountSnapshot
SELECT *
FROM @OLIGO_COUNT OC
ORDER BY SalesBranch, RecordType, MfgBranch, Department
    
DROP TABLE #ORD
DROP TABLE #DEPT
GO
GRANT EXECUTE ON  [dbo].[DailyOligoCountSnapshotBuild] TO [IDT_DbDeveloper]
GRANT EXECUTE ON  [dbo].[DailyOligoCountSnapshotBuild] TO [IDT_Reporter]
GRANT EXECUTE ON  [dbo].[DailyOligoCountSnapshotBuild] TO [IDT_WebApps]
GO
