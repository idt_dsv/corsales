SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*        
Author: Ed Sobolewski        
Date: 08/01/12        
Notes:  GET_PARTY_SHIPPING_CONFIRMATION_ADDRESSES returns a list of confirmation addresses for a given list of sales order numbers.       
     
Example: EXEC [GET_PARTY_SHIPPING_CONFIRMATION_ADDRESSES] @SalesOrderList = '6951951,7675919,176745,7672587'        
     
Permissions: Grant Execute On dbo.GET_PARTY_SHIPPING_CONFIRMATION_ADDRESSES to IDT_Apps        
     
Revisions:        
Date        Who         Notes        
-----------------------------------------     
08/01/12    EdS     Initial Release    
10/23/12    DBP     No longer returns distributors or OEMs
02/27/13	DBP		Checks PARTY_CONFIRM_ADDRESS for Orgs, Accounts and Customers  
*/  
CREATE PROCEDURE [dbo].[GET_PARTY_SHIPPING_CONFIRMATION_ADDRESSES]    
 @SALESORDERLIST VARCHAR(8000)    
AS    
    
--DECLARE @SALESORDERLIST VARCHAR(8000)    
--SET     @SalesOrderList = '2180181'         
  
SELECT  Cast( STRVALUE AS INT ) AS SALES_ORD_NBR  
INTO    #SALESORDERNBRS  
FROM    DBO.Breakapartstr( @SALESORDERLIST )  
  
SELECT  O.SALES_ORD_NBR , 
        O.ENDUSER_ID , 
		PCA.CONFIRM_ADDR , 
        O.PAY_AUTH_NBR 
FROM    #SALESORDERNBRS AS SON  
JOIN    SALES.DBO.ORD AS O WITH (NOLOCK) ON SON.SALES_ORD_NBR = O.SALES_ORD_NBR    
JOIN    SALES.DBO.ORG AS ORG WITH (NOLOCK) ON O.ORG_ID = ORG.PARTY_ID
JOIN    SALES.DBO.PARTY_CONFIRM_ADDR AS PCA WITH (NOLOCK) ON PCA.PARTY_ID IN ( O.ORG_ID , O.ACC_ID, O.ENDUSER_ID)    
WHERE   ORG.CLASS_TYPE_ID NOT IN ( 3, 5, 6, 7, 9 ) -- This removes Order Moved orders, Distributors and OEMs 
AND		PCA.CONFIRM_SHIPMENT = 1 -- We only want to send a confirmation if the org wants a confirmation 

DROP TABLE #SALESORDERNBRS
GO
GRANT EXECUTE ON  [dbo].[GET_PARTY_SHIPPING_CONFIRMATION_ADDRESSES] TO [IDT_APPS]
GO
