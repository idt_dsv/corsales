SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
  Author: RWHITAKER                           
  Date: 7/23/07                                 
  Notes: Recreate shipment confirmations based on sales order number   
       
  Modifications:
  
 */

CREATE   PROCEDURE [dbo].[CONFIRMATION_SHIPMENT_RESUBMIT] 
(@SALES_ORD_NUMBER int = 0)
 
 AS
  IF @SALES_ORD_NUMBER > 0
  BEGIN	 
    set nocount on 
	DECLARE @USER VARCHAR(25)
	SET @USER = USER
	UPDATE confirmation
	SET USER_NM = @USER, STATUS = 0, HOLD_ID = 0, RETRIES = 0, SENT_DTM = NULL, ERR_MSG = NULL
	WHERE SALES_ORD_NBR = @SALES_ORD_NUMBER and TYPE_ID = 'T' and (STATUS <> 0) -- MAY WANT TO NOT RECONFIRM IF ITEM WAITING?
  END


GO
GRANT EXECUTE ON  [dbo].[CONFIRMATION_SHIPMENT_RESUBMIT] TO [IDT_APPS]
GO
