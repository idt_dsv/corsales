SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: MDW
Date: 10/15/05
Notes:
 Created as an alias to PUBLIC_SQL_OBJECT_SEARCH to make developers happy

example:

grant execute on Where_used to idt_reporter
*/

CREATE procedure [dbo].[WHERE_USED]
(
@str varchar(128)
)
as

set @str = '%' + @str + '%'
exec reference.dbo.PUBLIC_SQL_OBJECT_SEARCH @str
GO
GRANT EXECUTE ON  [dbo].[WHERE_USED] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[WHERE_USED] TO [IDT_DATA_CLEANUP]
GO
