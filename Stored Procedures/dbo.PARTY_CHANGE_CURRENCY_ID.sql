SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE [dbo].[PARTY_CHANGE_CURRENCY_ID]
@PARTY_ID INT,
@CURRENCY_ID INT
AS
/*
Author: Bviering
Date: 4/12/03
Notes:
  Updates party's currency_id to org's currency_id
  --NOTE ORG is the master Currency_id for all accounts and persons in that org

Revisions:
4/12/03 MDW tuned update party query for org

*/

DECLARE @PARTY_SUB_TYPE AS VARCHAR(5)
begin
  set nocount on
  
  SELECT @PARTY_SUB_TYPE = PARTY_SUB_TYPE
  FROM PARTY
  WHERE PARTY_ID = @PARTY_ID
  
  IF @PARTY_SUB_TYPE = 'ORG' BEGIN
  	 --SET CURRENCY_ID FOR ACCOUNTS AND ENDUSERS
  	UPDATE PARTY
  	SET CURRENCY_ID = @CURRENCY_ID
    from dbo.party
  	 join(
         -- Accounts
         SELECT PARTY_ID 
         FROM dbo.ACCOUNT (nolock)
         WHERE ORG_ID = @PARTY_ID
  	   union all
         -- People
         SELECT PARTY_ID 
         FROM dbo.PERSON (nolock)
         WHERE org_ID = @PARTY_ID
       ) AccPlusPeople on AccPlusPeople.party_id = PARTY.PARTY_id
  
  -- 	WHERE PARTY_ID IN (SELECT PARTY_ID FROM ACCOUNT WHERE ORG_ID = @PARTY_ID)
  -- 	OR PARTY_ID IN (SELECT PARTY_ID FROM PERSON WHERE ACC_ID IN (SELECT PARTY_ID FROM ACCOUNT WHERE ORG_ID = @PARTY_ID))
  	 IF (@@ERROR <> 0) GOTO ERROR_RETURN
  END
  IF @PARTY_SUB_TYPE = 'ACCT' BEGIN
  	 --SET CURRENCY_ID FOR ENDUSERS
  	UPDATE PARTY
  	SET CURRENCY_ID = @CURRENCY_ID
    FROM dbo.PARTY
  	  join dbo.PERSON on PERSON.Party_id = PARTY.party_id and person.ACC_ID = @PARTY_ID
    IF (@@ERROR <> 0) GOTO ERROR_RETURN
  END

  set nocount off
  RETURN 0
end

ERROR_RETURN:
  set nocount off
  RETURN @@ERROR



GO
GRANT EXECUTE ON  [dbo].[PARTY_CHANGE_CURRENCY_ID] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[PARTY_CHANGE_CURRENCY_ID] TO [IDT_Reporter]
GO
