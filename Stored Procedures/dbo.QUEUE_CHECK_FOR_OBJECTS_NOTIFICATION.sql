SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
/*
Author: Neil McEntaggart
Date:   8/23/2005
Notes:  Takes a list of objects and returns a record if they are queued. List can be passed as 
        a comma separated list. Intended to be called from a selector.        
Usage:  EXEC dbo.QUEUE_CHECK_FOR_OBJECTS_MANUFACTURING 1,2,30,'1,2,3',0
 

Revisions:
Date   Who  Notes
-----------------------------------------

*/
CREATE PROCEDURE [dbo].[QUEUE_CHECK_FOR_OBJECTS_NOTIFICATION] (
   @QUEUE_KEY_TYPE int,
   @QUEUE_KEY_ID   int,
   @BF_BOBJECT_CID int,
   @QUEUE_ITEM_IDS varchar(8000),
   @ACTIVEONLY bit 
)
AS 
BEGIN 
  SET NOCOUNT ON
  DECLARE @ids table(id int) 

  INSERT INTO @ids 
    SELECT Convert(int, StrValue ) 
      FROM dbo.BreakApartStr(@QUEUE_ITEM_IDS) 
  SET NOCOUNT OFF
  DECLARE @QUEUE_KEY_TY_EVENT int SET @QUEUE_KEY_TY_EVENT = 1
  DECLARE @QUEUE_KEY_TY_GROUP int SET @QUEUE_KEY_TY_GROUP = 2
  IF @QUEUE_KEY_TYPE = @QUEUE_KEY_TY_EVENT   
  BEGIN  
 
    SELECT distinct Itm.BF_BOBJECT_OID 
       FROM QUEUE_ITEM_NOTIFICATION Itm 
       JOIN @ids ids ON ids.id = Itm.BF_BOBJECT_OID AND Itm.BF_BOBJECT_CID = @BF_BOBJECT_CID 
      WHERE (Itm.QUEUE_DEFINITION_ID = @QUEUE_KEY_ID AND ACTIVE = 1) AND
            ( (@ACTIVEONLY = 0)  or (Itm.ACTIVE = 1 AND COMPLETED_DTM is null))     
   END
   ELSE IF @QUEUE_KEY_TYPE = @QUEUE_KEY_TY_GROUP
     SELECT distinct Itm.BF_BOBJECT_OID 
       FROM QUEUE_ITEM_NOTIFICATION Itm
       JOIN @ids ids ON ids.id = Itm.BF_BOBJECT_OID AND Itm.BF_BOBJECT_CID = @BF_BOBJECT_CID 
       JOIN ACTIVE_QUEUE_DEFINITION_GROUP_MBRSHIP_VW m ON m.QUEUE_DEFINITION_ID = Itm.QUEUE_DEFINITION_ID 
      WHERE (m.QUEUE_DEFINITION_GROUP_ID = @QUEUE_KEY_ID AND ACTIVE = 1 ) 
             AND ( (@ACTIVEONLY = 0)  or (Itm.ACTIVE = 1 AND COMPLETED_DTM is null))
END

GO
GRANT EXECUTE ON  [dbo].[QUEUE_CHECK_FOR_OBJECTS_NOTIFICATION] TO [IDT_APPS]
GO
