SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
GRANT  EXECUTE  ON [dbo].[InsertConfirmationEmail] TO [IDT-Coralville\iusr_idt]
GRANT  EXECUTE  ON [dbo].[InsertConfirmationEmail] TO coliseum

Author: Phil Shaff
Date Created: January 27th, 2014
Purpose:

	Inserts new e-mail record to be sent

Modifications:
Who       Date        Note
---------------------------------------------------------------------------------------
pshaff    05/29/2014  Adding in logic for active time
*/

CREATE PROCEDURE [dbo].[InsertConfirmationEmail]
@OrderId int = null,
@SalesOrderNumber int = null,
@TypeId varchar(10),
@Destination varchar(100),
@Subject varchar(50),
@UserName varchar(100),
@Body text = '',
@OligoCardId int = null,
@NotificationInfoId int = NULL,
@HoldId INT = 0,
@ActiveDate datetime = NULL

AS

INSERT INTO sales.dbo.confirmation
            (ord_id,
             type_id,
             con_date,
             status,
             retries,
             dept_id,
             hold_id,
             dest,
             subject,
             user_nm,
             body,
             sales_ord_nbr,
             oligo_card_id,
             notification_info_id,
             active_dtm)
VALUES      (@OrderId,
             @TypeId,
             Getdate(),
             0,
             0,
             0,
             @HoldId,
             @Destination,
             @Subject,
             @UserName,
             @Body,
             @SalesOrderNumber,
             @OligoCardId,
             @NotificationInfoId,
             COALESCE(@ActiveDate, Getdate()))  

GO
GRANT EXECUTE ON  [dbo].[InsertConfirmationEmail] TO [coliseum]
GRANT EXECUTE ON  [dbo].[InsertConfirmationEmail] TO [IDT-CORALVILLE\IUSR_IDT]
GO
