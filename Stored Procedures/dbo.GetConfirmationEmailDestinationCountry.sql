SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
--GRANT  EXECUTE  ON [dbo].[GetConfirmationEmailDestinationCountry] TO [IDT-Coralville\iusr_idt]
--GRANT  EXECUTE  ON [dbo].[GetConfirmationEmailDestinationCountry] TO [Coliseum]
--GRANT  EXECUTE  ON [dbo].[GetConfirmationEmailDestinationCountry] TO [AppSalesSin]

Author: Phil Shaff
Date Created: Feb 26th, 2014
Purpose:

	Retrieves information necessary to setup Order Confirmation E-mail, this instance returns if an order is for a given country/vendor

Modifications:
Who	Date	Modification	Note
---------------------------------------------------------------------------------------

*/

CREATE PROCEDURE [dbo].[GetConfirmationEmailDestinationCountry]
@Country varchar(20),
@OrderId int

AS

if (@Country = 'Japan')
Begin
	SELECT 
		CASE 
			WHEN EXISTS(
				select o.ord_id
                from sales.dbo.ord AS o with (nolock)
				left outer join sales.dbo.party_rel as PR with (nolock) on pr.child_party_id = o.org_id
                    and pr.party_rel_type = 8 -- Parent party is the distributor for the child party
                    and pr.par_party_id = 192656 -- MBL - exclusive distributor for Japan
                where o.ord_id = @OrderId and (o.org_id = 192656 -- MBL - exclusive distributor for Japan
                    or pr.CHILD_PARTY_ID is not null))
        THEN 'YES'

        ELSE 'NO'
    END AS 'JapanOrder'
END
ELSE IF (@Country = 'Israel')
BEGIN
	SELECT 
		CASE 
			WHEN EXISTS(
				SELECT o.ord_id
                FROM sales.dbo.ORD AS o with (nolock)
                INNER JOIN sales.dbo.PARTY_REL as PR with (nolock) ON o.ORG_ID = PR.CHILD_PARTY_ID
					AND PR.PARTY_REL_TYPE = 8 -- Parent party is the distributor for the child party
					AND PR.PAR_PARTY_ID = 240592 -- Hy Laboratories, Ltd.
                WHERE o.ORD_ID = @OrderId)
        THEN 'YES'

        ELSE 'NO'
    END AS 'IsraelOrder'
END
ELSE
BEGIN
	SELECT 'Unknown Country' AS UnknownOrder
END

GO
GRANT EXECUTE ON  [dbo].[GetConfirmationEmailDestinationCountry] TO [coliseum]
GRANT EXECUTE ON  [dbo].[GetConfirmationEmailDestinationCountry] TO [IDT-CORALVILLE\IUSR_IDT]
GO
