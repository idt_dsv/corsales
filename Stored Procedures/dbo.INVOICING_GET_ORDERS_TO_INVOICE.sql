SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: Mark A Schebel
Date Created: April 3, 2006
Purpose:
  Obtain a list of orders available for invoicing
grant exec on INVOICING_GET_ORDERS_TO_INVOICE to IDT_Apps
Modifications:
Who     Date      Modification  Note
--------------------------------------------------------------
MAS     4/3/06  Mark Schebel	Created
MAS		7/25/06 Mark Schebel	Added HOTplate Product ID
MAS	    11/27/06 Mark Schebel	Added logic to account for invoicing on Sundays
MAS		11/27/06 Mark Schebel	Added logic to delay invoicing of Pfizer orders
MAS		1/8/07	Mark Schebel	Changed to get ship date from ord_stat_hist rather than shipment
MAS		1/9/07	Mark Schebel	Fix error
MAS		7/9/07  Mark Schebel	Take holidays into account
MAS		8/2/07  Mark Schebel	Added ord_meth_id to return query
MAS		9/2/07  Mark Schebel	Fix date logic
MAS		12/6/07 Mark Schebel	Adding another org to delay.  If we get 3 orgs, I'll move it to lookup from a table.
MAS		7/15/08 Mark Schebel	Adding BVBA Large Scale orders to be delayed.
MAS		10/3/08 Mark Schebel	Removing BVBA check on dept_id 4 as we also want CCM delayed in Inc 
MAS		5/26/2010 Mark Schebel Add 384 well plates
JJD		12/20/2011 Joe DeWaard	Updated plate query to pull product type and switched to WITH (NOLOCKS)
*/
CREATE   PROCEDURE [dbo].[INVOICING_GET_ORDERS_TO_INVOICE]
	as
set nocount on

	DECLARE @currentdate DATETIME
	DECLARE @offset int
	DECLARE @dayofweek INT
	DECLARE @offsetdate datetime
	
	--Goal for the determining offset is to find the day that is two business days prior to current date.
	--So, if it's Saturday, Sunday, or Monday, then find Thursday's date.  

	--This section takes weekends into account
	SELECT @currentdate = GETDATE()
	SELECT @dayofweek = datepart(dw, @currentdate)
	IF @dayofweek = 2
		SET @offset = 4
	else if @dayofweek = 1
		SET @offset = 3
	else
		SET @offset = 2

	--This section takes holidays into account. I.e., if day after offset date is a holiday, then move offset back a day.
	SELECT @offsetdate = @currentdate - @offset
	IF EXISTS(SELECT * FROM CALENDAR WHERE [DATE] = CONVERT(VARCHAR(10), @OFFSETDATE + 1,1) AND [MFG_DAY] = 0)
		SET @offset = @offset + 1
	

	select  
		 DISTINCT ord.ord_id
	into #PLATE_EXCLUDE
	from ord WITH (NOLOCK)
		join ord_line_item oli WITH (NOLOCK) on oli.ord_id = ord.ord_id
		JOIN product p WITH (NOLOCK) ON oli.OLI_PROD_ID = p.PROD_ID             
		join ord_stat_hist osh WITH (NOLOCK) on osh.ORD_ID = ord.ord_id and osh.ORD_STAT_TYPE_ID = 14 --connect to whether or not item shipped.
	where ord.ord_date > '3-1-2005 00:00:00'
	and ord.curr_stat = 14 --somewhat redundant, but speeds up query
	AND p.OBJ_TYPE = 9 --TPlateLineItem from PROD_OBJ_TYPE_LKP
	and Convert(varchar(10), osh.ORD_STAT_DT, 1) > getdate() - @offset --Exclude items that shipped previous business day

	select 
		distinct ord.ord_id
	into #PF_EXCLUDE
	from ord WITH (NOLOCK)
             join ord_line_item oli WITH (NOLOCK) on oli.ord_id = ord.ord_id
	join ord_stat_hist osh WITH (NOLOCK) on osh.ORD_ID = ord.ord_id and osh.ORD_STAT_TYPE_ID = 14 --connect to whether or not item shipped.
	where ord.ord_date > '3-1-2005 00:00:00'
	and ord.curr_stat = 14
	and oli.oli_prod_id = 2169
	and Convert(varchar(10), osh.ORD_STAT_DT, 1) > getdate() - @offset --Exclude items that shipped previous business day
	
	SELECT 
		DISTINCT ord.[ORD_ID]
	INTO #LARGE_SCALE_EXCLUDE
	FROM ord WITH (NOLOCK)
		JOIN line_item_ref_nbr lirn WITH (NOLOCK) ON lirn.fk_ord_id = ord.ord_id 
	JOIN ord_stat_hist osh WITH (NOLOCK) ON osh.[ORD_ID] = ord.ord_id AND osh.ORD_STAT_TYPE_ID = 14 --join if item is shipped
	WHERE ord.ord_date > '3-1-2005 00:00:00'
	AND ord.curr_stat = 14
	AND lirn.[DEPT_ID] = 4
	AND Convert(varchar(10), osh.ORD_STAT_DT, 1) > getdate() - @offset --Exclude items that shipped previous business day

	select
		distinct ord.ord_id
	into #ORG_EXCLUDE
	from ord WITH (NOLOCK)
	join ord_stat_hist osh WITH (NOLOCK) on osh.ORD_ID = ord.ord_id and osh.ORD_STAT_TYPE_ID = 14 --connect to whether or not item shipped.
	where ord.ord_date > '3-1-2005 00:00:00'
	and ord.curr_stat = 14
	and ord.org_id IN (358861, 318145) --ORG # 13144 Pfizer-UK ORG # 12678 Qiagen GmbH
	and Convert(varchar(10), osh.ORD_STAT_DT, 1) > getdate() - @offset --Exclude items that shipped previous business day
	
	SELECT
		DISTINCT ORD.[ORD_ID], ORD.[ORD_METH_ID]
	FROM  	[ORD]
	LEFT JOIN #PLATE_EXCLUDE PE ON PE.ORD_ID = ORD.ORD_ID
	LEFT JOIN #PF_EXCLUDE PFE ON PFE.ORD_ID = ORD.ORD_ID
	LEFT JOIN #ORG_EXCLUDE PZE ON PZE.ORD_ID = ORD.ORD_ID
	LEFT JOIN #LARGE_SCALE_EXCLUDE LSE ON LSE.ORD_ID = ORD.ORD_ID
	WHERE ORD.ORD_DATE > '3-1-2005 00:00:00'
	AND ORD.CURR_STAT = 14
	AND PE.ORD_ID IS NULL
	AND PFE.ORD_ID IS NULL
	AND PZE.ORD_ID IS NULL
	AND LSE.ORD_ID IS NULL

	DROP TABLE #PLATE_EXCLUDE
	DROP TABLE #PF_EXCLUDE
	DROP TABLE #ORG_EXCLUDE
	DROP TABLE #LARGE_SCALE_EXCLUDE

	set nocount off




GO
GRANT EXECUTE ON  [dbo].[INVOICING_GET_ORDERS_TO_INVOICE] TO [IDT_APPS]
GO
