SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[PARTY_SOURCE_TYPE_CHG_ACCOUNT](@ACC_NBR INT,@TYPE_ID INT)
/*
PARTY_SOURCE_TYPE_CHG_ACCOUNT
Author: NLaikhter
Date: 6/14/04
Description: Update Party_Source_Type_ID based on the input ACC_Nbr
Modifications:

*/
AS
BEGIN
                      
UPDATE PARTY
SET PARTY_SOURCE_TYPE_ID = @TYPE_ID
WHERE PARTY_ID = (SELECT PARTY_ID
                     FROM dbo.ACCOUNT WHERE ACC_NBR = @ACC_NBR)

RETURN @@ERROR

END
GO
GRANT EXECUTE ON  [dbo].[PARTY_SOURCE_TYPE_CHG_ACCOUNT] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[PARTY_SOURCE_TYPE_CHG_ACCOUNT] TO [IDT_GRANT_INFO_USER]
GO
