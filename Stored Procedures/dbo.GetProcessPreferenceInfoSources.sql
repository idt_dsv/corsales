SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
GRANT  EXECUTE  ON [dbo].[GetProcessPreferenceInfoSources] TO [Coliseum]
GRANT  EXECUTE  ON [dbo].[GetProcessPreferenceInfoSources] TO [AppSalesSin]

Author: Phil Shaff
Date Created: July 14th, 2014
Purpose:

	Retrieves information necessary for process preferece object sources

Modifications:
Who	Date	Modification	Note
---------------------------------------------------------------------------------------

*/

CREATE PROCEDURE [dbo].[GetProcessPreferenceInfoSources]
@OwnerId INT,
@TypeId INT

AS

SELECT [process_preference_expanded_sources_id],
       [option_owner_cid],
       [option_owner_oid],
       [source_owner_cid],
       [source_owner_oid]
FROM   [Sales].[dbo].[process_preference_expanded_sources] WITH (nolock)
WHERE  option_owner_oid = @OwnerId
       AND option_owner_cid = @TypeId 
ORDER BY process_preference_expanded_sources_id


GO
GRANT EXECUTE ON  [dbo].[GetProcessPreferenceInfoSources] TO [coliseum]
GO
