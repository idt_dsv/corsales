SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO




CREATE  proc [dbo].[LINE_ITEM_SEL_PACK]
/**********************************************************************************************************************************************
LINE_ITEM_SEL_PACK
 AUTHOR: Ben Viering
 CREATED ON: 3/23/99
 DESCRIPTION/ALGORITHM: 
 Retrieve the line items for a packing list and subtotal in hierarchial order by. 
 Algorithm is adapted from Expanding hierarchy from "Inside SQL Server"
 
 exec [LINE_ITEM_SEL_PACK] 345555

grant execute on [LINE_ITEM_SEL_PACK] to idt_apps

 REVISIONS:
 06/19/02  MDW  changed final select statement to 1) Changed OLIS(ord_line_item spec) reference to oligo_spec 2) order by ref_id instead of seq
 07/22/02  NL changed select statement to use Line_item_ref_nbr.LIRN_PROD_ID instead of Prod_line_item.
 11/1/07   MDW removed Line_ITEM_ADJ table from last query. It was a left join, table has 0 records, and not referenced in query
**********************************************************************************************************************************************/
(@ORD_ID INT) as
set nocount on 


/* Retrieve the line items in hierarchial order
 store the line items in a temp table
 then join the temp table with other tables to retrieve 
 the line item list & attributes */
DECLARE @level int, @current int, @line char(20), @count int, @PACK_LIST BIT, @OLD_LINE_ITEM INT,
                  @SUB_TOTAL MONEY, @REF_ID int, @rowcount int
CREATE TABLE #stack (item int, lev int, ON_PACK_LIST bit, SUB_TOTAL MONEY, ref_id int)

CREATE TABLE #results(LINE_ITEM_ID int, LEV int, SEQ int, ON_PACK_LIST BIT, SUB_TOTAL money, ref_id int)

-- get first level of hierarchy 
INSERT INTO #stack 
SELECT ORD_LINE_ITEM.LINE_ITEM_ID, 1, ON_PACK_LIST, ORD_LINE_ITEM.UNIT_PRICE * ORD_LINE_ITEM.QUANTITY AS SUBTOTAL, LINE_ITEM_REF_NBR.REF_ID
FROM dbo.ORD_LINE_ITEM
JOIN dbo.LINE_ITEM_REF_NBR ON ORD_LINE_ITEM.LINE_ITEM_ID = LINE_ITEM_REF_NBR.LINE_ITEM_ID
WHERE ORD_LINE_ITEM.ORD_ID = @ORD_ID AND
(LEV = 1)
ORDER BY REF_ID DESC

SET @level = 1
SET @count = 0
WHILE @level > 0
BEGIN
    IF EXISTS (SELECT * FROM #stack WHERE lev = @level)
    BEGIN
            SELECT @current = item, @PACK_LIST = ON_PACK_LIST, @SUB_TOTAL = SUB_TOTAL, @REF_ID = ref_id
            FROM #stack
            WHERE lev = @level
            
            IF @PACK_LIST = 1 
            BEGIN
              INSERT #results (LINE_ITEM_ID, LEV, SEQ, ON_PACK_LIST, SUB_TOTAL, REF_ID)
              VALUES (@current, @level, @count, @PACK_LIST, @SUB_TOTAL, @REF_ID)
              SET @count = @count + 1
              SET @OLD_LINE_ITEM = @current
            END
            
            IF @OLD_LINE_ITEM <> @current
            BEGIN
              UPDATE #results
                SET SUB_TOTAL = SUB_TOTAL + @SUB_TOTAL
              WHERE LINE_ITEM_ID = @OLD_LINE_ITEM
            END

            DELETE FROM #stack
            WHERE lev = @level
                AND item = @current

            INSERT #stack
/*                SELECT LINE_ITEM_ID, @level + 1, ORD_LINE_ITEM.ON_PACK_LIST, ORD_LINE_ITEM.UNIT_PRICE * ORD_LINE_ITEM.QUANTITY AS SUBTOTAL
                FROM ORD_LINE_ITEM 
                WHERE PAR_LINE_ITEM = @current
                ORDER BY POS DESC
*/
                SELECT ord_line_item.LINE_ITEM_ID, @level + 1, ORD_LINE_ITEM.ON_PACK_LIST, 
                  ORD_LINE_ITEM.UNIT_PRICE * ORD_LINE_ITEM.QUANTITY AS SUBTOTAL, 
                  isnull(lirn.ref_id, (select lirn2.ref_id from line_item_ref_nbr lirn2 where lirn2.line_item_id = ord_line_item.par_line_item))
                FROM dbo.ORD_LINE_ITEM 
                  left join dbo.line_item_ref_nbr lirn on lirn.line_item_id = ord_line_item.line_item_id
                WHERE PAR_LINE_ITEM = @current
                ORDER BY POS DESC

             SELECT @rowcount =  @@ROWCOUNT
             IF @rowcount  > 0
                SELECT @level = @level + 1
    END ELSE
    BEGIN
      SELECT @level = @level - 1
    END
END -- WHILE
SELECT -- #results.seq,
    #results.LINE_ITEM_ID,
    ORD_LINE_ITEM.ORD_ID,
    ORD_LINE_ITEM.QUOTE_ID,
    ORD_LINE_ITEM.LINE_ITEM_SUB_TYPE,
    ORD_LINE_ITEM.QUANTITY, 
    ORD_LINE_ITEM.UNIT_PRICE, 
    ORD_LINE_ITEM.ON_INVOICE, 
    ORD_LINE_ITEM.ON_PACK_LIST, 
    ORD_LINE_ITEM.IS_SHIPPED, 
    ORD_LINE_ITEM.EST_SHIP_DT,  
    ORD_LINE_ITEM.LEV,
    ORD_LINE_ITEM.POS,
    ORD_LINE_ITEM.PAR_LINE_ITEM,
    ORD_LINE_ITEM.LINE_ITEM_DESC,
    GUAR_SHIP_DT,
    LINE_ITEM_REF_NBR.REF_ID, 
    LINE_ITEM_REF_NBR.CURR_STAT,
    LINE_ITEM_REF_NBR.DEPT_ID,
    convert(varchar(255),OLIGO_SPEC.SEQ_DESC) SEQ_DESC, 
    ORD_LINE_ITEM.OLI_PROD_ID as PROD_ID,
    #results.SUB_TOTAL
FROM #results 
    JOIN dbo.ORD_LINE_ITEM ON #results.LINE_ITEM_ID = ORD_LINE_ITEM.LINE_ITEM_ID     
    LEFT JOIN dbo.LINE_ITEM_REF_NBR ON ORD_LINE_ITEM.LINE_ITEM_ID = LINE_ITEM_REF_NBR.LINE_ITEM_ID
    LEFT JOIN dbo.PRODUCT ON ORD_LINE_ITEM.OLI_PROD_ID = PRODUCT.PROD_ID
    LEFT JOIN dbo.OLIGO_SPEC ON LINE_ITEM_REF_NBR.REF_ID = OLIGO_SPEC.REF_ID
--    LEFT JOIN dbo.LINE_ITEM_ADJ ON ORD_LINE_ITEM.LINE_ITEM_ID = LINE_ITEM_ADJ.LINE_ITEM_ID
ORDER BY #results.ref_id, #results.seq

set nocount off


GO
GRANT EXECUTE ON  [dbo].[LINE_ITEM_SEL_PACK] TO [IDT_APPS]
GO
