SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		David Priebe
-- Create date: 1/28/2010
-- Description:	Loads sales order numbers for orders that 
-- have been moved to the specified manufacturing location
-- but have not been marked as shipped.
--
-- Permissions: IDT_Apps - Execute.
-- REVISIONS:
-- DATE		WHO				NOTES
-- 3/16/10	David Priebe	Add date range to improve performance.
-- 4/05/10	David Priebe	Restricted range to 1 year to improve performance.
-- =============================================
CREATE PROCEDURE [dbo].[OMMovedOrdersNotShipped] 
	-- Add the parameters for the stored procedure here
	@BranchLocation INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	CREATE TABLE #OrdIds ( OrdId INT NOT NULL, SalesOrdNbr INT NOT NULL )

	INSERT INTO #OrdIds
		SELECT DISTINCT o.[ORD_ID] AS OrdId, o.[SALES_ORD_NBR] AS SalesOrdNbr
		FROM [ORD] o (NOLOCK)
		WHERE o.[CURR_STAT] = 7 AND o.[ORD_DATE] > DATEADD(yy,-1,GETDATE())

	SELECT DISTINCT id.SalesOrdNbr 
	FROM #OrdIds id
	JOIN [LINE_ITEM_REF_NBR] lirn (NOLOCK) ON id.[OrdId] = lirn.[FK_ORD_ID]
	WHERE lirn.[BRANCH_LOCATION_ID] = @BranchLocation AND lirn.[CURR_STAT] IN (1,2)

	DROP TABLE #OrdIds
END




GO
GRANT EXECUTE ON  [dbo].[OMMovedOrdersNotShipped] TO [IDT_APPS]
GO
