SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/**********************************************
 SALES_TERR_INSERT
 Author:NLaikhter
 Date:10/24/03
 Description: Gives the ability to create new 
              record in SALES_TERR table 
 How to cal stored procedure: all the values should be provided
 
 begin tran
 Exec dbo.SALES_TERR_INSERT -100 ,'@SALES_TERR_NM','@SALES_TERR_DESC',null,1,'mdewaard,jlockhart'
 select * from salesTerritory where TerritoryId = -100 and territoryType = 'territory'
 select * from salesTerritoryManager stm join employee e on e.hr_employee_id = stm.hr_employee_id where SalesTerritoryID = (select SalesTerritoryID from salesTerritory where TerritoryId = -100)
 rollback
 
 
 grant execute on [SALES_TERR_INSERT] to idt_apps, idt_reporter
 
 Revision:
 12/7/07 MDW updated to write to SalesTerritory and SalesTeritoryManager tables; verified fixes

**********************************************/

CREATE   PROCEDURE [dbo].[SALES_TERR_INSERT](
@SALES_TERR_ID INT
,@SALES_TERR_NM varchar(50)
,@SALES_TERR_DESC varchar(1000)
,@SALES_TERR_ORD smallint = NULL
,@SALES_REGION_ID INT
,@LOGIN varchar(20)
) 
AS
BEGIN
DECLARE @err INT, @RowCount INT, @HR_EMPLOYEE_ID_List VARCHAR(1000), @RegionSalesTerritoryID INT, @ErrorMsg nvarchar(128), @NewSalesTerritoryID int

SELECT @RowCount = -1, @HR_EMPLOYEE_ID_List = NULL, @RegionSalesTerritoryID= NULL , @ErrorMsg = '', @HR_EMPLOYEE_ID_List =''


SELECT @HR_EMPLOYEE_ID_List = CAST(e.HR_EMPLOYEE_ID AS VARCHAR(10)) + ',' + @HR_EMPLOYEE_ID_List
FROM reference.dbo.employee e
  JOIN dbo.breakApartStr(@LOGIN) nm ON e.network_login_nm = nm.StrValue 
WHERE e.active = 1

IF @HR_EMPLOYEE_ID_List IS NULL
BEGIN
  SET @ErrorMsg = @ErrorMsg + ' Network Login "' + @LOGIN + '" does not exist;'
  goto ErrorHandler
END 

SELECT @RegionSalesTerritoryID = SalesTerritoryID FROM sales.dbo.salesTerritory WHERE TerritoryType = 'Region' AND TerritoryId = @SALES_REGION_ID
IF @RegionSalesTerritoryID IS NULL
BEGIN
  SET @ErrorMsg = @ErrorMsg + ' Sales Region ID "' + @SALES_REGION_ID + '" does not exist;'
  goto ErrorHandler
END 

IF EXISTS (SELECT * FROM sales.dbo.salesTerritory WHERE TerritoryType = 'Territory' AND TerritoryId = @SALES_TERR_ID)
BEGIN
  SET @ErrorMsg = @ErrorMsg + ' Sales Territory ID "' + @SALES_TERR_ID+'" does not exist;'
  goto ErrorHandler
END 
  BEGIN transaction
    INSERT INTO [SALES].[dbo].[SalesTerritory]
           ([TerritoryId], [Name], [Description], [ParentSalesTerritoryId], [TerritoryType])
     VALUES
           (@SALES_TERR_ID
           ,@SALES_TERR_NM
           ,@SALES_TERR_DESC
           ,@RegionSalesTerritoryID
           ,'Territory'
           )         
  SELECT @err = @@ERROR, @rowCount = @@ROWCOUNT, @NewSalesTerritoryID = @@identity 
  IF @err <> 0 OR @rowCount = 0 
  BEGIN
    SET @ErrorMsg = @ErrorMsg + ' Error inserting into SalesTerritory;'
    goto ErrorHandler
  END 

  INSERT SalesTerritoryManager (SalesTerritoryID, HR_EMPLOYEE_ID)
  SELECT @NewSalesTerritoryID,  CAST(StrValue AS INT)
  FROM dbo.breakApartStr(@HR_EMPLOYEE_ID_List)
  
  SELECT @err = @@ERROR, @rowCount = @@ROWCOUNT, @NewSalesTerritoryID = @@identity 
  IF @err <> 0 OR @rowCount = 0
  BEGIN
    SET @ErrorMsg = @ErrorMsg + ' Error inserting into SalesTerritoryManager;'
    goto ErrorHandler
  END 
  
  commit
  PRINT 'Data was inserted into SALES_TERR'
  RETURN 0

ErrorHandler:
 SET @ErrorMsg = N'No records updated - ' + @ErrorMsg
 IF @@TRANCOUNT > 0 
   rollback
 raiserror (@ErrorMsg,16,1)
 RETURN -1
END 



GO
GRANT EXECUTE ON  [dbo].[SALES_TERR_INSERT] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[SALES_TERR_INSERT] TO [IDT_Reporter]
GO
