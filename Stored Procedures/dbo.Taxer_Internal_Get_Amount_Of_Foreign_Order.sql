SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: Chris A. Sailor
Date Created: January 3, 2005
Purpose:

  This procedure is used to retrieve the exchange rate and
the foreign currency order total for a specified order, assuming
that the order is being invoiced in non-US amounts.

Permissions:  
Grant Execute On dbo.Taxer_Internal_Get_Amount_Of_Foreign_Order to IDT_WebApps  
Grant Execute On dbo.Taxer_Internal_Get_Amount_Of_Foreign_Order to IDT_Apps  

Modifications:
Who     Date      Modification  Note
--------------------------------------------------------------
CAS     12/28/04  Chris Sailor  Created
CAS     02/21/07  Chris Sailor  Moved to the Sales database
*/
Create Procedure [dbo].[Taxer_Internal_Get_Amount_Of_Foreign_Order]
	@InvoiceNumber Int
as
	set nocount on

	select	NAT = dbo.GET_INVOICE_TOTAL_NATURAL_CURRENCY ( INV_NBR ),
		EXCHANGE_RATE
	from	INV_HDR IH
	where	INV_NBR = @InvoiceNumber AND
		CURRENCY_ID <> 1

	set nocount off



GO
GRANT EXECUTE ON  [dbo].[Taxer_Internal_Get_Amount_Of_Foreign_Order] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[Taxer_Internal_Get_Amount_Of_Foreign_Order] TO [IDT_WebApps]
GO
