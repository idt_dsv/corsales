SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
/*
Author: ??
Date: ??
Notes
  inserts enduser into Party and person per MSCHEBEL

Modfifications:
SG  05/13/04  Updated @BillAcctID from int to Varchar(10)

*/

CREATE     PROCEDURE [dbo].[PARTY_ENDUSER_INS]
  @ObjID int,
  @AccountID int,
  @BillAcctID varchar(10),
  @BillConfig int,
  @BillCycleID int,
  @CreateDate datetime,
  @CreateUser varchar(25),
  @CreditRateID int,
  @CurrStatType int,
  @DefCarrierID int,
  @EmplCust int,
  @EmployeeID int,
  @EndUserNumber int,
  @FirstName varchar(50),
  @LastName varchar(50),
  @MiddleName varchar(25),
  @ModifyDate datetime,
  @ModifyUser varchar(25),
  @OrgID int,
  @PartySourceTypeID int,
  @PartySubType char(5),
  @PersonTitle varchar(10),
  @PrevLastName varchar(50),
  @PriceGroupID int,
  @SalesTax smallmoney,
  @SalesTerrID int,
  @Sex char(1),
  @ShipGroupID int,
  @Suffix varchar(10),
  @CurrencyID int,
  @EmailNotification bit
AS
BEGIN
DECLARE  @RowCount int, @Err int

  BEGIN TRANSACTION 
  INSERT INTO [PARTY]
  (
    [PARTY_ID], 
    [SHIP_GRP_ID], 
    [PRICE_GRP_ID], 
    [SALES_TERR_ID], 
    [BILL_CYCLE_ID], 
    [DEF_CARRIER_ID], 
    [CURR_STAT_TYPE], 
    [CRDT_RATE_ID], 
    [BILL_ACCT_ID], 
    [PARTY_SOURCE_TYPE_ID], 
    [SALES_TAX], 
    [PARTY_SUB_TYPE], 
    [CREATE_DT], 
    [CREATE_USER], 
    [MODIFY_DT], 
    [MODIFY_USER],
    [CURRENCY_ID],
    [SEND_PROMO_EMAIL]
  )
  VALUES
  (
    @ObjID, 
    @ShipGroupID, 
    @PriceGroupID, 
    @SalesTerrID, 
    @BillCycleID, 
    @DefCarrierID,
    @CurrStatType,
    @CreditRateID,
    @BillAcctID, 
    @PartySourceTypeID, 
    @SalesTax, 
    @PartySubType,
    @CreateDate, 
    @CreateUser,
    @ModifyDate,
    @ModifyUser,
    @CurrencyID,
    @EmailNotification
  )

  SELECT @RowCount = @@ROWCOUNT, @Err = @@ERROR
  IF (@Err <> 0) OR (@RowCount <> 1)
    GOTO errorhandler

  INSERT INTO [PERSON]
  (
    [PARTY_ID], 
    [ENDUSER_NBR], 
    [LAST_NM], 
    [FIRST_NM], 
    [MID_NM], 
    [PER_TITLE], 
    [SUFFIX], 
    [SEX], 
    [PREV_LAST_NM],  
    [EMPL_CUST], 
    [ACC_ID], 
    [ORG_ID], 
    [BILL_CONFIG], 
    [HR_EMPLOYEE_ID]
  )
  VALUES
  (
    @ObjID, 
    @EndUserNumber, 
    @LastName, 
    @FirstName, 
    @MiddleName, 
    @PersonTitle, 
    @Suffix, 
    @Sex, 
    @PrevLastName, 
    @EmplCust, 
    @AccountID, 
    @OrgID, 
    @BillConfig, 
    @EmployeeID
  )  

 SELECT @RowCount = @@ROWCOUNT, @Err = @@ERROR
  IF (@Err <> 0) OR (@RowCount <> 1)
    GOTO errorhandler

  COMMIT TRANSACTION 
  RETURN(0)

errorhandler:
  ROLLBACK TRANSACTION
  RAISERROR ('PARTY ENDUSER INSERT ERROR (ID %d)', 16, 1, @ObjID)
  RETURN(-1)
END
GO
GRANT EXECUTE ON  [dbo].[PARTY_ENDUSER_INS] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[PARTY_ENDUSER_INS] TO [IDT_GRANT_INFO_USER]
GO
