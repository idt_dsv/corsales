SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: Chris A. Sailor
Date Created: April 9, 2003
Purpose:

  This procedure is intended to retrieve a list of
Party IDs for those customers who are not to have
their orders automatically processed.

Modifications:
Who     Date      Modification  Note
--------------------------------------------------------------
CAS     04/09/03  Chris Sailor  Initial Implementation
CAS     04/15/03  Chris Sailor  Fixed Date Logic
*/
Create Procedure [dbo].[AutoEntry_Get_Restricted_Party_IDs]
	@Effective_Date DateTime
As
	set nocount on

	Select		AEO.Party_ID
	From		dbo.Auto_Entry_Override AEO
	Where		( AEO.From_Dt <= @Effective_Date ) and
			(( AEO.Through_Dt Is Null ) or ( AEO.Through_Dt > @Effective_Date ))
	Order By	AEO.Party_ID

	set nocount off



GO
GRANT EXECUTE ON  [dbo].[AutoEntry_Get_Restricted_Party_IDs] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[AutoEntry_Get_Restricted_Party_IDs] TO [IDT_Reporter]
GO
