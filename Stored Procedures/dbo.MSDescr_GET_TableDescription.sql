SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO

CREATE PROCEDURE [dbo].[MSDescr_GET_TableDescription]
 (@TableName nvarchar(128))
AS

declare @SQLStr nvarchar(4000)
set @SQLStr = 'select value description ' + 
  'from ::fn_listextendedproperty(N''MS_Description'', N''user'', N''dbo'', N''table'', N''' + @TableName +  ''', NULL, NULL) '


execute sp_sqlexec @SQLStr

GO
GRANT EXECUTE ON  [dbo].[MSDescr_GET_TableDescription] TO [IDT_APPS]
GO
