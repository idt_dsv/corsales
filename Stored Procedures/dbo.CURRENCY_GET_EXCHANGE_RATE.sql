SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
CREATE PROCEDURE [dbo].[CURRENCY_GET_EXCHANGE_RATE] 
 (@FromCurrency_id int,            -- Currency id of Currency to convert from (Usually USD)
  @ToCurrency_id int,              -- Currency id of Currency to convert to   (usually CDN)
  @effectiveDate datetime,         -- Date of order or of quote
  @Exchange_rate_id int output,    -- The id the rate is pulled from the Currency_Exchange_rate table (auditing purposes)
  @exchange_Rate float output      -- The actual rate to be used. (multiply to the from currency amount ro get the thru currency amount)
 )
 /*
 Author: MDeWaard
 DATE: 4/5/2003
 Notes: Created to help implement Multi-currency. 
   Should be used as a standard method to get the effective dated Exchange_rate for a currency conversion
 
 Revisions:
 Date   Who    Notes
 7-13-2005 Mark Schebel	 Made query inclusive of thru_dt
 09/02/07	JLB Stub SP moved to Reference
 -----------------------------------------------------
 
 */
 as
   Declare @RC int
 
 begin
   set nocount ON
   
EXECUTE @RC = [Reference].[dbo].[CURRENCY_GET_EXCHANGE_RATE] 
   @FromCurrency_id
  ,@ToCurrency_id
  ,@effectiveDate
  ,@Exchange_rate_id OUTPUT
  ,@exchange_Rate OUTPUT
    
 end
 
GO
GRANT EXECUTE ON  [dbo].[CURRENCY_GET_EXCHANGE_RATE] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[CURRENCY_GET_EXCHANGE_RATE] TO [IDT_Reporter]
GO
