
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*  
Author: Chris A. Sailor  
Date Created: August 1, 2005  
Purpose:  
  
This procedure is intended to retrieve a batch of sequences  
that need to be run through MFold and have values set for them.  
  
grant execute on DBO.MFold_ByOrder_Get_Seq_Data_To_Fold to IDT_Apps  
  
Modifications:  
Who     Date      Modification  Note  
--------------------------------------------------------------  
CAS     08/01/05  Chris Sailor  Created   
CAS     03/20/07  Chris Sailor  Moved out of Manufacturing database  
DBP	10/01/14  Drew Pittman	Added (NOLOCK) table hints and reformatted.
*/  
CREATE PROCEDURE [dbo].[MFOLD_BYORDER_GET_SEQ_DATA_TO_FOLD]  
(  
	@Ord_ID INT,  
	@Record_Count INT  
)  
AS  
BEGIN  
	SET NOCOUNT ON  

	SET ROWCOUNT @RECORD_COUNT  
  
	SELECT  os.REF_ID, SEQ = CONVERT ( TEXT, os.SEQ )  
	FROM	dbo.LINE_ITEM_REF_NBR AS lirn WITH (NOLOCK)  
	JOIN	dbo.OLIGO_SPEC AS os WITH (NOLOCK) ON lirn.REF_ID = os.REF_ID  
	LEFT JOIN dbo.MFOLD_CALCULATION AS mfc WITH (NOLOCK) ON OS.REF_ID = mfc.REF_ID  
	WHERE	lirn.FK_ORD_ID = @ORD_ID AND 
		COALESCE(mfc.UPDATED_DTM, os.OS_UPD_DTM) <= os.OS_UPD_DTM
		
	SET ROWCOUNT 0  

	SET NOCOUNT OFF  
END
GO

GRANT EXECUTE ON  [dbo].[MFOLD_BYORDER_GET_SEQ_DATA_TO_FOLD] TO [IDT_APPS]
GO
