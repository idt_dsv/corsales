SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
--GRANT  EXECUTE  ON [dbo].[UpdateConfirmationToggleHold] TO [IDT-Coralville\iusr_idt]

Author: Phil Shaff
Date Created: January 27th, 2014
Purpose:

	Updates confirmation e-mail to be sent, sets or unsets hold for e-mail

Modifications:
Who	Date	Modification	Note
---------------------------------------------------------------------------------------

*/

CREATE PROCEDURE [dbo].[UpdateConfirmationToggleHold]
@ConfirmationId int

AS

	UPDATE [dbo].CONFIRMATION 
		SET HOLD_ID = 1 - COALESCE(HOLD_ID,0) 
	WHERE COALESCE(STATUS,0) = 0 AND CON_ID = @ConfirmationId

GO
GRANT EXECUTE ON  [dbo].[UpdateConfirmationToggleHold] TO [coliseum]
GRANT EXECUTE ON  [dbo].[UpdateConfirmationToggleHold] TO [IDT-CORALVILLE\IUSR_IDT]
GO
