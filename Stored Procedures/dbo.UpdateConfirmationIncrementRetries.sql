SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
GRANT  EXECUTE  ON [dbo].[UpdateConfirmationIncrementRetries] TO [IDT-Coralville\iusr_idt]
GRANT  EXECUTE  ON [dbo].[UpdateConfirmationIncrementRetries] TO [coliseum]

Author: Phil Shaff
Date Created: January 27th, 2014
Purpose:

	Updates confirmation e-mail to be sent, sets e-mail for retry and logs error message

Modifications:
Who       Date        Note
---------------------------------------------------------------------------------------
pshaff    05/29/2014  Adding in logic for claiming work
*/

CREATE PROCEDURE [dbo].[UpdateConfirmationIncrementRetries]
@ConfirmationId int,
@ErrorMessage varchar(50) = null,
@WorkStationName varchar(64)

AS

DECLARE @WORKSTATION_ID INT

SET @WORKSTATION_ID = (SELECT workstation_id
                       FROM   framework_system.dbo.workstation WITH (nolock)
                       WHERE  name = @WorkStationName)

UPDATE sales.dbo.confirmation
SET    retries = COALESCE(retries, 0) + 1,
       status = 0,
       err_msg = @ErrorMessage,
       claimed_dtm = NULL,
       claimed_workstation_id = NULL
WHERE  COALESCE(status, 0) = 2
       AND con_id = @ConfirmationId  
	   AND claimed_dtm <= GETDATE()
	   AND CLAIMED_WORKSTATION_ID = @WORKSTATION_ID

GO
GRANT EXECUTE ON  [dbo].[UpdateConfirmationIncrementRetries] TO [coliseum]
GRANT EXECUTE ON  [dbo].[UpdateConfirmationIncrementRetries] TO [IDT-CORALVILLE\IUSR_IDT]
GO
