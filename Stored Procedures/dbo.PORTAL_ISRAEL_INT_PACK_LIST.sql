SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
SP Name:  PORTAL_ISRAEL_INT_PACK_LIST
 
Date: 5-31-05
Author:  Jen Lockhart
 
Description:  IDT-Israel Portal Shipping Report - Returns packing list results for IDT-Israel

exec dbo.PORTAL_ISRAEL_INT_PACK_LIST NULL

Permissions:  
GRANT EXECUTE ON PORTAL_ISRAEL_INT_PACK_LIST TO [IDT_APPS], [IDT_Reporter]

Modifications:
Who: When: Description
JL   6/21/05	Modified criteria to look for ship_via rather than country
JL   2/16/06	Added UDF ID for clarification on report
JL   3/13/06    Changed to use new shipping tables
JL   3/15/06	Added distinct
CS	 5/18/07	Made into SP from CR_ISRAEL_SHIPPING_REPORT
CS	 8/20/08	Converted to Syntezza report, using cons group, renamed from RPT_ISRAEL_SHIPPING_REPORT
------------------------------------------------------------------------------
*/
create PROCEDURE [dbo].[PORTAL_ISRAEL_INT_PACK_LIST]
 @reportDate datetime

as

--declare @ReportDate datetime
--set @ReportDate = NULL

if @ReportDate is null
set @ReportDate = getdate()

select distinct 
  udf.udf_field_value, 
  city_nm,  
  shipment.sales_ord_nbr, 
  ship_dt = convert (varchar (8), manifest_dt, 1)
from production.dbo.shipping_shipment_vw shipment (nolock)
  join production.dbo.shipping_addr_vw addr (nolock) on addr.shipping_addr_id = shipment.shipping_addr_id
  left join sales.dbo.ord ord on ord.sales_ord_nbr = shipment.sales_ord_nbr
  left join sales.dbo.ord_udf udf on udf.ord_id = ord.ord_id
		and udf.udf_id = 5
where manifest_dt between production.dbo.GET_LAST_SHIP_DATE(@ReportDate) 
	and production.dbo.GET_REPORT_SHIP_DATE(@ReportDate)
  and shipment.shipping_consolidation_group_id = 100013 --BVBA Syntezza
  and shipment_status = 1
order by  ship_dt desc


GO
GRANT EXECUTE ON  [dbo].[PORTAL_ISRAEL_INT_PACK_LIST] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[PORTAL_ISRAEL_INT_PACK_LIST] TO [IDT_Reporter]
GO
