SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********************************************
Author: Jamie Hall
Date: 6/9/08
Notes: 

EXEC dbo.RPT_SHIP_COURIER_RPT '09/21/09', 'Chicago'

grant exec on RPT_SHIP_COURIER_RPT to IDT_Reporter

--------ALSO NEED TO ALTER THE REPORT TO NOT SHOW A SO FOR UIC AND SHOW PACKAGE ID INSTEAD----------

Modifications:
Date    	Who    		  Notes
6/11/08     Jamie Hall    Converted to report that can be run for any courier.
9/16/09		Jen			  Excluded Abbott and only included parent box for UI-Chicago
**********************************************/

CREATE PROC [dbo].[RPT_SHIP_COURIER_RPT]
(@ReportDate datetime, @Courier varchar(30))

as

if @ReportDate is NULL
  set @ReportDate = getdate()


BEGIN
  SET NOCOUNT ON

select distinct
  ShipVia = ship.cms_ship_via,
  Enduser = ship.customer_name,
  Institution = org.org_nm, 
  Address1 = addr.addr1,
  Address2 = addr.addr2,
  Address3 = addr.addr3,
  City = addr.city_nm,
  State = addr.state_cd,
  Zip =  addr.post_cd,
  Phone = ship.customer_phone_number,
  SONbr = ship.sales_ord_nbr,
  ShipDt = convert(varchar(8), ship.manifest_dt, 1),
  PackageId = ship.shipping_package_id,
  TrackingNbr = ship.tracking_number
from production.dbo.shipping_package_vw ship
  join production.dbo.shipping_addr_vw addr on ship.shipping_addr_id = addr.shipping_addr_id
  join sales.dbo.org on ship.org_id = org.party_id
where ship.cms_ship_via like ('%' + @Courier + '%')
  and ship.manifest_dt >= convert(datetime, floor(convert(float, @ReportDate)))
  and ship.manifest_dt < convert(datetime, floor(convert(float, dateadd(dd, 1, @ReportDate))))
  and ship.shipping_consolidation_group_id not in (18,48) --Omit Abbott &amp; UI-Chicago
UNION
select distinct
  ShipVia = ship.cms_ship_via,
  Enduser = ship.customer_name,
  Institution = addr.addr1, 
  Address1 = addr.addr1,
  Address2 = addr.addr2,
  Address3 = addr.addr3,
  City = addr.city_nm,
  State = addr.state_cd,
  Zip =  addr.post_cd,
  Phone = ship.customer_phone_number,
  SONbr = 0,--ship.sales_ord_nbr,
  ShipDt = convert(varchar(8), ship.manifest_dt, 1),
  PackageId = ship.shipping_package_id,
  TrackingNbr = ship.tracking_number
from production.dbo.shipping_package_vw ship
  join production.dbo.shipping_addr_vw addr on ship.shipping_addr_id = addr.shipping_addr_id
where ship.cms_ship_via like ('%' + @Courier + '%')
  and ship.shipping_consolidation_group_id = 48 --UI-Chicago
  and ship.manifest_dt >= convert(datetime, floor(convert(float, @ReportDate)))
  and ship.manifest_dt < convert(datetime, floor(convert(float, dateadd(dd, 1, @ReportDate))))

SET NOCOUNT OFF
END
GO
GRANT EXECUTE ON  [dbo].[RPT_SHIP_COURIER_RPT] TO [IDT_Reporter]
GO
