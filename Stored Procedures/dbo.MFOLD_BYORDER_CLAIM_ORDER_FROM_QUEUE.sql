SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: Chris A. Sailor
Date Created: August 1, 2005
Purpose:

  This procedure is intended to claim a single order from the
MFold queue.

grant execute on DBO.MFold_ByOrder_Claim_Order_From_Queue to IDT_Apps

Modifications:
Who     Date      Modification  Note
--------------------------------------------------------------
CAS     08/01/05  Created
CAS     03/14/06  Updated to make use of updated load_queue_mfold function
                  and to streamline the code.
CAS     09/19/06  Updated to respect the Active_DTM data.
CAS     09/22/06  Updated to try to prevent two machines from claiming the same queue item.
CAS     03/20/07  Chris Sailor  Moved out of Manufacturing database
*/
CREATE Procedure [dbo].[MFOLD_BYORDER_CLAIM_ORDER_FROM_QUEUE]
	(
	@Workstation_ID Int
	)
As
BEGIN 
	set nocount on

	DECLARE @MFOLD_QUEUE_DEFINITION_ID	int	SET @MFOLD_QUEUE_DEFINITION_ID	= 13
	DECLARE @APPLICATION_ID_MFOLD		int	SET @APPLICATION_ID_MFOLD	= -3
	-- DECLARE @VIEW_TYPE_MYCLAIMED		int	SET @VIEW_TYPE_MYCLAIMED	= 3
	DECLARE @VIEW_TYPE_UNCLAIMED_MYCLAIMED	int	SET @VIEW_TYPE_UNCLAIMED_MYCLAIMED = 8
	DECLARE @QUEUE_KEY_TY_EVENT		int	SET @QUEUE_KEY_TY_EVENT		= 1
	DECLARE @TORDER_SPEC_CID		int	SET @TORDER_SPEC_CID		= 63

	DECLARE @IDs TABLE ( QUEUE_ITEM_ID int )
 
	BEGIN TRANSACTION

	INSERT	@IDs 
		SELECT top 1	QIM.QUEUE_ITEM_ID
		FROM			DBO.QUEUE_ITEM_MFOLD QIM
		JOIN			dbo.load_queue_mfold
						(
						@QUEUE_KEY_TY_EVENT,	
						@MFOLD_QUEUE_DEFINITION_ID,
						@TORDER_SPEC_CID,
						@VIEW_TYPE_UNCLAIMED_MYCLAIMED, -- @VIEW_TYPE_MYCLAIMED,
						@WORKSTATION_ID,
						@APPLICATION_ID_MFOLD) Q	On	QIM.Queue_Item_ID = Q.Queue_Item_ID
		Where			QIM.Active_DTM <= GetDate ( )
		ORDER BY		QIM.Claimed_DTM Desc,
						QIM.Priority,
						QIM.Active_DTM
  
	UPDATE	DBO.QUEUE_ITEM_MFOLD
	SET		CLAIMED_WORKSTATION_ID	= @workstation_id,
			APPLICATION_ID			= @APPLICATION_ID_MFOLD,
			CLAIMED_DTM				= GetDate()      
	FROM	@IDs IDs
	WHERE	( IDs.QUEUE_ITEM_ID = QUEUE_ITEM_MFOLD.QUEUE_ITEM_ID ) and
			(( Claimed_Workstation_ID is Null ) or ( Claimed_Workstation_ID = @Workstation_ID ))
  
	IF @@ERROR <> 0 
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION
  
	SELECT		Q.Queue_Item_ID,
				Ord_ID = Q.BF_BObject_OID
	FROM		DBO.QUEUE_ITEM_MFOLD Q
	JOIN		@IDs IDs		ON	Q.Queue_Item_ID = IDs.Queue_Item_ID
	Where		Q.Claimed_Workstation_ID = @Workstation_ID
	ORDER BY	Q.Claimed_DTM desc,
				Q.Priority,
				Q.Active_DTM

	set nocount off
END


GO
GRANT EXECUTE ON  [dbo].[MFOLD_BYORDER_CLAIM_ORDER_FROM_QUEUE] TO [IDT_APPS]
GO
