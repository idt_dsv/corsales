SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: MDEWAARD
Date: 1/29/03
Notes:
 -- Used to cancel items where the order was cancelled, but the items weren't then Places modified orders into MFOLD queue

Revisions
Date    Who    Notes
----------------------------------------------------
6/7/03  MDW    insert,update,deleted for performance
8/9/03  MDW    Removed reference to Adapta
8/17/03 MDW    Changed to reference ORD_STAT_HIST and REF_NBR_STAT_HIST to improve perfomance
4/7/05  MDW    Changed to improve performance - using RNSH- had to add index on date
4/14/05 MDW    Changed time to check for 12 hours instead of 2.5 hours
8/16/07 MDW    - @MinCancelDtm to parameter rather than a local var
               - subselect on invoice items to perform better
               - Added insert into mfold queue
               - renamed from FIX_ORD_REF_MFG_STATUS and moved to SALES
               - moved section to terminate mfg_item_ids that were cancelled to mfg db.

*/

CREATE        procedure [dbo].[FixRefIDsForCancelledOrder]
(
@MinCancelDtm datetime
)
as
set nocount on

--create tables needed for procedure
-- all cancelled orders
create table #osh (ord_id int) 
-- All ref_id's to be cancelled
create table #ref_nbr_stat_hist (Ord_id int, REF_ID int, STAT_ID int, STAT_DT datetime, REASON varchar(50), USER_NM varchar(35)) 
-- Orders to be reprocessed by mfold
create table #OrdUpd ([ID] int, Ord_id int) 

-- declare vars for this section
declare @DT datetime, @RowsAffected int, @err INT  --, @MinCancelDtm datetime
set @dt = getdate()
set @RowsAffected = 1
select @err = 0  -- , @MinCancelDtm = dateadd(hour,-72,getdate())

-- get recently cancelled orders
insert #osh 
select distinct osh.ord_id 
from ord_stat_hist osh (nolock)
where osh.ORD_STAT_DT > @MinCancelDtm
    and osh.ord_stat_type_id = 16

select @RowsAffected = @@Rowcount, @err = @@error
if @RowsAffected = 0 --no cancelled orders short circuit procedure
  return 0

waitfor delay '000:01:00' -- Wait 1 minute in case cancelling order is in error and will be uncancelled

-- insert ref_id's to update into temp table #ref_nbr_stat_hist
while @RowsAffected > 0 and @err = 0
begin
  insert #ref_nbr_stat_hist (ord_id, REF_ID, STAT_ID, STAT_DT, REASON, USER_NM)
  select top 100 osh.Ord_id, lirn.ref_id , 5, @dt, 'Cancelled order - Auto cleanup', 'AUTO'
  from #osh osh (nolock) 
   JOIN ord (nolock) on ord.ord_id = osh.ord_id
     and ord.curr_Stat = 16  -- 16 = cancelled -- link back in to make sure it is still cancelled
   join line_item_ref_nbr lirn (nolock) on lirn.fk_ord_id = ord.ord_id
     and lirn.curr_Stat <> 5 -- 5 = cancelled
  where -- not invoiced
    not exists (select *
      from dbo.inv_hdr
        join dbo.inv_line_items on inv_line_items.batch_ord_id = inv_hdr.batch_ord_id
      WHERE inv_hdr.sales_ord_nbr = ord.sales_ord_nbr
        AND inv_line_items.ref_nbr = lirn.ref_id
      )
  select @RowsAffected = @@Rowcount, @err = @@error
  if @err <> 0 
  begin
    RAISERROR ('Error in cancelling line_item_ref_nbr ', 16, 1) 
    return -1
  end
end

if not exists (select * from #ref_nbr_stat_hist) --short circuit if no ref_ids to cancel
  return 0

--- Add information to mfold queue so is processed by order converter
  declare @Priority int -- priority for queue
  -- get keys for each order that had an updated order
  DECLARE @RC int, @KEY_NM varchar(128), @NBR_OF_KEYS int, @NEXT_LOW_KEY_VAL int, @NEXT_HIGH_KEY_VAL int
  select @KEY_NM = 'queue_item_id', @NBR_OF_KEYS = @RowsAffected
  EXEC @RC = [dbo].[KEY_FIELD_GET_NEXT_X_VALUES] @KEY_NM, @NBR_OF_KEYS, @NEXT_LOW_KEY_VAL OUTPUT , @NEXT_HIGH_KEY_VAL OUTPUT 
  if @RC <> 0
    begin
      raiserror ( 'error getting key: %i',  @RC ,16,1)
      return @RC
    end

  SET @Priority = 1 -- use for resubmitting old orders that do not need to be manufacturesd or shipped
   
  insert #OrdUpd (ord_id)
  select ord_id
  from #ref_nbr_stat_hist

  set @NEXT_LOW_KEY_VAL = @NEXT_LOW_KEY_VAL -1 --set to proper value for update statement
  update #OrdUpd set @NEXT_LOW_KEY_VAL = [id] = @NEXT_LOW_KEY_VAL +1
  select * from #OrdUpd

--actually make the updates to the DB
begin transaction

  begin try
    insert ref_nbr_stat_hist (REF_ID, STAT_ID, STAT_DT, REASON, USER_NM)
    select  REF_ID, STAT_ID, STAT_DT, REASON, USER_NM 
    from #ref_nbr_stat_hist
    select @RowsAffected = @@Rowcount, @err = @@error


    INSERT QUEUE_ITEM_MFOLD (
      QUEUE_ITEM_ID, ENTERED_DTM, ACTIVE_DTM, BF_BOBJECT_CID, BF_BOBJECT_OID, 
      QUEUE_DEFINITION_ID, CLAIMED_WORKSTATION_ID, APPLICATION_ID, 
      CLAIMED_DTM, COMPLETED_DTM, PRIORITY, ACTIVE 
      )
    Select [ID], getDate(), getDate(), 63,  ord_id,
      13,  null, 0, 
      null, null, @Priority, 1
    from #OrdUpd

  end try
  begin catch
    print 'Error saving cancelled items'
    if @@trancount > 0
      rollback
  end catch

if @@trancount > 0
  commit

--cleanup
drop table #osh
drop table #ref_nbr_stat_hist
drop table #ordupd
set nocount off

GO
