
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
--     Author                      	Ben Viering                 
--     Creation Date	10-8-01                             
--     Comments     
                                
REVISIONS:
---------------------------------------------------------
6-10-03 Chris Sailor added new yield guarantee fields
6-24-03 Chris Sailor added new purity guarantee fields
8-01-03 Chris Sailor removed commented out code dealing with ord_line_item_spec
4-23-04 Mark Schebel Added Yield Guarantee Override flag
1-30-07 Mark Schebel Upped size of sequence fields
9/29/14		Cory Blythe	Changed the select, added nolock hint

*/


CREATE     PROCEDURE [dbo].[Oligo_Spec_Upd]
    (
      @REF_ID INT
    , @Bases INT
    , @Length INT
    , @Purif_ID INT
    , @ExtCoef FLOAT
    , @GC FLOAT
    , @MWHydrated FLOAT
    , @MWAnhydrous FLOAT
    , @OD_Obligation FLOAT
    , @OD_Max_Obligation INT
    , @OD_Min_Synth_Req FLOAT
    , @OD_Max_Synth_Req INT
    , @Purity_Guar_Pct FLOAT
    , @Purity_Guar_Explanation_ID INT
    , @Purity_Guar_Points INT
    , @TM FLOAT
    , @UnitSize FLOAT
    , @OldSequence VARCHAR(300)
    , @SequenceDesc2 VARCHAR(254)
    , @SequenceDesc VARCHAR(254)
    , @NewSequence VARCHAR(MAX)
    , @YieldGuarType CHAR(3)
    , @YieldGuarPoints INTEGER
    , @YieldGuarOverride INTEGER
    )
AS
    SET NOCOUNT ON
    BEGIN

        DECLARE @LINE_ITEM_ID INT

        SET @LINE_ITEM_ID = ( SELECT    LINE_ITEM_ID
                              FROM      LINE_ITEM_REF_NBR WITH (NOLOCK)
                              WHERE     REF_ID = @REF_ID
                            )
        IF @@ERROR <> 0
            GOTO HandleError

        UPDATE  [dbo].[OLIGO_SPEC]
        SET     [BASES] = @Bases
              , [LENGTH] = @Length
              , [PURIF_ID] = @Purif_ID
              , [EXT_COEFF] = @ExtCoef
              , [GC] = @GC
              , [MW_HYDRATED] = @MWHydrated
              , [MW_ANHYDROUS] = @MWAnhydrous
              , [OD_OBLIGATION] = @OD_Obligation
              , [OD_MAX_OBLIGATION] = @OD_Max_Obligation
              , [OD_MIN_SYNTH_REQ] = @OD_Min_Synth_Req
              , [OD_MAX_SYNTH_REQ] = @OD_Max_Synth_Req
              , [PURITY_GUAR_PCT] = @Purity_Guar_Pct
              , [PURITY_GUAR_EXPLANATION_ID] = @Purity_Guar_Explanation_ID
              , [PURITY_GUAR_POINTS] = @Purity_Guar_Points
              , [TM] = @TM
              , [UNIT_SIZE] = @UnitSize
              , [SEQ] = @NewSequence
              , [SEQ_DESC2] = @SequenceDesc2
              , [SEQ_DESC] = @SequenceDesc
              , [YIELD_GUAR_TYPE] = @YieldGuarType
              , [YIELD_GUAR_POINTS] = @YieldGuarPoints
              , [YIELD_GUAR_OVERRIDE] = @YieldGuarOverride
              , [OS_UPD_DTM] = GETDATE()
        WHERE   [REF_ID] = @REF_ID
        IF @@ERROR <> 0
            GOTO HandleError

        SET NOCOUNT OFF
        RETURN 0
/* Enter the code to handle exception conditions
 following this line */

        HandleError:
        SET NOCOUNT OFF
        RETURN @@ERROR
    END


GO

GRANT EXECUTE ON  [dbo].[Oligo_Spec_Upd] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[Oligo_Spec_Upd] TO [IDT_Reporter]
GO
