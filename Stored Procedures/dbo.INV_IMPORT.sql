SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
Author: Bviering
date: ??
Notes: Inserts values into inv_hdr for invoicing process

Examples: This one inserts data, so no example
exec INV_IMPORT 6609, 1067073

Permissions: IDT_APPS
grant exec on INV_IMPORT to IDT_Apps
Modifications:
date   Who           Notes
------------------------------------
7/16/03 Bschafbuch   Updates for AR integration into Epicor -- Added Inv_dt field
7/24/03 MDW          Changed call to get date w/o time, removed Audit trail sp
8/9/03  MDW          Changed Adapta reference to Epicor linked server
7/20/04 Bschafbuch   Added ZIP4 Fields
8/9/04 	Mark Schebel Joined to bill acct on ord; pull bill address according to ord inv_addr_type		
8/25/04 Bschafbuch   Used Billing Account Name to populate BILL_NM field
9/28/04 MDW          Updated to fix problem with linked server. ADDED TEMP TABLE
5/3/07  MAS			     Updated to include amounts in natural currency
8/18/07 Bob S		     Added Company ID
6/9/08  MAS			Added Incoterm to input
12/29/09 MAS        Truncating phone and fax numbers
3/4/2012 MAS		Setting active flag
7/13/2013 BTV		Added logic to calculate the IDT_TAX_ID_NBR during import
8/17/2013 MAS		Added support for WE UK
8/16/2013 DAP		Added RemitToAddressId parameter and set RemitToId in the table. 
09/09/2013 JAW		Added Default value of NULL for variable @RemitToAddressId INTEGER = NULL 
09/24/2013 BobS		Add Canadian Format support
12/11/2013 MDB		Broke INV_REPORT_SETTING logic out into seperate function
12/12/2013 DAP		Moved logic to determine partial exemption status to IDTBillingAccountIdHasPartailExemption on Taxes database
					Also minor code format cleanup
12/17/2013	DAP		Updated to get an exemption type id and pass that id into GetInvReportSettingId for handling
					partial exemption templates.
1/16/2014	DAP		Updated to pass the ship state code to the IDTBillingAccountIdHasPartailExemption function on the Taxes database
1/20/2014	DAP		Fixed bug - not sending sales order number to stored proc that determines if an order was shipped world ease.
Jan 14 Bob S		BVBA and AX
*/

CREATE  PROCEDURE [dbo].[INV_IMPORT]
(@BATCH_ID  INTEGER,
@SALES_ORD_NBR INTEGER,
@INCOTERM_CODE VARCHAR(20),
@RemitToAddressId INTEGER = NULL 
)

AS
SET NOCOUNT ON

DECLARE @BATCH_ORD_ID INTEGER
DECLARE @RefNbr INTEGER
DECLARE @ORD_ADDR_TYPE INT
DECLARE @ORD_DELIVERY_METHOD_ID INT
DECLARE @BILL_ACCT_ADDR INT
DECLARE @ENDUSER_BILL_ADDR INT
DECLARE @ENDUSER_SHIP_ADDR INT
DECLARE @ENDUSER_BILL_FAX INT

SET @BILL_ACCT_ADDR = 1
SET @ENDUSER_BILL_ADDR = 2
SET @ENDUSER_SHIP_ADDR = 3
SET @ENDUSER_BILL_FAX = 4

SELECT @ORD_ADDR_TYPE = INV_ADDR_TYPE_ID, @ORD_DELIVERY_METHOD_ID = INV_DELIVERY_METH_ID
FROM ORD WHERE SALES_ORD_NBR = @SALES_ORD_NBR

DECLARE @RC int, @KEY_NM varchar(128), @NBR_OF_KEYS int, @NEXT_LOW_KEY_VAL int, @NEXT_HIGH_KEY_VAL int
select @KEY_NM = 'BATCH_ORD_ID', @NBR_OF_KEYS = 1
EXEC @RC = [dbo].[KEY_FIELD_GET_NEXT_X_VALUES] @KEY_NM, @NBR_OF_KEYS, @NEXT_LOW_KEY_VAL OUTPUT , @NEXT_HIGH_KEY_VAL OUTPUT 

if @RC = 0
  select @KEY_NM, @NBR_OF_KEYS, @NEXT_LOW_KEY_VAL, @NEXT_HIGH_KEY_VAL
else 
BEGIN
  RAISERROR ('error: %i creating key for Batch_ORD_ID', 16,1, @RC)
  RETURN @RC
END 

/*Gather any current active invoices for order*/
select BATCH_ORD_ID
into #PREVIOUS_ACTIVE_INVOICES
from inv_hdr 
	where SALES_ORD_NBR = @SALES_ORD_NBR
	and ACTIVE = 1

/* INSERT ALL HEADER INFORMATION.  IF THIS IS REALLY SLOW MIGHT WANT TO BREAK IT INTO TWO QUERIES.
*/
--SELECT * FROM DELIVERY_METHOD
SELECT DISTINCT  
  BATCH_ORD_ID = @NEXT_LOW_KEY_VAL, 
  BATCH_ID = @BATCH_ID, 
  SALES_ORD_NBR = @SALES_ORD_NBR, 
  ORD.BILL_ACCT_ID, 
  ORG.ORG_NBR, 
  ACCT_NBR = ACC_NBR, 
  ENDUSER_NBR, 
  INV_TOTAL = 0,
  INV_TOTAL_NATURAL = 0, 
  INV_LINES = 0, 
  SHIP_DT=	(SELECT MAX(ORD_STAT_DT) 
  	FROM ORD_STAT_HIST WHERE ORD_ID = ORD.ORD_ID AND
  	ORD_STAT_TYPE_ID = 14
    ) ,
  ORD_DT = ORD_DATE, 
  INV_DT = CONVERT(varchar(8),getdate(),1),
  PAYMENT_TYPE = ORD_PAY_METH.PAY_METH_NM, 
  PAY_AUTH_NBR, 
  PRICE_GRP_CD, 
  SHIP_METH = NULL, 
  SHIP_NM = SHIP_ADDR.ADDR1,
  SHIP_ADDR1 = SHIP_ADDR.ADDR2, 
  SHIP_ADDR2 = SHIP_ADDR.ADDR3, 
  SHIP_CITY = SHIP_ADDR.CITY_NM, 
  SHIP_ST = SHIP_ADDR.STATE_CD, 
  SHIP_ZIP = SHIP_ADDR.POST_CD,
  SHIP_ZIP4 = SHIP_ADDR.PLUS4_ZIPDIGITS, 
  SHIP_CNTRY = SHIP_ADDR.CNTRY_NM, 
  SHIP_PH = CONVERT(VARCHAR(20), SHIP_CM.PH_NBR)
, 
  BILL_NM = CUST.CUSTOMER_NAME,
  BILL_ADDR1 = 
    CASE @ORD_ADDR_TYPE
    	WHEN @BILL_ACCT_ADDR THEN CUST.ADDR2 
    	WHEN @ENDUSER_BILL_ADDR THEN BILL_ADDR.ADDR2 
    	WHEN @ENDUSER_SHIP_ADDR THEN SHIP_ADDR.ADDR2 
    	ELSE BILL_ADDR.ADDR2 
    END,
  BILL_ADDR2 =
    CASE @ORD_ADDR_TYPE
    	WHEN @BILL_ACCT_ADDR THEN CUST.ADDR3 
    	WHEN @ENDUSER_BILL_ADDR THEN BILL_ADDR.ADDR3 
    	WHEN @ENDUSER_SHIP_ADDR THEN SHIP_ADDR.ADDR3 
    	ELSE BILL_ADDR.ADDR3 
    END,
  BILL_CITY =
    CASE @ORD_ADDR_TYPE
    	WHEN @BILL_ACCT_ADDR THEN CUST.CITY
    	WHEN @ENDUSER_BILL_ADDR THEN BILL_ADDR.CITY_NM 
    	WHEN @ENDUSER_SHIP_ADDR THEN SHIP_ADDR.CITY_NM
    	ELSE BILL_ADDR.CITY_NM 
    END, 
  BILL_ST =
    CASE @ORD_ADDR_TYPE
    	WHEN @BILL_ACCT_ADDR THEN CUST.STATE 
    	WHEN @ENDUSER_BILL_ADDR THEN BILL_ADDR.STATE_CD 
    	WHEN @ENDUSER_SHIP_ADDR THEN SHIP_ADDR.STATE_CD
    	ELSE BILL_ADDR.STATE_CD 
    END, 
  BILL_ZIP =
    CASE @ORD_ADDR_TYPE
    	WHEN @BILL_ACCT_ADDR THEN CUST.POSTAL_CODE 
    	WHEN @ENDUSER_BILL_ADDR THEN BILL_ADDR.POST_CD 
    	WHEN @ENDUSER_SHIP_ADDR THEN SHIP_ADDR.POST_CD 
    	ELSE BILL_ADDR.POST_CD 
    END,
  BILL_ZIP4 =
    CASE @ORD_ADDR_TYPE
    	WHEN @BILL_ACCT_ADDR THEN '' 
    	WHEN @ENDUSER_BILL_ADDR THEN BILL_ADDR.PLUS4_ZIPDIGITS 
    	WHEN @ENDUSER_SHIP_ADDR THEN SHIP_ADDR.PLUS4_ZIPDIGITS 
    	ELSE BILL_ADDR.PLUS4_ZIPDIGITS 
    END,
  BILL_CNTRY =
    CASE @ORD_ADDR_TYPE
    	WHEN @BILL_ACCT_ADDR THEN COUNTRY.[DESCRIPTION]
    	WHEN @ENDUSER_BILL_ADDR THEN BILL_ADDR.CNTRY_NM

    	WHEN @ENDUSER_SHIP_ADDR THEN SHIP_ADDR.CNTRY_NM
    	ELSE BILL_ADDR.CNTRY_NM 
    END, 
  BILL_PH = CONVERT(VARCHAR(20), BILL_CM.PH_NBR), 
  SHIP_ENDUSER_NM = PERSON.FIRST_NM + ' ' + PERSON.LAST_NM, 
  ORD.CURRENCY_ID, 
  ORD.EXCHANGE_RATE,
  BILL_FAX = CONVERT(VARCHAR(20), BILL_CM.FAX_NBR),
  COMPANY_ID = (select epicor_company_id
				from reference.dbo.branch_location
				where branch_location_id in (select branch_location_id 
											from reference.dbo.branch_configuration
											)
				)
  ,INCOTERM_CODE = @INCOTERM_CODE
into #tmp
FROM dbo.ORD WITH (nolock)
  LEFT JOIN dbo.ORG ON ORD.ORG_ID = ORG.PARTY_ID
  LEFT JOIN dbo.ACCOUNT ON ORD.ACC_ID = ACCOUNT.PARTY_ID
  LEFT JOIN dbo.PERSON ON ORD.ENDUSER_ID = PERSON.PARTY_ID
  LEFT JOIN dbo.PARTY ON PERSON.PARTY_ID = PARTY.PARTY_ID
  LEFT JOIN dbo.EPICOR_ARCUST_VW CUST WITH (nolock) ON ORD.BILL_ACCT_ID = CUST.CUSTOMER_CODE
  LEFT JOIN dbo.EPICOR_GL_COUNTRY_vw COUNTRY on CUST.country_code = COUNTRY.country_code
  LEFT JOIN dbo.ORD_PAY_METH ON ORD.PAY_METH_ID = ORD_PAY_METH.PAY_METH_ID
  LEFT JOIN dbo.ORD_STAT_HIST WITH (nolock) ON ORD.ORD_ID = ORD_STAT_HIST.ORD_ID AND
  				ORD_STAT_TYPE_ID = 14
  LEFT JOIN dbo.PRICE_GRP ON PARTY.PRICE_GRP_ID = PRICE_GRP.PRICE_GRP_ID AND
  				PRICE_GRP.SHIPPING = 0
  LEFT JOIN dbo.ADDR AS SHIP_ADDR ON ORD.SHIP_ADDR_ID = SHIP_ADDR.ADDR_ID
  LEFT JOIN dbo.CONT_MECH AS SHIP_CM ON ORD.ENDUSER_ID = SHIP_CM.PARTY_ID AND
  				SHIP_CM.CONT_ROLE_TYPE_ID = 1
  LEFT JOIN dbo.CONT_MECH AS BILL_CM ON ORD.ENDUSER_ID = BILL_CM.PARTY_ID AND
  				BILL_CM.CONT_ROLE_TYPE_ID = 2
  LEFT JOIN dbo.ADDR AS BILL_ADDR ON ORD.BILL_ADDR_ID = BILL_ADDR.ADDR_ID
WHERE SALES_ORD_NBR =  @SALES_ORD_NBR


-----------------------------
--Determine INV_REPORT_SETTINGS
-----------------------------
	DECLARE    @BranchLocation INT
  , @ShipCountry VARCHAR(50)
  , @ShipState VARCHAR(20)
  , @SalesOrderNbr INT
  , @ShippedWorldEase varchar(3)
  , @OrgNbr int
  , @ExemptionTypeId INT
  , @REPORT_SETTING_ID INT
  , @BillAcctId VARCHAR(25)
		
	SET @ShippedWorldEase = ''
	SET @ExemptionTypeId = 0
	SET @SalesOrderNbr = @SALES_ORD_NBR
	
	SELECT @BranchLocation = Branch_Location_Id
	FROM reference.dbo.Branch_Configuration

	SELECT @OrgNbr = ORG_NBR, 
		@ShipCountry = ship_cntry, 
		@ShipState = ship_st, 
		@BillAcctId = BILL_ACCT_ID  
	FROM #tmp
	
	IF @BranchLocation = 1
		SET @ExemptionTypeId = Taxes.dbo.IDTBillingAccountPartialExemptionTypeId(@BillAcctId, @ShipState)
	ELSE IF @BranchLocation = 3 --EU/BVBA, only want to do this if the inv actually needs it
		EXEC MFGSQL.PRODUCTION.dbo.usp_WorldShipped @SalesOrderNbr, @RefNbr, @ShippedWorldEase OUTPUT
	
	
	SET @REPORT_SETTING_ID = SALES.dbo.GetInvReportSettingId(
										@BranchLocation, 
										@ShipCountry, 
										@ShipState, 
										@ShippedWorldEase, 
										@OrgNbr, 
										@ExemptionTypeId)


---------------------------------
--End of Determine INV_REPORT_SETTINGS
---------------------------------



INSERT dbo.INV_HDR
  (BATCH_ORD_ID, BATCH_ID,SALES_ORD_NBR, BILL_ACCT_ID, ORG_NBR, ACCT_NBR, ENDUSER_NBR, INV_TOTAL, INV_LINES, SHIP_DT,
  ORD_DT, INV_DT, PAYMENT_TYPE, PAY_AUTH_NBR, PRICE_GRP_CD, SHIP_METH, SHIP_NM, SHIP_ADDR1, SHIP_ADDR2, SHIP_CITY, 
  SHIP_ST, SHIP_ZIP, SHIP_ZIP4,SHIP_CNTRY,
  SHIP_PH, BILL_NM, BILL_ADDR1, BILL_ADDR2, BILL_CITY, BILL_ST, BILL_ZIP,BILL_ZIP4, BILL_CNTRY, BILL_PH,SHIP_ENDUSER_NM, 
  CURRENCY_ID, EXCHANGE_RATE, BILL_FAX, DELIVERY_METHOD_ID, COMPANY_ID, INCOTERM_CODE, CREATE_DTM, ACTIVE, INV_REPORT_SETTING_ID,
  RemitToId)
select BATCH_ORD_ID, BATCH_ID,SALES_ORD_NBR, BILL_ACCT_ID, ORG_NBR, ACCT_NBR, ENDUSER_NBR, INV_TOTAL, INV_LINES, SHIP_DT,
  ORD_DT, INV_DT, PAYMENT_TYPE, PAY_AUTH_NBR, PRICE_GRP_CD, SHIP_METH, SHIP_NM, SHIP_ADDR1, SHIP_ADDR2, SHIP_CITY, 
  SHIP_ST, SHIP_ZIP, SHIP_ZIP4,SHIP_CNTRY,
  SHIP_PH, BILL_NM, BILL_ADDR1, BILL_ADDR2, BILL_CITY, BILL_ST, BILL_ZIP,BILL_ZIP4, BILL_CNTRY, BILL_PH,SHIP_ENDUSER_NM, 
  CURRENCY_ID, EXCHANGE_RATE, BILL_FAX, @ORD_DELIVERY_METHOD_ID,COMPANY_ID, INCOTERM_CODE, GETDATE(), 1, @REPORT_SETTING_ID,
  @RemitToAddressId
from #tmp

/*Deactivate previous active invoices*/
UPDATE INV_HDR SET ACTIVE = 0
FROM INV_HDR
JOIN #PREVIOUS_ACTIVE_INVOICES PEI ON PEI.BATCH_ORD_ID = INV_HDR.BATCH_ORD_ID

DROP TABLE #PREVIOUS_ACTIVE_INVOICES 


GO
GRANT EXECUTE ON  [dbo].[INV_IMPORT] TO [IDT_APPS]
GO
