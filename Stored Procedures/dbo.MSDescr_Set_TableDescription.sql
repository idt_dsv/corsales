SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO


CREATE  PROCEDURE [dbo].[MSDescr_Set_TableDescription]
(@TableName varchar(128), @Descr varchar(256))
AS
  DECLARE @SqlStr Nvarchar(256), @result int , @RowCount int 

create table  #tmp_tbl (Description sql_variant)

insert into #tmp_Tbl
exec MSDescr_get_TableDescription @TableName

Set @RowCount = @@rowcount

if @RowCount = 0  
  set @SqlStr = N'EXECUTE sp_addextendedproperty N''MS_Description'', '''+ @Descr +''' , N''user'', N''dbo'', N''table'', '+@TableName + ' , NULL, NULL' 
else
  set @SqlStr = N'EXECUTE sp_updateextendedproperty N''MS_Description'', '''+  @Descr +''' , N''user'', N''dbo'', N''table'', '+@TableName + ' , NULL, NULL' 

--select @sqlStr
execute @result = sp_executesql @SqlStr
return @result

GO
GRANT EXECUTE ON  [dbo].[MSDescr_Set_TableDescription] TO [IDT_APPS]
GO
