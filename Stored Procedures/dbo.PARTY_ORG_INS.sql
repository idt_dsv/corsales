SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
/*
Author: ??
Date: ??
Notes
  updates enduser into Party and org

Modfifications:
SG  05/13/04  Updated @BillAcctID from int to Varchar(10) per MSCHEBEL

*/


CREATE    PROCEDURE [dbo].[PARTY_ORG_INS]
  @ObjID int,
  @BillAcctID varchar(10),
  @BillCycleID int,
  @ClassTypeID int,
  @CountryID int,
  @CreateDate datetime,
  @CreateUser varchar(25),
  @CreditRateID int,
  @CurrStatType int,
  @DefCarrierID int,
  @FedTaxIDNumber varchar(10),
  @ModifyDate datetime,
  @ModifyUser varchar(25),
  @OrgAbbreviation int,
  @OrgName varchar(10),
  @OrgNumber int,
  @OrgShortName varchar(10),
  @OrgState varchar(10),
  @PartySourceTypeID int,
  @PartySubType char(5),
  @PriceGroupID int,
  @SalesTax smallmoney,
  @SalesTerrID int,
  @ShipGroupID int,
  @CurrencyID int,
  @EmailNotification bit
AS
BEGIN
DECLARE @RowCount int, @Err int
 
  BEGIN TRANSACTION 
  INSERT INTO [PARTY]
  (
    [PARTY_ID], 
    [SHIP_GRP_ID], 
    [PRICE_GRP_ID], 
    [SALES_TERR_ID], 
    [BILL_CYCLE_ID], 
    [DEF_CARRIER_ID], 
    [CURR_STAT_TYPE], 
    [CRDT_RATE_ID], 
    [BILL_ACCT_ID], 
    [PARTY_SOURCE_TYPE_ID], 
    [SALES_TAX], 
    [PARTY_SUB_TYPE], 
    [CREATE_DT], 
    [CREATE_USER], 
    [MODIFY_DT], 
    [MODIFY_USER],
    [CURRENCY_ID],
    [SEND_PROMO_EMAIL]
  )
  VALUES
  (
    @ObjID, 
    @ShipGroupID, 
    @PriceGroupID, 
    @SalesTerrID, 
    @BillCycleID, 
    @DefCarrierID,
    @CurrStatType,
    @CreditRateID,
    @BillAcctID, 
    @PartySourceTypeID, 
    @SalesTax, 
    @PartySubType,
    @CreateDate, 
    @CreateUser,
    @ModifyDate,
    @ModifyUser,
    @CurrencyID,
    @EmailNotification
  )

  SELECT @RowCount = @@ROWCOUNT, @Err = @@ERROR
  IF (@Err <> 0) OR (@RowCount <> 1)
    GOTO errorhandler

  INSERT INTO [ORG]
  (
    [PARTY_ID], 
    [CLASS_TYPE_ID], 
    [ORG_NBR], 
    [FED_TAX_ID_NBR], 
    [ORG_NM], 
    [ORG_ABBR], 
    [ORG_SHRT_NM], 
    [ORG_ST], 
    [CNTRY_ID]
  )
  VALUES
  (
    @ObjID, 
    @ClassTypeID, 
    @OrgNumber, 
    @FedTaxIDNumber, 
    @OrgName, 
    @OrgAbbreviation, 
    @OrgShortName, 
    @OrgState, 
    @CountryID
  )

 SELECT @RowCount = @@ROWCOUNT, @Err = @@ERROR
  IF (@Err <> 0) OR (@RowCount <> 1)
    GOTO errorhandler

  COMMIT TRANSACTION 
  RETURN(0)

errorhandler:
  ROLLBACK TRANSACTION
  RAISERROR ('PARTY ORG INSERT ERROR (ID %d)', 16, 1, @ObjID)
  RETURN(-1)
END
GO
GRANT EXECUTE ON  [dbo].[PARTY_ORG_INS] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[PARTY_ORG_INS] TO [IDT_GRANT_INFO_USER]
GO
