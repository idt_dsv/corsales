
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
--     Author                      	Ben Viering                 
--     Creation Date	10-8-01                             
--     Comments                                     
--     Modified on 6-10-03 by Chris Sailor to add new
--     yield guarantee fields.
--
--     Modified on 6-24-03 by Chris Sailor to add new
--     purity guarantee fields.

REVISIONS:
6/10/03		CS	Added new Yield Guar fields
6/24/03		CS	Added new Purity Guar fields
8/01/03 	CS 	Removed commented out code dealing with ord_line_item_spec
4/23/04		MS	Added Yield Guarantee Override flag
1/30/07		MS	Upped size of sequence fields
9/29/14		Cory Blythe	Changed the select, added nolock hint

*/

CREATE PROCEDURE [dbo].[Oligo_Spec_Ins]
    (
      @REF_ID INT
    , @Bases INT
    , @Length INT
    , @Purif_ID INT
    , @ExtCoef FLOAT
    , @GC FLOAT
    , @MWHydrated FLOAT
    , @MWAnhydrous FLOAT
    , @OD_Obligation FLOAT
    , @OD_Max_Obligation INT
    , @OD_Min_Synth_Req FLOAT
    , @OD_Max_Synth_Req INT
    , @Purity_Guar_Pct FLOAT
    , @Purity_Guar_Explanation_ID INT
    , @Purity_Guar_Points INT
    , @TM FLOAT
    , @UnitSize FLOAT
    , @OldSequence VARCHAR(500)
    , @SequenceDesc2 VARCHAR(254)
    , @SequenceDesc VARCHAR(254)
    , @NewSequence VARCHAR(MAX)
    , @YieldGuarType CHAR(3)
    , @YieldGuarPoints INTEGER
    , @YieldGuarOverride INTEGER
    )
AS
    SET NOCOUNT ON
/* Enter all variables cursors and constants following
 this line */
    DECLARE @LINE_ITEM_ID INT
    BEGIN
        SET @LINE_ITEM_ID = ( SELECT    LINE_ITEM_ID
                              FROM      dbo.LINE_ITEM_REF_NBR WITH (NOLOCK)
                              WHERE     REF_ID = @REF_ID
                            )

        INSERT  INTO [OLIGO_SPEC]
                (
                  [REF_ID]
                , [BASES]
                , [LENGTH]
                , [PURIF_ID]
                , [EXT_COEFF]
                , [GC]
                , [MW_HYDRATED]
                , [MW_ANHYDROUS]
                , [OD_OBLIGATION]
                , [OD_MAX_OBLIGATION]
                , [OD_MIN_SYNTH_REQ]
                , [OD_MAX_SYNTH_REQ]
                , [PURITY_GUAR_PCT]
                , [PURITY_GUAR_EXPLANATION_ID]
                , [PURITY_GUAR_POINTS]
                , [TM]
                , [UNIT_SIZE]
                , [SEQ]
                , [SEQ_DESC2]
                , [SEQ_DESC]
                , [YIELD_GUAR_TYPE]
                , [YIELD_GUAR_POINTS]
                , [YIELD_GUAR_OVERRIDE]
                )
        VALUES  (
                  @REF_ID
                , @Bases
                , @Length
                , @Purif_ID
                , @ExtCoef
                , @GC
                , @MWHydrated
                , @MWAnhydrous
                , @OD_Obligation
                , @OD_Max_Obligation
                , @OD_Min_Synth_Req
                , @OD_Max_Synth_Req
                , @Purity_Guar_Pct
                , @Purity_Guar_Explanation_ID
                , @Purity_Guar_Points
                , @TM
                , @UnitSize
                , @NewSequence
                , @SequenceDesc2
                , @SequenceDesc
                , @YieldGuarType
                , @YieldGuarPoints
                , @YieldGuarOverride
                )
        IF @@ERROR <> 0
            GOTO HandleError

        SET NOCOUNT OFF
        RETURN 0
/* Enter the code to handle exception conditions
 following this line */

        HandleError:
        SET NOCOUNT OFF
        RETURN @@ERROR

    END


GO

GRANT EXECUTE ON  [dbo].[Oligo_Spec_Ins] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[Oligo_Spec_Ins] TO [IDT_Reporter]
GO
