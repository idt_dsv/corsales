SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-----------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[GET_NEW_PROCESS_PREFERENCE_EXPANDED_DATA]
		@FromDate varchar(10),
		@ThroughDate varchar(10)
AS
-----------------------------------------------------------------------------------------
-- Set local evnironmental settings
-- Declare all variables and create all temp tables

SET	NOCOUNT ON

CREATE TABLE #SALES_AUDIT_DATA
		(PROCESS_PREFERENCE_SALES_ID int,
		VERSION int,
		OWNER_CID int,
		OWNER_OID int)

DECLARE	@START_DATE datetime,
		@STOP_DATE datetime

SET		@START_DATE = CAST(@FromDate as datetime)
SET		@STOP_DATE = CAST(@ThroughDate as datetime)

-----------------------------------------------------------------------------------------
-- Insert MAX(Version) into #SALES_AUDIT_DATA

INSERT INTO #SALES_AUDIT_DATA
		(PROCESS_PREFERENCE_SALES_ID,
		VERSION,
		OWNER_CID,
		OWNER_OID)
SELECT	PR.PROCESS_PREFERENCE_SALES_ID,
		MAX(PR.VERSION),
		PR.OWNER_CID,
		PR.OWNER_OID
FROM	SALES.dbo.PROCESS_PREFERENCE_SALES_AUDIT AS PR WITH (NOLOCK)
WHERE	PR.CHANGED_DTM BETWEEN @START_DATE AND @STOP_DATE
GROUP BY PR.PROCESS_PREFERENCE_SALES_ID,
		PR.OWNER_CID,
		PR.OWNER_OID

-----------------------------------------------------------------------------------------
-- Return list of DISTINCT PROCESS_PREFERENCE_SALES_ID values

SELECT	DISTINCT
		SAD.PROCESS_PREFERENCE_SALES_ID
FROM	#SALES_AUDIT_DATA AS SAD
INNER JOIN SALES.dbo.PROCESS_PREFERENCE_EXPANDED AS PV WITH (NOLOCK) ON
		SAD.OWNER_CID = PV.OPTION_OWNER_CID
AND		SAD.OWNER_OID = PV.OPTION_OWNER_OID
WHERE	SAD.VERSION >= PV.VERSION_NBR

DROP TABLE #SALES_AUDIT_DATA

RETURN
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------




GO
GRANT EXECUTE ON  [dbo].[GET_NEW_PROCESS_PREFERENCE_EXPANDED_DATA] TO [IDT_APPS]
GO
