SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		David Priebe
-- Create date: 1/28/2010
-- Description:	Takes a comma delimited list of
-- sales order numbers and returns a list of all
-- the sales order numbers in that list that have
-- been shipped.
--
-- Permissions: IDT_Apps - Execute.
-- =============================================
CREATE PROCEDURE [dbo].[OMLineItemsShippedInSalesOrdList] 
	-- Add the parameters for the stored procedure here
	@SalesOrdNbrs VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	CREATE TABLE #SalesOrdNbrs ( SalesOrdNbr INT NOT NULL )

	INSERT INTO #SalesOrdNbrs 
		SELECT Convert(int, StrValue) 
		FROM dbo.BreakApartStr(@SalesOrdNbrs) 

	SELECT lirn.[EXTERNAL_REF_ID]
	FROM [ORD] o (NOLOCK)
	JOIN [LINE_ITEM_REF_NBR] lirn (NOLOCK) ON o.[ORD_ID] = lirn.[FK_ORD_ID]
	WHERE o.[CURR_STAT] <> 7
		AND o.[SALES_ORD_NBR] IN ( SELECT son.[SalesOrdNbr] FROM #SalesOrdNbrs son )
		AND lirn.[CURR_STAT] = 4

	DROP TABLE #SalesOrdNbrs
END



GO
GRANT EXECUTE ON  [dbo].[OMLineItemsShippedInSalesOrdList] TO [IDT_APPS]
GO
