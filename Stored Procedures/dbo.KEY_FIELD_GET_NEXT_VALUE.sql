SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO





/*
Author: Mark DeWaard
Date Created: 3/21/03
Purpose:
  Gets next key for a table that uses a key from the next_key_val table
  Took GET_NEXT_KEY SP to meet naming standards
    and renamed it and added error logic

example:
declare @i int, @err int
exec @err = dbo.KEY_FIELD_GET_NEXT_VALUE 'scale_info_id', @i output
select @err,  @i
grant exec on KEY_FIELD_GET_NEXT_X_VALUES to IDT_Apps
Modifications:
Who     Date      Modification  Note
--------------------------------------------------------------
MDW     7/30/05   Changed to reuse KEY_FIELD_GET_NEXT_X_VALUES stored procedure for easier maintenance
*/


CREATE  PROCEDURE [dbo].[KEY_FIELD_GET_NEXT_VALUE]
@KEY_NM VARCHAR(128),
@NEXT_KEY_VAL INT = -1 OUTPUT
AS 

  DECLARE @Err int
  DECLARE @NEXT_LOW_KEY_VAL int
  DECLARE @NEXT_HIGH_KEY_VAL int
begin
  set nocount on
  -- To maintain only one procedure call KEY_FIELD_GET_NEXT_X_VALUES SP 
  EXEC @Err = dbo.KEY_FIELD_GET_NEXT_X_VALUES @KEY_NM, 1, @NEXT_LOW_KEY_VAL OUTPUT , @NEXT_HIGH_KEY_VAL OUTPUT 
  IF @Err = 0 
  begin
    set @NEXT_KEY_VAL = @NEXT_LOW_KEY_VAL
    RETURN 0    -- success
  end
  ELSE IF @Err <> 0
    RETURN @Err  -- Error on SQL side, allows user to handle error and try again if needed

  set nocount off
END






GO
GRANT EXECUTE ON  [dbo].[KEY_FIELD_GET_NEXT_VALUE] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[KEY_FIELD_GET_NEXT_VALUE] TO [IDT_GRANT_INFO_USER]
GRANT EXECUTE ON  [dbo].[KEY_FIELD_GET_NEXT_VALUE] TO [IDT_WebApps]
GO
