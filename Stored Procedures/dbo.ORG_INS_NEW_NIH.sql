SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/**********************************************************************************************************************************************
ORG_INS_NEW_NIH
 AUTHOR: Devora Millen
 CREATED ON: 7/30/04
 DESCRIPTION/ALGORITHM: INSERT A NEW NIH POTENTIAL ORGANIZATION
                        WITH UNKNOWN-UNKNOWN ACCOUNT
 PERMISSIONS:
 grant execute on ORG_INS_NEW_NIH to IDT_GRANT_INFO 
 REVISIONS:
Date	Who	Notes
-------------------------------------
8/11/04 DM	As per RU, accounts are created with party_source_type_id of 'IDT'
1/23/06 DM	Added null value for @PARTY_SOURCE_TYPE_ID on org_ins and acct_ins call
**********************************************************************************************************************************************/
CREATE     procedure [dbo].[ORG_INS_NEW_NIH]
@org_nm varchar(50),
@org_state varchar(2),
@sales_terr_nm varchar(50),
@class_type varchar(25)

as 

declare @class_type_id int
declare @sales_terr_id int
declare @rcc int


if (@class_type = 'academic') 
   set @class_type_id=0
else 
   set @class_type_id=1

set @sales_terr_id=(select sales_terr_id
                      from sales_terr
                     where sales_terr_nm=@sales_terr_nm)


DECLARE @RC int
DECLARE @Party_id int, @ORG_NBR int, @acc_id int, @acc_nbr int
DECLARE @CREATE_DT datetime
-- Set parameter values
EXEC @RC = [KEY_FIELD_GET_NEXT_VALUE] 'party_id', @Party_id OUTPUT 

if (@RC <>0 or @rc is null) begin 
  Raiserror ('Cannot get org_party_id', 16, 1)

  return (isnull(@rc,55))
end 

 
EXEC @RC = [KEY_FIELD_GET_NEXT_VALUE] 'org_nbr', @Org_Nbr OUTPUT 
if @RC <>0 begin
  Raiserror ('Cannot get org_nbr', 16, 1)
  return (-1)
end

EXEC @RC = [KEY_FIELD_GET_NEXT_VALUE] 'party_id', @acc_id OUTPUT 
if @RC <>0 begin 
  Raiserror ('Cannot get party_id', 16, 1)

  return (@rc)
end

EXEC @RC = [KEY_FIELD_GET_NEXT_VALUE] 'acc_nbr', @acc_nbr OUTPUT 
if @RC <>0 begin
  Raiserror ('Cannot get acc_nbr', 16, 1)
  return (-1)
end

 
set @CREATE_DT=getdate() 

 
Begin transaction t1

SET XACT_ABORT ON


EXEC @RC = [ORG_INS] @party_ID, 10,0,1,12,1,@sales_terr_id,    1,0,0,@class_type_id,'',@org_nm,@org_nm,@org_state,'gs',@CREATE_DT, @ORG_NBR, 5500,1, null,5,null

if @RC <> 0 
begin
  rollback
  Raiserror ('Cannot add org', 16, 1)
  return(-1)
end



EXEC @RC = [ACCT_INS] @acc_ID, 10, 0, 1, 12, 1, @SALES_TERR_ID, 1, 0, 0, 'Unknown-Unknown', @party_id,  'gs',  @CREATE_DT, @acc_nbr, 1, 1, null

if @RC = 0 
begin
  commit transaction t1
end
else begin
  rollback transaction t1
  Raiserror ('Cannot add_acct', 16, 1)
  return(-1)
end

SET XACT_ABORT OFF

select @party_id
GO
GRANT EXECUTE ON  [dbo].[ORG_INS_NEW_NIH] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[ORG_INS_NEW_NIH] TO [IDT_GRANT_INFO_USER]
GO
