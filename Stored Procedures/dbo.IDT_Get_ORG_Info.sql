SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
Author: Phil Shaff
Date: 10/4/2012
Purpose: Sql to get data from ORG DB when needed

Permissions:
-----------------------------------------------
--GRANT  EXECUTE  ON [dbo].[IDT_Get_ORG_Info]  TO [IDT_APPS]
--GRANT  EXECUTE  ON [dbo].[IDT_Get_ORG_Info]  TO [IDT-CORALVILLE\DBO_ShippingConfigMgr]

Revisions
Date		Who	BL		Note
-----------------------------------------------------------------------------------------------------------------------
10/4/2012   PES			Initial creation

*/



CREATE PROCEDURE [dbo].[IDT_Get_ORG_Info]
@ORG_NAME varchar(24) = null,
@ORG_ID integer = null

AS

SELECT PARTY_ID,
	   ORG_NBR,
	   ORG_NM
  FROM [SALES].[dbo].[ORG] with (nolock)
  where (@ORG_ID = ORG_NBR AND @ORG_ID IS NOT NULL) OR 
		(ORG_NM LIKE '%' + @ORG_NAME + '%' AND @ORG_NAME IS NOT NULL) 
  ORDER BY ORG_NM

GO
GRANT EXECUTE ON  [dbo].[IDT_Get_ORG_Info] TO [IDT_APPS]
GO
