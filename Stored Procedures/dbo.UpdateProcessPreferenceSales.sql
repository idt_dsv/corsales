
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
GRANT  EXECUTE  ON [dbo].[UpdateProcessPreferenceSales] TO [Coliseum]

Author: Phil Shaff
Date Created: April 14th, 2014
Purpose:

	Updates PROCESS_PREFERENCE_SALES record

Modifications:
Who	Date	Modification	Note
---------------------------------------------------------------------------------------
PS  7/11/14 Added output to the keystore key allocation query
*/

CREATE PROCEDURE [dbo].[UpdateProcessPreferenceSales]
    @TypeId INT
  , @OwnerId INT
  , @PreferenceData TEXT
  , @StateId BIT
AS
    DECLARE @id INT
    SET @id = ( SELECT  PROCESS_PREFERENCE_SALES_ID
                FROM    SALES.DBO.PROCESS_PREFERENCE_SALES WITH ( NOLOCK )
                WHERE   OWNER_CID = @TypeId
                        AND OWNER_OID = @OwnerId
              )

    IF @id IS NULL
        OR @id = 0
        BEGIN
            DECLARE @NEXT_KEY_VAL INT
            EXEC KEY_FIELD_GET_NEXT_VALUE 'PROCESS_PREFERENCE_SALES_ID',
                @NEXT_KEY_VAL OUTPUT

            INSERT  INTO DBO.PROCESS_PREFERENCE_SALES
                    (
                      PROCESS_PREFERENCE_SALES_ID
                    , OWNER_CID
                    , OWNER_OID
                    , PREFERENCE_DATA
                    , CREATE_DTM
                    , SPEC_STATE_ID
                    , [VERSION]
                    , VERSION_DTM
                    )
            VALUES  (
                      @NEXT_KEY_VAL
                    , @TypeId
                    , @OwnerId
                    , @PreferenceData
                    , GETDATE()
                    , 1
                    , 1
                    , GETDATE()
                    )
        END
    ELSE
        BEGIN
            UPDATE  DBO.PROCESS_PREFERENCE_SALES
            SET     OWNER_CID = @TypeId
                  , OWNER_OID = @OwnerId
                  , PREFERENCE_DATA = @PreferenceData
                  , SPEC_STATE_ID = @StateId
                  , [VERSION] = [VERSION] + 1
                  , VERSION_DTM = GETDATE()
            WHERE   OWNER_CID = @TypeId
                    AND OWNER_OID = @OwnerId
                    AND PROCESS_PREFERENCE_SALES_ID = @id
        END

GO

GRANT EXECUTE ON  [dbo].[UpdateProcessPreferenceSales] TO [coliseum]
GO
