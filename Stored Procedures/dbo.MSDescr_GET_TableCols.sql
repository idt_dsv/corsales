SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
Author: MDW
Date: ??
Notes: Returns descriptions for a table's columns

example:
exec dbo.MSDescr_GET_TableCols 'mfg_proc_hist'

*/

CREATE PROCEDURE [dbo].[MSDescr_GET_TableCols]
(@TableName varchar(128))
as

select @TableName Table_name, cols.column_name, descr.descr , ordinal_position
from information_schema.columns cols
 join vw_MSDescriptions descr on descr.tableName =  cols.table_name 
   and cols.ordinal_position = descr.columnOrder
where cols.table_name = @TableName
order by ordinal_position



GO
GRANT EXECUTE ON  [dbo].[MSDescr_GET_TableCols] TO [IDT_APPS]
GO
