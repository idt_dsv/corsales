SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
GRANT  EXECUTE  ON [dbo].[GetConfirmationEmailOrderInfo] TO [IDT-Coralville\iusr_idt] 
GRANT  EXECUTE  ON [dbo].[GetConfirmationEmailOrderInfo] TO [Coliseum]
GRANT  EXECUTE  ON [dbo].[GetConfirmationEmailOrderInfo] TO [AppSalesSin]

Author: Phil Shaff
Date Created: Feb 26th, 2014
Purpose:

	Retrieves information necessary to setup Order Confirmation E-mail

Modifications:
Who	Date	Modification	Note
---------------------------------------------------------------------------------------
pshaff 4/10/2014 Added Japanese Distributor functionality to proc
*/

CREATE PROCEDURE [dbo].[GetConfirmationEmailOrderInfo] @salesOrdNbr INT
AS
    CREATE TABLE #JapaneseDistributor ( ord_id INT )

    INSERT  INTO #JapaneseDistributor
            (
              ord_id
            )
            SELECT  ord_id
            FROM    sales.dbo.ORD AS o WITH ( NOLOCK )
                    INNER JOIN sales.dbo.party_rel AS PR WITH ( NOLOCK ) ON pr.child_party_id = o.org_id
                                                              AND pr.party_rel_type = 8 /*Parent party is the distributor for the child party */
                    INNER JOIN sales.dbo.org AS ParentOrg WITH ( NOLOCK ) ON ParentOrg.party_id = pr.PAR_PARTY_ID
                                                              AND ParentOrg.ORG_NM = 'Medical and Biological Laboratories Co., Ltd.'
            WHERE   o.SALES_ORD_NBR = @salesOrdNbr

    SELECT  o.ORD_ID
          , o.ENDUSER_ID
          , o.ORD_DATE
          , o.QUOTE_ID
          , o.PAY_METH_ID
          , o.PAY_AUTH_NBR
          , o.CURRENCY_ID
          , oli.UNIT_PRICE_NATURAL AS ShippingCost
          , o.TAX
          , o.items_on_ord
          , o.ord_total
          , o.sales_ord_nbr
          , p.first_nm + ' ' + p.last_nm AS 'Name'
          , org.ORG_NM
          , org.ORG_NBR
          , org.CLASS_TYPE_ID
          , sa.addr1 AS 'ShipInst'
          , sa.addr2 AS 'ShipAddress'
          , sa.addr3 AS 'ShipDept'
          , sa.city_nm AS 'ShipCity'
          , sa.state_cd AS 'ShipState'
          , sa.post_cd AS 'ShipPostalCd'
          , sa.cntry_nm AS 'ShipCountry'
          , ba.addr1 AS 'BillInst'
          , ba.addr2 AS 'BillAddress'
          , ba.addr3 AS 'BillDept'
          , ba.city_nm AS 'BillCity'
          , ba.state_cd AS 'BillState'
          , ba.post_cd AS 'BillPostalCd'
          , ba.cntry_nm AS 'BillCountry'
          , cm.ph_nbr
          , cm.email
          , org.party_id
          , cc.CC_NBR
          , ct.NAME AS CreditCardNm
          , COALESCE(coprice.ORG_SETTING_ID, 0) AS DontShowPrice
          , COALESCE(ppe.OPTION_VALUE, 'True') AS ShipComplete
          , CASE WHEN J.ord_id IS NOT NULL THEN 'YES'
                 ELSE 'NO'
            END AS 'JapanOrder'
    FROM    sales.dbo.ORD AS o WITH ( NOLOCK )
            INNER JOIN sales.dbo.org AS org WITH ( NOLOCK ) ON org.party_id = o.org_id
            INNER JOIN sales.dbo.person AS p WITH ( NOLOCK ) ON p.party_id = o.enduser_id
            INNER JOIN sales.dbo.addr AS sa WITH ( NOLOCK ) ON sa.addr_id = o.ship_addr_id
            INNER JOIN sales.dbo.addr AS ba WITH ( NOLOCK ) ON ba.ADDR_ID = o.bill_addr_id
            LEFT OUTER JOIN sales.dbo.cont_mech AS cm WITH ( NOLOCK ) ON cm.party_id = p.party_id
                                                              AND CONT_ROLE_TYPE_ID = 1 -- Shipping Contact
            LEFT OUTER JOIN sales.dbo.ord_line_item AS oli WITH ( NOLOCK ) ON oli.ord_id = o.ord_id
                                                              AND oli.OLI_PROD_ID = 1130 -- shipping product id
            LEFT OUTER JOIN sales.dbo.cc AS cc WITH ( NOLOCK ) ON cc.CC_ID = o.CC_ID
            LEFT OUTER JOIN sales.dbo.CC_TYPE AS ct WITH ( NOLOCK ) ON cc.CC_TYPE_ID = ct.CC_TYPE_ID
            LEFT OUTER JOIN sales.dbo.CONFIRM_ORG_SETTING AS coprice WITH ( NOLOCK ) ON coprice.ORG_ID = o.ORG_ID
                                                              AND coprice.ORG_SETTING_ID = 1 -- No Pricing
            LEFT OUTER JOIN sales.dbo.PROCESS_PREFERENCE_EXPANDED AS ppe WITH ( NOLOCK ) ON ppe.OPTION_OWNER_CID = 63
                                                              AND ppe.PROC_PREF_DEF_ID = 8
                                                              AND OPTION_OWNER_OID = o.ord_id  -- check for ship complete
            LEFT OUTER JOIN #JapaneseDistributor AS J ON J.ord_id = o.ord_id
    WHERE   o.SALES_ORD_NBR = @salesOrdNbr

    DROP TABLE #JapaneseDistributor


GO
GRANT EXECUTE ON  [dbo].[GetConfirmationEmailOrderInfo] TO [coliseum]
GRANT EXECUTE ON  [dbo].[GetConfirmationEmailOrderInfo] TO [IDT-CORALVILLE\IUSR_IDT]
GO
