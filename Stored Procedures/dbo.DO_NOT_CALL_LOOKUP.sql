SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*************************************************************
Stored Procedure: DO_NOT_CALL_LOOKUP
Author: Bill Sorensen
Date: 7/12/06
Notes: Searches for matching records in the IDT Support Do Not Call List.

Parameter may be person or organization.
For a person, their organization will also be searched.


May return multiple matches (person, organization, and reasons).

grant execute on dbo.DO_NOT_CALL_LOOKUP to idt_apps

Revisions:
Date    Who Notes
-------------------------------------
07/13/2006 BS  Adjusted for new table design - now sole parameter is PARTY_ID.
12/14/2006 NM  Re-Wrote for new approach. 
08/29/2007 NM  Modified to load DNCs for a party's distributor. 
05/02/2014 PS  Adding nolocks to prevent lock ups
***************************************************************/
CREATE PROCEDURE [dbo].[DO_NOT_CALL_LOOKUP] @PARTY_ID INT
AS
    BEGIN
        DECLARE @additional_party_id INT
          , @Distributor_ID INT 
        SELECT  @additional_party_id = org_id
        FROM    PERSON WITH ( NOLOCK )
        WHERE   PARTY_ID = @PARTY_ID 

        SELECT  @Distributor_ID = PAR_PARTY_ID
        FROM    PARTY_REL WITH ( NOLOCK )
        WHERE   CHILD_PARTY_ID = @additional_party_id
                AND PARTY_REL_TYPE = 8

        SELECT  DO_NOT_CALL_ID ID
              , PARTY_ID AS PartyId
              , DO_NOT_CALL_REASON_ID ReasonId
              , CUSTOM_REASON CustomReason
              , REVIEW_DTM ReviewDtm
              , MatchType = CASE WHEN PARTY_ID = @PARTY_ID THEN 1
                                 ELSE 2
                            END
        FROM    DO_NOT_CALL WITH ( NOLOCK )
        WHERE   PARTY_ID IN ( @PARTY_ID, @additional_party_id, @Distributor_ID )
                AND GETDATE() BETWEEN FROM_DTM AND DATEADD(day, 1, THRU_DTM)

    END

GO
GRANT EXECUTE ON  [dbo].[DO_NOT_CALL_LOOKUP] TO [IDT_APPS]
GO
