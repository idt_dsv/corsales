SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*==================================================
Author: Tommy Harris
Create Date: 7/13/2011
Description: Gets all of the active organizations

Example:
EXEC dbo.GetActiveOrganizations

Permissions: GRANT EXECUTE ON dbo.GetActiveOrganizations to idt_apps
DROP: DROP dbo.GetActiveOrganizations

Modifications:
Who		Date		Modification Note
----------------------------------------
tharris	7/18/11		Added INC/BVBA filtering

====================================================*/

CREATE PROCEDURE [dbo].[GetActiveOrganizations]
	@Company INT
AS
BEGIN

	IF @Company = 0 --INC
		SELECT o.*
		FROM dbo.ORG o WITH (NOLOCK)
		JOIN dbo.PARTY p WITH (NOLOCK) ON o.PARTY_ID = p.PARTY_ID
		JOIN dbo.PARTY_STAT_TYPE pst WITH (NOLOCK) ON p.CURR_STAT_TYPE = pst.PARTY_STAT_TYPE_ID
		WHERE pst.PARTY_STAT_TYPE_ID IN (1,2,3,10,11,12,13,14) AND CLASS_TYPE_ID <> 7
	IF @Company = 1 --BVBA
		SELECT o.*
		FROM dbo.ORG o WITH (NOLOCK)
		JOIN dbo.PARTY p WITH (NOLOCK) ON o.PARTY_ID = p.PARTY_ID
		JOIN dbo.PARTY_STAT_TYPE pst WITH (NOLOCK) ON p.CURR_STAT_TYPE = pst.PARTY_STAT_TYPE_ID
		WHERE pst.PARTY_STAT_TYPE_ID IN (1,2,3,10,11,12,13,14) AND CLASS_TYPE_ID = 7
	
END
GO
GRANT EXECUTE ON  [dbo].[GetActiveOrganizations] TO [IDT_APPS]
GO
