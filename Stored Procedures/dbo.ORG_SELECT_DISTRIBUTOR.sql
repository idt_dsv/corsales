SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
Author: Bill Sorensen
Date: 1/9/2008
Notes: 
  Returns the distributor (if any) for an organization.
Usage:
  Execute dbo.ORG_SELECT_DISTRIBUTOR 52673
Permissions
  grant execute on dbo.ORG_SELECT_DISTRIBUTOR to idt_apps 
Revisions:
Date   Who  Notes
-----------------------------------------

*/
CREATE PROCEDURE [dbo].[ORG_SELECT_DISTRIBUTOR] 
	@ORG_PARTY_ID int
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @Now datetime;
	SET @Now = GETDATE();
	
	SELECT distributor.*
	FROM ORG distributor WITH (NOLOCK)
	JOIN PARTY_REL pr WITH (NOLOCK)
	  ON distributor.PARTY_ID = pr.PAR_PARTY_ID AND pr.PARTY_REL_TYPE = 8 -- Distributor of
	WHERE pr.CHILD_PARTY_ID = @ORG_PARTY_ID
    AND (pr.FROM_DT IS NULL OR pr.FROM_DT <= @Now)
    AND (pr.THRU_DT IS NULL OR pr.THRU_DT > @Now);

END

GO
GRANT EXECUTE ON  [dbo].[ORG_SELECT_DISTRIBUTOR] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[ORG_SELECT_DISTRIBUTOR] TO [IDT_Reporter]
GO
