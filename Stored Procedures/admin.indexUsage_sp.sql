SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: MDW
date: 7/1/08
Notes: Inserts a snapshot of the index usage data into IndexUsage table

example: 
  exec [admin].[indexUsage_sp]

  select * from  [admin].indexUsage 


Modifications
*/

create procedure [admin].[indexUsage_sp]
as 
    set nocount on

    insert  IndexUsage
            (
              [ObjectName],
              [IndexName],
              USER_SEEKS,
              USER_SCANS,
              USER_LOOKUPS,
              USER_UPDATES,
              IndexColumns,
              IncludedFields,
              Is_primary_key,
              IS_Clustered,
              Is_unique,
              Index_create_date,
              index_id,
              data_space_id,
              [ignore_dup_key],
              fill_factor,
              is_padded,
              is_disabled,
              is_hypothetical,
              [allow_row_locks],
              [allow_page_locks]
            )
            SELECT  ObjectName = OBJECT_NAME(S.[OBJECT_ID]),
                    IndexName = I.[NAME],
                    S.USER_SEEKS,
                    S.USER_SCANS,
                    S.USER_LOOKUPS,
                    S.USER_UPDATES,
                    IndexColumns = [admin].[indexFields_udf](i.name, 'Indexed'),
                    IncludedFields = [admin].[SortCsv_udf]([admin].[indexFields_udf](i.name, 'Included'), ','),
                    i.Is_primary_key,
                    IS_Clustered = INDEXPROPERTY(OBJECT_ID(object_name(i.[object_id])),
                                                 i.name, 'IsClustered'),
                    Is_unique = INDEXPROPERTY(OBJECT_ID(object_name(i.[object_id])),
                                              i.name, 'IsUnique'),
                    Index_Create_Date = ( select  create_date
                                    from    sys.objects o
                                    where   o.object_id = i.object_id
                                  ),
                    i.index_id,
                    i.data_space_id,
                    i.ignore_dup_key,
                    i.fill_factor,
                    i.is_padded,
                    i.is_disabled,
                    i.is_hypothetical,
                    i.allow_row_locks,
                    i.allow_page_locks
            FROM    SYS.DM_DB_INDEX_USAGE_STATS AS S
                    INNER JOIN SYS.INDEXES AS I ON I.[OBJECT_ID] = S.[OBJECT_ID]
                                                   AND I.INDEX_ID = S.INDEX_ID
            WHERE   OBJECTPROPERTY(S.[OBJECT_ID], 'IsUserTable') = 1
                    and i.name is not null
            ORDER BY OBJECT_NAME(S.[OBJECT_ID]),
                    indexColumns
            option (maxdop 1)

    set nocount off


GO
