SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Description:		Monitors the ord table to ensure billing address is being set.

--- Modified

-- =============================================

CREATE PROCEDURE [dbo].[MONITORING_ORD_BILLING_ADDR]
AS
    BEGIN
        DECLARE @CurrentTime DATETIME
        DECLARE @Recipients VARCHAR(50)
        DECLARE @NotificationRecipients VARCHAR(50)
        DECLARE @NoBillingAccountCount INT
        DECLARE @NoBillingAccountMinutes INT
        DECLARE @NoBillingAccountThreshold INT
        DECLARE @Message VARCHAR(MAX)

        SET @CurrentTime = GETDATE()
        SET @Recipients = 'helpdesk@idtdna.com'
        SET @NotificationRecipients = 'pshaff@idtdna.com'
        SET @NoBillingAccountMinutes = 30
        SET @NoBillingAccountThreshold = 0

        SET @NoBillingAccountCount = ( SELECT   COUNT(O.ORD_ID)
                                       FROM     Sales.dbo.ORD AS O WITH ( NOLOCK )
                                       WHERE    O.BILL_ACCT_ID = '-1'
                                                AND O.ORD_DATE < DATEADD(MI,
                                                              -@NoBillingAccountMinutes,
                                                              @CurrentTime)
                                     )

        IF @NoBillingAccountCount > @NoBillingAccountThreshold
            BEGIN
                SET @Message = 'This message indicates the number of orders with a -1 for a billing address ID is over the threshold of '
                    + CONVERT(VARCHAR(10), @NoBillingAccountThreshold) + '. '
                    + CHAR(13) + CHAR(13)
                    + 'Run this SQL to view the messages: ' + CHAR(13)
                    + CHAR(13) + 'SELECT	* ' + CHAR(13)
                    + 'FROM	Sales.dbo.ORD AS O WITH (NOLOCK)' + CHAR(13)
                    + 'WHERE	O.BILL_ACCT_ID = ''-1'' AND ' + CHAR(13)
                    + 'O.ORD_DATE < '''
                    + CAST(DATEADD(MI, -@NoBillingAccountMinutes, @CurrentTime) AS VARCHAR(25))
                    + ''''
	
                EXEC MSDB.DBO.SP_SEND_DBMAIL @PROFILE_NAME = 'IDTDNA',
                    @RECIPIENTS = @Recipients,
                    @SUBJECT = 'Orders with no billing address found',
                    @BODY = @Message
            END

    END
GO
