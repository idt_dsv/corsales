SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*  
Author: BTV  
Date: 3/10/05  
Notes: Moved to a stored procedure for performance.  
Example:  
exec dbo.Invoice_Report_Summary 'CUS00096', '2/01/2004', '2/28/2004'  
  
Permissions:  
  grant execute on Invoice_Report_Summary to idt_apps  
  
Revisions:  
08/07/2007	Mark Schebel	use invoice_total_natural where appropriate  
12/11/2008	Mark Schebel	above change implemented in Inc   
02/20/2009	Mark Schebel	Use Paid_flag instead of checking invoice amount  
07/31/2009	Mark Schebel	Adding optional PO Number parameter  
04/15/2010	Mark Schebel	Removing electronic invoices from statement  
02/28/2013	Eric Borman		Adding Package Ids to the result set  
04/12/2013	Eric Borman		Added field unpaidBalance  
04/23/2013	Eric Borman		Changed to not convert unpaidbalace - leave as natural currency – as the invoice total is always in the natural currency  
08/23/2013	Michael Beekman	Added RemitToId to results
11/05/2013	Drew Pittman	Rounding INV_TOTAL value.  Resolved coding standard issues.

*/

CREATE PROCEDURE [dbo].[INVOICE_REPORT_SUMMARY]
(
 @BILL_ACCT_ID VARCHAR(10), -- The billing account to print the summary  
 @FROM_DT DATETIME,			-- The start date for the summary            
 @TO_DT DATETIME,			-- The end date for the summary    
 @PAY_AUTH_NBR VARCHAR(50) = ''
)
AS
BEGIN
	SET NOCOUNT ON 
	
	EXEC EPICOR_INTEGRATION.DBO.EPICOR_AR_INVOICE_SUMMARY_UPDATE

	SELECT	IH.INV_NBR,
			P.LAST_NM + ', ' + P.FIRST_NM
								AS [FULL_NM],
			IH.PAY_AUTH_NBR		AS [PAY_AUTH_NBR],
			IH.ORD_DT			AS [ORD_DT],
			IH.INV_DT			AS [INV_DT],
			OM.ORD_METH_CD		AS [ORD_METH_CD],
			(	SELECT	COUNT( REF_NBR )
				FROM	DBO.INV_LINE_ITEMS AS ili WITH (NOLOCK)
				WHERE	REF_NBR IS NOT NULL AND 
						BATCH_ORD_ID = IH.BATCH_ORD_ID ) 
								AS [OLIGOS],
			CASE
				WHEN INV_TOTAL_NATURAL IS NULL OR INV_TOTAL_NATURAL < 0.00 
					THEN ROUND(COALESCE( DBO.GET_INVOICE_TOTAL_NATURAL_CURRENCY( IH.INV_NBR ), 0.00 ), c2.NumDigits)
				ELSE ROUND(INV_TOTAL_NATURAL, c2.NumDigits)
			END					AS [INV_TOTAL],
			CUSTOMER_NAME,
			ADDR2,
			ADDR3,
			CITY,
			STATE,
			POSTAL_CODE,
			GLC.[DESCRIPTION]	AS [DESCRIPTION],
			@FROM_DT			AS [FROM_DT],
			@TO_DT				AS [TO_DT],
			IH.SALES_ORD_NBR	AS [SALES_ORD_NBR],
			IH.BILL_ACCT_ID		AS [BILL_ACCT_ID],
			C.NAT_CUR_CODE		AS [NAT_CUR_CODE],
			IH.CURRENCY_ID		AS [CURRENCY_ID],
			DBO.GETPACKAGEIDSFORSALESORDERNBR( IH.SALES_ORD_NBR )
								AS [PACKAGEIDS],
			DBO.CURRENCY_NATURAL_CONVERSION( COALESCE( INVOICE.UNPAID_BALANCE, 0.00 ), 1 / IH.EXCHANGE_RATE, 1 ) 
				--Stored in natural currency in Epicor CONVERT(MONEY, NULL)
								AS [UNPAID_BALANCE],
			IH.REMITTOID,
			RT.ADDRESS
	FROM      DBO.INV_HDR AS IH WITH (NOLOCK)
	JOIN	  DBO.CURRENCY AS c2 WITH (NOLOCK) ON IH.CURRENCY_ID = c2.CURRENCY_ID
	LEFT JOIN DBO.PERSON AS P WITH (NOLOCK) ON IH.ENDUSER_NBR = P.ENDUSER_NBR
	LEFT JOIN DBO.ORD AS O WITH (NOLOCK) ON IH.SALES_ORD_NBR = O.SALES_ORD_NBR
	LEFT JOIN DBO.ORD_METH AS OM WITH (NOLOCK) ON O.ORD_METH_ID = OM.ORD_METH_ID
	LEFT JOIN DBO.EPICOR_ARCUST_VW AS C WITH (NOLOCK) ON IH.BILL_ACCT_ID = C.CUSTOMER_CODE
	LEFT JOIN DBO.EPICOR_ARIN1PST_VW AS INVOICE WITH (NOLOCK) ON INVOICE.DOC_CTRL_NUM = CAST( IH.INV_NBR AS VARCHAR( 16 ))
	LEFT JOIN EPICOR.IDT_PROD.DBO.GL_COUNTRY AS GLC WITH (NOLOCK) ON GLC.COUNTRY_CODE = C.COUNTRY_CODE
	LEFT JOIN REFERENCE.DBO.REMITTO AS RT WITH (NOLOCK) ON RT.REMITTOID = IH.REMITTOID
	WHERE     INV_DT BETWEEN @FROM_DT AND @TO_DT
		 AND IH.BILL_ACCT_ID = @BILL_ACCT_ID
		 AND INVOICE.[Paid_flag] = 0
		 AND IH.[DELIVERY_METHOD_ID] <> 4
		 AND ( ( @PAY_AUTH_NBR = '' ) OR ( IH.PAY_AUTH_NBR = @PAY_AUTH_NBR ) )
	ORDER BY IH.CURRENCY_ID, IH.BILL_ACCT_ID, IH.INV_NBR

	SET NOCOUNT OFF
END 
GO
GRANT EXECUTE ON  [dbo].[INVOICE_REPORT_SUMMARY] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[INVOICE_REPORT_SUMMARY] TO [IDT_Reporter]
GO
