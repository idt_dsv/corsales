SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO


CREATE  PROCEDURE [dbo].[MSDescr_Set_FieldDescription]
(@TableName varchar(128), @fieldName varchar(128), @Descr varchar(256))
AS
  DECLARE @SqlStr Nvarchar(256), @RowCount int, @Err int   

create table  #tmp_tbl (Description sql_variant)

insert into #tmp_Tbl
exec MSDescr_GET_FieldDescription @TableName, @FieldName

Set @RowCount = @@rowcount

if @RowCount = 0  
  set @SqlStr = N'EXECUTE sp_addextendedproperty N''MS_Description'', '''+ @Descr +''' , N''user'', N''dbo'', N''table'', '+@TableName + ' , ''column'', ''' + @fieldname +''''
else
  set @SqlStr = N'EXECUTE sp_updateextendedproperty N''MS_Description'', '''+  @Descr +''' , N''user'', N''dbo'', N''table'', '+@TableName + ' , ''column'', ''' + @fieldname +''''

--select @sqlStr
execute sp_executesql @SqlStr

Set @Err = @@ERROR
return @Err

GO
GRANT EXECUTE ON  [dbo].[MSDescr_Set_FieldDescription] TO [IDT_APPS]
GO
