SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/* [GetBioRadSynthetics]
Author: Tony DeJaynes/Carl Zauche
Date: 6/19/2012
Notes: -Retrieves data for a bio rad order
Determines how many synthetic DNA/RNA are attached to an order for process validation

Permissions:
grant execute on [GetBioRadSynthetics] to IDT_APPS
Execute [GetBioRadSynthetics] 7554930, 60


Modifications:
Date	Who	Notes
-------------------------------------
					new parts
*/
CREATE  PROCEDURE [dbo].[GetBioRadSynthetics]
	@SalesId INT, @BioRadLineNbr INT
AS
BEGIN

	SELECT sales_ord_nbr, 
	       BioRadLineNbr, 
	       COUNT(*) AS SyntheticsFound
		FROM 
			(SELECT o.SALES_ORD_NBR, 
			        lirnes.DATA.value('(//Extrinsic[@name="BioRadLineNbr"])[1]', 'nvarchar(100)') AS BioRadLineNbr
			 FROM sales.dbo.ord AS o WITH (NOLOCK) 
			 INNER JOIN sales.dbo.ORD_LINE_ITEM AS oli WITH (NOLOCK) 
			    ON o.ORD_ID = oli.ORD_ID
			 LEFT JOIN sales.dbo.LINE_ITEM_REF_NBR AS lirn WITH (NOLOCK) 
			    ON oli.LINE_ITEM_ID = lirn.LINE_ITEM_ID
			 INNER JOIN sales.dbo.LINE_ITEM_REF_NBR_EXT_SPEC AS lirnes WITH (NOLOCK) 
			    ON lirnes.REF_ID = lirn.REF_ID
			 WHERE o.SALES_ORD_NBR = @SalesId
			 AND oli.LEV = 1 
			 AND oli.OLI_PROD_ID IN (7596, 5729))  AS A -- synthetic DNA & RNA, attached to plate
	WHERE BioRadLineNbr = @BioRadLineNbr
	GROUP BY SALES_ORD_NBR, BioRadLIneNbr
	
END
GO
GRANT EXECUTE ON  [dbo].[GetBioRadSynthetics] TO [IDT_APPS]
GO
