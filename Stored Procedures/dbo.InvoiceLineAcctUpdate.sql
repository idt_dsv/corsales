SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
Author: BSchafbuch
Date: 8/9/03
Notes:
  Updates GL Accounts on for posting into Epic'r

Modifications
Other History....
WHEN		WHO		BL#			WHAT
8/7/09		Bob	S	BL-3513		Move intercompany assignment to last statement
9/3/09		Bob	S	BL-3815		Remove Par Grp or Ref # Condition for Intercompany revenue.
1/27/11		Alan C	US-6537		For intercompany credit now uses function to get account.	
05/17/2011	TTL		BL-8037		Modify intercompany GL account for Inc revenue from 4002 to 5106
09/30/2011	TTL		BL-8947		Reset GL account on Australia invoices to 2257
1/29/2013   Bob S   BL-3		Added support for inter-co Shipping, Credits
11/25/2013  Bob S				Added other Interco and Dicerna (WO).  Changed interco Back to 4002 from 5106
18-DEC-2013	Bob S				Added Additional Customer Record for INC.
*/

CREATE PROCEDURE [dbo].[InvoiceLineAcctUpdate]
  @INV_NBR INTEGER
, @REF_NBR INTEGER
, @ORG_NBR INTEGER 

AS

DECLARE  
  @SalesAccount varchar(50)
, @PAR_GRPID int
, @BATCH_ORDID int
, @COMPANY_ID int
, @SHIP_STATE varchar(20)
, @bill_acct_id varchar(8)
, @branch_location_id int
, @depart_id int
, @exempt bit
, @cntry_id varchar(32)

--initialize
set @salesaccount = ''
set @PAR_GRPID = 0
set @BATCH_ORDID = 0
set @Bill_Acct_ID = ''
set @branch_location_id = 0
set @depart_id = 0
set @cntry_id = ''

select @SHIP_STATE = 
  ( select ship_st
    from dbo.inv_hdr
    where inv_nbr = @INV_NBR
  )

SELECT @CNTRY_ID = CNTRY_ID
FROM dbo.CNTRY
WHERE CNTRY_SHRT_NM = 
  ( select ship_cntry 
    from dbo.inv_hdr 
    where inv_nbr = @INV_NBR
  )

--Get Sales Account and Par_Grp
select
  @PAR_GRPID = par_grp
, @SalesAccount = sales_acct
, @BATCH_ORDID = ih.batch_ord_id
, @branch_location_id = lirn.BRANCH_LOCATION_ID
, @Bill_Acct_ID = ih.BILL_ACCT_ID 
from dbo.inv_hdr AS ih
  join dbo.inv_line_items AS ili 
    on ili.batch_ord_id = ih.batch_ord_id
  join dbo.line_item_ref_nbr AS lirn 
    on lirn.ref_id = ili.ref_nbr
  join dbo.depart_branch_location AS dbl 
    on dbl.depart_id = lirn.dept_id
    and dbl.branch_location_id = lirn.branch_location_id
where ih.inv_nbr = @INV_NBR
  and ili.ref_nbr = @ref_nbr
  and ili.line_item_type = 1

set @depart_id = 0

select @exempt =
  case
    when exists 
      ( select * 
        from dbo.inv_line_items 
        where batch_ord_id = @BATCH_ORDID 
          and line_item_type = 3 
          and unit_price > 0
      )
      then 0
    else 1
  end

select @BATCH_ORDID = batch_ord_id 
from dbo.INV_HDR 
where INV_NBR = @INV_NBR
------------------Update GL Account for the Invoice Lines

/*Update Product Records*/
UPDATE dbo.INV_LINE_ITEMS
  SET dbo.INV_LINE_ITEMS.ACCT_ID = @SalesAccount
where dbo.INV_LINE_ITEMS.batch_ord_id = @BATCH_ORDID
  and dbo.INV_LINE_ITEMS.PAR_GRP = @PAR_GRPID

/*OLIGO CARD ACCOUNT UPDATE*/

UPDATE dbo.INV_LINE_ITEMS
  SET  ACCT_ID = '2439000000000'
WHERE line_item_type = 1
  and BATCH_ORD_ID = @BATCH_ORDID
  AND PROD_DESC LIKE 'OLIGOCARD%'

/*Update Product Lines for Orders from Molecular Biology Employees in IDT-Inc.
to proper GL Account*/

UPDATE dbo.INV_LINE_ITEMS
  SET ACCT_ID = '4001820000000'
From dbo.INV_LINE_ITEMS AS ili
  join  dbo.INV_HDR AS IH 
    on ili.batch_ord_id = ih.batch_ord_id 
Where ili.PAR_GRP = @PAR_GRPID
 and ih.batch_ord_id = @BATCH_ORDID
 and ih.org_nbr = 3639


/*Update Product Records for Purification Dept. work*/

UPDATE dbo.INV_LINE_ITEMS
  SET ACCT_ID = SALES_ACCT
FROM dbo.INV_HDR AS IH
, INV_LINE_ITEMS AS ILI
, DEPART AS D
, PRODUCT AS P
WHERE IH.BATCH_ORD_ID = ILI.BATCH_ORD_ID
  AND CAST(D.DEPART_DESC AS VARCHAR(6)) = P.COL_DISP
  and P.PROD_NM = ILI.PROD_DESC
  AND P.PROD_TYPE_ID = 2    
  and ili.line_item_type = 1
  AND AMOUNT > 0
  and exists (select * from dbo.prod_cat_class where cat_id = 2)
  AND IH.BATCH_ORD_ID = @BATCH_ORDID


/*Update Shipping Records*/

UPDATE dbo.INV_LINE_ITEMS
  SET ACCT_ID = '4080000000000'
from dbo.inv_line_items AS ili
  join dbo.inv_hdr AS ih 
    on ih.batch_ord_id = ili.batch_ord_id
where line_item_type = 2
  and ili.BATCH_ORD_ID = @BATCH_ORDID
  and bill_acct_id not in ('CUS00012','CUS05250','CUS06403')

/*SALES TAX ACCOUNT UPDATE*/

UPDATE dbo.INV_LINE_ITEMS
  SET ACCT_ID = 
    ( select receivables_account
      from taxes.dbo.tax_state_config AS tc
      where tc.state_cd = @SHIP_STATE
    )
WHERE LINE_ITEM_TYPE = 3
  and BATCH_ORD_ID = @BATCH_ORDID
	
/*DISCRETIONARY CREDITS ACCOUNT UPDATE*/
		
UPDATE dbo.INV_LINE_ITEMS
  SET  ACCT_ID = '4100000000000'
from dbo.inv_line_items AS ili
  join dbo.inv_hdr AS ih 
    on ih.batch_ord_id = ili.batch_ord_id
WHERE LINE_ITEM_TYPE = 4
  and ili.BATCH_ORD_ID = @BATCH_ORDID
  and bill_acct_id not in ('CUS00012','CUS05250','CUS06403')

		
--Interco to PTE
UPDATE dbo.INV_LINE_ITEMS
  SET ACCT_ID = '4004000000000'		
From dbo.INV_LINE_ITEMS AS ili
  join  dbo.INV_HDR as IH 
    on ili.batch_ord_id = ih.batch_ord_id 
Where ih.bill_acct_id in ('CUS05250','CUS06403')
  and line_item_type = 1
  and ili.BATCH_ORD_ID = @BATCH_ORDID 

UPDATE dbo.INV_LINE_ITEMS
  SET ACCT_ID = '5431000000000'
from dbo.inv_line_items AS ili
  join dbo.inv_hdr AS ih 
    on ih.batch_ord_id = ili.batch_ord_id
where line_item_type = 2
  and ili.BATCH_ORD_ID = @BATCH_ORDID
  and bill_acct_id in ('CUS05250','CUS06403')
	
UPDATE dbo.INV_LINE_ITEMS
  SET ACCT_ID = '4104000000000'		
From dbo.INV_LINE_ITEMS AS ili
  join  dbo.INV_HDR AS IH 
    on ili.batch_ord_id = ih.batch_ord_id 
Where ih.bill_acct_id in ('CUS05250','CUS06403')
  and line_item_type = 4
  and ili.BATCH_ORD_ID = @BATCH_ORDID 

--Interco to BVBA
UPDATE dbo.INV_LINE_ITEMS
  SET ACCT_ID = '4002000000000'		
From dbo.INV_LINE_ITEMS AS ili
  join  dbo.INV_HDR AS IH 
    on ili.batch_ord_id = ih.batch_ord_id 
Where ih.bill_acct_id = 'CUS00012'
  and line_item_type = 1
  and ili.BATCH_ORD_ID = @BATCH_ORDID 

UPDATE dbo.INV_LINE_ITEMS
  SET ACCT_ID = '5434000000000'
from dbo.inv_line_items AS ili
  join dbo.inv_hdr AS ih 
    on ih.batch_ord_id = ili.batch_ord_id
where line_item_type = 2
  and ili.BATCH_ORD_ID = @BATCH_ORDID
  and bill_acct_id = 'CUS00012'

UPDATE dbo.INV_LINE_ITEMS
  SET ACCT_ID = '4101000000000'		
From dbo.INV_LINE_ITEMS AS ili
  join  dbo.INV_HDR AS IH 
    on ili.batch_ord_id = ih.batch_ord_id 
Where ih.bill_acct_id = 'CUS00012'
  and line_item_type = 4
  and ili.BATCH_ORD_ID = @BATCH_ORDID 

--Dicerna
UPDATE dbo.INV_LINE_ITEMS
  SET ACCT_ID = '7010000000000'		
From dbo.INV_LINE_ITEMS AS ili
  join  dbo.INV_HDR AS IH 
    on ili.batch_ord_id = ih.batch_ord_id 
Where ih.bill_acct_id = 'CUS06473'
and line_item_type = 1
and ili.BATCH_ORD_ID = @BATCH_ORDID 


SET ANSI_NULLS OFF



GO
GRANT EXECUTE ON  [dbo].[InvoiceLineAcctUpdate] TO [IDT_APPS]
GO
