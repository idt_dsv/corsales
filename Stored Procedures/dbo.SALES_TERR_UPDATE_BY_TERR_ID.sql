SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/**********************************************
 SALES_TERR_UPDATE_BY_TERR_ID
 Author:NLaikhter
 Date:10/24/03
 Description: Updates SALES_TERR_NM,SALES_TERR_DESC,SALES_REGION_ID  
              for Sales_Terr_ID in SALES_TERR table 
 How to call stored Procedure: you need to provide all the values,
           regarding of whther you need to update that value or not

select * from sales_terr where sales_terr_id = 18
begin tran
 Exec dbo.[SALES_TERR_UPDATE_BY_TERR_ID] @Sales_terr_id = 18 
 ,@SALES_TERR_NM = 'Eastern Canada - Quebec & Maritimes'
 ,@SALES_TERR_DESC = 'Quebec and the Maritimes '
 ,@SALES_REGION_ID = 11
 ,@NETWORK_LOGIN_NM = 'sberardi'
select * from sales_terr where sales_terr_id = 18

 select * from salesTerritory where territoryType = 'territory' and TerritoryId = 18
 select * from salesTerritoryManager stm join employee e on e.hr_employee_id = stm.hr_employee_id 
 where SalesTerritoryID = (select SalesTerritoryID from salesTerritory where TerritoryId = 18 and territoryType = 'territory' )
 IF @@TRANCOUNT > 0 
   rollback

grant execute on [SALES_TERR_UPDATE_BY_TERR_ID] to idt_apps, idt_reporter
 
 Revision:
4/18/05 NL Added ability to update NETWORK_LOGIN_NM field
11/30/07 MDW updated to use new SalesTerritory tables and allows multiple users as network_login_nm
**********************************************/




CREATE   PROCEDURE [dbo].[SALES_TERR_UPDATE_BY_TERR_ID]
               ( @SALES_TERR_ID INT
               , @SALES_TERR_NM VARCHAR(50)
               , @SALES_TERR_DESC VARCHAR(1000)
               , @SALES_REGION_ID INT
               , @NETWORK_LOGIN_NM VARCHAR(1000)
               )
AS
BEGIN 
DECLARE @RowCount INT, @HR_EMPLOYEE_ID_List VARCHAR(1000), @RegionSalesTerritoryID INT, @ErrorMsg nvarchar(128), @SalesTerritoryID INT, @err int

SELECT @RowCount = -1, @HR_EMPLOYEE_ID_List = '', @RegionSalesTerritoryID= Null , @ErrorMsg = ''

--Verify data
SELECT @SalesTerritoryID = SalesTerritoryID FROM sales.dbo.salesTerritory WHERE TerritoryType = 'Territory' AND TerritoryId = @SALES_TERR_ID
IF @SalesTerritoryID IS NULL
BEGIN
  SET @ErrorMsg = @ErrorMsg + ' Sales Territory ID "' + CAST(@SALES_TERR_ID AS VARCHAR(100)) + '" does not exist;'
  goto ErrorHandler
END 

SELECT @HR_EMPLOYEE_ID_List = CAST(e.HR_EMPLOYEE_ID AS VARCHAR(10)) + ',' + @HR_EMPLOYEE_ID_List
FROM reference.dbo.employee e
  JOIN dbo.breakApartStr(@NETWORK_LOGIN_NM) nm ON e.network_login_nm = nm.StrValue 
WHERE e.active = 1

SELECT @HR_EMPLOYEE_ID_List = CASE WHEN (LEN(@HR_EMPLOYEE_ID_List) >= 1) THEN LEFT(@HR_EMPLOYEE_ID_List, LEN(@HR_EMPLOYEE_ID_List)-1) ELSE @HR_EMPLOYEE_ID_List END 

PRINT @HR_EMPLOYEE_ID_List

IF @HR_EMPLOYEE_ID_List IS NULL OR @HR_EMPLOYEE_ID_List = ''
BEGIN
  SET @ErrorMsg = @ErrorMsg + ' Network Login "' + @NETWORK_LOGIN_NM + '" does not exist;'
  goto ErrorHandler
END 

SELECT @RegionSalesTerritoryID = SalesTerritoryID FROM sales.dbo.salesTerritory WHERE TerritoryType = 'Region' AND TerritoryId = CAST(@SALES_REGION_ID AS INT)
IF @RegionSalesTerritoryID IS NULL
BEGIN
  SET @ErrorMsg = @ErrorMsg + ' Sales Region ID "' + CAST(@SALES_REGION_ID AS VARCHAR(100)) + '" does not exist;'
  goto ErrorHandler
END 



--UPDATE data
BEGIN TRAN 

  UPDATE SalesTerritory
  SET [Name] = @SALES_TERR_NM,
      Description = @SALES_TERR_DESC,
      ParentSalesTerritoryID = @RegionSalesTerritoryID
  WHERE TerritoryType = 'Territory' AND TerritoryId = @SALES_TERR_ID
  SELECT @RowCount = @@ROWCOUNT, @err=@@Error
  IF @RowCount = 0 OR @err <> 0
  BEGIN
    GOTO Errorhandler
  end
  -- remove old names
  DELETE SalesTerritoryManager
  WHERE  SalesTerritoryId = @SalesTerritoryID

  SELECT @SalesTerritoryID,  CAST(StrValue AS INT)
  FROM dbo.breakApartStr(@HR_EMPLOYEE_ID_List)

  INSERT SalesTerritoryManager (SalesTerritoryID, HR_EMPLOYEE_ID)
  SELECT @SalesTerritoryID,  CAST(StrValue AS INT)
  FROM dbo.breakApartStr(@HR_EMPLOYEE_ID_List)
  
  SELECT @RowCount = @@ROWCOUNT, @err=@@Error
  IF @RowCount = 0 OR @err <> 0
  BEGIN
    GOTO Errorhandler
  END
  
IF @@TRANCOUNT > 0
  COMMIT

PRINT 'Data was inserted into SALES_TERR'
RETURN 0

ErrorHandler:
 IF @@TRANCOUNT > 0 
   rollback
 SET @ErrorMsg = N'No records updated - ' + @ErrorMsg
 raiserror (@ErrorMsg,16,1)
 RETURN -1
END 






GO
GRANT EXECUTE ON  [dbo].[SALES_TERR_UPDATE_BY_TERR_ID] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[SALES_TERR_UPDATE_BY_TERR_ID] TO [IDT_Reporter]
GO
