SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
Author: MDW
Date: 3/4/03
Notes
  returns table for intranet to return the count of oligos sold by department for current day


grant execute on [DAILY_OLIGO_COUNT_SELECT] to idt_reporter, idt_webapps

 
Revisions

*/

create   procedure [dbo].[DAILY_OLIGO_COUNT_SELECT]
as 
select * 
from dbo.oligo_count (nolock)
order by department






GO
GRANT EXECUTE ON  [dbo].[DAILY_OLIGO_COUNT_SELECT] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[DAILY_OLIGO_COUNT_SELECT] TO [IDT_Reporter]
GRANT EXECUTE ON  [dbo].[DAILY_OLIGO_COUNT_SELECT] TO [IDT_WebApps]
GO
