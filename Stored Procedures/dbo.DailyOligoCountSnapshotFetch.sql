SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
AUTHOR: Jim Borland
DATE: 09/09/08
NOTES
  Returns table for intranet to return the count of oligos sold by branch and the manufacturing site/department
	assigned to make for current day.

PERMISSION:
	GRANT EXECUTE ON [DailyOligoCountSnapshotFetch] TO IDT_WEBAPPS, IDT_REPORTER, IDT_APPS
 
REVISIONS:
*/

CREATE   PROCEDURE [dbo].[DailyOligoCountSnapshotFetch]
AS 
SELECT *
FROM SALES.DBO.DailyOligoCountSnapshot WITH ( NOLOCK )
ORDER BY SalesBranch ,RecordType ,MfgBranch ,Department
GO
GRANT EXECUTE ON  [dbo].[DailyOligoCountSnapshotFetch] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[DailyOligoCountSnapshotFetch] TO [IDT_Reporter]
GRANT EXECUTE ON  [dbo].[DailyOligoCountSnapshotFetch] TO [IDT_WebApps]
GO
