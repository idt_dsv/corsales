SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: Eric Borman
Date: 10/19/2008
Notes: selects the next number of items in the queue and selectecs them
Usage : EXEC dbo.QUEUE_SELECT_AND_CLAIM_ORDER_TRANSFORM_ITEMS 1, 401, 63, 1330, -99

Permission: grant execute on DBO.QUEUE_SELECT_AND_CLAIM_ORDER_TRANSFORM_ITEMS to IDT_Apps 

Revisions:
Date   Who  Notes
-----------------------------------------
*/
Create  PROCEDURE [dbo].[QUEUE_SELECT_AND_CLAIM_ORDER_TRANSFORM_ITEMS] (
	@QueueKeyType int,
	@QueueKeyId int, 
	@BfBobjectClassId int, 
	@ClaimingWorkstationId int, 
    @ApplicationId int  )
AS 
BEGIN 
	DECLARE @View INT		SET @View = 8 --MyClaimed and Unclaimed items
	DECLARE @SelectedIds TABLE (QUEUE_ITEM_ID INT)

	BEGIN TRANSACTION ClaimItems
	
    -- the function dbo.load_queue_ORDER_TRANSFORM limits the number of records to 10
	INSERT INTO @SelectedIds
	SELECT top 100  itm.QUEUE_ITEM_ID
	FROM QUEUE_ITEM_ORDER_TRANSFORM Itm		
		JOIN dbo.load_queue_ORDER_TRANSFORM(@QueueKeyType, @QueueKeyId, @BfBobjectClassId, @View, @ClaimingWorkstationId, @ApplicationId) m ON m.QUEUE_ITEM_ID = Itm.QUEUE_ITEM_ID 
	Order by Itm.priority asc, Itm.Queue_Item_ID ASC
	
	-- Create a comma delimited string of all the selected ids	
	DECLARE @QItemIds varchar(MAX) 	SET @QItemIds = '' 
	
	SELECT  @QItemIds = @QItemIds + Convert(varchar(10), QUEUE_ITEM_ID) + ','
	FROM @SelectedIds

	DECLARE @ItemCount INT SET @ItemCount = 0
	
	SELECT @ItemCount = COUNT(*)
	FROM @SelectedIds

	IF @ItemCount > 0
	BEGIN	
		-- remove the last comma from the list
		SELECT @QItemIds = Left(@QItemIds, Len(@QItemIds) -1) 

		DECLARE @ClaimedItems TABLE (
			QUEUE_ITEM_ID INT,
			WorkStationClaimedID INT,
			ApplicationId INT,	
			ClaimedDateTime	DATETIME,
			CompleatedDateTime DATETIME
			)
		
		-- claim the items in the queue
		INSERT INTO @ClaimedItems Exec dbo.QUEUE_GET_CLAIM_CHECK_MULTIPLE_ORDER_TRANSFORM @QItemIds, @ItemCount, @ClaimingWorkstationId, @ApplicationId
		
		DECLARE @ClaimedCount INT SET @ClaimedCount = 0
		
		SELECT @ClaimedCount = COUNT(*)
		FROM @ClaimedItems c
	END	
		
	IF @@ERROR <> 0 OR @ClaimedCount <> @ItemCount   
	BEGIN       
		IF @@TRANCOUNT > 0 
			ROLLBACK TRANSACTION ClaimItems   
	END
	ELSE   
		COMMIT TRANSACTION ClaimItems 
		
	
	-- return the claimed queue items	
	SELECT   
		Itm.QUEUE_ITEM_ID,
		Itm.ENTERED_DTM            as EnteredDateTime,   
		Itm.ACTIVE_DTM             as ActiveDateTime, 
		Itm.BF_BOBJECT_CID         as ObjectClassID,    
		Itm.BF_BOBJECT_OID         as ObjectInstanceID,     
		Itm.QUEUE_DEFINITION_ID    as QueueEventID,   
		Itm.CLAIMED_WORKSTATION_ID as WorkStationClaimedID,         
		Itm.APPLICATION_ID         as ApplicationId,      
		Itm.CLAIMED_DTM            as ClaimedDateTime,      
		Itm.COMPLETED_DTM          as CompletedDateTime,   
		Itm.PRIORITY               as Priority, 
		Itm.ACTIVE                 as Active
	FROM QUEUE_ITEM_ORDER_TRANSFORM Itm
		JOIN @SelectedIds si ON Itm.QUEUE_ITEM_ID = si.QUEUE_ITEM_ID		
END
GO
GRANT EXECUTE ON  [dbo].[QUEUE_SELECT_AND_CLAIM_ORDER_TRANSFORM_ITEMS] TO [IDT_APPS]
GO
