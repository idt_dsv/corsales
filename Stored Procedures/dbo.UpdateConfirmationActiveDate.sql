SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
GRANT  EXECUTE  ON [dbo].[UpdateConfirmationActiveDate] TO [IDT-Coralville\iusr_idt]
GRANT  EXECUTE  ON [dbo].[UpdateConfirmationActiveDate] TO [coliseum]
GRANT  EXECUTE  ON [dbo].[UpdateConfirmationActiveDate] TO [AppSalesSin]
GRANT  EXECUTE  ON [dbo].[UpdateConfirmationActiveDate] TO [AppManufacturingSin]

Author: Phil Shaff
Date Created: May 29th, 2014
Purpose:

	Updates confirmation e-mail Activation date

Modifications:
Who       Date        Note
---------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[UpdateConfirmationActiveDate]
@ConfirmationId int,
@ActiveDate DateTime

AS

UPDATE sales.dbo.confirmation
SET    ACTIVE_DTM = @ActiveDate
WHERE  COALESCE(status, 0) = 0
       AND con_id = @ConfirmationId  
	   AND claimed_dtm IS NULL
	   AND CLAIMED_WORKSTATION_ID IS NULL

GO
GRANT EXECUTE ON  [dbo].[UpdateConfirmationActiveDate] TO [coliseum]
GRANT EXECUTE ON  [dbo].[UpdateConfirmationActiveDate] TO [IDT-CORALVILLE\IUSR_IDT]
GO
