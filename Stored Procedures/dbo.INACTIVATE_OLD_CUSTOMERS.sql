SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO


/*******************************************************/
/*	                                                 */
/*	    Author         Unknown                          */
/*	    Creation Date   4/17/01                     */
/*	    Comments:                                  

example:
 exec sales.dbo.[INACTIVATE_OLD_CUSTOMERS] 365

 - MDW  and NL- 7/2/02 - added logic to check on active customers;
                                          if customers are inactive, do not insert into 
                                          PARTY_STAT_HIST table
 - MAS 4/22/2004 - Removed status of 10 (potential) from queries
 - MDW 6/25/07   - Added section to set person records where org_id =0 or acc_id = 0. Per Jen and Candace WO#: 40749
 - MDW 11/20/07  - Added restriction of login to 25 characters to match table. Also changed update trigger on party_history 
 */
/*******************************************************/

Create PROCEDURE [dbo].[INACTIVATE_OLD_CUSTOMERS]
(@DAYS_SINCE INTEGER)
 AS

BEGIN

  --get all active users in this temp table
  CREATE TABLE #TMP_Active_ENDUSERS
  ( PARTY_ID INTEGER )
  
  INSERT #TMP_Active_ENDUSERS
  SELECT DISTINCT ENDUSER_ID
  FROM dbo.ORD (nolock)
    join dbo.party on ord.enduser_id = party.party_id 
      and party.curr_stat_type in (1)  
  WHERE ORD_DATE >= GETDATE() - @DAYS_SINCE
  
  
  INSERT #TMP_Active_ENDUSERS
  SELECT DISTINCT ENDUSER_ID 
  FROM dbo.CUST_CALL
    join dbo.party on cust_call.enduser_id = party.party_id 
      and party.curr_stat_type in (1) 
  WHERE CALL_DT >= GETDATE() - @DAYS_SINCE
  
  --Set all those not in the temp table as inactive
  INSERT dbo.PARTY_STAT_HIST
  (PARTY_ID, PARTY_STAT_TYPE, FROM_DT, CREATE_USER, REASON_TXT)
  SELECT PERSON.PARTY_ID, PARTY_STAT_TYPE = 2, FROM_DT = GETDATE(), CREATE_USER = CAST(RIGHT(SUSER_SNAME(),25) AS VARCHAR(25)), REASON_TXT = 'AUTOMATICALLY SETTING TO INACTIVE' 
  FROM dbo.PERSON
    join dbo.party on person.party_id = party.party_id 
      and party.curr_stat_type in (1) 
  WHERE person.party_id not in (select party_id from #TMP_Active_ENDUSERS)

  INSERT dbo.PARTY_STAT_HIST
  (PARTY_ID, PARTY_STAT_TYPE, FROM_DT, CREATE_USER, REASON_TXT)
  SELECT person.party_id, 8, getdate(), CAST(RIGHT(SUSER_SNAME(),25) AS VARCHAR(25)), 'AUTOMATICALLY SETTING TO INACTIVE-Error' 
  FROM person
  JOIN party ON person.party_id = party.party_id
    AND party.curr_stat_type <> 8
  WHERE person.org_id = 0 OR acc_id = 0
  AND NOT EXISTS(SELECT * FROM ord WHERE ord.enduser_id = person.party_id)


END

GO
