SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: Chris A. Sailor
Date Created: April 23, 2004
Purpose:

  This procedure is intended to retrieve the services associated
with a specified reference ID

Modifications:
Who     Date      Modification  Note
--------------------------------------------------------------
CAS     04/23/04  Chris Sailor  Created
CAS     03/20/07  Chris Sailor  Moved out of Manufacturing database
*/
Create Procedure [dbo].[YieldGuar_2_Aux_GetServicesForRefID]
	@Ref_ID Int
As
	set nocount on
	select		-- LIRN.Ref_ID,
			-- LIRN.Line_Item_ID,
			-- LIRN.LIRN_Prod_ID,
			-- OLI.Line_Item_ID,
			-- OLI.Par_Line_Item,
			-- OLI.Line_Item_Desc,
			Prod_ID = OLI.OLI_Prod_ID,
			OLI.Quantity
	From		dbo.Line_Item_Ref_Nbr LIRN (nolock)
	Join		dbo.Ord_Line_Item OLI (nolock)		On	LIRN.Line_Item_ID = OLI.Par_Line_Item
	Join		dbo.Prod_Cat_Class PCC (nolock)		On	OLI.OLI_Prod_ID = PCC.Prod_ID
	where		PCC.Cat_ID = 22 and	-- Additional Services
			LIRN.Ref_ID = @REF_ID
	order by	OLI.OLI_Prod_ID
	set nocount off


GO
GRANT EXECUTE ON  [dbo].[YieldGuar_2_Aux_GetServicesForRefID] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[YieldGuar_2_Aux_GetServicesForRefID] TO [IDT_WebApps]
GO
