SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[ORG_NM_LIST]
/* org_nm_list
Author: D Millen
Date: Aug 6, '04
Notes: -Retrieves all orgs from production org table. 
Used by org name matching to search for possible org to match. 

Permissions:
grant execute on org_nm_list to IDT_GRANT_INFO

Modifications:
Date	Who	Notes
-------------------------------------
8/11/04 DM	Does not pull curr_stat_type of inactive-duplicate and Inactive-Internal Error 
*/
as
select org.party_id as org_id
  , org_nm
  , org_st 
from dbo.org org (nolock)
 join dbo.party party (nolock) on party.party_id=org.party_id
where party.curr_stat_type not in (5,8) 
order by org_nm
GO
GRANT EXECUTE ON  [dbo].[ORG_NM_LIST] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[ORG_NM_LIST] TO [IDT_GRANT_INFO_USER]
GO
