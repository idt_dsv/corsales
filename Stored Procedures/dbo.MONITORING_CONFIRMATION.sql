
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Description:		Monitors the Confirmation table to ensure email confirmations are being sent to customers.

--- Modified
--- JLB 5/3/2007	Commented out type_id line in 2nd query and changed to dbmail
--- APG	5/3/2011	Moved job from WarehouseSQL to MFGSQL.  Replaced production.dbo.confirmation_notification_vw
---					pyramid view with source tables.
--- DBP 10/24/2013	Refactored for clarity.  
---					Removed filtering on type_id. We should be monitoring all confirmations.
---					Added a check for ERR_MSG issues.
--- PES 6/16/2014   Updated to handle new database schema and the operations after the service rewrite
-- =============================================

CREATE PROCEDURE [dbo].[MONITORING_CONFIRMATION]

AS

BEGIN
	DECLARE @CurrentTime			DATETIME
	DECLARE	@Recipients				VARCHAR(50)
	DECLARE	@NotificationRecipients	VARCHAR(50)
	DECLARE @UnprocessedCount		INT,
			@InProcessCount			INT,
			@ErrorMessageCount		INT,
			@RetryMessageCount		INT,
			@MaxRetryCount			INT,
			@BadClaimCount			INT

	DECLARE @UnprocessedMinutes		INT,
			@InProcessMinutes		INT,
			@ErrorMessageMinutes	INT,
			@RetryMessageMinutes	INT,
			@MaxRetryMinutes		INT

	DECLARE @UnprocessedThreshold	INT,
			@InProcessThreshold		INT,
			@ErrorMessageThreshold	INT,
			@RetryMessageThreshold	INT,
			@MaxRetryThreshold		INT

	DECLARE @Message				VARCHAR(MAX)

	SET	@CurrentTime =				GETDATE()
	SET	@Recipients =				'helpdesk@idtdna.com'
	SET	@NotificationRecipients =	'pshaff@idtdna.com'
	SET	@UnprocessedMinutes =		90
	SET	@InProcessMinutes =			90
	SET @ErrorMessageMinutes =		300
	SET @RetryMessageMinutes =		30
	SET	@MaxRetryMinutes =			30

	SET @UnprocessedThreshold =		15
	SET @InProcessThreshold =		5
	SET @ErrorMessageThreshold =	20
	SET @RetryMessageThreshold =	5
	SET	@MaxRetryThreshold =		1

	--Count of items that haven't been processed in more than @UnprocessedMinutes minutes.
	SET @UnprocessedCount = ( 
		SELECT	COUNT( C.CON_ID )
		FROM	Sales.dbo.CONFIRMATION AS C WITH (NOLOCK)
		LEFT JOIN Sales.dbo.ORD AS O WITH (NOLOCK) ON O.ORD_ID = C.ORD_ID
		WHERE	C.HOLD_ID = 0 AND
				( O.CURR_STAT <> 16 OR o.ORD_ID IS NULL ) AND
				C.STATUS = 0 AND
				C.RETRIES < 3 AND
				C.CON_DATE < DATEADD( MI, -@UnprocessedMinutes, @CurrentTime) AND 
				C.ACTIVE_DTM < @CurrentTime)

	--Count of items that have begun being processed but haven't completed in more than @InProcessMinutes minutes.
	SET @InProcessCount = ( 
		SELECT	COUNT( C.CON_ID )
		FROM	Sales.dbo.CONFIRMATION AS C WITH (NOLOCK)
		LEFT JOIN Sales.dbo.ORD AS O WITH (NOLOCK) ON O.ORD_ID = C.ORD_ID
		WHERE	C.HOLD_ID = 0 AND
				( O.CURR_STAT <> 16 OR o.ORD_ID IS NULL ) AND
				C.STATUS = 2 AND
				C.CON_DATE < DATEADD( MI, -@UnprocessedMinutes, @CurrentTime) AND 
				C.ACTIVE_DTM < @CurrentTime)

	--Count of items that have error messages written in the last @ErrorMessageMinutes.			
	SET	@ErrorMessageCount = (
		SELECT COUNT( C.CON_ID )
		FROM	Sales.dbo.CONFIRMATION AS C WITH (NOLOCK)
		WHERE	( C.ERR_MSG <> '' AND C.ERR_MSG IS NOT NULL ) AND
				(C.SENT_DTM > DATEADD( MI, -@ErrorMessageMinutes, @CurrentTime) OR
				C.CON_DATE > DATEADD( MI, -@ErrorMessageMinutes, @CurrentTime) OR
				C.CLAIMED_DTM > DATEADD( MI, -@ErrorMessageMinutes, @CurrentTime)))

	--Count of items that have retried in the last monitor period
	SET	@RetryMessageCount = (
		SELECT COUNT( C.CON_ID )
		FROM	Sales.dbo.CONFIRMATION AS C WITH (NOLOCK)
		WHERE	C.HOLD_ID = 0 AND C.RETRIES > 0 AND C.RETRIES < 3 AND
				(C.SENT_DTM > DATEADD( MI, -@RetryMessageMinutes, @CurrentTime) OR
				C.CON_DATE > DATEADD( MI, -@RetryMessageMinutes, @CurrentTime) OR
				C.CLAIMED_DTM > DATEADD( MI, -@RetryMessageMinutes, @CurrentTime)))

	--Count of items that have reached max retries in the last monitor period
	SET	@MaxRetryCount = (
		SELECT COUNT( C.CON_ID )
		FROM	Sales.dbo.CONFIRMATION AS C WITH (NOLOCK)
		WHERE	C.HOLD_ID = 0 AND C.RETRIES >= 3 AND
				(C.SENT_DTM > DATEADD( MI, -@MaxRetryMinutes, @CurrentTime) OR
				C.CON_DATE > DATEADD( MI, -@MaxRetryMinutes, @CurrentTime) OR
				C.CLAIMED_DTM > DATEADD( MI, -@MaxRetryMinutes, @CurrentTime)))

	--Count of items that are in a status that should not have a claim
	SET	@BadClaimCount = (
		SELECT COUNT( C.CON_ID )
		FROM	Sales.dbo.CONFIRMATION AS C WITH (NOLOCK)
		WHERE	C.HOLD_ID = 0 AND C.STATUS NOT IN (1, 2) AND 
				(C.CLAIMED_DTM IS NOT NULL OR C.CLAIMED_WORKSTATION_ID IS NOT NULL))

	IF @UnprocessedCount > @UnprocessedThreshold
	BEGIN
		SET @Message = 'This message indicates the number of (status = 0) Confirmations not sent is over the threshold of ' +
				 CONVERT( VARCHAR(10), @UnprocessedThreshold )
				 + '. The number of confirmations not sent is: ' + CONVERT( VARCHAR(20), @UnprocessedCount ) + char(13) + char(13) +
				'Run this SQL to view the messages: ' + char(13) + char(13) +
				'SELECT	* ' + char(13) +
				'FROM	Sales.dbo.CONFIRMATION AS C WITH (NOLOCK)' + char(13) +
				'LEFT JOIN Sales.dbo.ORD AS O WITH (NOLOCK) ON O.ORD_ID = C.ORD_ID' + char(13) +
				'WHERE	C.HOLD_ID = 0 AND' + char(13) +
				'( O.CURR_STAT <> 16 OR o.ORD_ID IS NULL ) AND' + char(13) +
				'C.STATUS = 0 AND' + char(13) +
				'C.RETRIES < 3 AND' + char(13) +
				'C.CON_DATE < ''' + CAST(DATEADD( MI, -@UnprocessedCount, @CurrentTime ) AS VARCHAR(25)) + ''' AND ' + char(13) +
				'C.ACTIVE_DTM < ''' + CAST(@CurrentTime AS VARCHAR(25)) + ''')'
	
		EXEC MSDB.DBO.SP_SEND_DBMAIL @PROFILE_NAME = 'IDTDNA', @RECIPIENTS = @Recipients, @SUBJECT = 'Confirmation Warning (status = 0)', @BODY = @Message
END

	IF @InProcessCount > @InProcessThreshold
	BEGIN
		SET @Message = 'This message indicates the number of (status = 2) Confirmations not sent is over the threshold of ' +
				 CONVERT( VARCHAR(10), @InProcessThreshold )
				 + '. The number of confirmations not sent is ' + CONVERT( VARCHAR(20), @InProcessCount ) + char(13) + char(13) +
				'Run this SQL to view the messages: ' + char(13) + char(13) +
				'SELECT * ' + char(13) +
				'FROM Sales.dbo.CONFIRMATION AS C WITH (NOLOCK) ' + char(13) +
				'LEFT JOIN Sales.dbo.ORD AS O WITH (NOLOCK) ON O.ORD_ID = C.ORD_ID ' + char(13) +
				'WHERE	C.HOLD_ID = 0 AND ' + char(13) +
				'( O.CURR_STAT <> 16 OR o.ORD_ID IS NULL ) AND ' + char(13) +
				'C.STATUS = 2 AND ' + char(13) +
				'C.CON_DATE < ''' + CAST(DATEADD( MI, -@UnprocessedMinutes, @CurrentTime) AS VARCHAR(25))+ ''' AND ' + char(13) +
				'C.ACTIVE_DTM < ''' + CAST(@CurrentTime AS VARCHAR(25))+ ''

		EXEC MSDB.DBO.SP_SEND_DBMAIL @PROFILE_NAME = 'IDTDNA', @RECIPIENTS = @Recipients, @SUBJECT = 'Confirmation Warning (status = 2)', @BODY = @Message
	END

	IF	@ErrorMessageCount > @ErrorMessageThreshold
	BEGIN
		SET	@Message = 'This message indicates the number of Confirmations with error messages is over the threshold of ' +
				CONVERT( VARCHAR(10), @ErrorMessageThreshold )
				+ '. The number of confirmations with error messages is ' + CONVERT( VARCHAR(20), @ErrorMessageCount) + char(13) + char(13) +
				'Run this SQL to view the messages: ' + char(13) + char(13) +
				'SELECT * ' + char(13) +
				'FROM Sales.dbo.CONFIRMATION AS C WITH (NOLOCK) ' + char(13) +
				'WHERE ( C.ERR_MSG <> '' AND C.ERR_MSG IS NOT NULL ) AND ' + char(13) +
				'(C.SENT_DTM > ''' + CAST(DATEADD( MI, -@ErrorMessageMinutes, @CurrentTime) AS VARCHAR(25))+ ''' OR ' + char(13) +
				'C.CON_DATE > ''' + CAST(DATEADD( MI, -@ErrorMessageMinutes, @CurrentTime) AS VARCHAR(25)) + ''' OR ' + char(13) +
				'C.CLAIMED_DTM > ''' + CAST(DATEADD( MI, -@ErrorMessageMinutes, @CurrentTime) AS VARCHAR(25)) + ''')'

		EXEC MSDB.DBO.SP_SEND_DBMAIL @PROFILE_NAME = 'IDTDNA', @RECIPIENTS = @Recipients, @SUBJECT = 'Confirmation Warning ERR_MSG', @BODY = @Message
	END

	IF	@RetryMessageCount >= @RetryMessageThreshold
	BEGIN
		SET	@Message = N'<H1>Confirmation Warning Retried Messages - ' + CAST((SELECT @@SERVERNAME) AS NVARCHAR(MAX)) + '</H1>' +
			N'<H4>This message indicates the number of Confirmations that have been retried more than once has exceeded its limit.</H4>' +
			N'<table border="1">' +
			N'<tr><th>Con ID</th><th>Type ID</th>' +
			N'<th>Status</th><th>Retries</th><th>Destination</th><th>Error Message</th>' +
			N'<th>Confirmation Date</th><th>Sales Order Number</th></tr>' +
			CAST ( ( SELECT td = con_id, '',
							td = type_id, '',
							td = status, '',
							td = retries, '',
							td = dest, '',
							td = err_msg, '',
							td = con_date, '',
							td = sales_ord_nbr, ''
						FROM	Sales.dbo.CONFIRMATION AS C WITH (NOLOCK) 
						WHERE	C.HOLD_ID = 0 AND C.RETRIES > 0 AND C.RETRIES < 3 AND
								(C.SENT_DTM > DATEADD( MI, -@RetryMessageMinutes, @CurrentTime) OR
								C.CON_DATE > DATEADD( MI, -@RetryMessageMinutes, @CurrentTime) OR
								C.CLAIMED_DTM > DATEADD( MI, -@RetryMessageMinutes, @CurrentTime))
						FOR XML PATH('tr'), TYPE 
			) AS NVARCHAR(MAX) ) +
			N'</table>' ;

		EXEC MSDB.DBO.SP_SEND_DBMAIL @PROFILE_NAME = 'IDTDNA', @RECIPIENTS = @Recipients, @SUBJECT = 'Confirmation Warning Retried Messages', @BODY = @Message, @body_format = 'HTML' 
	END

	IF	@MaxRetryCount >= @MaxRetryThreshold
	BEGIN
		SET	@Message = N'<H1>Confirmation Warning Max Retries - ' + CAST((SELECT @@SERVERNAME) AS NVARCHAR(MAX)) + '</H1>' +
					N'<H4>This message indicates the number of Confirmations that have been retried the maximum amount of tries.</H4>' +
					N'<table border="1">' +
					N'<tr><th>Con ID</th><th>Type ID</th>' +
					N'<th>Status</th><th>Retries</th><th>Destination</th><th>Error Message</th>' +
					N'<th>Confirmation Date</th><th>Sales Order Number</th></tr>' +
					CAST ( ( SELECT td = con_id, '',
									td = type_id, '',
									td = status, '',
									td = retries, '',
									td = dest, '',
									td = err_msg, '',
									td = con_date, '',
									td = sales_ord_nbr, ''
								FROM	Sales.dbo.CONFIRMATION AS C WITH (NOLOCK) 
								WHERE	C.HOLD_ID = 0 AND C.RETRIES >= 3 AND (C.SENT_DTM > DATEADD( MI, -@MaxRetryMinutes, @CurrentTime) OR C.CON_DATE > DATEADD( MI, -@MaxRetryMinutes, @CurrentTime) OR C.CLAIMED_DTM > DATEADD( MI, -@MaxRetryMinutes, @CurrentTime))
								FOR XML PATH('tr'), TYPE 
					) AS NVARCHAR(MAX) ) +
					N'</table>' ;

		EXEC MSDB.DBO.SP_SEND_DBMAIL @PROFILE_NAME = 'IDTDNA', @RECIPIENTS = @Recipients, @SUBJECT = 'Confirmation Warning Max Retries', @BODY = @Message, @body_format = 'HTML' 
	END

	IF	@BadClaimCount > 0
	BEGIN
			SET	@Message = N'<H1>Auto Corrected Message Alert - ' + CAST((SELECT @@SERVERNAME) AS NVARCHAR(MAX)) + '</H1>' +
			N'<H4>The following messages have had their claimed information cleared because they were in a state that should have not had claim information.</H4>' +
			N'<table border="1">' +
			N'<tr><th>Con ID</th><th>Type ID</th>' +
			N'<th>Status</th><th>Retries</th><th>Destination</th><th>Error Message</th><th>Claimed Date</th><th>Workstation</th>' +
			N'<th>Confirmation Date</th></tr>' +
			CAST ( ( SELECT td = con_id, '',
							td = type_id, '',
							td = status, '',
							td = retries, '',
							td = dest, '',
							td = err_msg, '',
							td = con_date, '',
							td = CLAIMED_DTM, '',
							td = CLAIMED_WORKSTATION_ID, ''
						FROM	Sales.dbo.CONFIRMATION AS C WITH (NOLOCK) 
						WHERE	HOLD_ID = 0 AND STATUS NOT IN (1, 2) AND 
								(CLAIMED_DTM IS NOT NULL OR CLAIMED_WORKSTATION_ID IS NOT NULL)
						FOR XML PATH('tr'), TYPE 
			) AS NVARCHAR(MAX) ) +
			N'</table>' ;

		EXEC MSDB.DBO.SP_SEND_DBMAIL @PROFILE_NAME = 'IDTDNA', @RECIPIENTS = @NotificationRecipients, @SUBJECT = 'Auto Corrected Message Alert', @BODY = @Message, @body_format = 'HTML' 

		UPDATE Sales.dbo.CONFIRMATION
		SET CLAIMED_DTM = NULL, CLAIMED_WORKSTATION_ID = NULL
		WHERE	HOLD_ID = 0 AND STATUS NOT IN (1, 2) AND 
				(CLAIMED_DTM IS NOT NULL OR CLAIMED_WORKSTATION_ID IS NOT NULL)
	END
END	
GO
