SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/**********************************************************************************************************************************************
OLIGO_CARD_INS
 AUTHOR: Karl Zumdome
 CREATED ON: 11/01/2004
 DESCRIPTION/ALGORITHM: To Insert a new OligoCard
 
 REVISIONS:
 8/20/2004 Initial Implementation
                     
**********************************************************************************************************************************************/

CREATE  PROCEDURE [dbo].[OLIGO_CARD_USER_INS]
@OLIGO_CARD_ID  int,
@WEB_ACCT_NBR int = NULL,
@ENDUSER_ID int = NULL,
@FROM_DT datetime,
@THRU_DT datetime,
@IS_OLIGO_CARD_OWNER bit

AS

DECLARE @OLIGO_CARD_USER_ID INT, @Error int

EXEC @Error = dbo.KEY_FIELD_GET_NEXT_VALUE 'OLIGO_CARD_USER_ID', @OLIGO_CARD_USER_ID OUTPUT

if @Error = 0 
Insert into OLIGO_CARD_USER (OLIGO_CARD_USER_ID, OLIGO_CARD_ID, WEB_ACCT_NBR, ENDUSER_ID, FROM_DT, THRU_DT, IS_OLIGO_CARD_OWNER) 
values(@OLIGO_CARD_USER_ID,@OLIGO_CARD_ID,@WEB_ACCT_NBR,@ENDUSER_ID,@FROM_DT,@THRU_DT,@IS_OLIGO_CARD_OWNER)
else
RAISERROR ('Error returning New Key value for OLIGO_CARD_USE_ID. Contact Support', 16, 1)
GO
GRANT EXECUTE ON  [dbo].[OLIGO_CARD_USER_INS] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[OLIGO_CARD_USER_INS] TO [IDT_Reporter]
GO
