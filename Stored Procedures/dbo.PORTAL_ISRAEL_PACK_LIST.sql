SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
SP Name:  PORTAL_ISRAEL_PACK_LIST
 
Date: 3-8-06
Author:  Jen Lockhart
 
Description:  Returns packing list results for Syntezza - Israel Distributor
Input:  start_dt, end_dt

 declare @start_dt datetime, @end_dt datetime
 set @start_dt = '08/01/08'
 set @end_dt = getdate()
 
exec dbo.PORTAL_ISRAEL_PACK_LIST  '08/01/08',  '08/20/08'

grant exec on PORTAL_ISRAEL_PACK_LIST to IDT_Reporter

Modifications:
Who: When:     Description
JL   3/10/06   Added bases_count per Christine
JL   3/10/06   Link Spec tables by refid rather than salesordnbr
JL   3/13/06   Added PI Name per Doron
JL   3/24/06   Changed spec tables to left join spec_ tables so ReadyMades will show up!
JCH  9/12/08   Added address line 1 to query
------------------------------------------------------------------------------
*/ 
create  proc [dbo].[PORTAL_ISRAEL_PACK_LIST] (@start_dt datetime , @end_dt datetime)

as

-- declare @start_dt datetime, @end_dt datetime
-- set @start_dt = '08/01/08'
-- set @end_dt = getdate()

declare @ctemp table (
  delivery_zone varchar (255),
  sales_ord_nbr int,
  ref_id int,
  enduser_id int,
  org_id int,
  order_date datetime,
  pay_auth_nbr varchar(50),
  ord_total float,
  items_on_ord int,
  Ship_dt datetime)


insert @ctemp
select ou.udf_field_value,
  ord.sales_ord_nbr,
  shipment.ref_id,
  ord.enduser_id,
  ord.org_id,
  order_date = convert (varchar (10), ord.ord_date, 1),
  ord.pay_auth_nbr,
  ord.ord_total,
  ord.items_on_ord,
  Ship_dt = convert(datetime,floor(convert(float,production.dbo.GET_REPORT_SHIP_DATE(getdate()))))
from production.dbo.ord ord (nolock) 
    join production.dbo.shipping_package_vw shipment (nolock) on shipment.sales_ord_nbr = ord.sales_ord_nbr
	and shipment.shipment_status not in (0,4)
        and shipment.manifest_dt between @start_dt and @end_dt
    left join production.dbo.ord_udf ou on ou.ord_id = ord.ord_id
	and ou.udf_id = 5
where shipment.shipping_consolidation_group_id = 100013 --BVBA Syntezza


--Get oligo information
select distinct ctemp.delivery_zone,
  ctemp.sales_ord_nbr,
  ctemp.order_date,
  ref_id = sp.external_ref_id,
  ctemp.pay_auth_nbr,
  ctemp.ord_total,
  ctemp.items_on_ord,
  seq_desc = isnull(
                    (select os.seq_desc from production.dbo.oligo_spec os (nolock) where os.ref_id = ctemp.ref_id),
                    (select dos.spec_name from production.dbo.duplex_oligo_spec dos (nolock) where dos.ref_id = ctemp.ref_id)  
                   ),
  prod_nm = (select prod_nm from production.dbo.product product where product.prod_id = sp.prod_id),
  purif = production.dbo.get_purification_name (ctemp.ref_id),
  mods = case when ssi.mod_count <> 0 then 'Yes' else 'No' end,
  bases = ssi.bases_count,
  ctemp.ship_dt,
  addr.Name,
  addr.acc_nm,
  addr.ph_nbr,
  addr.fax_nbr,
  addr.email,
  addr.ship_addr_1,
  addr.ship_addr_2,
  addr.ship_addr_3,
  addr.Ship_CSZ,
  addr.bill_addr_2,
  addr.bill_addr_3,
  addr.Bill_CSZ,
  addr.org_nm
  from @ctemp ctemp
  join production.dbo.ADDRESS_INFO_VW addr (nolock) on addr.person_party_id = ctemp.enduser_id
--  JOIN production.dbo.SPEC_ORDER SO WITH (NOLOCK) ON SO.SALES_ORD_NBR = ctemp.SALES_ORD_NBR
  left JOIN production.dbo.SPEC_OLIGO SP WITH (NOLOCK) ON SP.ref_id = ctemp.ref_id AND SP.SPEC_STATE_ID <> 3 /*ANY STATE BUT CANCELED*/
  left JOIN production.dbo.SPEC_SEQ_INFO SSI WITH (NOLOCK) ON  SSI.SPEC_OLIGO_ID = SP.SPEC_OLIGO_ID AND SSI.SPEC_STATE_ID <> 3 /*ANY STATE BUT CANCELED*/
order by ctemp.sales_ord_nbr


GO
GRANT EXECUTE ON  [dbo].[PORTAL_ISRAEL_PACK_LIST] TO [IDT_Reporter]
GO
