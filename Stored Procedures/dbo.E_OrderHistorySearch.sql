SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: MDW
Date: 5/22/08
Notes: To replace dynamic sql in order histery web search. 
  It is a proxy object call to order history.
  -- If a customer call the Enduser_nbr will allways have a value, 
     otherwise it is a super-user and will have a org_nbr
Search criteria options:
    @BeginDt datetime                 -- Required - First query date. Time will be truncated
  , @EndDt datetime                   -- Required - End query date. Time will be truncated
  , @RecordsReturned int              -- Required - specifies the number of rows to be returned - for paging controls on the web
  , @SalesOrderNbrMax int             -- Required - Will filter results to Sales_Ord_nbr <= the returned value-- defaults to max int
-- Either @EnduserNbr or OrgNbrList must be not null.
  , @OrgNbrList varchar(max) = null   -- Comma separated list of Org_nbr's 
  , @EnduserNbr int = null            -- Either this field or OrgNbrList must be not null
-- Optional Search parameters
  , @PO nvarchar(50) = null           -- optional - Purchase Order
  , @SalesOrderNbr int = null         -- optional - Sales Order Number; trumps all other search criteria; UI must verify it belongs to org or enduser
  , @FirstName nvarchar(50) = null    -- optional - End user First name
  , @LastName nvarchar(50) = null     -- optional - End user Last name 
  , @AccountName nvarchar(50) = null  -- optional - Account name
  , @AccountNbr nvarchar(50) = null   -- optional - Account Number
  , @SequenceName varchar(80) = null  -- optional - Begins with query

select * from org where org_nm like 'integrated%'
Example:
declare  @BeginDt datetime
  , @EndDt datetime
  , @RecordsReturned int
  , @SalesOrderNbrMax int 
  , @OrgNbrList varchar(max)
  , @EnduserNbr int
  , @PO nvarchar(50)
  , @SalesOrderNbr int
  , @FirstName nvarchar(50)
  , @LastName nvarchar(50)
  , @AccountName nvarchar(50)
  , @AccountNbr nvarchar(50)
  , @SequenceName varchar(80)
select @BeginDt = dateadd(Month,-2, getdate())
  , @endDt = getdate()
  , @RecordsReturned = 10
  , @SalesOrderNbrMax = 2147483647
  , @OrgNbrList = '12766,9068,5796,11763,4201380,1000,14311,12616,13350,6916,3639,13573,5042,10834,12630,5969,5250,14375,3510' --'3639' --
  , @EndUserNbr = null
  , @PO = null
  , @SALESORDERNBR = null
  , @FirstName = null
  , @LastName = null
  , @AccountName = null
  , @AccountNbr = null
  , @SequenceName = null
exec dbo.E_OrderHistorySearch @BeginDt, @endDt, @RecordsReturned, @SalesOrderNbrMax, @OrgNbrList, @EndUserNbr, @PO, @SALESORDERNBR, @FIRSTNAME, @LASTNAME, @AccountName, @AccountNbr, @SequenceName


Permissions:
Grant execute on E_OrderHistorySearch to idt_webapps, idt_reporter

Modifications
10/24/08 JLB Placed enduser filter orders returned
01/08/09 JLB Added raise error if the org_id(s) not found
02/27/09 JLB fixed enduser filter error on super user requests

*/
CREATE procedure [dbo].[E_OrderHistorySearch] 
 (
    @BeginDt datetime
  , @EndDt datetime
  , @RecordsReturned int
  , @SalesOrderNbrMax int = 2147483647
  , @OrgNbrList nvarchar(max) = null   
  , @EnduserNbr int = null            
  , @PO nvarchar(50) = null
  , @SalesOrderNbr int = null         
  , @FirstName nvarchar(50) = null
  , @LastName nvarchar(50) = null
  , @AccountName nvarchar(50) = null
  , @AccountNbr nvarchar(50) = null
  , @SequenceName nvarchar(80) = null  
)
AS
BEGIN
--drop table #order
SET NOCOUNT ON
SET NOCOUNT ON

create table #order (
      Curr_Stat int
    , SALES_ORD_NBR int
    , ORD_DATE datetime
    , ORD_ID int
    , ITEMS_ON_ORD int
    , PAY_METH_ID int
    , PAY_AUTH_NBR nvarchar(50)
    , EndUser_id int -- For linking to other tables -Perhaps can 
    , Org_id int
    , Acc_id int
)

--Validate
If @OrgNbrList is null and @EnduserNbr is null
begin
  raiserror ('Must set either have value for @OrgNbrList or @EnduserNbr parameters',1,16)
end

--Setup Variables
DECLARE @OrgIDList varchar(max), @AccIdList varchar(max), @EndUserID int, @SuperUser bit, @sqlStr NVarchar(max)
SELECT @OrgIdList = '', @AccIdList = '', @EndUserID = null, @sqlStr = ''

if @EnduserNbr is not NULL
begin
	set @EndUserID = isnull((Select party_id from dbo.person with (nolock) where enduser_Nbr = @EndUserNbr),-1)
end

if @AccountName is not null
begin
	Select @AccIdList = @AccIdList + cast(party_id as varchar(10)) + ',' from dbo.account with (nolock) join dbo.breakApartStr(@OrgNbrList) bas on account.acc_nm = @AccountName
	select @AccIdList = case when left(@AccIdList,1) <> '' then  left(@AccIdList,len(@AccIdList) -1) else '' end
end

If @OrgNbrList is null
	set @SuperUser = 0
else 
begin
	set @SuperUser = 1
	Select @OrgIDList = @OrgIDList + cast(party_id as varchar(10)) + ',' from dbo.org with (nolock) join dbo.breakApartStr(@OrgNbrList) bas on org.org_nbr = cast(bas.strValue as varchar(100))
	select @OrgIDList = case when left(@OrgIDList,1) <> '' then  left(@OrgIDList,len(@OrgIDList) -1) else '' END
	IF @ORGIDLIST = ''
	BEGIN
		RAISERROR ('Org_id(s) NOT found at local sales database for the given @OrgNbrList parameters',1,16)
	END    
END

if @SalesOrderNbr IS NULL
begin
  set @sqlStr = 'insert into #order
   SELECT ORD.CURR_STAT
    , SALES_ORD_NBR
    , ORD_DATE = ORD_DATE
    , ORD_ID
    , ITEMS_ON_ORD
    , ORD.PAY_METH_ID
    , ORD.PAY_AUTH_NBR
    , ORD.ENDUSER_ID 
    , ORD.ORG_ID
    , ORD.ACC_ID
  FROM dbo.ORD with (nolock)
  WHERE ord.SALES_ORD_NBR <= ' + cast(@SalesOrderNbrMax as varchar(20)) +'
    and Ord.Ord_Date between '''+convert(varchar(30),@beginDt,1)+''' and '''+ convert(varchar(30),dateAdd( day, 1, convert(varchar(10),@endDt,1)),1) +'''
      and '+ case when @SuperUser = 1 then 'Org_Id in (' + isnull(@OrgIDList,'') + ')' else ' EndUser_ID = ' + isnull(Cast(@EndUserID as varchar(20)),'') end + '
   ' + case when @PO is null then '' else ' AND Ord.PAY_AUTH_NBR = ''' + @PO +'''' end + '
   ' + case when @AccountName is null then '' else ' AND Ord.Acc_id in ( ' + isnull(@AccIDList,'') + ')'  end 

end else -- @SalesOrderNbr is not null
begin
  set @sqlStr = 'insert into #order
   SELECT ord.Curr_Stat
    , SALES_ORD_NBR
    , ORD_DATE = ORD_DATE
    , ORD_ID
    , ITEMS_ON_ORD
    , ORD.PAY_METH_ID
    , ORD.PAY_AUTH_NBR
    , ord.EndUser_id -- For linking to other tables -Perhaps can 
    , ord.ORG_id
    , ord.acc_id
  FROM dbo.ORD with (nolock)
  WHERE Sales_ord_nbr = ' + cast(@SalesOrderNbr as varchar(20)) +
	' and '+ case when @SuperUser = 1 then 'Org_Id in (' + isnull(@OrgIDList,'') + ')' else ' EndUser_ID = ' + isnull(Cast(@EndUserID as varchar(20)),'') end

end -- Load #order table

print '@SqlStr = ' + @SqlStr

exec sp_ExecuteSql @SqlStr

--if @SequenceName is not null 
--begin
--  delete ord
--  from #order ord
--end

set @SqlStr = 'SELECT TOP ('+ cast(@RecordsReturned as varchar(10)) +')
    ord.Curr_Stat
  , SALES_ORD_NBR
  , ORD_DATE = ORD_DATE
  , ORD_ID
  , ITEMS_ON_ORD
  , ORD.PAY_METH_ID
  , ORD.PAY_AUTH_NBR
  , PERSON.FIRST_NM
  , PERSON.LAST_NM
  , ACCOUNT.ACC_NM
FROM #order ORD 
JOIN dbo.PERSON with (nolock) ON ORD.ENDUSER_ID = PERSON.PARTY_ID 
' + 
case when @LastName is not null 
then '
  AND PERSON.LAST_NM LIKE ISNULL(''' + @LastName + ''', ''%'')' 
else '' 
end + 
case when @FirstName is not null 
then '
  AND PERSON.FIRST_NM LIKE ISNULL(''' + @FirstName + ''', ''%'')' 
else '' 
end + '
JOIN dbo.ORG with (nolock) ON ORD.ORG_ID = ORG.PARTY_ID 
JOIN dbo.ACCOUNT with (nolock) ON ORD.ACC_ID = ACCOUNT.PARTY_ID 
' + 
case when @SequenceName is not null 
then 
'where exists
    ( 
      select *
      from dbo.line_item_ref_nbr lirn (nolock) 
      JOIN dbo.oligo_spec os (nolock) ON os.ref_id = lirn.ref_id 
      where lirn.fk_ord_id = ord.ord_id 
        and os.seq_desc like isnull(''' + @SequenceName + ''','''') + ''%''
    )
'  
else '' 
end + '
ORDER BY SALES_ORD_NBR DESC,ORD_DATE DESC'


print '@SqlStr = ' + @SqlStr

exec sp_ExecuteSql @SqlStr
 
drop table #order

SET NOCOUNT OFF
END

GO
GRANT EXECUTE ON  [dbo].[E_OrderHistorySearch] TO [IDT_Reporter]
GRANT EXECUTE ON  [dbo].[E_OrderHistorySearch] TO [IDT_WebApps]
GO
