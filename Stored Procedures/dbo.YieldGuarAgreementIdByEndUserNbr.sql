SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Andrew Felsing>
-- Create date: <March 2, 2010>
-- =============================================
CREATE PROCEDURE [dbo].[YieldGuarAgreementIdByEndUserNbr]
	@EnduserNbr int,
	@PurificationId int
AS
BEGIN
SET NOCOUNT ON;

declare @Effective_Date DateTime	
set @Effective_Date = Convert ( DateTime, Convert ( Varchar ( 20 ), GetDate ( ), 101 ))

create table #yield (yield_guar_agreement_id int)

insert into #yield 
	select a.YIELD_GUAR_AGREEMENT_ID 
	from Sales.dbo.agreement a WITH (NOLOCK)
	where agreement_id = 1 and 
            (a.begin_dt <= @effective_date) and
            ((a.end_dt is null ) or (@Effective_Date < a.end_dt ))

-- HPLC
-- if @PurificationId = 1038 
if exists (
      select      *
      from  Sales.dbo.Product P  WITH (NOLOCK)
    join    Sales.dbo.Prod_Cat_Class PCC WITH (NOLOCK)
            on    ( P.Prod_ID = @PurificationID ) and
                  ( P.Prod_ID = PCC.Prod_ID ) and  
                  ( P.intro_Dt <= @Effective_Date ) and  
                  (( P.sales_disc_dt is null ) or ( P.sales_disc_dt > @Effective_Date )) and  
                  ( PCC.Cat_ID = 24 )  
  )
  insert into #yield 
	select a.agreement_id 
	from Sales.dbo.agreement a  WITH (NOLOCK)
	where yield_guar_agreement_id = 55 and 
            (a.begin_dt <= @effective_date) and
            ((a.end_dt is null ) or (@Effective_Date < a.end_dt ))

-- PAGE
-- if @PurificationId = 1037 
if exists (
      select      *
      from  SALES.dbo.Product P  WITH (NOLOCK)
    join    SALES.dbo.Prod_Cat_Class PCC WITH (NOLOCK)
            on    ( P.Prod_ID = @PurificationID ) and
                  ( P.Prod_ID = PCC.Prod_ID ) and  
                  ( P.intro_Dt <= @Effective_Date ) and  
                  (( P.sales_disc_dt is null ) or ( P.sales_disc_dt > @Effective_Date )) and  
                  ( PCC.Cat_ID = 23 )  
  )
  insert into #yield select a.agreement_id from SALES.dbo.agreement a WITH (NOLOCK) where yield_guar_agreement_id = 4 and 
            (a.begin_dt <= @effective_date) and
            ((a.end_dt is null ) or (@Effective_Date < a.end_dt ))


insert into #yield
select a.yield_guar_agreement_id
from SALES.dbo.person p WITH (NOLOCK)
join SALES.dbo.party_agreement pa WITH (NOLOCK)
	on pa.party_id in (p.party_id, p.acc_id, p.org_id)
join SALES.dbo.agreement a WITH (NOLOCK)
	on a.agreement_id = pa.agreement_id
where p.enduser_nbr = @EnduserNbr and
            (a.begin_dt <= @effective_date) and
            ((a.end_dt is null ) or (@Effective_Date < a.end_dt ))
order by a.begin_dt desc

select yield_guar_agreement_id from #yield  WITH (NOLOCK) order by yield_guar_agreement_id desc

drop table #yield
---
end
GO
GRANT EXECUTE ON  [dbo].[YieldGuarAgreementIdByEndUserNbr] TO [IDT_APPS]
GO
