
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*  
Author: Chris A. Sailor  
Date Created: March 14, 2006  
Purpose:  
  
This procedure is intended to retrieve the Reference IDs and  
Branch Location IDs for any items on the order that have an invalid  
Branch Location ID.  
  
grant execute on DBO.MFOLD_BYORDER_GET_REF_IDS_WITH_BAD_LOCATION_IDS to IDT_Apps  
  
Modifications:  
Who     Date      Modification  Note  
--------------------------------------------------------------  
CAS     03/14/06  Chris Sailor  Created   
CAS     03/20/07  Chris Sailor  Moved out of Manufacturing database
DBP	10/02/14  Drew Pittman	Added (NOLOCK)s, then refactored a bit for speed
*/  
CREATE PROCEDURE [dbo].[MFOLD_BYORDER_GET_REF_IDS_WITH_BAD_LOCATION_IDS] ( @Queue_Item_ID INT )
AS
    BEGIN  
        SET NOCOUNT ON  
 
/*	SELECT	REF_ID, lirn.BRANCH_LOCATION_ID 
	FROM	Sales.dbo.QUEUE_ITEM_MFOLD AS qim WITH (NOLOCK)
	JOIN	Sales.dbo.LINE_ITEM_REF_NBR AS lirn WITH (NOLOCK) ON BF_BOBJECT_OID = FK_ORD_ID
	LEFT JOIN Reference.dbo.BRANCH_LOCATION AS bl WITH (NOLOCK) ON lirn.BRANCH_LOCATION_ID = bl.BRANCH_LOCATION_ID
	WHERE	QUEUE_ITEM_ID = @Queue_Item_ID AND
		bl.BRANCH_LOCATION_ID IS NULL */
		
        SELECT  REF_ID
              , lirn.BRANCH_LOCATION_ID
        FROM    Sales.dbo.QUEUE_ITEM_MFOLD AS qim WITH ( NOLOCK )
                JOIN Sales.dbo.LINE_ITEM_REF_NBR AS lirn WITH ( NOLOCK ) ON BF_BOBJECT_OID = FK_ORD_ID
        WHERE   QUEUE_ITEM_ID = @Queue_Item_ID
                AND NOT EXISTS ( SELECT BRANCH_LOCATION_ID
                                 FROM   Reference.dbo.BRANCH_LOCATION AS bl
                                        WITH ( NOLOCK )
                                 WHERE  bl.BRANCH_LOCATION_ID = lirn.BRANCH_LOCATION_ID )

        SET NOCOUNT OFF  
    END
GO

GRANT EXECUTE ON  [dbo].[MFOLD_BYORDER_GET_REF_IDS_WITH_BAD_LOCATION_IDS] TO [IDT_APPS]
GO
