SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: Chris A. Sailor
Date Created: April 9, 2003
Purpose:

  This procedure is intended to retrieve the Party IDs
for the end user, account, and organization for a specified
end user number.  (Stored in the PDOXCustNbr field in the
web database.)


Modifications:
Who     Date      Modification  Note
--------------------------------------------------------------
CAS     04/09/03  Chris Sailor  Initial Implementation
*/
Create Procedure [dbo].[AutoEntry_Get_Production_Party_IDs]
	@EndUserNbr Integer
As
	set nocount on

	select	Party_ID,
		Acc_ID,
		Org_ID
	From	dbo.Person
	Where	EndUser_Nbr = @EndUserNbr

	set nocount off



GO
GRANT EXECUTE ON  [dbo].[AutoEntry_Get_Production_Party_IDs] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[AutoEntry_Get_Production_Party_IDs] TO [IDT_Reporter]
GO
