SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create proc [dbo].[usp_custinfo_SalesUpdate]
as

/*--------------------------------------------------------------------------------
Created by: Andrew Gould
Created on: 11/10/2009

Purpose:
	This proc is designed to find outdated price_grp_ids and ship_grp_ids on the
	website and update them with the current values based on Sales.  Due to
	performance concerns its broken into a few steps.
	
	Grab all current data from the website.  Do the comparisons.  Push all records
	that need to be updated back to the website in a staging table 
	([WNSQL\WEBSITE].NewWebSite.dbo.custinfo_SalesUpdate) and then call a remote
	stored procedure to do the final update locally on WNSQL\Website.
	
Objects:
	[WNSQL\WEBSITE].NewWebSite.dbo.custinfo_SalesUpdate
	[WNSQL\WEBSITE].NewWebSite.dbo.custinfo
	Sales.dbo.Person
	Sales.dbo.Party
	[WNSQL\WEBSITE].NewWebSite.dbo.usp_custinfo_SalesUpdate
--------------------------------------------------------------------------------*/

----------------------------------------------------------------------------------
-- Create temp table.
----------------------------------------------------------------------------------
create table #Sync
	(SyncID int identity(1,1) primary key
	, W_acctnbr int
	, S_enduser_nbr int
	, W_pdoxcustnbr int
	, S_price_grp_id int
	, W_price_grp_id int
	, S_ship_grp_id int
	, W_ship_grp_id int)

----------------------------------------------------------------------------------
-- Empty the staging table.
----------------------------------------------------------------------------------
delete from [WNSQL\WEBSITE].NewWebSite.dbo.custinfo_SalesUpdate

----------------------------------------------------------------------------------
-- Pull records over from the website.
----------------------------------------------------------------------------------
insert into #Sync
	(W_acctnbr
	, W_pdoxcustnbr
	, W_price_grp_id
	, W_ship_grp_id)
select acctnbr
	, pdoxcustnbr
	, price_grp_id
	, ship_grp_id
from [WNSQL\WEBSITE].NewWebSite.dbo.custinfo
where pdoxcustnbr is not null

----------------------------------------------------------------------------------
-- Update all the records with their current price_grp_ids and ship_grp_ids.
----------------------------------------------------------------------------------
update #Sync
set S_enduser_nbr = P.enduser_nbr
	, S_price_grp_id = Pa.price_grp_id
	, S_ship_grp_id = Pa.ship_grp_id
from #Sync as S with (nolock)
inner join Sales.dbo.Person as P with (nolock)
	on S.W_pdoxcustnbr = P.enduser_nbr
inner join Sales.dbo.Party as Pa with (nolock)
	on P.Org_ID = Pa.Party_ID

----------------------------------------------------------------------------------
-- Push the records that need to be changed back to the website.
----------------------------------------------------------------------------------
insert into [WNSQL\WEBSITE].NewWebSite.dbo.custinfo_SalesUpdate
	(acctnbr
	, price_grp_id
	, ship_grp_id)
select W_acctnbr
	, S_price_grp_id
	, S_ship_grp_id
from #Sync as S with (nolock)
where W_price_grp_id <> S_price_grp_id
	or W_ship_grp_id <> S_ship_grp_id

----------------------------------------------------------------------------------
-- Run the proc to do the update locally to the website.
----------------------------------------------------------------------------------
exec [WNSQL\WEBSITE].NewWebSite.dbo.usp_custinfo_SalesUpdate

----------------------------------------------------------------------------------
-- Clean up.
----------------------------------------------------------------------------------
drop table #Sync
----------------------------------------------------------------------------------
----------------------------------------------------------------------------------
GO
