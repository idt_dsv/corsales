SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
Author: MDW
Date: 3/4/03
Notes
  Builds table for intranet to return the count of oligos sold by department for current day

Example:
  exec DAILY_OLIGO_COUNT_BUILD '12/22/05'
  exec DAILY_OLIGO_COUNT_select

grant execute on [DAILY_OLIGO_COUNT_BUILD] to idt_webapps, idt_reporter, idt_Dbdeveloper

Revisions
8/4/03 MDW  Added case statement to prevent divide by 0 errors. added curr_stat <> cancelled filter on @order query
4/14/05 MDW Modified for performance (added #ord Temp table)
12/22/05 MDW Modified to show Branch_location sales
*/

CREATE     procedure [dbo].[DAILY_OLIGO_COUNT_BUILD] 
(@DT datetime)
as 
set nocount on
-- drop table #ord drop table #oligo_count drop table #dept
--  select * from oligo_count
--  declare @dt datetime
--  set @dt = '8/4/03'

  declare @orders int, @DT_Built datetime
  set  @DT_Built = getdate()
  select  @orders = count(*) 
    from dbo.ord (nolock) 
    where ord.ord_date between convert(datetime, convert(varchar(8),@DT,1)) and convert(datetime, convert(varchar(8),@DT+ 1,1))
      and curr_Stat <> 16 --cancelled

  Select depart_id, depart_nm = case when depart_nm like 'prod%' then 'Main' else depart_nm end
  into #dept
  from depart
  where depart.depart_id not in (6,12,14,15)

  
print @orders

  if @orders = 0
  begin
    begin transaction
      truncate table sales.dbo.oligo_count
  
      insert sales.dbo.oligo_count
      select case when depart_nm like 'prod%' then 'Main' else depart_nm end , 0,0,0, @DT_Built 
      from #dept 

      insert sales.dbo.oligo_count
      select 'San Diego - Main' , 0,0,0, @DT_Built 

    commit
    return 0
  end

  begin transaction
    select Ord.ord_id 
    into #Ord
    from ord 
    where ord.curr_stat <> 16 
      AND ord.ord_date between convert(datetime, convert(varchar(8),@DT,1)) and convert(datetime, convert(varchar(8),@DT+ 1,1))
 
    -- insert items into temp table
   SELECT 
      Department = case when lirn.branch_location_id = 1 then depart.depart_nm else bl.name + '-'+depart.depart_nm end
    , Oligos = count (*)  
    , Bases = sum(os.bases)
    , Ave_len = case when count(*) = 0 then 0 else sum(os.bases)/count(*) end
    , Date_updated = @DT_Built
    into #oligo_count
    FROM #ord ord
      JOIN dbo.line_item_ref_nbr AS lirn (nolock) on lirn.fk_ord_id = ord.ord_id 
        and WORK_SPEC_OBJ_TYPE like 'TOLIGO%'
        AND LIRN.Curr_Stat <> 5
      join #dept depart on depart.depart_id = lirn.dept_id
      join dbo.oligo_spec os (nolock) on os.ref_id = lirn.ref_id
      join Branch_location bl on bl.Branch_location_id = lirn.Branch_location_id
    group by case when lirn.branch_location_id = 1 then depart.depart_nm else bl.name + '-'+depart.depart_nm end
    
    insert #oligo_count 
    select depart_nm, 0,0,0, @DT_Built 
    from #dept depart 
    where not exists(select * from #oligo_count oc where depart.depart_nm = oc.department)

    if not exists(select * from  #oligo_count oc where oc.Department like '%san diego%')   
      insert #oligo_count
      select 'San Diego - Main' , 0,0,0, @DT_Built 
    
    select  @orders = count(*) from #Ord
    
    insert #oligo_count
    select [Total orders] = convert(varchar(50), 'Total Orders - ' + convert(varchar,@orders)), sum(oligos), sum(bases), 
      case when sum(oligos) = 0 then 0 else sum(bases)/sum(oligos) end, @DT_Built
    from #oligo_count

    truncate table sales.dbo.oligo_count

    insert sales.dbo.oligo_count
    select * from #oligo_count order by department
  commit












GO
GRANT EXECUTE ON  [dbo].[DAILY_OLIGO_COUNT_BUILD] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[DAILY_OLIGO_COUNT_BUILD] TO [IDT_DbDeveloper]
GRANT EXECUTE ON  [dbo].[DAILY_OLIGO_COUNT_BUILD] TO [IDT_Reporter]
GRANT EXECUTE ON  [dbo].[DAILY_OLIGO_COUNT_BUILD] TO [IDT_WebApps]
GO
