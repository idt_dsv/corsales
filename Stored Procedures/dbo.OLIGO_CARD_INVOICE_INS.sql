SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO


/**********************************************************************************************************************************************
 INS_MFG_ITEM_NEW
 AUTHOR: Mark Schebel
 CREATED ON: 8/20/2004
 DESCRIPTION/ALGORITHM: To ALTER  record of an oligo card invoice
                        for importation into epic'r 
 
 REVISIONS:
 8/20/2004 Initial Implementation
                     
**********************************************************************************************************************************************/

CREATE   PROCEDURE [dbo].[OLIGO_CARD_INVOICE_INS]
	@OLIGO_CARD_ID INT,
	@INV_NBR INT,
	@PROD_ID INT,
	@AMOUNT MONEY
AS

DECLARE @OLIGO_CARD_INV_ID INT, @Error int

EXEC @Error = dbo.KEY_FIELD_GET_NEXT_VALUE 'OLIGO_CARD_INVOICE_ID', @OLIGO_CARD_INV_ID OUTPUT

if @Error = 0 
  INSERT OLIGO_CARD_INVOICE
  values(	@OLIGO_CARD_INV_ID,	@OLIGO_CARD_ID,	@INV_NBR,	@PROD_ID,	@AMOUNT,	0)
else
  RAISERROR ('Error returning New Key value for OLIGO_CARD_INVOICE_ID. Contact Support', 16, 1)
 





GO
GRANT EXECUTE ON  [dbo].[OLIGO_CARD_INVOICE_INS] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[OLIGO_CARD_INVOICE_INS] TO [IDT_Reporter]
GO
