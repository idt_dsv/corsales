SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SALES_TERR_LIST]
/* SALES_TERR_LIST
Author: dmillen
Date: 8/9/04
Notes: -Called by org_name_matching
       -Gets all sales_terr names for drop down to assign sales_terr to new potential org

Permissions:
grant execute on SALES_TERR_LIST to IDT_GRANT_INFO

Modifications:
Date	Who	Notes
-------------------------------------
*/
AS
SELECT SALES_TERR_ID,SALES_TERR_NM
  FROM DBO.SALES_TERR
GO
GRANT EXECUTE ON  [dbo].[SALES_TERR_LIST] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[SALES_TERR_LIST] TO [IDT_GRANT_INFO_USER]
GO
