
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
GRANT  EXECUTE  ON [dbo].[GetConfirmationEmailsToSend] TO [IDT-Coralville\iusr_idt]
GRANT  EXECUTE  ON [dbo].[GetConfirmationEmailsToSend] TO [Coliseum]

Author: Phil Shaff
Date Created: January 24th, 2014
Purpose:

	Retrieve all confirmation e-mails to be sent

Modifications:
Who       Date        Note
---------------------------------------------------------------------------------------
pshaff    05/29/2014  Adding in logic for active time
pshaff    4/2/2014    Added c.NOTIFICATION_INFO_ID to pull in information for type A e-mails
pshaff    7/16/2014   Added or to merge ord info if there is an org_id or sales_org_nbr
pshaff    8/21/2014   Added attachment checking
*/

CREATE PROCEDURE [dbo].[GetConfirmationEmailsToSend]

AS

    SELECT  c.con_id
          , c.ord_id
          , c.sales_ord_nbr
          , c.type_id
          , c.status
          , c.retries
          , c.hold_id
          , c.dest
          , c.subject
          , c.body
          , c.oligo_card_id
          , o.org_nbr
          , r.pay_auth_nbr
          , CASE WHEN NULLIF(r.last_edit_by, '') IS NOT NULL
                 THEN r.last_edit_by + '@idtdna.com'
                 ELSE NULL
            END AS ReturnToEMailAddress
          , e.first_name + ' ' + e.last_name AS ReturnToFullName
          , sa.cntry_id AS DestinationCountry
          , c.notification_info_id
		  , CASE WHEN a.CON_ID IS NOT NULL
                 THEN 1
                 ELSE 0 
			END AS Attachments
    FROM    sales.dbo.confirmation AS c WITH ( NOLOCK )
            LEFT OUTER JOIN sales.dbo.ord AS r WITH ( NOLOCK ) ON r.SALES_ORD_NBR = c.SALES_ORD_NBR
            LEFT OUTER JOIN sales.dbo.addr AS sa WITH ( NOLOCK ) ON sa.addr_id = r.ship_addr_id
            LEFT OUTER JOIN sales.dbo.org AS o WITH ( NOLOCK ) ON o.party_id = r.org_id
            LEFT OUTER JOIN reference.dbo.employee AS e WITH ( NOLOCK ) ON r.last_edit_by = e.network_login_nm
			LEFT OUTER JOIN sales.dbo.confirmation_attachment AS a WITH (NOLOCK) ON c.CON_ID = a.CON_ID
    WHERE   COALESCE(c.status, 0) = 0
            AND (
                  c.active_dtm <= GETDATE()
                  OR c.ACTIVE_DTM IS NULL
                )
            AND c.claimed_dtm IS NULL
            AND c.claimed_workstation_id IS NULL
            AND COALESCE(c.retries, 0) < 3
            AND COALESCE(c.hold_id, 0) = 0
            AND (
                  COALESCE(c.ord_id, 0) = 0
                  OR r.curr_stat <> 16
				  OR r.ord_id IS NULL
                )  
            AND c.ORD_ID IS NULL

UNION ALL

    SELECT  c.con_id
          , c.ord_id
          , c.sales_ord_nbr
          , c.type_id
          , c.status
          , c.retries
          , c.hold_id
          , c.dest
          , c.subject
          , c.body
          , c.oligo_card_id
          , o.org_nbr
          , r.pay_auth_nbr
          , CASE WHEN NULLIF(r.last_edit_by, '') IS NOT NULL
                 THEN r.last_edit_by + '@idtdna.com'
                 ELSE NULL
            END AS ReturnToEMailAddress
          , e.first_name + ' ' + e.last_name AS ReturnToFullName
          , sa.cntry_id AS DestinationCountry
          , c.notification_info_id
		  , CASE WHEN a.CON_ID IS NOT NULL
                 THEN 1
                 ELSE 0 
			END AS Attachments
    FROM    sales.dbo.confirmation AS c WITH ( NOLOCK )
            LEFT OUTER JOIN sales.dbo.ord AS r WITH ( NOLOCK ) ON r.ord_id = c.ord_id
            LEFT OUTER JOIN sales.dbo.addr AS sa WITH ( NOLOCK ) ON sa.addr_id = r.ship_addr_id
            LEFT OUTER JOIN sales.dbo.org AS o WITH ( NOLOCK ) ON o.party_id = r.org_id
            LEFT OUTER JOIN reference.dbo.employee AS e WITH ( NOLOCK ) ON r.last_edit_by = e.network_login_nm
			LEFT OUTER JOIN sales.dbo.confirmation_attachment AS a WITH (NOLOCK) ON c.CON_ID = a.CON_ID
    WHERE   COALESCE(c.status, 0) = 0
            AND (
                  c.active_dtm <= GETDATE()
                  OR c.ACTIVE_DTM IS NULL
                )
            AND c.claimed_dtm IS NULL
            AND c.claimed_workstation_id IS NULL
            AND COALESCE(c.retries, 0) < 3
            AND COALESCE(c.hold_id, 0) = 0
            AND (
                  COALESCE(c.ord_id, 0) = 0
                  OR r.curr_stat <> 16
                  OR r.ord_id IS NULL
                )  
            AND c.ord_id IS NOT NULL

GO


GRANT EXECUTE ON  [dbo].[GetConfirmationEmailsToSend] TO [coliseum]
GRANT EXECUTE ON  [dbo].[GetConfirmationEmailsToSend] TO [IDT-CORALVILLE\IUSR_IDT]
GO
