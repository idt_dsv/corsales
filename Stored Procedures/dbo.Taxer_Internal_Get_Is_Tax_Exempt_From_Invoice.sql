SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: Chris A. Sailor
Date Created: December 28, 2004
Purpose:


  This procedure returns one record if the invoice is exempt.  This should
not be called if the Ship_Cntry is not in the IDT_TAXER_COUNTRY_SPECIFIC_DATA
table.

  It is intended to be replaced

Permissions:  
Grant Execute On dbo.Taxer_Internal_Get_Is_Tax_Exempt_From_Invoice to IDT_WebApps  
Grant Execute On dbo.Taxer_Internal_Get_Is_Tax_Exempt_From_Invoice to IDT_Apps  

Modifications:
Who     Date      Modification  Note
--------------------------------------------------------------
CAS     12/28/04  Created
CAS     05/14/07  Moved to the Sales database
CAS     07/25/07  Reworked logic used to determine if an invoice is exempt.
                  One record is returned if the invoice is exempt.  This should
                  not be called if the Ship_Cntry is not in the IDT_TAXER_COUNTRY_SPECIFIC_DATA
                  table.
*/
Create Procedure [dbo].[Taxer_Internal_Get_Is_Tax_Exempt_From_Invoice]
	@Invoice_Number Int
as
	set nocount on

	select		Descr =
				case	when	(( TCSD.Tax_Orders_With_OligoCards = 0 ) and ( Tmp.NumOligoCardItems > 0 ))
						then	'Exempt Oligo card order'
					when	(( TCSD.Tax_Orders_Paid_With_OligoCards = 0 ) and ( Tmp.Order_Paid_With_Oligo_Card = 1 ))
						then	'Exempt order paid with oligo card'
					else		'Taxable order'
				end
	from		Taxes.dbo.IDT_TAXER_COUNTRY_SPECIFIC_DATA TCSD
	join		(
			select		IH.Inv_Nbr,
					IH.Ship_Cntry,
					IH.Payment_Type,
					Order_Paid_With_Oligo_Card =
						convert ( bit, 
							case	when	IH.Payment_Type = 'OligoCard'	then	1
								else						0
							end
						),
					NumOligoCardItems =
						(
						select		count (*)
						from		dbo.Inv_Line_Items ILI
						where		( ILI.Batch_Ord_ID = IH.Batch_Ord_ID ) and
								( ILI.Prod_Desc Like 'OLIGOCARD%' )
						)
			from		dbo.Inv_Hdr IH
			where		IH.INV_NBR = @Invoice_Number
			) Tmp						On	( TCSD.Country_Name = Tmp.Ship_Cntry )
	where		((( TCSD.Tax_Orders_With_OligoCards = 0 ) and ( Tmp.NumOligoCardItems > 0 )) or
			 (( TCSD.Tax_Orders_Paid_With_OligoCards = 0 ) and ( Tmp.Order_Paid_With_Oligo_Card = 1 )))

	set nocount off



GO
GRANT EXECUTE ON  [dbo].[Taxer_Internal_Get_Is_Tax_Exempt_From_Invoice] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[Taxer_Internal_Get_Is_Tax_Exempt_From_Invoice] TO [IDT_WebApps]
GO
