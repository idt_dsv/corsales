SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
Author: Ben Viering
Date Created: 6/13/09
Purpose:
  Gets Manufacturing Location information for a list of SalesOrderNumbers

example:
exec dbo.[MfgLocationsForSalesOrderNumbers] "160308,160309"

Permissions:  grant execute on dbo.[MfgLocationsForSalesOrderNumbers]  to idt_apps

Modifications:
Who     Date        Modification  Note
--------------------------------------------------------------

*/
CREATE PROCEDURE [dbo].[MfgLocationsForSalesOrderNumbers]
(@SalesOrderNumbers varchar(max))
AS
BEGIN
		
	select SalesOrderNbr = Ord.Sales_Ord_Nbr
	, ReferenceNbr = REF_ID
	, ProductId = LIRN_PROD_ID
	, BranchLocationId = BRANCH_LOCATION_ID
	from dbo.breakApartStr(@SalesOrderNumbers) bas 
	join ord (nolock) on ord.Sales_Ord_Nbr = bas.StrValue
	join line_item_ref_nbr lirn (nolock) on ord.ord_id = lirn.fk_ord_id
	
END



GO
GRANT EXECUTE ON  [dbo].[MfgLocationsForSalesOrderNumbers] TO [IDT_APPS]
GO
