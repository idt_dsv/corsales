SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
GRANT  EXECUTE  ON [dbo].[GetProcessPreferenceInfo] TO [Coliseum]
GRANT  EXECUTE  ON [dbo].[GetProcessPreferenceInfo] TO [AppSalesSin]

Author: Phil Shaff
Date Created: April 14th, 2014
Purpose:

	Retrieves information necessary create a process preferece object

Modifications:
Who	Date	Modification	Note
---------------------------------------------------------------------------------------

*/

CREATE PROCEDURE [dbo].[GetProcessPreferenceInfo]
@OwnerId int,
@TypeId int

AS

SELECT [PROCESS_PREFERENCE_EXPANDED_ID]
      ,[PROCESS_PREFERENCE_CID]
      ,[PROC_PREF_GRP_ID]
      ,[PROC_PREF_DEF_ID]
      ,[OPTION_VALUE]
      ,[OPTION_OWNER_CID]
      ,[OPTION_OWNER_OID]
      ,[LOCATION_ID]
      ,[OVERRIDE_TYPE]
      ,[OVERRIDE_SOURCE_CID]
      ,[OVERRIDE_SOURCE_OID]
      ,[VERSION_NBR]
      ,[VERSION_DTM]
FROM [SALES].[dbo].[PROCESS_PREFERENCE_EXPANDED] WITH (NOLOCK)
WHERE OPTION_OWNER_OID = @OwnerId AND OPTION_OWNER_CID = @TypeId


GO
GRANT EXECUTE ON  [dbo].[GetProcessPreferenceInfo] TO [coliseum]
GO
