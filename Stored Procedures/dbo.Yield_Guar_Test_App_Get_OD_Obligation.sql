SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: Chris A. Sailor
Date Created: February 28, 2005
Purpose:

  This procedure is intended the OD Obligation for a specified
oligo for the Yield_Guar_Test_App.

Permissions:
Grant Execute On dbo.Yield_Guar_Test_App_Get_OD_Obligation to IDT_WebApps
Grant Execute On dbo.Yield_Guar_Test_App_Get_OD_Obligation to IDT_Apps

Modifications:
Who     Date      Modification  Note
--------------------------------------------------------------
CAS     02/22/05  Chris Sailor  Created
CAS     04/13/07  Chris Sailor  Moved out of Manufacturing database
*/
CREATE procedure [dbo].[Yield_Guar_Test_App_Get_OD_Obligation]
	@Ref_ID Int
as
	set nocount on

	select	OD_OBLIGATION 
	from	dbo.oligo_spec (nolock)
	where	ref_id = @Ref_ID

	set nocount off


GO
GRANT EXECUTE ON  [dbo].[Yield_Guar_Test_App_Get_OD_Obligation] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[Yield_Guar_Test_App_Get_OD_Obligation] TO [IDT_WebApps]
GO
