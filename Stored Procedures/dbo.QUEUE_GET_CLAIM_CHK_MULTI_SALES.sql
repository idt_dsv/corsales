SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO



/*
Author: Neil McEntaggart
Date: 2/27/2004
Notes: Takes a list of queue ids and claims them for the calling app. List 
       can be passed as a comma separated list. Intended to be called from 
       a selector for the claim check object.        
Usage : EXEC dbo.QUEUE_GET_CLAIM_CHK_MULTI_SALES '10,11,13,14',4,1,'MyApp' 
 
grant execute on QUEUE_GET_CLAIM_CHK_MULTI_SALES to IDT_Apps
Revisions:
Date   Who  Notes
-----------------------------------------
2005/08/05 NOM  Added @@TRANCOUNT check to fix 
                "not in transaction" error in case 
                where the update failed and the tran 
                was rolled back. 

*/
CREATE    PROCEDURE [dbo].[QUEUE_GET_CLAIM_CHK_MULTI_SALES] ( 
   @QUEUE_ITEM_SALES_IDS varchar(8000), 
   @COUNT_QUEUE_ITEMS int, 
   @CLAIMED_WORKSTATION_ID int, 
   @APPLICATION_ID int  )
AS 
BEGIN 
  /*You can only claim the record if you already have a claim,  
  in which case the age of your claim will be updated or if 
  it is currently unclaimed*/

  DECLARE @RowCount int, @Err int 
  DECLARE @ids table (id int)

  INSERT INTO @ids 
    SELECT Convert(int, StrValue ) 
      FROM dbo.BreakApartStr(@QUEUE_ITEM_SALES_IDS) 
  
  BEGIN TRANSACTION QUEUE_GET_CLAIM_CHK_MULTI_SALES
  IF (SELECT count(*) 
        FROM dbo.QUEUE_ITEM_SALES Q 
             JOIN @ids i ON Q.QUEUE_ITEM_SALES_ID = i.ID 
       WHERE ( (CLAIMED_WORKSTATION_ID = @CLAIMED_WORKSTATION_ID AND 
                APPLICATION_ID         = @APPLICATION_ID) OR            
               (IsNull(CLAIMED_WORKSTATION_ID,0) = 0 AND 
                IsNull(APPLICATION_ID,0) = 0 )
              )) = @COUNT_QUEUE_ITEMS 
  BEGIN 
    UPDATE QUEUE_ITEM_SALES
       SET CLAIMED_WORKSTATION_ID = @CLAIMED_WORKSTATION_ID,
           APPLICATION_ID         = @APPLICATION_ID,
           CLAIMED_DTM            = GetDate()
      FROM @ids i
     WHERE QUEUE_ITEM_SALES_ID = i.ID
       AND ( (CLAIMED_WORKSTATION_ID = @CLAIMED_WORKSTATION_ID AND 
              APPLICATION_ID           = @APPLICATION_ID) OR            
             (IsNull(CLAIMED_WORKSTATION_ID,0) = 0 AND 
              IsNull(APPLICATION_ID,0)         = 0 )
           )      
  END
  
  SELECT @RowCount = @@ROWCOUNT, @Err = @@ERROR
  IF @Err <> 0 OR @RowCount <> @COUNT_QUEUE_ITEMS  
  BEGIN 
    IF  @@TRANCOUNT  > 0 
      ROLLBACK TRANSACTION QUEUE_GET_CLAIM_CHK_MULTI_SALES   
  END
  ELSE   
    COMMIT TRANSACTION QUEUE_GET_CLAIM_CHK_MULTI_SALES
   
  SELECT QUEUE_ITEM_SALES_ID,
         CLAIMED_WORKSTATION_ID as WorkStationClaimedID,         
         APPLICATION_ID         as ApplicationId,      
         CLAIMED_DTM            as ClaimedDateTime,      
         COMPLETED_DTM          as CompleatedDateTime   
    FROM dbo.QUEUE_ITEM_SALES Q
         JOIN @ids i ON Q.QUEUE_ITEM_SALES_ID = i.ID 

   
END


GO
GRANT EXECUTE ON  [dbo].[QUEUE_GET_CLAIM_CHK_MULTI_SALES] TO [IDT_APPS]
GO
