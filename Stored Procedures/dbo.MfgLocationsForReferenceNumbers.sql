SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
Author: Ben Viering
Date Created: 6/13/09
Purpose:
  Gets Manufacturing Location information for a list of Reference Numbers

example:
exec dbo.[MfgLocationsForReferenceNumbers] "689707,689708,689709"

Permissions:  grant execute on dbo.[MfgLocationsForReferenceNumbers]  to idt_apps

Modifications:
Who     Date        Modification  Note
--------------------------------------------------------------

*/
CREATE PROCEDURE [dbo].[MfgLocationsForReferenceNumbers]
(@ReferenceNumbers varchar(max))
AS
BEGIN
		
	select SalesOrderNbr = Ord.Sales_Ord_Nbr
	, ReferenceNbr = REF_ID
	, ProductId = LIRN_PROD_ID
	, BranchLocationId = BRANCH_LOCATION_ID
	from dbo.breakApartStr(@ReferenceNumbers) bas 
	join line_item_ref_nbr lirn (nolock) on lirn.Ref_ID = bas.StrValue
	join ord (nolock) on ord.ord_id = lirn.fk_ord_id
	
END




GO
GRANT EXECUTE ON  [dbo].[MfgLocationsForReferenceNumbers] TO [IDT_APPS]
GO
