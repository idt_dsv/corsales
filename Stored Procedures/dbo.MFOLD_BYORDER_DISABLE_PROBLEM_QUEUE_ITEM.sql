SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: Chris A. Sailor
Date Created: March 14, 2006
Purpose:

  This procedure is intended to retrieve a batch of sequences
that need to be run through MFold and have values set for them.

grant execute on DBO.MFOLD_BYORDER_DISABLE_PROBLEM_QUEUE_ITEM to IDT_Apps

Modifications:
Who     Date      Modification  Note
--------------------------------------------------------------
CAS     03/14/06  Chris Sailor  Created 
CAS     03/20/07  Chris Sailor  Moved out of Manufacturing database
*/
Create Procedure [dbo].[MFOLD_BYORDER_DISABLE_PROBLEM_QUEUE_ITEM]
	(
	@Queue_Item_ID Int
	)
AS
Begin
	set nocount on

	update	dbo.Queue_Item_MFold
	set	Active = 0
	where	Queue_Item_ID = @Queue_Item_ID

	set nocount off
End

                                                                                                                                
GO
GRANT EXECUTE ON  [dbo].[MFOLD_BYORDER_DISABLE_PROBLEM_QUEUE_ITEM] TO [IDT_APPS]
GO
