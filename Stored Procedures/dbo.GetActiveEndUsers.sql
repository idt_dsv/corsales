SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*==================================================
Author: Tommy Harris
Create Date: 7/21/2011
Description: Gets all of the active end users

Example:
EXEC dbo.GetActiveEndUsers

Permissions: GRANT EXECUTE ON dbo.GetActiveEndUsers to idt_apps
DROP: DROP dbo.GetActiveEndUsers

Modifications:
Who		Date		Modification Note
----------------------------------------

====================================================*/

CREATE PROCEDURE [dbo].[GetActiveEndUsers]
	@Company INT
AS
BEGIN

	CREATE TABLE #PERSON_PARTY (EUNUM INT)
	CREATE TABLE #PERSON_ORG (PID INT, EUNUM INT)

	INSERT INTO #PERSON_PARTY
		SELECT ENDUSER_NBR
		FROM dbo.PERSON per WITH (NOLOCK)
		JOIN PARTY par WITH (NOLOCK) ON per.PARTY_ID = par.PARTY_ID
		WHERE par.CURR_STAT_TYPE IN (1,2,3,10,11,12,13,14)
		
	IF @Company = 0 --INC
		INSERT INTO #PERSON_ORG
			SELECT per.PARTY_ID, ENDUSER_NBR
			FROM dbo.PERSON per WITH (NOLOCK)
			JOIN dbo.ORG o WITH (NOLOCK) ON per.ORG_ID = o.PARTY_ID
			WHERE CLASS_TYPE_ID <> 7
		
	IF @Company = 1 --BVBA
		INSERT INTO #PERSON_ORG
			SELECT per.PARTY_ID, ENDUSER_NBR
			FROM dbo.PERSON per WITH (NOLOCK)
			JOIN dbo.ORG o WITH (NOLOCK) ON per.ORG_ID = o.PARTY_ID
			WHERE CLASS_TYPE_ID = 7
	
	SELECT p.*
	FROM #PERSON_ORG po WITH (NOLOCK)
	JOIN #PERSON_PARTY pp WITH (NOLOCK) ON po.EUNUM = pp.EUNUM
	JOIN dbo.PERSON p WITH (NOLOCK) ON pp.EUNUM = p.ENDUSER_NBR

	DROP TABLE #PERSON_ORG
	DROP TABLE #PERSON_PARTY		
	
END
GO
GRANT EXECUTE ON  [dbo].[GetActiveEndUsers] TO [IDT_APPS]
GO
