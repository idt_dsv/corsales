SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
Author: Scott Ford
Date Created: 2/6/07
Purpose:

  This procedure is intended to retrieve data used to populate an InvoiceLineItems web object.

Modifications:
Who     Date      Modification  Note
--------------------------------------------------------------
sf	    10/8/08   added ORDERBY clause to make par group items come back in display order
SF		3/1/07	  Changed return value of the ConvertedSubTotal to be the (converted price) * (qty).  Rounding errors were resulting from directly converting the subtotal  
KMZ		3/3/03	  Added extended xml for cxml Invoicing so we can send SAP line numbers
John Wood 10/04/2011 Added dedup logic as Coliseum is double writing to sales.LINE_ITEM_REF_NBR_EXT_SPEC
SF		12/8/11		Added union subquery, to always pull punchout extendedspecs first, then top 1 of anything else if null (cxml invoices require line # from punchout specs, so it must be selected if it can be)
SSD		3/5/14	  Added natural unit price and natural subtotal, since those seem more reliable than the converted prices
*/
CREATE Procedure [dbo].[INVOICE_GET_LINEITEMS]
	@BatchOrdID int,
	@ExchangeRate nvarchar(50) 
As
set nocount on

/* --Original
        select INV_LINE_ITEM_ID, QTY, PROD_DESC, UNIT_PRICE, SEQ_DESC, REF_NBR, 
			LINE_ITEM_TYPE, AMOUNT, PAR_GRP, 
			dbo.Currency_Natural_Conversion(UNIT_PRICE, @ExchangeRate, 0) as ConvertedUnitPrice,   
			dbo.Currency_Natural_Conversion(UNIT_PRICE, @ExchangeRate, 0) * QTY as ConvertedSubTotal,
			XML_SCHEMA_TYPE_ID,
			DATA
		from INV_LINE_ITEMS with (nolock) left join
		LINE_ITEM_REF_NBR_EXT_SPEC with (nolock) on LINE_ITEM_REF_NBR_EXT_SPEC.REF_ID = INV_LINE_ITEMS.REF_NBR
		where batch_ord_id = @BatchOrdID
		order by LINE_ITEM_TYPE,PAR_GRP,INV_LINE_ITEM_ID
		
*/

SELECT
	ili.INV_LINE_ITEM_ID, 
	ili.QTY, 
	ili.PROD_DESC, 
	ili.UNIT_PRICE, 
	ili.SEQ_DESC,  
	ili.REF_NBR ,
    ili.LINE_ITEM_TYPE, 
    ili.AMOUNT, 
    ili.PAR_GRP,
    ili.UNIT_PRICE_NATURAL,
    ili.AMOUNT_NATURAL,
    dbo.Currency_Natural_Conversion(ili.UNIT_PRICE, @ExchangeRate, 0) AS ConvertedUnitPrice,   
    dbo.Currency_Natural_Conversion(ili.UNIT_PRICE, @ExchangeRate, 0) * ili.QTY AS ConvertedSubTotal,
    ext.XML_SCHEMA_TYPE_ID,
	ext.DATA,
	ext.LINE_ITEM_REF_NBR_EXT_SPEC_ID
FROM SALES.DBO.INV_LINE_ITEMS AS ili WITH (NOLOCK) 
LEFT OUTER JOIN SALES.DBO.LINE_ITEM_REF_NBR_EXT_SPEC AS ext WITH (NOLOCK) 
	ON ext.REF_ID = ili.REF_NBR
WHERE 
ili.BATCH_ORD_ID = @BatchOrdID

AND 
(
      ext.LINE_ITEM_REF_NBR_EXT_SPEC_ID = 
                  (
                              
                                (
                                      (
                                            SELECT  top 1 lirnes2.LINE_ITEM_REF_NBR_EXT_SPEC_ID
                                            FROM SALES.DBO.LINE_ITEM_REF_NBR_EXT_SPEC AS lirnes2 WITH (NOLOCK) 
                                            WHERE XML_SCHEMA_TYPE_ID = 13
                                                      AND
                                                      lirnes2.REF_ID = ili.REF_NBR 
                              )
                                          UNION
                              (
                                            SELECT  top 1 lirnes2.LINE_ITEM_REF_NBR_EXT_SPEC_ID
                                            FROM SALES.DBO.LINE_ITEM_REF_NBR_EXT_SPEC AS lirnes2 WITH (NOLOCK)
                                            where 
                                            NOT EXISTS (
                                                                        SELECT * 
                                                                        FROM SALES.DBO.LINE_ITEM_REF_NBR_EXT_SPEC AS lirnes2_sub  WITH (NOLOCK) 
                                                                        WHERE lirnes2.REF_ID = lirnes2_sub.REF_id
                                                                                    AND
                                                                                    XML_SCHEMA_TYPE_ID = 13
                                                                  )
                                                AND lirnes2.REF_ID = ili.REF_NBR
                                          )
                                )
                              )
                 
      OR 
      ext.LINE_ITEM_REF_NBR_EXT_SPEC_ID IS NULL
)
ORDER BY ili.LINE_ITEM_TYPE
      ,ili.PAR_GRP
      ,ili.INV_LINE_ITEM_ID


	set nocount off





GO
GRANT EXECUTE ON  [dbo].[INVOICE_GET_LINEITEMS] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[INVOICE_GET_LINEITEMS] TO [IDT_Reporter]
GO
