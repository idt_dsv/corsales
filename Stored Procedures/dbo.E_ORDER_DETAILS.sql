SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Stored Procedure

/**********************************************************************************************************************************************
 Web_Order_Details
 AUTHOR: 
 CREATED ON: 
 DESCRIPTION/ALGORITHM: 
	NOTE: Coalesce is added to prevent nulls, b/c they are hard to manage in ASP

 example:
 exec E_ORDER_DETAILS 1500000
 exec E_ORDER_DETAILS 5397217 --Duplex Kit
 exec E_ORDER_DETAILS 5401310 --Gene
 exec E_ORDER_DETAILS 5401335 --Plate
 exec E_ORDER_DETAILS 5398172 --Modifications
 EXEC E_ORDER_DETAILs '2105328' --PrimeTime

 Permissions:
  grant exec on E_ORDER_DETAILS to idt_webapps

 REVISIONS:
 - 7/23/02 NL Changed Select Statement to use OLIGO_SPEC instead of LORD_LINE_ITEM_SPEC and 
                              LINE_ITEM_REF_NBR instead of PROD_LINE_ITEM table
 - 12/3/02 MDW Added Shipped_MFG_ITEM_ID per Mark Schebel's request and added (NOLOCK) to reduce contention
 - 10/8/03 MDW Modified for Duplex Oligo
 - 2/23/04 MDW fixed bug in Duplex oligo - added link to ref_nbr_mfg to get correct mfg_item_id
           and used new function IS_PROD_CAT_ON_REF_ID
 - 11/15/05 CHANGE named from Web_Order_Details, removed is_starfire and added mfg location
            shipped_mfg_item - due to db splits
 - 2/22/07 JRH Added price and exchange rate
 - 11/19/09 RHW Modified to include information for genes and plates into the returned data set.
 - 10/6/11 JJD Returns a Seq_Desc (Project_Name from LINE_ITEM_REF_NBR) for Kits and Mixes
**********************************************************************************************************************************************/

CREATE	Procedure [dbo].[E_ORDER_DETAILS]
	@SALES_ORD_NBR int
As

  set nocount on

  CREATE TABLE #orderdetail 
  (
    PROD_ID int,
    Product_NM VARCHAR(255),  
    Purification_NM VARCHAR(255), 
    Purif_ID int,   
    OligoNbr int,
    Quantity int,
    SeqLen int,
    SeqDesc VARCHAR(255),
    Seq VARCHAR(255),
    Notes VARCHAR(255),
    OldSeq VARCHAR(255),
    BRANCH_LOCATION_ID int,
    REF_ID int,
    PRICE MONEY,
    ExchangeRate FLOAT
  )

  INSERT #orderdetail
  Select 
    PROD_ID = LINE_ITEM_REF_NBR.LIRN_PROD_ID,
    Product_NM = ORD_LINE_ITEM.LINE_ITEM_DESC,  
    Purification_NM = COALESCE(dbo.GET_PURIFICATION_NAME(LINE_ITEM_REF_NBR.REF_ID),''), 
    Purif_ID = 0,   
    OligoNbr = COALESCE(LINE_ITEM_REF_NBR.REF_ID,0),
    Quantity = COALESCE(ORD_LINE_ITEM.QUANTITY,0),
    SeqLen = 0,
    SeqDesc = '',
    Seq = '',
    Notes = '',
    OldSeq = '',
    BRANCH_LOCATION_ID,
    LINE_ITEM_REF_NBR.REF_ID,
    PRICE = dbo.GET_LINE_ITEM_PRICE (ORD_LINE_ITEM.LINE_ITEM_ID),
    ExchangeRate = COALESCE(ORD.[EXCHANGE_RATE],1)
  From Ord with (nolock)
    JOIN dbo.LINE_ITEM_REF_NBR with (nolock) ON ORD.ORD_ID = LINE_ITEM_REF_NBR.FK_ORD_ID
    JOIN ORD_LINE_ITEM with (nolock) ON LINE_ITEM_REF_NBR.LINE_ITEM_ID = ORD_LINE_ITEM.LINE_ITEM_ID
  Where
  ( ORD.SALES_ORD_NBR = @SALES_ORD_NBR )
  Order By LINE_ITEM_REF_NBR.REF_ID

    -- update information for those items that are kits or mixes
  UPDATE #orderdetail
  SET 
    SeqDesc = COALESCE(CONVERT(VARCHAR(35), lirn.PROJECT_NAME), '')
  FROM #orderdetail od
  join product p with (nolock) on p.prod_id = od.PROD_ID AND p.OBJ_TYPE In (18, 22)
  JOIN LINE_ITEM_REF_NBR lirn WITH (NOLOCK) ON od.REF_ID = lirn.REF_ID

  -- update information for those items that are oligos
  UPDATE #orderdetail
  SET 
    Purif_ID = COALESCE(OLIGO_SPEC.PURIF_ID,0),   
    SeqLen = COALESCE(OLIGO_SPEC.BASES,0),
    SeqDesc = COALESCE(CONVERT(VARCHAR(35), OLIGO_SPEC.SEQ_DESC),
                (select DOS.SPEC_NAME from DUPLEX_OLIGO_SPEC DOS (nolock) where dos.ref_id = [#orderdetail].REF_ID),
                 ''),
    Seq = COALESCE(CONVERT(VARCHAR(255), dbo.FORMAT_OLIGO_SEQUENCE(OLIGO_SPEC.SEQ)),''),
    Notes = COALESCE(CONVERT(VARCHAR(255), OLIGO_SPEC.SEQ_DESC2),'')
  FROM #orderdetail
  JOIN OLIGO_SPEC with (nolock) ON [#orderdetail].REF_ID = OLIGO_SPEC.REF_ID

  -- update information for those items that are oligos
  UPDATE #orderdetail
  SET 
    SeqDesc = COALESCE(CONVERT(VARCHAR(35), DUPLEX_OLIGO_SPEC.SPEC_NAME), '')
  FROM #orderdetail
  JOIN DUPLEX_OLIGO_SPEC with (nolock) ON [#orderdetail].REF_ID = DUPLEX_OLIGO_SPEC.REF_ID

  -- update information for those items that are genes
  UPDATE #orderdetail
  SET 
    SeqLen = DATALENGTH(GENE_SPEC.GENE_SEQ),
    SeqDesc = COALESCE(CONVERT(VARCHAR(35), GENE_SPEC.GENE_DESCR), ''),
    Seq = COALESCE(CONVERT(VARCHAR(255), GENE_SPEC.GENE_SEQ),''),
    Notes = COALESCE(CONVERT(VARCHAR(255), GENE_SPEC.GENE_NOTES),'')
  FROM #orderdetail
  JOIN GENE_SPEC WITH (NOLOCK) ON [#orderdetail].REF_ID = GENE_SPEC.REF_ID


  -- update information for those items that are plates
  UPDATE #orderdetail
  SET 
    SeqDesc = COALESCE(CONVERT(VARCHAR(35),PLATE_SPEC.NAME), '')
  FROM #orderdetail
  JOIN PLATE_SPEC WITH (NOLOCK) ON [#orderdetail].REF_ID = PLATE_SPEC.REF_ID


  SELECT *		
  FROM #orderdetail
  ORDER BY [#orderdetail].REF_ID
  
GO
GRANT EXECUTE ON  [dbo].[E_ORDER_DETAILS] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[E_ORDER_DETAILS] TO [IDT_Reporter]
GO
