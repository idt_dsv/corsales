SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
--GRANT  EXECUTE  ON [dbo].[GetConfirmationEmailPlateInfo] TO [IDT-Coralville\iusr_idt]
--GRANT  EXECUTE  ON [dbo].[GetConfirmationEmailPlateInfo] TO [Coliseum]
--GRANT  EXECUTE  ON [dbo].[GetConfirmationEmailPlateInfo] TO [AppSalesSin]

Author: Phil Shaff
Date Created: Feb 26th, 2014
Purpose:

	Retrieves information necessary to setup Order Confirmation E-mail, this instance returns plate information

Modifications:
Who	Date	Modification	Note
---------------------------------------------------------------------------------------

*/

CREATE PROCEDURE [dbo].[GetConfirmationEmailPlateInfo]
@plateRefId int

AS

				SELECT ps.item_count
					, cd.name as PlateType
					, ps.name
					, lrn.normalized_qty
					, uom.name as UOM
					, lrn.final_volume
					, ls.name as LoadScheme
					, lrn.ship_wet
					,lrn.Line_item_id
                FROM sales.dbo.plate_spec as ps with (nolock)
                INNER JOIN sales.dbo.container_defn as cd with (nolock) on ps.container_def_id = cd.container_def_id
                INNER JOIN sales.dbo.line_item_ref_nbr as lrn with (nolock) on lrn.ref_id = ps.ref_id
                LEFT OUTER JOIN sales.dbo.uom AS uom with (nolock) on uom.uom_id = lrn.normalized_uom_id
                INNER JOIN sales.dbo.loading_scheme as ls with (nolock) on ls.loading_scheme_id = ps.loading_scheme_id
                WHERE ps.ref_id =  @plateRefId


GO
GRANT EXECUTE ON  [dbo].[GetConfirmationEmailPlateInfo] TO [coliseum]
GRANT EXECUTE ON  [dbo].[GetConfirmationEmailPlateInfo] TO [IDT-CORALVILLE\IUSR_IDT]
GO
