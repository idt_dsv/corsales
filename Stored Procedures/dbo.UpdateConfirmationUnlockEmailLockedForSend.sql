
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
--GRANT  EXECUTE  ON [dbo].[UpdateConfirmationUnlockEmailLockedForSend] TO [IDT-Coralville\iusr_idt]

Author: Phil Shaff
Date Created: January 27th, 2014
Purpose:

	Updates confirmation e-mail to be sent, unlocks e-mail locked for send

Modifications:
Date	  Who		  Modification	
---------------------------------------------------------------------------------------
7/17/14   pshaff      Added workstation information
*/

CREATE PROCEDURE [dbo].[UpdateConfirmationUnlockEmailLockedForSend]
@ConfirmationId INT,
@WorkStationName varchar(64)

AS

	DECLARE @WORKSTATION_ID INT

	SET @WORKSTATION_ID = (SELECT workstation_id
                       FROM   framework_system.dbo.workstation WITH (nolock)
                       WHERE  name = @WorkStationName)

	UPDATE [dbo].CONFIRMATION 
		SET STATUS = 0, claimed_dtm = null, claimed_workstation_id = null
	WHERE COALESCE(STATUS,0) = 2 
		AND CON_ID = @ConfirmationId 
		AND CLAIMED_WORKSTATION_ID = @WORKSTATION_ID

GO

GRANT EXECUTE ON  [dbo].[UpdateConfirmationUnlockEmailLockedForSend] TO [coliseum]
GRANT EXECUTE ON  [dbo].[UpdateConfirmationUnlockEmailLockedForSend] TO [IDT-CORALVILLE\IUSR_IDT]
GO
