SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
Author: Scott Ford
Date Created: 2/6/07
Purpose:

  This procedure is intended to retrieve data used to populate an InvoiceHeader web object.

Modifications:
Who     Date      Modification  Note
--------------------------------------------------------------
sf		2/19/07	  Changed proc to pull data from INV_HDR_VW instead of the INV_HDR table.  the view retrieves the most recent invoice data
sf	    3/28/13   Added Incoterm code
sf		7/31/13   Added TAX_RATE
SSD		3/5/14	  Added natural currency invoice total, since that's more reliable than converting the USD total
*/
CREATE Procedure [dbo].[INVOICE_GET_HEADER]
	@SalesOrdNbr int
As
set nocount on

            select 
			i.BATCH_ORD_ID,
			i.BATCH_ID, 
			i.SALES_ORD_NBR, 
			i.INV_NBR, 
			i.CURR_STAT, 
			i.BILL_ACCT_ID, 
			i.ORG_NBR, 
			i.ACCT_NBR, 
			i.ENDUSER_NBR, 
			i.INV_TOTAL, 
			i.INV_LINES, 
			i.SHIP_DT, 
			i.ORD_DT, 
			i.INV_DT,
			i.PAY_AUTH_NBR, 
			i.PAYMENT_TYPE, 
			i.PRICE_GRP_CD, 
			i.SHIP_METH,
			i.SHIP_NM, 
			i.SHIP_ADDR1, 
			i.SHIP_ADDR2, 
			i.SHIP_CITY, 
			i.SHIP_ST, 
			i.SHIP_ZIP, 
			i.SHIP_CNTRY, 
			i.SHIP_PH, 
			i.BILL_NM, 
			i.BILL_ADDR1, 
			i.BILL_ADDR2, 
			i.BILL_CITY, 
			i.BILL_ST, 
			i.BILL_ZIP, 
			i.BILL_CNTRY, 
			i.BILL_PH, 
			i.SHIP_ENDUSER_NM, 
			i.ORD_METH, 
			c.CURRENCY_ID, 
			i.EXCHANGE_RATE, 
			i.BILL_FAX, 
			i.DELIVERY_METHOD_ID, 
			c.EPICOR_CURRENCY_CODE, 
			c.SYMBOL, 
			c.REMIT_TO_ADDRESS, 
			e.terms_code, 
			i.INCOTERM_CODE , 
			i.TAX_RATE,
			i.INV_TOTAL_NATURAL
            from Sales.dbo.inv_hdr_vw as i with (nolock) 
				left outer join Sales.dbo.currency as c with (nolock) on i.currency_id = c.CURRENCY_ID 
				left outer join Sales.dbo.EPICOR_ARCUST_VW as e with (nolock) on e.customer_code = i.BILL_ACCT_ID 
            where i.sales_ord_nbr = @SalesOrdNbr

	set nocount off




GO
GRANT EXECUTE ON  [dbo].[INVOICE_GET_HEADER] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[INVOICE_GET_HEADER] TO [IDT_Reporter]
GO
