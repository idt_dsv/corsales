SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: Chris A. Sailor
Date Created: August 1, 2005
Purpose:

  This procedure is intended to insert or update the data calculated
by the MFold service, as well as any guarantee changes that occur
because of secondary structure.

grant execute on DBO.MFold_ByOrder_Update_Fold_Data to IDT_Apps

Modifications:
Who     Date      Modification  Note
--------------------------------------------------------------
CAS     08/01/05  Created
CAS     09/18/06  Uncommented out the purity update to spec
CAS     03/20/07  Chris Sailor  Moved out of Manufacturing database
*/
CREATE Procedure [dbo].[MFOLD_BYORDER_UPDATE_FOLD_DATA]
	(
	@Ref_ID Integer,
	@Delta_G numeric ( 10, 2 ),
	@Delta_S numeric ( 10, 2 ),
	@Delta_H numeric ( 10, 2 ),
	@Fold_TM numeric ( 10, 2 ),
	@Fold_Affects_Guarantees bit,
	@MFold_Engine_Version_ID Int,
	@MFold_Explanation_ID Int,
	@UpdatedByBatchProcess bit,
	@Comment varchar ( 2000 ),
	@UpdateYield bit,
	@UpdatePurity bit,
	@OD_OBLIGATION float,
	@OD_MAX_OBLIGATION int,
	@OD_MIN_SYNTH_REQ float,
	@OD_MAX_SYNTH_REQ int,
	@YIELD_GUAR_POINTS int,
	@PURITY_GUAR_PCT float,
	@PURITY_GUAR_EXPLANATION_ID int,
	@PURITY_GUAR_POINTS int
	)
As
	set nocount on

	Declare @UpdatedDate DateTime
	Declare @RefIDCount Int

	Set @UpdatedDate = GetDate ( )

/*	-- Restore these when we are ready to actually begin using the MFold data
	-- Update the Yield Guarantee info, if needed
	if ( @UpdateYield = 1 ) begin
		Update	dbo.Oligo_Spec
		Set	OD_OBLIGATION = @OD_OBLIGATION,
			OD_MAX_OBLIGATION = @OD_MAX_OBLIGATION,
			OD_MIN_SYNTH_REQ = @OD_MIN_SYNTH_REQ,
			OD_MAX_SYNTH_REQ = @OD_MAX_SYNTH_REQ,
			YIELD_GUAR_POINTS = @YIELD_GUAR_POINTS
		Where	Ref_ID = @Ref_ID
	end
*/
	-- Update the Purity Guarantee info, if needed
	if ( @UpdatePurity = 1 ) begin
		Update	dbo.Oligo_Spec
		Set	PURITY_GUAR_PCT = @PURITY_GUAR_PCT,
			PURITY_GUAR_EXPLANATION_ID = @PURITY_GUAR_EXPLANATION_ID,
			PURITY_GUAR_POINTS = @PURITY_GUAR_POINTS
		Where	Ref_ID = @Ref_ID
	end


	-- Determine if we need to insert or update a record into the
	-- MFold_Calculation table
	Select	@RefIDCount = count (*)
	from	dbo.MFold_Calculation
	where	Ref_ID = @Ref_ID

	if ( @RefIDCount > 0 ) begin
		Update	dbo.MFold_Calculation
		Set	Delta_G = @Delta_G,
			Delta_S = @Delta_S,
			Delta_H = @Delta_H,
			Fold_TM = @Fold_TM,
			Fold_Affects_Guarantees = @Fold_Affects_Guarantees,
			MFold_Engine_Version_ID = @MFold_Engine_Version_ID,
			MFold_Explanation_ID = @MFold_Explanation_ID,
			Updated_By_Batch_Process = @UpdatedByBatchProcess,
			Updated_DTM = @UpdatedDate,
			Debugging_Comment = @Comment,
			UpdateYield = @UpdateYield,
			UpdatePurity = @UpdatePurity,
			OD_OBLIGATION = @OD_OBLIGATION,
			OD_MAX_OBLIGATION = @OD_MAX_OBLIGATION,
			OD_MIN_SYNTH_REQ = @OD_MIN_SYNTH_REQ,
			OD_MAX_SYNTH_REQ = @OD_MAX_SYNTH_REQ,
			YIELD_GUAR_POINTS = @YIELD_GUAR_POINTS,
			PURITY_GUAR_PCT = @PURITY_GUAR_PCT,
			PURITY_GUAR_EXPLANATION_ID = @PURITY_GUAR_EXPLANATION_ID,
			PURITY_GUAR_POINTS = @PURITY_GUAR_POINTS
		Where	Ref_ID = @Ref_ID
	end
	else begin
		Insert Into dbo.MFold_Calculation (
			Ref_ID,
			Delta_G,
			Delta_S,
			Delta_H,
			Fold_TM,
			Fold_Affects_Guarantees,
			MFold_Engine_Version_ID,
			MFold_Explanation_ID,
			Updated_By_Batch_Process,
			Updated_DTM,
			Debugging_Comment,
			UpdateYield,
			UpdatePurity,
			OD_OBLIGATION,
			OD_MAX_OBLIGATION,
			OD_MIN_SYNTH_REQ,
			OD_MAX_SYNTH_REQ,
			YIELD_GUAR_POINTS,
			PURITY_GUAR_PCT,
			PURITY_GUAR_EXPLANATION_ID,
			PURITY_GUAR_POINTS
			)
		Values (
			@Ref_ID,
			@Delta_G,
			@Delta_S,
			@Delta_H,
			@Fold_TM,
			@Fold_Affects_Guarantees,
			@MFold_Engine_Version_ID,
			@MFold_Explanation_ID,
			@UpdatedByBatchProcess,
			@UpdatedDate,
			@Comment,
			@UpdateYield,
			@UpdatePurity,
			@OD_OBLIGATION,
			@OD_MAX_OBLIGATION,
			@OD_MIN_SYNTH_REQ,
			@OD_MAX_SYNTH_REQ,
			@YIELD_GUAR_POINTS,
			@PURITY_GUAR_PCT,
			@PURITY_GUAR_EXPLANATION_ID,
			@PURITY_GUAR_POINTS
			)
			
	end
	
	set nocount off


GO
GRANT EXECUTE ON  [dbo].[MFOLD_BYORDER_UPDATE_FOLD_DATA] TO [IDT_APPS]
GO
