SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: MDEWAARD
Date: 3/19/03
NOTES:
-- Expires quotes that ended today
-- Requested by J Moore to be added in nightly updates

revisions:

*/

create procedure [dbo].[Quote_Expire]
as
begin

update quote set quote_Status_id = 2
where thru_Dt <= convert(varchar(8),Getdate(),1)
 and quote_Status_id = 1

end
GO
