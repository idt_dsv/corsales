SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/* [GetBioRadSalesData]
Author: Tony DeJaynes/Carl Zauche
Date: 6/19/2012
Notes: -Retrieves data for a bio rad order
Used by org name matching to search for possible org to match. 

Permissions:
grant execute on [GetBioRadSalesData] to IDT_APPS
Execute [GetBioRadSalesData] 7554937 


Modifications:
Date	Who	Notes
-------------------------------------
7/10/2012 TAD	Added snythetic RNA also
8/07/2012 TAD	Changed to get QTY as a sum for replicates, or others broken down into 
					new parts
*/
CREATE  PROCEDURE [dbo].[GetBioRadSalesData]
	@SalesId INT
AS
BEGIN
	SELECT a.OLI_PROD_ID, BioRadLineNbr, XML_RetailPrice, retail_price, CustPrice, unit_price,
		SALES_ORD_NBR, BioRadSalesOrderNbr, LINE_ITEM_NAME, BioRadWebOrderNbr, BioRadProductName,
		ProductSKU, SUM(a.qty) QTY
		FROM (
	SELECT oli.OLI_PROD_ID,
	  lirnes.DATA.value('(//Extrinsic[@name="BioRadLineNbr"])[1]', 'nvarchar(100)') AS BioRadLineNbr,
	  lirnes.DATA.value('(//Extrinsic[@name="RetailPrice"])[1]', 'Money') AS XML_RetailPrice,
	  oli.retail_price,
	  lirnes.DATA.value('(//Extrinsic[@name="CustPrice"])[1]', 'Money') AS CustPrice,
	  RETAIL_OVER_PRICE_NATURAL unit_price,
	  o.SALES_ORD_NBR, 
	  lirnes.DATA.value('(//Extrinsic[@name="BioRadSalesOrderNbr"])[1]', 'nvarchar(100)') AS BioRadSalesOrderNbr,  
	  lirn.LINE_ITEM_NAME,
	  lirnes.DATA.value('(//Extrinsic[@name="BioRadWebOrderNbr"])[1]', 'nvarchar(100)') AS BioRadWebOrderNbr,
	  lirnes.DATA.value('(//Extrinsic[@name="BioRadProductName"])[1]', 'nvarchar(100)') AS BioRadProductName,
	  lirnes.DATA.value('(//Extrinsic[@name="ProductSKU"])[1]', 'nvarchar(100)') AS ProductSKU,
	  Qty = oli.QUANTITY + (SELECT COUNT (*) FROM sales.dbo.ORD_LINE_ITEM oli2  WITH (NOLOCK) 
		WHERE oli2.PAR_LINE_ITEM = oli.LINE_ITEM_ID AND oli2.OLI_PROD_ID = 2157) -- 2157 = replicate plate

	FROM sales.dbo.ord AS o WITH (NOLOCK) 
	JOIN sales.dbo.ORD_LINE_ITEM AS oli WITH (NOLOCK) ON o.ORD_ID = oli.ORD_ID
	LEFT JOIN sales.dbo.LINE_ITEM_REF_NBR AS lirn WITH (NOLOCK) ON oli.LINE_ITEM_ID = lirn.LINE_ITEM_ID
	JOIN sales.dbo.LINE_ITEM_REF_NBR_EXT_SPEC AS lirnes WITH (NOLOCK) ON lirnes.REF_ID = lirn.REF_ID
	INNER JOIN dbo.LINE_ITEM_PRICE_DETAIL AS lipd ON lirn.LINE_ITEM_ID = lipd.LINE_ITEM_ID
	WHERE o.SALES_ORD_NBR = @SalesId
	AND oli.LEV = 1 AND oli.OLI_PROD_ID NOT IN (7596, 5729) -- synthetic DNA & RNA, attached to plate
	) AS A
	GROUP BY a.OLI_PROD_ID, BioRadLineNbr, XML_RetailPrice, retail_price, CustPrice, unit_price,
		SALES_ORD_NBR, BioRadSalesOrderNbr, LINE_ITEM_NAME, BioRadWebOrderNbr, BioRadProductName,
		ProductSKU
	
	
END
GO
GRANT EXECUTE ON  [dbo].[GetBioRadSalesData] TO [IDT_APPS]
GO
