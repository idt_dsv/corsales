SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
--GRANT  EXECUTE  ON [dbo].[GetConfirmationEmailOrderedItems] TO [IDT-Coralville\iusr_idt]
--GRANT  EXECUTE  ON [dbo].[GetConfirmationEmailOrderedItems] TO [Coliseum]
--GRANT  EXECUTE  ON [dbo].[GetConfirmationEmailOrderedItems] TO [AppSalesSin]

Author: Phil Shaff
Date Created: Feb 26th, 2014
Purpose:

	Retrieves information necessary to setup Order Confirmation E-mail, ordered items

Modifications:
Who	Date	Modification	Note
---------------------------------------------------------------------------------------

*/

CREATE PROCEDURE [dbo].[GetConfirmationEmailOrderedItems]
@OrderId int

AS

	create table #Ord_Line_Item
	   (ord_id int,
	    line_item_id int,
	    par_line_item int,
		oli_prod_id int,
		line_item_desc varchar(50),
		master_line_item_id int)

	insert into #Ord_Line_Item
	   (ord_id,
	    line_item_id,
	    par_line_item,
		oli_prod_id,
		line_item_desc,
		master_line_item_id)
	select ord_id,
	    line_item_id,
	    par_line_item,
		oli_prod_id,
		line_item_desc,
		master_line_item_id = sales.dbo.get_master_line_item_id(oli.line_item_id, oli.par_line_item)
	from sales.dbo.ord_line_item  as oli with (nolock)
	where ord_id = @OrderId


	select 
			 lirn.ref_id
           , lirn.curr_stat
           , cast(lirn.guar_comp_dt as date) as GuarCompDate
           , lirn2.curr_stat as curr_stat2
           , psl.plate_ref_id
           , oli.line_item_id
           , oli.par_line_item
           , oli.oli_prod_id as prod_id
           , oli.line_item_desc
           , prod.obj_type
           , o.currency_id
           , lirn.NORM_STYLE_ID
           , purif_prod.prod_nm AS Purification
           , lirn.Concentration
           , sales.dbo.get_line_item_price_consolidated(oli.line_item_id, 1,1) AS Sub_Total
           , isnull(
                         case 
                               when (lirn.line_item_name IS NOT NULL) AND (lirn.line_item_name <> '') then lirn.line_item_name 
                               when pcc.prod_id is not null then ps.NAME
                               when prod.obj_type = 12 then dos.spec_name
                               else os.seq_Desc
                         end 
              , '') AS Sequence_Name
           , os.SEQ
           , sales.dbo.get_services_list(lirn.ref_id) as Services
           , sales.dbo.get_mods_list(lirn.ref_id) as Modifications
           , cast(lirn.guar_comp_dt as date) as GuarCompDate
           , isnull(os.od_obligation, 0.0) AS OD_Guarantee
           , os.ext_coeff
           , os.Length
           , prod.prod_nm AS Product
           ,     cast(
                        case 
                            when oli.par_line_item is null then 0 
                            when pcc2.CAT_ID  is not null then 1 
  							else 0 
  						end 
                  as bit) AS Is_TriFecta_Kit_Component
           ,      cast(
                         case 
                               when oli.oli_prod_id is null then 0 
                               when pc.cat_id is not null then 187
                               else 0 
                         end 
                  as integer) AS  cat_id
           from #Ord_Line_Item AS oli with (nolock) 
           inner join sales.dbo.line_item_ref_nbr AS lirn with (nolock) on lirn.line_item_id = oli.line_item_id 
           inner join sales.dbo.ord AS o with (nolock) on oli.ord_id = o.ord_id 
           inner join sales.dbo.product as prod with (nolock) on oli.oli_prod_id = prod.prod_id 
           left outer join sales.dbo.oligo_spec AS os with (nolock) on os.ref_id = lirn.ref_id 
           left outer join sales.dbo.product AS purif_prod with (nolock) on purif_prod.prod_id = os.purif_id 
           left outer join sales.dbo.plate_spec_loc AS psl with (nolock) on psl.oligo_ref_id = lirn.ref_id 
           left outer join sales.dbo.line_item_ref_nbr AS lirn2 with (nolock) on psl.plate_ref_id = lirn2.ref_id 
		   left outer join sales.dbo.prod_cat_class as pc with (nolock) on oli.oli_prod_id = pc.prod_id and pc.cat_id = 187
		   left outer join sales.dbo.prod_cat_class AS pcc with (nolock) on pcc.cat_id = 14 and oli.oli_prod_id = pcc.prod_id
		   left outer join sales.dbo.plate_spec AS ps with (nolock) on ps.ref_id = lirn.ref_id
		   left outer join sales.dbo.duplex_oligo_spec AS dos with (nolock) on lirn.ref_id = dos.ref_id
		   left outer join sales.dbo.ord_line_item AS oli2 with (nolock) on oli2.line_item_id = oli.master_line_item_id
           left outer join sales.dbo.prod_cat_class AS pcc2 with (nolock) on pcc2.prod_id  = oli2.oli_prod_id and pcc2.CAT_ID = 17
           where lirn.curr_stat <> 5 
           and ((lirn2.curr_stat <> 5) or (lirn2.curr_stat is null)) 
           and o.curr_stat <> 16 
           order by lirn.ref_id


	drop table #Ord_Line_Item


GO
GRANT EXECUTE ON  [dbo].[GetConfirmationEmailOrderedItems] TO [coliseum]
GRANT EXECUTE ON  [dbo].[GetConfirmationEmailOrderedItems] TO [IDT-CORALVILLE\IUSR_IDT]
GO
