SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create trigger DDLAudit
on database
for DDL_DATABASE_LEVEL_EVENTS

as

set arithabort on

declare @XmlEvent		xml
	, @PostTime			datetime
	, @DatabaseUser		varchar(500)
	, @HostName			varchar(500)
	, @ApplicationName	varchar(500)
	, @Event			varchar(500)
	, @Schema			varchar(500)
	, @Object			varchar(500)
	, @TSQL				varchar(max)

select @XmlEvent		= eventdata()
select @PostTime		= @XmlEvent.value('(/EVENT_INSTANCE/PostTime)[1]','varchar(50)')
select @DatabaseUser	= @XmlEvent.value('(/EVENT_INSTANCE/LoginName)[1]','varchar(500)')
select @HostName		= left(host_name(),500)
select @ApplicationName = left(app_name(),500)
select @Event			= @XmlEvent.value('(/EVENT_INSTANCE/EventType)[1]','varchar(500)')
select @Schema			= @XmlEvent.value('(/EVENT_INSTANCE/SchemaName)[1]','varchar(500)')
select @Object			= @XmlEvent.value('(/EVENT_INSTANCE/ObjectName)[1]','varchar(500)')
select @TSQL			= @XmlEvent.value('(/EVENT_INSTANCE/TSQLCommand/CommandText)[1]', 'varchar(max)')
select @TSQL			= ltrim(rtrim(@TSQL))

insert into dbo.DatabaseLog
	(PostTime
	, DatabaseUser
	, HostName
	, ApplicationName
	, [Event]
	, [Schema]
	, [Object]
	, [TSQL]
	, XmlEvent)
select @PostTime
	, @DatabaseUser
	, @HostName
	, @ApplicationName
	, @Event
	, @Schema
	, @Object
	, @TSQL
	, @XmlEvent

GO
