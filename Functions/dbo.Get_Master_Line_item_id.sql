SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
select dbo.Get_Master_Line_item_id
(45815265, 45815264)
declare @Line_item_id int, @Par_line_item int
select @Line_item_id =45815264,@Par_line_item = 45815300
select @Line_item_id = @par_Line_item, @par_Line_item = (select par_Line_item from ord_line_item where line_item_id = @par_Line_item)
select @Line_item_id, @par_Line_item

*/

CREATE function  [dbo].[Get_Master_Line_item_id]
(@Line_item_id int,@Par_line_item int)
returns int
begin
  declare @result int
  if @Par_Line_item is null
    set @result = @line_item_id
  else
  begin 
    select @Line_item_id = @par_Line_item, @par_Line_item = (select par_Line_item from ord_line_item where line_item_id = @par_Line_item)
    set @result = dbo.Get_Master_Line_item_id(@Line_item_id, @par_Line_item)
  end
  return @result
end



GO
GRANT EXECUTE ON  [dbo].[Get_Master_Line_item_id] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[Get_Master_Line_item_id] TO [IDT_Reporter]
GO
