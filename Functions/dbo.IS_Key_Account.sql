SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		MDW
-- Create date: 6/27/07
-- Description:	

-- =============================================
create FUNCTION [dbo].[IS_Key_Account]
(
	-- Add the parameters for the function here
	@org_id INT, @acc_id INT, @Enduser_ID INT

)
RETURNS Bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result int

	/* Portal Customer */
	IF EXISTS (SELECT * FROM reports.dbo.IDT_PARTY_RESPONSIBLE_DEPT iprd WHERE iprd.party_id in (@org_id, @acc_id, @Enduser_ID) )
    SET @result = 1
  ELSE 
    SET @result = 0

	-- Return the result of the function
	RETURN @Result

END
GO
GRANT EXECUTE ON  [dbo].[IS_Key_Account] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[IS_Key_Account] TO [IDT_Reporter]
GO
