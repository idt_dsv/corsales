SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*-- Author:		MDW
-- Create date: 6/26/07
-- Description:	

Example:
select *
from dbo.SecurityTerritory('jclark')

select * from salesTerritoryManagerVw
where network_login_nm = 'jclark'

*/

CREATE FUNCTION [dbo].[SecurityTerritory] 
(	
	-- Add the parameters for the function here
	@networkLoginName nvarchar(100) 
)
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
  select distinct Sales_terr_id = TerritoryID
  from [SalesTerritorySecurityVw]
  WHERE network_Login_nm LIKE @networkLoginName
  and TerritoryType = 'territory'
)

GO
GRANT SELECT ON  [dbo].[SecurityTerritory] TO [IDT_APPS]
GRANT SELECT ON  [dbo].[SecurityTerritory] TO [IDT_WebApps]
GO
