SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE function [dbo].[FORMAT_OLIGO_SEQUENCE_WITH_DELIMITER]
(@str1 varchar(8000), @delimiter char(1))
RETURNS varchar(8000)
AS 

/*
Author:MDW
Date: 3/4/05
Notes:
--It expects the Sequence in IDT format as a string.  It will return the sequence with disp_nm from the sci_mods table instead of the prod_id's. 
--  If delimiter is passed with a null it will use a '/'

--It is used in for COA's to format the sequence to be similar to the way the spec sheet shows the sequence.

  Example use:
  select dbo.FORMAT_OLIGO_SEQUENCE_WITH_DELIMITER('[1018]ACGTGC','/') 
  
  will return:
  /5Phos/CGTGC
permissions:
 grant execute on FORMAT_OLIGO_SEQUENCE_WITH_DELIMITER to idt_apps, idt_reporter, idt_lab_access

Revisions:
-------------------------------------------------------
*/

BEGIN

declare @str2 varchar(8000)
if @delimiter is null
  set @delimiter = '/'
set @str2 = ''
	IF (@str1 IS NULL ) 
	BEGIN
		--Just return NULL if input string is NULL
		set @str1 = ''
	END
	
	--Character variable declarations
	--Integer variable declarations
	DECLARE @ctrStr1 int, @lenStr1 int, @Str1IsEOF bit, @Prod_ID_STR varchar(20), @CurrChar char(1), @ModDispNm varchar(20)

	--Constant declarations
	DECLARE @LowerASCIILimit int, @UpperASCIILimit int
	
	--Variable/Constant initializations
	SET @ctrStr1 = 1
	SET @lenStr1 = LEN(@Str1)

	SET @LowerASCIILimit = 33 -- ! one above a space
	set @UpperASCIILimit = 126 -- ~

	WHILE @ctrStr1 <= @lenStr1
	BEGIN
		--This loop will take care of reccuring white spaces
		WHILE (ASCII(SUBSTRING(@Str1,@ctrStr1,1)) not between @LowerASCIILimit and @UpperASCIILimit)
			and (@ctrStr1 <= @lenStr1)
		BEGIN
			SET @ctrStr1 = @ctrStr1 + 1
		END
    set @CurrChar = SUBSTRING(@str1,@ctrstr1,1)
   
		IF @CurrChar <> '['  -- Not Start of a product_id for a mod so add it to result
		BEGIN
      set @str2 = @str2 + @CurrChar
			SET @ctrstr1 = @ctrstr1 + 1
		END
		ELSE -- Start of a product_id for a mod convert Prod_ID convert to product ID
    BEGIN
			SET @ctrstr1 = @ctrstr1 + 1
      set @CurrChar = SUBSTRING(@str1,@ctrstr1,1)
      set @Prod_ID_STR = ''
      WHILE @CurrChar <> ']'
   			and (@ctrStr1 <= @lenStr1)
      begin
        set @Prod_ID_STR = @Prod_ID_STR + @CurrChar
  			SET @ctrstr1 = @ctrstr1 + 1
        set @CurrChar = SUBSTRING(@str1,@ctrstr1,1)
      end  --End up at ']'. It will be eaten at end of loop

      set @ModDispNm = (select rtrim(ltrim(Disp_nm)) from sci_mods where prod_id = convert(int,@Prod_ID_STR))

      set @str2 = @str2 + @delimiter + @ModDispNm + @delimiter

			SET @ctrstr1 = @ctrstr1 + 1
    END

	END
--  print '@str2 = ' + @str2
return @str2


--RETURN 0

END










GO
GRANT EXECUTE ON  [dbo].[FORMAT_OLIGO_SEQUENCE_WITH_DELIMITER] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[FORMAT_OLIGO_SEQUENCE_WITH_DELIMITER] TO [IDT_WebApps]
GO
