SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/* GET_LINE_ITEM_PRICE_NATURAL_CURRENCY
Author: Bill Sorensen
Date: 11/10/2005 
Notes: Based on GET_LINE_ITEM_PRICE by MDW, but returns in natural currency.

Parameters:
  @LINE_ITEM_ID
  @USE_RETAIL_PRICE is 1 to use retail pricing, 0 to use distributor pricing.

permissions:
  grant execute on GET_LINE_ITEM_PRICE_NATURAL_CURRENCY to idt_apps

Modifications:

*/

CREATE FUNCTION [dbo].[GET_LINE_ITEM_PRICE_NATURAL_CURRENCY] (@LINE_ITEM_ID int, @USE_RETAIL_PRICE bit)
returns money
as
begin
  declare @Prod_type varchar(50), @Sub_total money, @Exchange_Rate float, @CURRENCY_ID int
  select @Prod_type = WORK_SPEC_OBJ_TYPE from dbo.line_item_ref_nbr (nolock) where line_item_id = @LINE_ITEM_ID

  select @CURRENCY_ID = CURRENCY_ID, @Exchange_Rate = o.exchange_rate from dbo.ord o (nolock)
  where o.ord_id = (
    select top 1 oli.ord_id from dbo.ord_line_item oli (nolock)
    where oli.line_item_id = @LINE_ITEM_ID
  )

  if (@prod_type like 'Toligo%') or (@prod_type like '%kit%')
  begin

    select @Sub_total = sum(oli.Quantity * dbo.CURRENCY_NATURAL_CONVERSION((CASE @USE_RETAIL_PRICE WHEN 1 THEN ISNULL(retail_price, unit_price) ELSE unit_price END), @Exchange_Rate, 0))
    from dbo.ord_line_item oli (nolock) 
    where (oli.par_line_item = @LINE_ITEM_ID
      or oli.line_item_id = @LINE_ITEM_ID)
                                
  end else
  begin
    select @Sub_total = sum(quantity * dbo.CURRENCY_NATURAL_CONVERSION((CASE @USE_RETAIL_PRICE WHEN 1 THEN ISNULL(retail_price, unit_price) ELSE unit_price END), @Exchange_Rate, 0)) 
    from dbo.ord_line_item oli (nolock) 
    where (oli.line_item_id = @LINE_ITEM_ID)

    set @Sub_total = @Sub_total + 
         (select sum(quantity * dbo.CURRENCY_NATURAL_CONVERSION((CASE @USE_RETAIL_PRICE WHEN 1 THEN ISNULL(retail_price, unit_price) ELSE unit_price END), @Exchange_Rate, 0)) 
          from dbo.ord_line_item oli (nolock) 
            left join dbo.line_item_ref_nbr lirn (nolock) on lirn.line_item_id = oli.line_item_id
          where (oli.par_line_item = @LINE_ITEM_ID)
            and lirn.ref_id is null
          )
   
  end 
 
  return isnull(@Sub_total,0.0)
    
end



GO
GRANT EXECUTE ON  [dbo].[GET_LINE_ITEM_PRICE_NATURAL_CURRENCY] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[GET_LINE_ITEM_PRICE_NATURAL_CURRENCY] TO [IDT_Reporter]
GO
