SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
Author: Eric Borman
Date:   12/18/2012
Notes:  Returns all the package ids for a given sales order number in 
	a comma delimited string

Usage:
  select * from dbo.GetPackageIdsForSalesOrderNbr (4645809)
  
Permissions
  grant execute on[dbo].[GetPackageIdsForSalesOrderNbr] to idt_apps

Revisions:
Date       Who		Notes
4/12/2013	 Wood	Correcting grant in comments above to grant access reflect corect function name.	
-----------------------------------------
*/
CREATE function [dbo].[GetPackageIdsForSalesOrderNbr](@SalesOrderNbr int)
  Returns varchar(max)
as
begin	
	declare @listStr VARCHAR(MAX)

	select @listStr = COALESCE(@listStr+', ' ,'') + cast(PackageId as varchar(10))
	from dbo.SalesOrderPackageLink sopl WITH (NOLOCK) 
	where sopl.SalesOrderNumber = @SalesOrderNbr

return @listStr

end


GO
GRANT EXECUTE ON  [dbo].[GetPackageIdsForSalesOrderNbr] TO [IDT_APPS]
GO
