SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: MDW
Date: 12/3/07
Notes: returns a comma separated list of network login names  that are assigned to a leve in the sales hierarchy 

grant execute on dbo.getSalesTerritoryManagerNetworkNames to idt_apps, idt_reporter

*/
CREATE FUNCTION [dbo].[getSalesTerritoryManagerNetworkNames]
(
  @SalesTerritoryId int
)
RETURNS NVARCHAR(4000)
AS
BEGIN 
  DECLARE @network_login_nm NVARCHAR (4000)
  SELECT @network_login_nm = ''

  SELECT @network_login_nm = 
       @network_login_nm 
    + (SELECT network_login_nm FROM reference.dbo.employee emp WHERE emp.hr_employee_id = stm.hr_employee_id)
    + ','
  FROM salesTerritoryManager stm
  WHERE SalesTerritoryId = @SalesTerritoryId

  return CASE WHEN LEN(@network_login_nm) > 0 THEN left(@network_login_nm, LEN(@network_login_nm) - 1) ELSE @network_login_nm END 
END 
GO
GRANT EXECUTE ON  [dbo].[getSalesTerritoryManagerNetworkNames] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[getSalesTerritoryManagerNetworkNames] TO [IDT_Reporter]
GO
