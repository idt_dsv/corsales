SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE function [dbo].[BreakApartStr]
	(@OriginalString varchar(max))
returns @BreakApart table (StrValue varchar(max))

as
/*------------------------------------------------------------------------------------------
Written By: Andrew Gould
Written On: 04/28/2010

Purpose:
	This function is designed to replace the current BreakApartStr so that it can handle
	text values and strings larger than 8000 characters.

Permissions:
	grant exec on dbo.BreakApartStrText to IDT_APPs

Objects:
	None

Revisions
	None

Notes:
	The name BreakApartStrText is so that both functions can exist at the same time.
	Once this new process has been tested and validated it will be renamed to BreakApartStr
	and the old function will be retired.
------------------------------------------------------------------------------------------*/
begin
	declare @TempString varchar(max)
		, @Chunk varchar(max)
		, @CommaPosition int

	-- Set default values for variables
	-- We use the @TempString variable so that the original passed data remains untouched.
	set @TempString = @OriginalString
	set @Chunk = ''
	set @CommaPosition = patindex('%,%',@TempString)

	-- If no comma is found, return the passed text.
	if @CommaPosition = 0
	begin
		if len(ltrim(@TempString)) > 0 
		begin
			insert into @BreakApart 
				(StrValue)
			values (ltrim(@TempString))
		end
	end

	-- While there are commas in the text, parse the chunks.
	while (@CommaPosition) > 0
	begin
		-- Assume this is the last chunk
		set @Chunk = @TempString

		-- Search for next comma.  Returns 0 if no comma found.
		set @CommaPosition = patindex('%,%',@TempString)

		-- If a comma is found set the new chunk (beginning of current string to the next comma).
		if @CommaPosition > 0
			set @Chunk = substring(@TempString, 1, @CommaPosition - 1)

		-- Make sure the chunk is more than just whitespace, then load it into the result set.
		if len(ltrim(@Chunk)) > 0 
		begin
			insert into @BreakApart 
				(StrValue)
			values (ltrim(@Chunk))
		end

		-- Remove the chunk from the parsing string.
		set @TempString = substring(@TempString, @CommaPosition + 1, len(@TempString))
	end

	return
end
GO
GRANT SELECT ON  [dbo].[BreakApartStr] TO [IDT_APPS]
GRANT SELECT ON  [dbo].[BreakApartStr] TO [IDT_WebApps]
GO
