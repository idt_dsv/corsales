SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
 CREATE   function [admin].[indexFields_udf]
 (@indexName sysname, @IndexedOrIncludedFields varchar(10))
 returns varchar(max)
 /*
 Author: MDW
 date: 7/6/05
 Notes: returns index fields for an index
 Works well with comma separated strings. Need to add one to get count of items in list
 
 example: 
Select [admin].[indexFields_udf] ('ncix_dam_auction_03', 'included') 
Select [admin].[indexFields_udf] ('IX_CustInfoUdf_AcctNbr', 'Indexed')
 */
 begin
   declare @str varchar(max) 
   set @str = ''
   select @str = @str + col_name(i.object_id, ic.column_id) + ','
   from sys.indexes i
     join sys.index_columns ic on i.object_id = ic.object_id and i.index_id = ic.index_id
   where i.name = @indexName
     and ic.is_included_column = case when @IndexedOrIncludedFields = 'Indexed' then 0 else 1 end
   order by ic.index_column_id, col_name(i.object_id, ic.column_id) 

   return case when len(left(@str,1)) > 0 then left(@str, len(@str) -1 ) else '' end
 end
GO
