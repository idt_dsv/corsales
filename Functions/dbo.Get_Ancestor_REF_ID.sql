SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: MDW
Date: 6/20/07
Notes:
--For Sales Data Mart 
-- Gets the top level ref_id except for:
   plates - 
   shipping - Will return null (view will exclude it)

example: 
select top 10 oli.line_item_id
, anc_ref_id = dbo.Get_Ancestor_REF_ID(oli.line_item_id) 
, lirn.ref_id, oli.par_line_item 
from ord_line_item oli
  left join line_item_ref_nbr lirn on lirn.line_item_id = oli.line_item_id
order by oli.line_item_id desc

select * from ord_line_item where line_item_id = 96631313

grant execute on Get_Ancestor_REF_ID to idt_apps, idt_reporter

*/
create FUNCTION [dbo].[Get_Ancestor_REF_ID]
(@line_item_id INT )
RETURNS INT
AS
begin
  DECLARE @id INT;
	WITH CTE (LINE_ITEM_ID, par_line_item, LVL)
	AS
	(
		--ANCHOR
		SELECT LINE_ITEM_ID, par_line_item, 0 AS LVL
		FROM DBO.ORD_LINE_ITEM OLI WITH (NOLOCK)
		WHERE OLI.LINE_ITEM_ID = @LINE_ITEM_ID

		UNION ALL

		--RECURSIVE
		SELECT P.LINE_ITEM_ID, P.par_line_item, C.LVL + 1
		FROM CTE AS C
		JOIN ORD_LINE_ITEM (NOLOCK) AS P ON C.PAR_LINE_ITEM = P.LINE_ITEM_ID
	)
  SELECT @id = MIN(ref_id)
  FROM CTE
    LEFT JOIN line_item_ref_nbr lirn WITH (NOLOCK) ON lirn.line_item_id = cte.line_item_id
  WHERE lirn.ref_id IS NOT NULL AND lirn.work_Spec_obj_Type NOT LIKE '%plate%'

  IF @id IS NULL
  BEGIN;
	  WITH CTE (LINE_ITEM_ID, par_line_item, LVL)
	  AS
	  (
		  --ANCHOR
		  SELECT LINE_ITEM_ID, par_line_item, 0 AS LVL
		  FROM DBO.ORD_LINE_ITEM OLI WITH (NOLOCK)
		  WHERE OLI.LINE_ITEM_ID = @LINE_ITEM_ID

		  UNION ALL

		  --RECURSIVE
		  SELECT P.LINE_ITEM_ID, P.par_line_item, C.LVL + 1
		  FROM CTE AS C
		  JOIN ORD_LINE_ITEM (NOLOCK) AS P ON C.PAR_LINE_ITEM = P.LINE_ITEM_ID
	  )
      SELECT @id = MIN(ref_id)
      FROM CTE
        LEFT JOIN line_item_ref_nbr lirn WITH (NOLOCK) ON lirn.line_item_id = cte.line_item_id
      WHERE lirn.work_Spec_obj_Type LIKE '%plate%'
  END 
  RETURN(@ID)
end
GO
GRANT EXECUTE ON  [dbo].[Get_Ancestor_REF_ID] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[Get_Ancestor_REF_ID] TO [IDT_Reporter]
GO
