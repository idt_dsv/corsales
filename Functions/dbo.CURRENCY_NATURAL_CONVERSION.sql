SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
Name: MDEWAARD + BViering
Date: 12/21/04
Notes:
  Converts a money value to the natural currency value
  Change to use consistent rounding rules with invoicing procedures 9/15/05 MAS

example:
select dbo.Currency_Natural_Conversion(3.12, 1.5234, 0)

permissions:
grant execute on dbo.Currency_Natural_Conversion to idt_apps, idt_webapps


Revisions
mschebel 9/20/05 rounded result to 4 digits to match Epicor

*/
create   function [dbo].[CURRENCY_NATURAL_CONVERSION]
(
  @Value money
, @Currency_Rate float
, @Home_currency bit
)
returns money
as 
begin
  declare @results money
  if (@Home_currency = 1 or round(@Currency_Rate,4) = 1.0000)
    set @results = @Value
  else
    set @results = ROUND((@Value * CONVERT(NUMERIC(10, 4), @Currency_rate)), 2)
   
  return @results

end




GO
GRANT EXECUTE ON  [dbo].[CURRENCY_NATURAL_CONVERSION] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[CURRENCY_NATURAL_CONVERSION] TO [IDT_WebApps]
GO
