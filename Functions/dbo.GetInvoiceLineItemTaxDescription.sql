SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO



CREATE FUNCTION [dbo].[GetInvoiceLineItemTaxDescription] 
(@Inv_Nbr varchar(32),
@JurisdictionHierarchy int)

RETURNS varchar(255)
AS
BEGIN


declare @JurisdictionDescription varchar(255)
	
if @JurisdictionHierarchy = 1 select @JurisdictionDescription=LTRIM(RTRIM(A_StateName)) from taxes.dbo.accum_archive aa where a_invoicenum = @Inv_Nbr else
if @JurisdictionHierarchy = 2 select @JurisdictionDescription=LTRIM(RTRIM(A_Countyname)) from taxes.dbo.accum_archive where a_invoicenum = @Inv_Nbr else
if @JurisdictionHierarchy = 3 select @JurisdictionDescription=LTRIM(RTRIM(A_Cityname)) from taxes.dbo.accum_archive where a_invoicenum = @Inv_Nbr else
if @JurisdictionHierarchy = 4 select @JurisdictionDescription=LTRIM(RTRIM(A_Transitname)) from taxes.dbo.accum_archive where a_invoicenum = @Inv_Nbr else
if @JurisdictionHierarchy = 5 select @JurisdictionDescription=LTRIM(RTRIM(A_Other1name)) from taxes.dbo.accum_archive where a_invoicenum = @Inv_Nbr else
if @JurisdictionHierarchy = 6 select @JurisdictionDescription=LTRIM(RTRIM(A_Other2name)) from taxes.dbo.accum_archive where a_invoicenum = @Inv_Nbr else
if @JurisdictionHierarchy = 7 select @JurisdictionDescription=LTRIM(RTRIM(A_Other3name)) from taxes.dbo.accum_archive where a_invoicenum = @Inv_Nbr else
if @JurisdictionHierarchy = 8 select @JurisdictionDescription=LTRIM(RTRIM(A_Other4name)) from taxes.dbo.accum_archive where a_invoicenum = @Inv_Nbr 

RETURN isnull(@JurisdictionDescription,'')

END



GO
GRANT EXECUTE ON  [dbo].[GetInvoiceLineItemTaxDescription] TO [IDT-CORALVILLE\DPT_APD]
GRANT EXECUTE ON  [dbo].[GetInvoiceLineItemTaxDescription] TO [IDT-CORALVILLE\DPT_WDV]
GRANT EXECUTE ON  [dbo].[GetInvoiceLineItemTaxDescription] TO [IDT-CORALVILLE\IUSR_IDT]
GO
