SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****************************************************************************
 GET_MODS_LIST
 AUTHOR: Natalia Laikhter
 CREATED ON: 11/06/02
 DESCRIPTION/ALGORITHM: 
 Returns List of Mods in comma separated list format
 REVISIONS:
2/23/06	JL	Modify to use a semicolon rather than a comma as delimiter
****************************************************************************/
create  FUNCTION [dbo].[GET_MODS_LIST](@REF_ID INTEGER) RETURNS VARCHAR(255) AS BEGIN

   DECLARE @MOD_LIST VARCHAR(255)
   set @MOD_LIST = ''

   DECLARE @MOD VARCHAR (100)
   SET @MOD = ''
   
   DECLARE MODS_CURSOR  CURSOR FORWARD_ONLY READ_ONLY FOR   
	SELECT distinct OLI.LINE_ITEM_DESC 'MODZ'
	FROM dbo.LINE_ITEM_REF_NBR LIRN (nolock)
	  JOIN dbo.ORD_LINE_ITEM OLI (nolock) ON  OLI.PAR_LINE_ITEM = LIRN.LINE_ITEM_ID 
	  JOIN dbo.PROD_CAT_CLASS PCC (nolock) ON PCC.PROD_ID = OLI.OLI_PROD_ID 
	   AND CAT_ID = 19
        WHERE REF_ID = @REF_ID
     
   OPEN MODS_CURSOR
-- Get first record
   FETCH NEXT FROM MODS_CURSOR
      INTO @MOD
   SET @MOD_LIST = rtrim(@mod)
   WHILE @@FETCH_STATUS = 0
    BEGIN
	FETCH NEXT FROM MODS_CURSOR
	  INTO @MOD
	-- If after last record exit loop
	if @@FETCH_STATUS <> 0
	begin
		break
	end

	SET @MOD_LIST =  RTRIM(@MOD) + '; ' + RTRIM(@MOD_LIST) 
    END
   
   CLOSE MODS_CURSOR
   DEALLOCATE MODS_CURSOR

   RETURN @MOD_LIST
END


GO
GRANT EXECUTE ON  [dbo].[GET_MODS_LIST] TO [IDT_Reporter]
GO
