SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create function [dbo].[ExternalRefIdEnabledOrgIds]()
returns table
as
return
(
	select O.PARTY_ID as ORG_ID
	from Sales.dbo.ORG as O with (nolock)
	inner join Sales.dbo.CLASS_TYPE as CT with (nolock)
		on O.CLASS_TYPE_ID = CT.CLASS_TYPE_ID
	where (O.ORG_NM like 'Integrated DNA Technologies%'
		and CT.CLASS_TYPE_ID = 3)
		or O.PARTY_ID = 4201377 
)
GO
