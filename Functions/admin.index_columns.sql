SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create function [admin].[index_columns](@objname nvarchar(776), @indname sysname)
returns nvarchar(2126)
as 
begin
declare @objid int;
declare @indid int;
declare @keys nvarchar(2126) --Length (16*max_identifierLength)+(15*2)+(16*3)
declare @i int
declare @thiskey nvarchar(131) -- 128+3

-- Check to see the the table exists and initialize @objid.
select @objid = object_id(@objname)
if @objid is NULL
begin
	return null;
end

-- Check to see the the index exists and initialize @objid.
select @indid = indexproperty(@objid, @indname, 'IndexID')
if @indid is NULL
begin
	return null;
end

select @keys = index_col(@objname, @indid, 1), @i = 2
if (indexkey_property(@objid, @indid, 1, 'isdescending') = 1)
	select @keys = @keys  + '(-)'

select @thiskey = index_col(@objname, @indid, @i)
if ((@thiskey is not null) and (indexkey_property(@objid, @indid, @i, 'isdescending') = 1))
	select @thiskey = @thiskey + '(-)'

while (@thiskey is not null )
begin
	select @keys = @keys + ', ' + @thiskey, @i = @i + 1
	select @thiskey = index_col(@objname, @indid, @i)
	if ((@thiskey is not null) and (indexkey_property(@objid, @indid, @i, 'isdescending') = 1))
		select @thiskey = @thiskey + '(-)'
end
return @keys
end

GO
