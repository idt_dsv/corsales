SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
Author: James Borland
Date: 03/11/2004
Purpose: Fetch Shipping Annotations for a given Sales Order Number

grant select on GET_SHIPPING_ANNOTATIONS_FOR_SALES_ORD_NBR to IDT_Apps

Revisions
Date                 Who     Note
------------------------------
05/23/04            JLB       Not all Annotation have an emplyee ID therefore added LEFT OUTER and Coalsce()
02/05/05            BTV      Added an additional lookup for distributor notes
06/30/05            ejb       Copnverted to a UDF
*/
CREATE   FUNCTION [dbo].[GET_SHIPPING_ANNOTATIONS_FOR_SALES_ORD_NBR](@SALES_ORD_NBR int, @CurrentDateTime datetime)
  Returns @ShippingAnnotations table(
  ANNOTATION_TYPE varchar (20),
  ANNOTATION_ID int,
  BF_BOBJECT_CID int,
  BF_BOBJECT_OID int,
  ANNOTATION varchar(7000),
  AUTHOR_ID int,
  CREATE_DTM datetime,
  MACHINE_STAMP varchar(50),
  WORKSTATION_ID int,
  CANCEL_BY_ID int,
  NOTE_CATEGORY int,
  CANCEL_DTM datetime
)
AS
BEGIN
  --Shipping Notes for Organization
  INSERT into @ShippingAnnotations
  SELECT 'ORGANIZATION', * 
  from dbo.ANNOTATION (nolock)
  where BF_BOBJECT_CID = 59--REPLACE WITH TOrganization Class ID
    and BF_BOBJECT_OID = (SELECT ORG_ID FROM dbo.ORD (nolock) WHERE SALES_ORD_NBR = @SALES_ORD_NBR)
    and NOTE_CATEGORY = 16 -- shipping note category
  
    --Shipping Notes for Account
  INSERT into @ShippingAnnotations
  SELECT 'ACCOUNT', *
  from dbo.ANNOTATION (nolock)
  where BF_BOBJECT_CID = 57--REPLACE WITH TAccount Class ID
    and BF_BOBJECT_OID = (SELECT ACC_ID FROM dbo.ORD (nolock) WHERE SALES_ORD_NBR = @SALES_ORD_NBR)
    and NOTE_CATEGORY = 16 -- shipping note category
  
    --Shipping Notes for Enduser
  INSERT into @ShippingAnnotations
  SELECT 'ENDUSER', *
  from dbo.ANNOTATION (nolock)
  where BF_BOBJECT_CID = 58--REPLACE WITH TEnduser Class ID
    and BF_BOBJECT_OID = (SELECT ENDUSER_ID FROM dbo.ORD (nolock) WHERE SALES_ORD_NBR = @SALES_ORD_NBR)
    and NOTE_CATEGORY = 16 -- shipping note category
  
    --Shipping Notes for Order
  INSERT into @ShippingAnnotations
  SELECT 'ORDER', *
  from dbo.ANNOTATION (nolock)
  where BF_BOBJECT_CID = 63--REPLACE WITH TOrder Class ID
    and BF_BOBJECT_OID = (SELECT ORD_ID FROM dbo.ORD (nolock) WHERE SALES_ORD_NBR = @SALES_ORD_NBR)
    and NOTE_CATEGORY = 16 -- shipping note category
  
    --Shipping Notes for Distributor
    --Looks up the ditributor Party_id by joining the ord.ord_id to the party_rel table
    --and then retrieving the organization shipping notes for that distributor
  INSERT into @ShippingAnnotations
  SELECT 'DISTRIBUTOR', *
  from dbo.ANNOTATION (nolock)
  where BF_BOBJECT_CID = 59--REPLACE WITH TOrder Class ID
    and BF_BOBJECT_OID = 
      (SELECT PAR_PARTY_ID FROM dbo.ORD (nolock) 
      JOIN dbo.PARTY_REL (nolock) ON ORD.ORG_ID = PARTY_REL.CHILD_PARTY_ID
      AND PARTY_REL_TYPE = 8 --Distributor relationship
      AND (FROM_DT < @CurrentDateTime AND (THRU_DT > @CurrentDateTime OR THRU_DT IS NULL))
      WHERE SALES_ORD_NBR = @SALES_ORD_NBR)
    and NOTE_CATEGORY = 16 -- shipping note category

  Return
End







GO
GRANT SELECT ON  [dbo].[GET_SHIPPING_ANNOTATIONS_FOR_SALES_ORD_NBR] TO [IDT_APPS]
GRANT SELECT ON  [dbo].[GET_SHIPPING_ANNOTATIONS_FOR_SALES_ORD_NBR] TO [IDT_WebApps]
GO
