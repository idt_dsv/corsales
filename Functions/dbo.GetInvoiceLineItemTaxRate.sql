SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO

/*
grant execute on dbo.GetReportIdsForContainerBarcodeIds to [IDT-CORALVILLE\IUSR_IDT], [IDT-CORALVILLE\DPT_APD], [IDT-CORALVILLE\DPT_WDV]
*/

CREATE FUNCTION [dbo].[GetInvoiceLineItemTaxRate]
    (
      @Inv_Nbr VARCHAR(32)
    , @JurisdictionHierarchy INT
    )
RETURNS DECIMAL(10, 4)
AS
    BEGIN

        DECLARE @taxrate DECIMAL(10, 4)
	
        IF @JurisdictionHierarchy = 1
            SELECT  @taxrate = a_starate
            FROM    taxes.dbo.accum_archive WITH ( NOLOCK )
            WHERE   a_invoicenum = @Inv_Nbr
        ELSE
            IF @JurisdictionHierarchy = 2
                SELECT  @taxrate = a_couRate
                FROM    taxes.dbo.accum_archive WITH ( NOLOCK )
                WHERE   a_invoicenum = @Inv_Nbr
            ELSE
                IF @JurisdictionHierarchy = 3
                    SELECT  @taxrate = a_citRate
                    FROM    taxes.dbo.accum_archive WITH ( NOLOCK )
                    WHERE   a_invoicenum = @Inv_Nbr
                ELSE
                    IF @JurisdictionHierarchy = 4
                        SELECT  @taxrate = a_traRate
                        FROM    taxes.dbo.accum_archive WITH ( NOLOCK )
                        WHERE   a_invoicenum = @Inv_Nbr
                    ELSE
                        IF @JurisdictionHierarchy = 5
                            SELECT  @taxrate = a_oth1Rate
                            FROM    taxes.dbo.accum_archive WITH ( NOLOCK )
                            WHERE   a_invoicenum = @Inv_Nbr
                        ELSE
                            IF @JurisdictionHierarchy = 6
                                SELECT  @taxrate = a_oth2Rate
                                FROM    taxes.dbo.accum_archive WITH ( NOLOCK )
                                WHERE   a_invoicenum = @Inv_Nbr
                            ELSE
                                IF @JurisdictionHierarchy = 7
                                    SELECT  @taxrate = a_oth3Rate
                                    FROM    taxes.dbo.accum_archive WITH ( NOLOCK )
                                    WHERE   a_invoicenum = @Inv_Nbr
                                ELSE
                                    IF @JurisdictionHierarchy = 8
                                        SELECT  @taxrate = a_oth4Rate
                                        FROM    taxes.dbo.accum_archive WITH ( NOLOCK )
                                        WHERE   a_invoicenum = @Inv_Nbr 

        RETURN ISNULL(@taxrate,0)

    END



GO
GRANT EXECUTE ON  [dbo].[GetInvoiceLineItemTaxRate] TO [IDT-CORALVILLE\DPT_APD]
GRANT EXECUTE ON  [dbo].[GetInvoiceLineItemTaxRate] TO [IDT-CORALVILLE\DPT_WDV]
GRANT EXECUTE ON  [dbo].[GetInvoiceLineItemTaxRate] TO [IDT-CORALVILLE\IUSR_IDT]
GO
