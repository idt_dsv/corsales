SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: MDW
Date: 9/6/2007
Notes:
  built for reporting and used in sales datamart

grant execute on [GetOrdPostedMfgDtm] to idt_apps,idt_reporting
*/


create FUNCTION [dbo].[GetOrdPostedMfgDtm] ( @ORD_ID INT )
RETURNS DATETIME
AS 
BEGIN

  RETURN (
           SELECT TOP 1
            ord_stat_dt
           FROM
            ord_stat_hist osh
           where
            osh.ord_id = @Ord_ID
            AND osh.ORD_STAT_TYPE_ID = 7
            AND osh.ord_stat_dt = (
                                    SELECT
                                      MAX(ord_stat_dt)
                                    from
                                      ord_Stat_hist osh2
                                    WHERE
                                      osh.ord_id = osh2.ord_id
                                      AND osh2.ORD_STAT_TYPE_ID = 7
                                  )
         )

END 
GO
GRANT EXECUTE ON  [dbo].[GetOrdPostedMfgDtm] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[GetOrdPostedMfgDtm] TO [IDT_Reporter]
GO
