SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO


/*
Author: Neil McEntaggart
Date:   03/03/2004
Notes:  Returns the queue and OID for a specific queue definition.        

Revisions:
Date       Who  Notes
-----------------------------------------
03/24/2004 RHW  Added check for Active = 1 to return only active items.
05/03/2004 NOM  Added VIEW_TYPE_ALL_NOT_COMP and added check for active items. 
09/30/2004 ejb  Changed isnull ApplicationID check from string ('') to integer (0)
                          Added VIEW_TYPE_UNCLAIMED_MYCLAIMED
08/05/05   ejb  Cloned for Nacturing queue
10/07/2005 ejb  prevent multiple instances of the same object from being claimed multiple times
03/26/2010 DAP  only return items whose active date is <= the current date.
*/
CREATE FUNCTION [dbo].[LOAD_QUEUE_NOTIFICATION]( 
  @QUEUE_KEY_TYPE int,
  @QUEUE_KEY_ID   int,
  @BF_BOBJECT_CID int,
  @VIEW           int,
  @WORKSTATION_ID int,
  @APPLICATION_ID int)
  RETURNS @Q TABLE (QUEUE_ITEM_ID  int, 
                    BF_BOBJECT_OID int)
AS
BEGIN 
  DECLARE @VIEW_TYPE_ALL          int SET @VIEW_TYPE_ALL         = 1  
  DECLARE @VIEW_TYPE_UNCLAIMED    int SET @VIEW_TYPE_UNCLAIMED   = 2
  DECLARE @VIEW_TYPE_MYCLAIMED    int SET @VIEW_TYPE_MYCLAIMED   = 3
  DECLARE @VIEW_TYPE_ALLCLAIMED   int SET @VIEW_TYPE_ALLCLAIMED  = 4
  DECLARE @VIEW_TYPE_MYCOMPLEATED int SET @VIEW_TYPE_MYCOMPLEATED= 5
  DECLARE @VIEW_TYPE_COMPLEATED   int SET @VIEW_TYPE_COMPLEATED  = 6
  DECLARE @VIEW_TYPE_ALL_NOT_COMP int SET @VIEW_TYPE_ALL_NOT_COMP= 7
  DECLARE @VIEW_TYPE_UNCLAIMED_MYCLAIMED int SET @VIEW_TYPE_UNCLAIMED_MYCLAIMED = 8
        
  DECLARE @QUEUE_KEY_TY_EVENT int SET @QUEUE_KEY_TY_EVENT = 1
  DECLARE @QUEUE_KEY_TY_GROUP int SET @QUEUE_KEY_TY_GROUP = 2
  
  

  IF @QUEUE_KEY_TYPE = @QUEUE_KEY_TY_EVENT   
     INSERT @Q
     SELECT top 10 Itm.QUEUE_ITEM_ID, Itm.BF_BOBJECT_OID 
       FROM QUEUE_ITEM_NOTIFICATION Itm
      WHERE (Itm.QUEUE_DEFINITION_ID = @QUEUE_KEY_ID 
             AND BF_BOBJECT_CID = @BF_BOBJECT_CID
			 AND Itm.ACTIVE_DTM <= GETDATE()
             AND Itm.ACTIVE = 1)
      AND( 
           (@VIEW = @VIEW_TYPE_COMPLEATED AND COMPLETED_DTM is not null)
        OR (@VIEW = @VIEW_TYPE_MYCOMPLEATED AND COMPLETED_DTM is not null AND CLAIMED_WORKSTATION_ID = @WORKSTATION_ID AND APPLICATION_ID = @APPLICATION_ID) 
        OR (@VIEW = @VIEW_TYPE_MYCLAIMED  AND CLAIMED_WORKSTATION_ID = @WORKSTATION_ID AND APPLICATION_ID = @APPLICATION_ID AND COMPLETED_DTM is null) 
        OR (@VIEW = @VIEW_TYPE_UNCLAIMED  AND IsNull(CLAIMED_WORKSTATION_ID,0) = 0 AND IsNull(APPLICATION_ID,0) = 0 AND COMPLETED_DTM is null)
        OR (@VIEW = @VIEW_TYPE_ALLCLAIMED AND IsNull(CLAIMED_WORKSTATION_ID,0) = 0 AND IsNull(APPLICATION_ID,0) = 0 AND COMPLETED_DTM is null)
        OR (@VIEW = @VIEW_TYPE_ALL_NOT_COMP AND COMPLETED_DTM is null)
        OR (@VIEW = @VIEW_TYPE_ALL)
        OR (@VIEW = @VIEW_TYPE_UNCLAIMED_MYCLAIMED AND COMPLETED_DTM is null and 
              ((IsNull(CLAIMED_WORKSTATION_ID,0) = 0 AND IsNull(APPLICATION_ID, 0) = 0) or -- Unclaimed
               (CLAIMED_WORKSTATION_ID = @WORKSTATION_ID AND APPLICATION_ID = @APPLICATION_ID))) -- MyClaimed
         ) 
       and itm.BF_BOBJECT_OID not in(
         select bf_bobject_oid 
         from QUEUE_ITEM_NOTIFICATION q
         where active = 1
         and completed_dtm is null 
         and claimed_dtm is not null 
         and q.bf_bobject_oid = Itm.BF_BOBJECT_OID
         and q.queue_item_id <> Itm.Queue_item_id
       )
   ELSE IF @QUEUE_KEY_TYPE = @QUEUE_KEY_TY_GROUP
     INSERT @Q
     SELECT top 10 Itm.QUEUE_ITEM_ID, Itm.BF_BOBJECT_OID 
       FROM QUEUE_ITEM_NOTIFICATION Itm
       JOIN ACTIVE_QUEUE_DEFINITION_GROUP_MBRSHIP_VW m ON m.QUEUE_DEFINITION_ID = Itm.QUEUE_DEFINITION_ID --AND THRU_DTM > ENTERED_DTM
      WHERE (m.QUEUE_DEFINITION_GROUP_ID = @QUEUE_KEY_ID 
             AND BF_BOBJECT_CID = @BF_BOBJECT_CID
			 AND Itm.ACTIVE_DTM <= GETDATE()
             AND Itm.ACTIVE = 1)
      AND( 
           (@VIEW = @VIEW_TYPE_COMPLEATED AND COMPLETED_DTM is not null)
        OR (@VIEW = @VIEW_TYPE_MYCOMPLEATED AND COMPLETED_DTM is not null AND CLAIMED_WORKSTATION_ID = @WORKSTATION_ID AND APPLICATION_ID = @APPLICATION_ID) 
        OR (@VIEW = @VIEW_TYPE_MYCLAIMED  AND CLAIMED_WORKSTATION_ID = @WORKSTATION_ID AND APPLICATION_ID = @APPLICATION_ID AND COMPLETED_DTM is null) 
        OR (@VIEW = @VIEW_TYPE_UNCLAIMED  AND IsNull(CLAIMED_WORKSTATION_ID,0) = 0 AND IsNull(APPLICATION_ID,0) = 0 AND COMPLETED_DTM is null)
        OR (@VIEW = @VIEW_TYPE_ALLCLAIMED AND IsNull(CLAIMED_WORKSTATION_ID,0) = 0 AND IsNull(APPLICATION_ID,0) = 0 AND COMPLETED_DTM is null)
        OR (@VIEW = @VIEW_TYPE_ALL_NOT_COMP AND COMPLETED_DTM is null)        
        OR (@VIEW = @VIEW_TYPE_UNCLAIMED_MYCLAIMED AND COMPLETED_DTM is null and 
              ((IsNull(CLAIMED_WORKSTATION_ID,0) = 0 AND IsNull(APPLICATION_ID, 0) = 0) or -- Unclaimed
               (CLAIMED_WORKSTATION_ID = @WORKSTATION_ID AND APPLICATION_ID = @APPLICATION_ID))) -- MyClaimed
        OR (@VIEW = @VIEW_TYPE_ALL)
         )
       and itm.BF_BOBJECT_OID not in(
         select bf_bobject_oid 
         from QUEUE_ITEM_NOTIFICATION q
         where active = 1
         and completed_dtm is null 
         and claimed_dtm is not null 
         and q.bf_bobject_oid = Itm.BF_BOBJECT_OID
         and q.queue_item_id <> Itm.Queue_item_id
       )
  RETURN 
END




GO
GRANT SELECT ON  [dbo].[LOAD_QUEUE_NOTIFICATION] TO [IDT_APPS]
GRANT SELECT ON  [dbo].[LOAD_QUEUE_NOTIFICATION] TO [IDT_WebApps]
GO
