SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  function [dbo].[FORMAT_OLIGO_SEQUENCE]
(@str1 varchar(8000))
RETURNS varchar(8000)
AS 

/*
Author:MDW
Date: 4/18/03
Notes:
--It expects the Sequence in IDT format as a string.  It will return the sequence with disp_nm from the sci_mods table instead of the prod_id's. Let me know if there are any problems.

--It is used in for COA's to format the sequence to be similar to the way the spec sheet shows the sequence.

  Example use:
  select dbo.Format_Oligo_sequence('[1018]ACGTGC') 
  
  will return:
  \5Phos\CGTGC


Revisions:
-------------------------------------------------------
2/22/05  MDW  Changed the delimiter from "\" to a "/" to match the lab view dispay (oligo object codon representation)
2/22/05  MDW  Changed the delimiter back to "/" from "\". Created a second function to handle different delimiters 
3/30/05  MDW  Added check for if string contains a mod. If not just return the string. For performance. on 4000 reduced time from 30 seconds to 1 second with 1% modified oligos
*/

BEGIN
if charindex('[',@str1) > 0 --If contains a mod. the mod always starts with [
begin
  declare @str2 varchar(8000)
  set @str2 = ''
  	IF (@str1 IS NULL ) 
  	BEGIN
  		--Just return NULL if input string is NULL
  		set @str1 = ''
  	END
  	
  	--Character variable declarations
  	--Integer variable declarations
  	DECLARE @ctrStr1 int, @lenStr1 int, @Str1IsEOF bit, @Prod_ID_STR varchar(20), @CurrChar char(1), @ModDispNm varchar(20)
  
  	--Constant declarations
  	DECLARE @LowerASCIILimit int, @UpperASCIILimit int
  	
  	--Variable/Constant initializations
  	SET @ctrStr1 = 1
  	SET @lenStr1 = LEN(@Str1)
  
  	SET @LowerASCIILimit = 33 -- ! one above a space
  	set @UpperASCIILimit = 126 -- ~
  
  	WHILE @ctrStr1 <= @lenStr1
  	BEGIN
  		--This loop will take care of reccuring white spaces
  		WHILE (ASCII(SUBSTRING(@Str1,@ctrStr1,1)) not between @LowerASCIILimit and @UpperASCIILimit)
  			and (@ctrStr1 <= @lenStr1)
  		BEGIN
  			SET @ctrStr1 = @ctrStr1 + 1
  		END
      set @CurrChar = SUBSTRING(@str1,@ctrstr1,1)
     
  		IF @CurrChar <> '['  -- Not Start of a product_id for a mod so add it to result
  		BEGIN
        set @str2 = @str2 + @CurrChar
  			SET @ctrstr1 = @ctrstr1 + 1
  		END
  		ELSE -- Start of a product_id for a mod convert Prod_ID convert to product ID
      BEGIN
  			SET @ctrstr1 = @ctrstr1 + 1
        set @CurrChar = SUBSTRING(@str1,@ctrstr1,1)
        set @Prod_ID_STR = ''
        WHILE @CurrChar <> ']'
     			and (@ctrStr1 <= @lenStr1)
        begin
          set @Prod_ID_STR = @Prod_ID_STR + @CurrChar
    			SET @ctrstr1 = @ctrstr1 + 1
          set @CurrChar = SUBSTRING(@str1,@ctrstr1,1)
        end  --End up at ']'. It will be eaten at end of loop
  
        set @ModDispNm = (select rtrim(ltrim(Disp_nm)) from sci_mods where prod_id = convert(int,@Prod_ID_STR))
  
        set @str2 = @str2 + '\' + @ModDispNm + '\'
  
  			SET @ctrstr1 = @ctrstr1 + 1
      END
  
  	END
END else set @str2 = @str1  -- No mod condition
--  print '@str2 = ' + @str2
return @str2


--RETURN 0

END











GO
GRANT EXECUTE ON  [dbo].[FORMAT_OLIGO_SEQUENCE] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[FORMAT_OLIGO_SEQUENCE] TO [IDT_WebApps]
GO
