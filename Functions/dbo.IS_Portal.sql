SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		MDW
-- Create date: 6/27/07
-- Description:	
-- =============================================
Create FUNCTION [dbo].[IS_Portal] 
(
	-- Add the parameters for the function here
	@org_id INT, @acc_id INT, @Enduser_ID INT

)
RETURNS Bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result int

	/* Portal Customer */
	IF EXISTS (
        SELECT *
        FROM sales.dbo.Core_Inst_Org_Map map
          JOIN sales.dbo.party party ON party.party_id = map.org_id 
        WHERE map.ORG_ID = @org_id
        )
    SET @result = 1
  ELSE 
    SET @result = 0

	-- Return the result of the function
	RETURN @Result

END
GO
GRANT EXECUTE ON  [dbo].[IS_Portal] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[IS_Portal] TO [IDT_Reporter]
GO
