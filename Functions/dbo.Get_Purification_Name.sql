SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
Author: RAF
Date: 9/29/03

Permissions:
grant execute on Get_Purification_Name to idt_apps

Notes: Given a ref_id it will return the purification_name
Examples:
 select dbo.Get_Purification_Name(8587371)
-- select * from duplex_oligo_spec
-- select * from purification_info where purification_info_id = 1354

*/
CREATE function [dbo].[Get_Purification_Name]
(@REF_ID int)

returns varchar(50)
as

begin
 return (
    select p.prod_nm -- non duplex 
      from dbo.product p (nolock)
       join dbo.oligo_spec os (nolock) on os.purif_id = p.prod_id  
    where  os.ref_id = @ref_id 
  UNION 
    select p.prod_nm --duplex 
      from dbo.duplex_oligo_spec dos(nolock)           
       join dbo.purification_info purif (nolock) on purif.purification_info_id = dos.purification_info_id
       join dbo.product p (nolock) on p.prod_id = purif.prod_id
    where dos.ref_id = @ref_id 
  )
end



GO
GRANT EXECUTE ON  [dbo].[Get_Purification_Name] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[Get_Purification_Name] TO [IDT_Reporter]
GO
