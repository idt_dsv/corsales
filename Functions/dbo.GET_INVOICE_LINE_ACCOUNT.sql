SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  function [dbo].[GET_INVOICE_LINE_ACCOUNT] 
(@lineitemtype int, @branch int, @Departid int, 
@Exempt bit,@cntry_id varchar(32), @bill_acct_id varchar(8))
/*
Author: BSchafbuch
Date: 6/8/08
Notes:
  Returns GL Accounts 

Modifications
06/09/08			JLB	Modified to return the rest of the world code when no custom boundary found
01/03/11	Alan C.	BL-6236 Added bill_acct_id parameter. Used to get intercompany accounts.
*/
returns varchar(32)

as
begin

declare @account varchar(32)

SELECT @account = acct_id
FROM sales.dbo.INVOICE_LINE_ITEM_GL_ACCOUNT
WHERE GEOGRAPHIC_BOUNDARY_CODE IN (
				SELECT TOP 1 GB1.CODE
				FROM [GEOGRAPHIC_BOUNDARY] GB1 WITH (NOLOCK)
				JOIN GEOGRAPHIC_BOUNDARY_ASSOCIATION GBA ON GBA.PARENT_GEOGRAPHIC_BOUNDARY_ID = GB1.GEOGRAPHIC_BOUNDARY_ID
				JOIN GEOGRAPHIC_BOUNDARY GB2 ON GB2.GEOGRAPHIC_BOUNDARY_ID = GBA.CHILD_GEOGRAPHIC_BOUNDARY_ID
				WHERE GB1.GEOGRAPHIC_BOUNDARY_TYPE_ID = 17
				AND GB2.CODE IN (@CNTRY_ID, 'Everywhere')
				ORDER BY GB2.GEOGRAPHIC_BOUNDARY_TYPE_ID DESC
	       )
        
  AND DEPART_ID = @Departid
  AND INVOICE_TAX_EXEMPT_FLAG = @Exempt
  AND BRANCH_LOCATION_ID = @BRANCH
  and line_item_type = @lineitemtype
  and isnull(bill_acct_id,' ') = @bill_acct_id
	
return @Account

end
GO
