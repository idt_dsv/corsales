SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GET_INVOICE_TOTAL_NATURAL_CURRENCY](@INV_NBR INT)
/***********************
Author: Bob Schafbuch
Date: 7/9/04
Description:

Permissions:
Grant Execute On DBO.GET_INVOICE_TOTAL_NATURAL_CURRENCY to IDT_WebApps
Grant Execute On DBO.GET_INVOICE_TOTAL_NATURAL_CURRENCY to IDT_Apps
Grant Execute On DBO.GET_INVOICE_TOTAL_NATURAL_CURRENCY to idt_reporter

Revisions:
  Properly calculates the Invoice Total for invoices billed in foreign currencies
  Moved from Manufacturing to Sales database.  If you change the function, remember to check the stub in Manufacturing

MDW + BAS 7/14/04 Updated to split out Foreign Currency, because by rule us currency can be priced at 3 digits to right of decimal
BAS	  4/4/05  Extend Exchange Rate to 4 decimal places, instead of 2 (BL# 2860)
CAS       2/21/07 Moved to the Sales database
***********************/
RETURNS money
AS
BEGIN

  declare @Exchange_Rate numeric(10,4), @Batch_ORD_ID int, @CURRENCY_ID int, @RESULT money
  
  select @CURRENCY_ID = CURRENCY_ID, @Exchange_Rate = Exchange_Rate, @Batch_ORD_ID = Batch_ORD_ID from inv_hdr where INV_NBR = @INV_NBR
  
  if @CURRENCY_ID = 1 -- USA
    select @RESULT =  sum(UNIT_PRICE * @Exchange_Rate *QTY)
          FROM INV_LINE_ITEMS ILI 
          WHERE ILI.BATCH_ORD_ID = @Batch_ORD_ID
  else
    select @RESULT =  sum(round(UNIT_PRICE * @Exchange_Rate,2) *QTY)
          FROM INV_LINE_ITEMS ILI 
          WHERE ILI.BATCH_ORD_ID = @Batch_ORD_ID
  
  return @Result
END


GO
GRANT EXECUTE ON  [dbo].[GET_INVOICE_TOTAL_NATURAL_CURRENCY] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[GET_INVOICE_TOTAL_NATURAL_CURRENCY] TO [IDT_Reporter]
GRANT EXECUTE ON  [dbo].[GET_INVOICE_TOTAL_NATURAL_CURRENCY] TO [IDT_WebApps]
GO
