SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GET_INVOICE_TOTAL_CUSTOMER_CURRENCY](@INV_NBR INT)
/***********************
Author: Mark Schebel
Date: 12/17/08
Description: Uses natural pricing fields on invoice_line_item

Permissions:
Grant Execute On DBO.GET_INVOICE_TOTAL_CUSTOMER_CURRENCY to IDT_WebApps, IDT_Apps,idt_reporter

Revisions:

***********************/
RETURNS money
AS
BEGIN

  declare  @Batch_ORD_ID int, @RESULT money
  
  select @Batch_ORD_ID = Batch_ORD_ID from inv_hdr where INV_NBR = @INV_NBR
  
  select @RESULT =  sum(AMOUNT_NATURAL)
          FROM INV_LINE_ITEMS ILI 
          WHERE ILI.BATCH_ORD_ID = @Batch_ORD_ID
 
  return @Result
END
GO
GRANT EXECUTE ON  [dbo].[GET_INVOICE_TOTAL_CUSTOMER_CURRENCY] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[GET_INVOICE_TOTAL_CUSTOMER_CURRENCY] TO [IDT_Reporter]
GRANT EXECUTE ON  [dbo].[GET_INVOICE_TOTAL_CUSTOMER_CURRENCY] TO [IDT_WebApps]
GO
