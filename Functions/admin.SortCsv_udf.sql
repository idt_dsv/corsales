SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE function [admin].[SortCsv_udf]
(
    @csv varchar(8000)
  , @delimiter char(1)
--  , SortDirection char(4)
)
returns varchar(8000)
as
begin
  declare @str varchar(8000)
  declare @tbl table(element varchar(8000))
  
  set @str = ''
  --Had to write data to table variable or the var wouldn't accumulate on the udf table
  insert @tbl
  select tfl.[element] 
  from [admin].[tableFromListGet_udf](@csv, @delimiter) as tfl
  order by tfl.element
  
  select @str = element + @delimiter + @str 
  from @tbl
  order by element desc
    
  return case when len(@str) > 0 then left(@str, len(@str)-1) else '' end
end
GO
