SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/* Author:		MDW
 Create date: 6/26/07
 Description:	

Example:
select *
from dbo.SecurityRegion('tmartin')

select *
from dbo.SecurityRegion('mberrios') st
join sales..party on Party.sales_terr_id = st.sales_terr_id

*/

CREATE FUNCTION [dbo].[SecurityRegion] 
(	
	-- Add the parameters for the function here
	@networkLoginName nvarchar(100) 
)
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
  select distinct Sales_Region_id = TerritoryID
  from [SalesTerritorySecurityVw]
  WHERE network_Login_nm LIKE @networkLoginName
  and TerritoryType = 'region'
)

GO
GRANT SELECT ON  [dbo].[SecurityRegion] TO [IDT_APPS]
GRANT SELECT ON  [dbo].[SecurityRegion] TO [IDT_WebApps]
GO
