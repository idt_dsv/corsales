SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE function [admin].[tableFromListGet_udf]
(
    @list nvarchar(max), 
    @delimiter nchar(1)
)
/*******************************************************************************
    Purpose: 
        Output @list elements as a table;
        Parse @list with @delimiter;

    Usage: 
        select * from [admin].tableFromListGet_udf('a,b,c', ',');

    Notes: 
        Derived from http://www.sqlservercentral.com/articles/XML/63633/

    When        Who What
    2008-09-09  BLD Created;
*******************************************************************************/
returns @element table(element nvarchar(max))
as
begin
   declare @xml xml;
   select @xml = '<x><y><z>' + replace(replace(replace(@list,'&', '&amp;'),'<', '&lt;'), @delimiter, '</z></y><y><z>') + '</z></y></x>';
   insert into @element select [x].element.value('z[1]','nvarchar(max)') from @xml.nodes('/x/y') as x(element);
   return;
end;
GO
