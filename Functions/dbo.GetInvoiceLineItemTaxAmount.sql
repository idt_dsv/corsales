SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO

/*
grant execute on dbo.GetReportIdsForContainerBarcodeIds to [IDT-CORALVILLE\IUSR_IDT], [IDT-CORALVILLE\DPT_APD], [IDT-CORALVILLE\DPT_WDV]
*/
					  
CREATE FUNCTION [dbo].[GetInvoiceLineItemTaxAmount]
    (
      @Inv_Nbr VARCHAR(32)
    , @inv_line_item_id INT
    , @BaseNatural MONEY
    , @JurisdictionHierarchy INT
    )
RETURNS VARCHAR(255)
AS
    BEGIN

        SELECT  @Inv_Nbr = 0
        FROM    INV_LINE_ITEMS ili
        WHERE   ili.INV_LINE_ITEM_ID = @inv_line_item_id
                AND LINE_ITEM_TYPE = 3

        DECLARE @RECORDID INT

        SELECT  @RECORDID = MIN(RECORDID)
        FROM    TAXES.DBO.ACCUM_ARCHIVE
        WHERE   A_InvoiceNum = @Inv_Nbr

        DECLARE @tax DECIMAL(10, 4)
	
        IF @JurisdictionHierarchy = 1
            SELECT  @tax = a_starate * @BaseNatural
            FROM    taxes.dbo.accum_archive WITH ( NOLOCK )
            WHERE   RecordID = @RECORDID
        ELSE
            IF @JurisdictionHierarchy = 2
                SELECT  @tax = a_couRate * @BaseNatural
                FROM    taxes.dbo.accum_archive WITH ( NOLOCK )
                WHERE   RecordID = @RECORDID
            ELSE
                IF @JurisdictionHierarchy = 3
                    SELECT  @tax = a_citRate * @BaseNatural
                    FROM    taxes.dbo.accum_archive WITH ( NOLOCK )
                    WHERE   RecordID = @RECORDID
                ELSE
                    IF @JurisdictionHierarchy = 4
                        SELECT  @tax = a_traRate * @BaseNatural
                        FROM    taxes.dbo.accum_archive WITH ( NOLOCK )
                        WHERE   RecordID = @RECORDID
                    ELSE
                        IF @JurisdictionHierarchy = 5
                            SELECT  @tax = a_oth1Rate * @BaseNatural
                            FROM    taxes.dbo.accum_archive WITH ( NOLOCK )
                            WHERE   RecordID = @RECORDID
                        ELSE
                            IF @JurisdictionHierarchy = 6
                                SELECT  @tax = a_oth2Rate * @BaseNatural
                                FROM    taxes.dbo.accum_archive WITH ( NOLOCK )
                                WHERE   RecordID = @RECORDID
                            ELSE
                                IF @JurisdictionHierarchy = 7
                                    SELECT  @tax = a_oth3Rate * @BaseNatural
                                    FROM    taxes.dbo.accum_archive WITH ( NOLOCK )
                                    WHERE   RecordID = @RECORDID
                                ELSE
                                    IF @JurisdictionHierarchy = 8
                                        SELECT  @tax = a_oth4Rate
                                                * @BaseNatural
                                        FROM    taxes.dbo.accum_archive WITH ( NOLOCK )
                                        WHERE   RecordID = @RECORDID

        RETURN ISNULL(@tax,0)

    END



GO
GRANT EXECUTE ON  [dbo].[GetInvoiceLineItemTaxAmount] TO [IDT-CORALVILLE\DPT_APD]
GRANT EXECUTE ON  [dbo].[GetInvoiceLineItemTaxAmount] TO [IDT-CORALVILLE\DPT_WDV]
GRANT EXECUTE ON  [dbo].[GetInvoiceLineItemTaxAmount] TO [IDT-CORALVILLE\IUSR_IDT]
GO
