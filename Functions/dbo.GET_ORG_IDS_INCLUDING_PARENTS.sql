SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*************************************************************
Function: GET_ORG_IDS_INCLUDING_PARENTS
Author: Bill Sorensen
Date: 7/10/06
Notes: Finds org IDs at or above the starting ID in the hierarchy.
Revisions:
***************************************************************/

CREATE FUNCTION [dbo].[GET_ORG_IDS_INCLUDING_PARENTS](@starting_org_id int)
RETURNS @org_id_list TABLE
(
    org_id int NOT NULL
)
AS
BEGIN
  declare @child_org_id int,
    @parent_org_id int,
    @levels int,
    @max_levels int

  set @max_levels = 5

  if exists (select * from org with (nolock) where party_id = @starting_org_id)
  begin
    insert @org_id_list values (@starting_org_id)

    set @child_org_id = @starting_org_id
    set @levels = 1

    while @levels < @max_levels
    begin
      set @parent_org_id = null

      select
        @parent_org_id = org_parent_id
      from org with (nolock)
      where party_id = @child_org_id

      if (@parent_org_id is null) or (@parent_org_id = @child_org_id)
        break

      insert @org_id_list values (@parent_org_id)

      set @child_org_id = @parent_org_id
      set @levels = @levels + 1
    end
  end

  RETURN
END

GO
GRANT SELECT ON  [dbo].[GET_ORG_IDS_INCLUDING_PARENTS] TO [IDT_APPS]
GRANT SELECT ON  [dbo].[GET_ORG_IDS_INCLUDING_PARENTS] TO [IDT_WebApps]
GO
