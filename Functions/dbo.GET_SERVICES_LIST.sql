SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/****************************************************************************
 GET_SERVICES_LIST
 AUTHOR: Natalia Laikhter
 CREATED ON: 8/26/05
 DESCRIPTION/ALGORITHM: 
 Returns List of Services in comma separated list format
 
 example:
 
   SELECT top 100 ref_id, OLI.LINE_ITEM_DESC 
  FROM
    dbo.LINE_ITEM_REF_NBR LIRN ( nolock )
    JOIN dbo.ORD_LINE_ITEM OLI ( nolock ) ON OLI.PAR_LINE_ITEM = LIRN.LINE_ITEM_ID
    JOIN dbo.PROD_CAT_CLASS PCC ( nolock ) ON PCC.PROD_ID = OLI.OLI_PROD_ID
                                              AND CAT_ID = 22
  order by ref_id desc

 declare @ref_id int
 set @ref_id = 79709767
 select rpt.GET_SERVICES_LIST(@ref_id)
 
 REVISIONS:
 11/20/07 MDW removed cursor logic, changed to csv list style
 06/19/13 CRS Updating w/additional cat_id's
****************************************************************************/
CREATE FUNCTION [dbo].[GET_SERVICES_LIST] (@REF_ID INTEGER)
RETURNS VARCHAR(255)
AS 
BEGIN
  DECLARE
    @SERV_LIST VARCHAR(255)
  , @SERVICES VARCHAR(100)
  SELECT
    @SERV_LIST = ''
  , @SERVICES = ''

  SELECT
    @SERV_LIST = RTRIM(SERVICES) + ', ' + @SERV_LIST
  FROM 
    (SELECT distinct
      OLI.LINE_ITEM_DESC 'SERVICES'
    FROM
      sales.dbo.LINE_ITEM_REF_NBR LIRN ( nolock )
      JOIN sales.dbo.ORD_LINE_ITEM OLI ( nolock ) ON OLI.PAR_LINE_ITEM = LIRN.LINE_ITEM_ID
      JOIN reference.dbo.PROD_CAT_CLASS PCC ( nolock ) ON PCC.PROD_ID = OLI.OLI_PROD_ID
                                                AND CAT_ID in (90,210,22)
    WHERE
      REF_ID = @REF_ID
    ) A   

  RETURN CASE WHEN LEN(@SERV_LIST) > 0 THEN LEFT(@SERV_LIST, LEN(@SERV_LIST)-1) ELSE  '' END 
END

GO
GRANT EXECUTE ON  [dbo].[GET_SERVICES_LIST] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[GET_SERVICES_LIST] TO [IDT_Reporter]
GO
