SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		MDW
-- Create date: 6/27/07
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[IS_CoreLab] 
(
	-- Add the parameters for the function here
	@org_id INT, @acc_id INT, @Enduser_ID INT

)
RETURNS Bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result int

	/* Portal Customer */
	IF EXISTS (
        SELECT *
        FROM sales.dbo.Core_Inst_Org_Map map
          JOIN sales.dbo.party party ON party.party_id = map.org_id 
        WHERE map.ORG_ID = @org_id
        )
	/* Core Price Group and Acedemic or Corporate */
     OR EXISTS (
        SELECT *
        FROM sales.dbo.Party 
          JOIN sales.dbo.org org ON org.party_id = @org_id AND CLASS_TYPE_ID <= 2
        WHERE price_grp_id IN (SELECT Price_grp_id FROM reference.dbo.price_grp WHERE Price_grp_cd LIKE 'COR%')
          AND party.party_id IN (@org_id, @acc_id, @Enduser_ID)
        )
    SET @result = 1
  ELSE 
    SET @result = 0

	-- Return the result of the function
	RETURN @Result

END
GO
GRANT EXECUTE ON  [dbo].[IS_CoreLab] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[IS_CoreLab] TO [IDT_Reporter]
GO
