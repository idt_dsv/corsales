SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/**********************************************************************
GetInvReportSettingId

Date: 12/11/03
Author:  Michael Beekman

Description:  Returns INV_REPORT_SETTING_ID (from Reference.dbo.INV_REPORT_SETTING).
Created to keep INV_IMPORT stored procedure and manual invoices in sync

Input:  @BranchLocation,		-- The location where the function is being run
		@ShipCountry,			-- The shipping destination country
		@ShipState,				-- The shipping destination state
		@ShippedWorldEase,		-- 'Yes' if the order shipped world-ease
		@OrgNbr,				-- The Org Number (0 for no org)
		@ExemptionTypeId		-- 0 = None, Otherwise see Taxes.dbo.ExemptionHandleType 
		 
return: @REPORT_SETTING_ID


grant execute on GetInvReportSettingId to idt_apps

select dbo.GetInvReportSettingId(1,'CANADA','QC','no',3120,2)	-- Returns 46
select dbo.GetInvReportSettingId(1,'Canada','QC','no',13364,0) -- Returns 29

Modifications:
Who:	When:		Description
------------------------------------------------------------------------
DAP		12/12/2013	Moved logic to determine DE World Ease Order and UK World Ease Order from Inv_Import into this function.
MDB		12/13/2013	Added logic for Canadian partial exemptions
MDB     12/16/2013  Removed BranchLocation = 1, ShipCountry = Australia logic (should default to return 1 (the else clause) instead)
DAP		12/17/2013	Updated parameters to handle templates for both federal only taxation and local only taxation.  Also updated
					code to support templates for local only taxation in Canada.
DAP		12/17/2013	replaced bit parameters with exemption type id parameter.  Updated case to handle partial exemptions based on type.

***********************************************************************/
CREATE FUNCTION [dbo].[GetInvReportSettingId]
  (
    @BranchLocation INT
  , @ShipCountry VARCHAR(50)
  , @ShipState VARCHAR(20)
  , @ShippedWorldEase varchar(3)
  , @OrgNbr int
  , @ExemptionTypeId INT
  )
  RETURNS INT
  AS
  BEGIN
  	DECLARE @REPORT_SETTING_ID INT
  	DECLARE @DEWorldEaseOrder INT
  	DECLARE @UKWorldEaseOrder INT
	
	--US/Inc
	IF @BranchLocation = 1 
	BEGIN
		
		SELECT @REPORT_SETTING_ID = CASE
								WHEN @ShipCountry = 'CANADA' AND @ExemptionTypeId = 2 THEN 46
								WHEN @ShipCountry = 'CANADA' AND @ExemptionTypeId = 3 AND @ShipState = 'MB' THEN 47 
								WHEN @ShipCountry = 'CANADA' AND @ExemptionTypeId = 3 AND @ShipState = 'BC' THEN 48 
								WHEN @ShipCountry = 'CANADA' AND @ExemptionTypeId = 3 AND @ShipState = 'SK' THEN 49 
								WHEN @ShipCountry = 'CANADA' AND @ExemptionTypeId = 3 AND @ShipState = 'QC' THEN 50 
								WHEN @ShipCountry = 'CANADA' AND @ShipState = 'NT' THEN 24
								WHEN @ShipCountry = 'CANADA' AND @ShipState = 'NU' THEN 24
								WHEN @ShipCountry = 'CANADA' AND @ShipState = 'YT' THEN 24
								WHEN @ShipCountry = 'CANADA' AND @ShipState = 'AB' THEN 24
								WHEN @ShipCountry = 'CANADA' AND @ShipState = 'BC' THEN 25
								WHEN @ShipCountry = 'CANADA' AND @ShipState = 'SK' THEN 26
								WHEN @ShipCountry = 'CANADA' AND @ShipState = 'MB' THEN 27
								WHEN @ShipCountry = 'CANADA' AND @ShipState = 'ON' THEN 28
								WHEN @ShipCountry = 'CANADA' AND @ShipState = 'QC' THEN 29
								WHEN @ShipCountry = 'CANADA' AND @ShipState = 'NL' THEN 30
								WHEN @ShipCountry = 'CANADA' AND @ShipState = 'NF' THEN 30
								WHEN @ShipCountry = 'CANADA' AND @ShipState = 'NS' THEN 31
								WHEN @ShipCountry = 'CANADA' AND @ShipState = 'NB' THEN 32
								WHEN @ShipCountry = 'CANADA' AND @ShipState = 'PE' THEN 33
								WHEN @ShipCountry = 'USA' AND (@OrgNbr = 6890 OR @OrgNbr = 6824) THEN 7
								WHEN @ShipCountry = 'USA' AND @ShipState = 'CA' THEN 6
								WHEN @ShipCountry = 'USA' AND @ShipState = 'NV' THEN 13
								ELSE 1
							END
	END

	--EU/BVBA
	IF @BranchLocation = 3 
	BEGIN
		IF @ShipCountry IN ('AUSTRIA','BULGARIA', 'CYPRUS', 'CZECH REPUBLIC' ,'DENMARK' ,'ESTONIA' ,
							'SPAIN', 'FINLAND' ,'FRANCE', 'GREECE','HUNGARY', 'IRELAND', 'ITALY' ,'LITHUANIA', 
							'LUXEMBOURG', 'LATVIA','MALTA' ,'NETHERLANDS', 'POLAND' ,'PORTUGAL', 'ROMANIA', 
							'SWEDEN', 'SLOVENIA', 'SLOVAKIA') 
		BEGIN
			SET @DEWorldEaseOrder = 1
			SET @UKWorldEaseOrder = 0 
		END
		ELSE IF @ShipCountry in ('IRELAND') 
		BEGIN
			SET @DEWorldEaseOrder = 0
			SET @UKWorldEaseOrder = 1 
		END
		ELSE
		BEGIN 
			SET @DEWorldEaseOrder = 0
			SET @UKWorldEaseOrder = 0 
		END
							
		
		SELECT @REPORT_SETTING_ID = CASE 
								WHEN ((@ShipCountry = 'UNITED KINGDOM') OR 
										(@UKWorldEaseOrder = 1 AND @ShippedWorldEase = 'Yes')) THEN 8
								WHEN ((@ShipCountry = 'GERMANY') OR 
										(@DEWorldEaseOrder = 1 AND @ShippedWorldEase = 'Yes')) THEN 10
								ELSE 2
							END
	END

	--AP/Singapore
	IF @BranchLocation = 4 
	BEGIN
		SELECT @REPORT_SETTING_ID = CASE
								WHEN @ShipCountry = 'AUSTRALIA' THEN 45
								ELSE 3	
							END
	END
  
  
  
	RETURN @REPORT_SETTING_ID  
  END
GO
GRANT EXECUTE ON  [dbo].[GetInvReportSettingId] TO [IDT_APPS]
GO
