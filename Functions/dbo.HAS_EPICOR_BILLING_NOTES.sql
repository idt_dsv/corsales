SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO



/* HAS_EPICOR_BILLING_NOTES - Has Epicor notes 
Author: Mark Schebel
Date: 3/2/2005 
Notes: Returns whether or not given epicor customer code has any invoicing notes associated with it.

Parameters:
@CUSTOMER_CODE is the epicor customer number 

permissions:
grant execute on dbo.HAS_EPICOR_BILLING_NOTES to idt_apps, idt_reporter

Modifications:

*/
CREATE FUNCTION [dbo].[HAS_EPICOR_BILLING_NOTES] 
(@CUSTOMER_CODE VARCHAR(20))
RETURNS bit AS  
BEGIN 
declare 
  @result bit


	if exists (
  		Select * 
  		  from dbo.EPICOR_BILLING_ACCT_NOTES_VW
  		 where CUSTOMER_CODE = @CUSTOMER_CODE
  		)
	set @result = 1
	else set @result = 0


return @result

END








GO
GRANT EXECUTE ON  [dbo].[HAS_EPICOR_BILLING_NOTES] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[HAS_EPICOR_BILLING_NOTES] TO [IDT_Reporter]
GO
