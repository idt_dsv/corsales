SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE function [dbo].[FORMAT_OLIGO_SEQUENCE_NO_MODS]
(@str1 varchar(8000))
RETURNS varchar(8000)
AS

/*
Author:MDW
Date: 7/30/03
Notes:
--It expects the Sequence in IDT format as a string.  It will return the sequence with no mods and substitute 'x' for mixed base sites

--It is used in for Royalty report

  Example use:
  select dbo.FORMAT_OLIGO_SEQUENCE_NO_MODS('[1018]ACGTGC(N:25252525)') 
  
  will return:
  CGTGCx


Revisions:
-------------------------------------------------------

*/

BEGIN
-- declare @str1 varchar(8000)
-- set @str1 = '[1018]ACGTGC(N:25252525)'
  declare @str2 varchar(8000)
  set @str2 = ''

	IF (@str1 IS NULL )	--Just return NULL if input string is NULL
		set @str1 = ''
	
	--Character variable declarations
	--Integer variable declarations
	DECLARE @ctrStr1 int, @lenStr1 int, @Str1IsEOF bit, @CurrChar char(1)

	--Constant declarations
	DECLARE @LowerASCIILimit int, @UpperASCIILimit int
	
	--Variable/Constant initializations
	SET @ctrStr1 = 1
	SET @lenStr1 = LEN(@Str1)

	SET @LowerASCIILimit = 33 -- ! one above a space
	set @UpperASCIILimit = 126 -- ~

	WHILE @ctrStr1 <= @lenStr1
	BEGIN
		--This loop will take care of reccuring white spaces and prod_id
		WHILE ((isnumeric(SUBSTRING(@Str1,@ctrStr1,1)) = 1)
      or (SUBSTRING(@Str1,@ctrStr1,1) in ('[',']'))
      or (ASCII(SUBSTRING(@Str1,@ctrStr1,1)) not between @LowerASCIILimit and @UpperASCIILimit))
			and (@ctrStr1 <= @lenStr1)
		BEGIN
			SET @ctrStr1 = @ctrStr1 + 1
		END

    set @CurrChar = SUBSTRING(@str1,@ctrstr1,1)
		IF @CurrChar not in  ('(')  -- Not Start of a product_id for a mod so add it to result
		BEGIN
      set @str2 = @str2 + @CurrChar
		END ELSE -- Start of a product_id for a mod convert Prod_ID convert to product ID
    BEGIN
      set @CurrChar = SUBSTRING(@str1,@ctrstr1,1)
      WHILE @CurrChar <> ')'
   			and (@ctrStr1 <= @lenStr1)
      BEGIN
        set @CurrChar = SUBSTRING(@str1,@ctrstr1,1)
  			SET @ctrstr1 = @ctrstr1 + 1
      END  --End up at ')'. It will be eaten at end of loop
      set @CurrChar = 'x'
      --- set counter on ctrl str back b/c too far
      SET @ctrstr1 = @ctrstr1 - 1

      set @str2 = @str2 + @CurrChar
    END -- IF Else
		SET @ctrstr1 = @ctrstr1 + 1
  END -- WHILE @ctrStr1 <= @lenStr1

  return @str2

END --end Function







GO
GRANT EXECUTE ON  [dbo].[FORMAT_OLIGO_SEQUENCE_NO_MODS] TO [IDT_APPS]
GRANT EXECUTE ON  [dbo].[FORMAT_OLIGO_SEQUENCE_NO_MODS] TO [IDT_WebApps]
GO
