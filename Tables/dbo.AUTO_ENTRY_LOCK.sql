CREATE TABLE [dbo].[AUTO_ENTRY_LOCK]
(
[AUTO_ENTRY_LOCK_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[PROCESS_NAME] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LOCK_STATUS] [int] NOT NULL CONSTRAINT [DF__AUTO_ENTR__LOCK___6FE99F9F] DEFAULT ((0)),
[USER_NAME] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LOCK_DTM] [datetime] NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[AUTO_ENTRY_LOCK] ADD CONSTRAINT [PK_AUTO_ENTRY_LOCK] PRIMARY KEY CLUSTERED  ([AUTO_ENTRY_LOCK_ID]) ON [SALES_DATA]
GO
EXEC sp_addextendedproperty N'MS_Description', N'The Automatic Entry procedure requires numerous locks for different processes. Also, these locks can have numerous states.  This table exists to store the locks and their states w/ an associated user and date.', 'SCHEMA', N'dbo', 'TABLE', N'AUTO_ENTRY_LOCK', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary Key', 'SCHEMA', N'dbo', 'TABLE', N'AUTO_ENTRY_LOCK', 'COLUMN', N'AUTO_ENTRY_LOCK_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Time of lock creation or update', 'SCHEMA', N'dbo', 'TABLE', N'AUTO_ENTRY_LOCK', 'COLUMN', N'LOCK_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Denotes which type of lock the process is in.', 'SCHEMA', N'dbo', 'TABLE', N'AUTO_ENTRY_LOCK', 'COLUMN', N'LOCK_STATUS'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Name of locked auto entry process', 'SCHEMA', N'dbo', 'TABLE', N'AUTO_ENTRY_LOCK', 'COLUMN', N'PROCESS_NAME'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Name of logged in person when lock is created/updated.', 'SCHEMA', N'dbo', 'TABLE', N'AUTO_ENTRY_LOCK', 'COLUMN', N'USER_NAME'
GO
