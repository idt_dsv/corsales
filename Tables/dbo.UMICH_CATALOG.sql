CREATE TABLE [dbo].[UMICH_CATALOG]
(
[PROD_COMP_ID] [int] NOT NULL,
[PRODUCT_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMPONENT_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROD_21_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UMICH_PRICE] [money] NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[UMICH_CATALOG] ADD CONSTRAINT [PK_UMICH_CATALOG] PRIMARY KEY CLUSTERED  ([PROD_COMP_ID]) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_UMICH_CATALOG_1] ON [dbo].[UMICH_CATALOG] ([COMPONENT_NAME]) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_UMICH_CATALOG] ON [dbo].[UMICH_CATALOG] ([PRODUCT_NAME]) ON [SALES_DATA]
GO
EXEC sp_addextendedproperty N'MS_Description', N'University of Michigan Catalog. It is a mapping table that maps the UMich catalog from the prod_desc in the inv_line_item_detail table to the specific prod_comp_id from prod_comp_det table. This table is used in invoicing the U of Michigan account', 'SCHEMA', N'dbo', 'TABLE', N'UMICH_CATALOG', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'IDT Component Name, Associated with a product', 'SCHEMA', N'dbo', 'TABLE', N'UMICH_CATALOG', 'COLUMN', N'COMPONENT_NAME'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Unique Text string combining the Product and Component Descriptions', 'SCHEMA', N'dbo', 'TABLE', N'UMICH_CATALOG', 'COLUMN', N'PROD_21_CODE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary Key of Product and Component combination, same as Catalog', 'SCHEMA', N'dbo', 'TABLE', N'UMICH_CATALOG', 'COLUMN', N'PROD_COMP_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'IDT Product Name', 'SCHEMA', N'dbo', 'TABLE', N'UMICH_CATALOG', 'COLUMN', N'PRODUCT_NAME'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Price for the Price / Component Combination', 'SCHEMA', N'dbo', 'TABLE', N'UMICH_CATALOG', 'COLUMN', N'UMICH_PRICE'
GO
