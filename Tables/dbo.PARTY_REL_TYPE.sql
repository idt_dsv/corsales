CREATE TABLE [dbo].[PARTY_REL_TYPE]
(
[PARTY_REL_TYPE_ID] [int] NOT NULL,
[PARTY_REL_NM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PARTY_REL_DESC] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [SALES_DATA] TEXTIMAGE_ON [SALES_DATA]
GO
ALTER TABLE [dbo].[PARTY_REL_TYPE] ADD CONSTRAINT [PK_PARTY_REL_TYPE] PRIMARY KEY CLUSTERED  ([PARTY_REL_TYPE_ID]) ON [SALES_DATA]
GO
CREATE UNIQUE NONCLUSTERED INDEX [XPKParty_Relationship_Type] ON [dbo].[PARTY_REL_TYPE] ([PARTY_REL_TYPE_ID]) ON [SALES_DATA]
GO
GRANT SELECT ON  [dbo].[PARTY_REL_TYPE] TO [IDT_DATA_CLEANUP]
GRANT INSERT ON  [dbo].[PARTY_REL_TYPE] TO [IDT_DATA_CLEANUP]
GRANT DELETE ON  [dbo].[PARTY_REL_TYPE] TO [IDT_DATA_CLEANUP]
GRANT UPDATE ON  [dbo].[PARTY_REL_TYPE] TO [IDT_DATA_CLEANUP]
GO
