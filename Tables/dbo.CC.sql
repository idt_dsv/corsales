CREATE TABLE [dbo].[CC]
(
[CC_ID] [int] NOT NULL,
[CC_TYPE_ID] [int] NOT NULL,
[CC_NBR] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CC_ACCOUNT_NAME] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CC_EXPIRATION] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ACTIVE] [bit] NULL CONSTRAINT [DF__CC__ACTIVE__32AB8735] DEFAULT ((1))
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[CC] ADD CONSTRAINT [PK2] PRIMARY KEY CLUSTERED  ([CC_ID]) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[CC] WITH NOCHECK ADD CONSTRAINT [RefCC_TYPE3] FOREIGN KEY ([CC_TYPE_ID]) REFERENCES [dbo].[CC_TYPE] ([CC_TYPE_ID]) NOT FOR REPLICATION
GO
EXEC sp_addextendedproperty N'MS_Description', N'Credit Card table that stores credit card information for customers.', 'SCHEMA', N'dbo', 'TABLE', N'CC', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Shows if the credit Card is whether active = 1 or inactive = 0', 'SCHEMA', N'dbo', 'TABLE', N'CC', 'COLUMN', N'ACTIVE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The name on the credit card', 'SCHEMA', N'dbo', 'TABLE', N'CC', 'COLUMN', N'CC_ACCOUNT_NAME'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The expriation date of the credit card. Stored as a 4 characters in (MMYY) Format, with leading zeros for month and year.', 'SCHEMA', N'dbo', 'TABLE', N'CC', 'COLUMN', N'CC_EXPIRATION'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary Key. from Next_Key_Value table', 'SCHEMA', N'dbo', 'TABLE', N'CC', 'COLUMN', N'CC_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The encrypted Credit card number', 'SCHEMA', N'dbo', 'TABLE', N'CC', 'COLUMN', N'CC_NBR'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The type of Credit card. Foriegn Key to CC_TYPE table', 'SCHEMA', N'dbo', 'TABLE', N'CC', 'COLUMN', N'CC_TYPE_ID'
GO
