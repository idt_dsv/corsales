CREATE TABLE [dbo].[LINE_ITEM_REF_NBR_EXT_SPEC]
(
[LINE_ITEM_REF_NBR_EXT_SPEC_ID] [int] NOT NULL,
[REF_ID] [int] NOT NULL,
[XML_SCHEMA_TYPE_ID] [int] NOT NULL,
[DATA] [xml] NULL,
[CREATE_DTM] [datetime] NULL,
[CANCEL_DTM] [datetime] NULL
) ON [SALES_DATA] TEXTIMAGE_ON [SALES_DATA]
GO
ALTER TABLE [dbo].[LINE_ITEM_REF_NBR_EXT_SPEC] ADD CONSTRAINT [PK_LINE_ITEM_REF_NBR_EXT_SPEC] PRIMARY KEY CLUSTERED  ([LINE_ITEM_REF_NBR_EXT_SPEC_ID]) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_REF_ID] ON [dbo].[LINE_ITEM_REF_NBR_EXT_SPEC] ([REF_ID]) ON [SALES_DATA]
GO
EXEC sp_addextendedproperty N'MS_DESCRIPTION', N'Holds extended spec data for a ref ID.  Data is in XML.', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_REF_NBR_EXT_SPEC', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date spec was cancelled', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_REF_NBR_EXT_SPEC', 'COLUMN', N'CANCEL_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date spec was created', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_REF_NBR_EXT_SPEC', 'COLUMN', N'CREATE_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Data of the extended spec', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_REF_NBR_EXT_SPEC', 'COLUMN', N'DATA'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Key', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_REF_NBR_EXT_SPEC', 'COLUMN', N'LINE_ITEM_REF_NBR_EXT_SPEC_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to line_item_ref_nbr', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_REF_NBR_EXT_SPEC', 'COLUMN', N'REF_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Lookup value for XML schema type', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_REF_NBR_EXT_SPEC', 'COLUMN', N'XML_SCHEMA_TYPE_ID'
GO
