CREATE TABLE [dbo].[AUTO_ENTRY_OVERRIDE]
(
[AUTO_ENTRY_OVERRIDE_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[PARTY_ID] [int] NOT NULL,
[DESCRIPTION] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FROM_DT] [datetime] NULL,
[THROUGH_DT] [datetime] NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[AUTO_ENTRY_OVERRIDE] ADD CONSTRAINT [PK_AUTO_ENTRY_OVERRIDE] PRIMARY KEY CLUSTERED  ([AUTO_ENTRY_OVERRIDE_ID]) ON [SALES_DATA]
GO
EXEC sp_addextendedproperty N'MS_Description', N'List of Party_id''s to prevent auto web order entry', 'SCHEMA', N'dbo', 'TABLE', N'AUTO_ENTRY_OVERRIDE', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key (autoIncrement)
', 'SCHEMA', N'dbo', 'TABLE', N'AUTO_ENTRY_OVERRIDE', 'COLUMN', N'AUTO_ENTRY_OVERRIDE_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The information of why this party''s orders are not included in auto processing. (usually including who authorized it, and why it was done)
', 'SCHEMA', N'dbo', 'TABLE', N'AUTO_ENTRY_OVERRIDE', 'COLUMN', N'DESCRIPTION'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Begining effective date for the override. This field doesn''t include time and midnight is assumed.
', 'SCHEMA', N'dbo', 'TABLE', N'AUTO_ENTRY_OVERRIDE', 'COLUMN', N'FROM_DT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The party whose orders will not be automatically processed. It could be at any level (org, account, or party)
', 'SCHEMA', N'dbo', 'TABLE', N'AUTO_ENTRY_OVERRIDE', 'COLUMN', N'PARTY_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Ending effective date for the override. This field doesn''t include time and midnight is assumed.
', 'SCHEMA', N'dbo', 'TABLE', N'AUTO_ENTRY_OVERRIDE', 'COLUMN', N'THROUGH_DT'
GO
