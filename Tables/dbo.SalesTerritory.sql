CREATE TABLE [dbo].[SalesTerritory]
(
[SalesTerritoryId] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[TerritoryId] [int] NOT NULL,
[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentSalesTerritoryId] [int] NULL,
[TerritoryType] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IDT_GUID] [uniqueidentifier] NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[SalesTerritory] ADD CONSTRAINT [PK_SalesTerritory] PRIMARY KEY CLUSTERED  ([SalesTerritoryId]) ON [SALES_DATA]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SalesTerritory] ON [dbo].[SalesTerritory] ([TerritoryId], [TerritoryType]) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[SalesTerritory] ADD CONSTRAINT [FK_SalesTerritory_SalesTerritory] FOREIGN KEY ([ParentSalesTerritoryId]) REFERENCES [dbo].[SalesTerritory] ([SalesTerritoryId])
GO
