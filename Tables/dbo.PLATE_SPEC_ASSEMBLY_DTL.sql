CREATE TABLE [dbo].[PLATE_SPEC_ASSEMBLY_DTL]
(
[PLATE_SPEC_ASSEMBLY_DTL_ID] [int] NOT NULL,
[PLATE_ASSEMBLY_GROUP] [int] NOT NULL,
[REF_ID] [int] NOT NULL,
[BAND] [int] NOT NULL,
[LAYER] [int] NOT NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[PLATE_SPEC_ASSEMBLY_DTL] ADD CONSTRAINT [PLATE_SPEC_ASSEMBLY_DTL_PK] PRIMARY KEY CLUSTERED  ([PLATE_SPEC_ASSEMBLY_DTL_ID]) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[PLATE_SPEC_ASSEMBLY_DTL] ADD CONSTRAINT [PLATE_SPEC_ASSEMBLY_DTL_U2] UNIQUE NONCLUSTERED  ([PLATE_ASSEMBLY_GROUP], [BAND], [LAYER]) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[PLATE_SPEC_ASSEMBLY_DTL] ADD CONSTRAINT [PLATE_SPEC_ASSEMBLY_DTL_U] UNIQUE NONCLUSTERED  ([REF_ID]) ON [SALES_DATA]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table is used to link all of the internal plates that are used to synthesize the oligos for a single shipping plate.', 'SCHEMA', N'dbo', 'TABLE', N'PLATE_SPEC_ASSEMBLY_DTL', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Larger plates are split into 96 well plates for synthesis.  This field is used to identify how to reassemble the shipping plate from its component pieces.', 'SCHEMA', N'dbo', 'TABLE', N'PLATE_SPEC_ASSEMBLY_DTL', 'COLUMN', N'BAND'
GO
EXEC sp_addextendedproperty N'MS_Description', N'When multiple oligos are shipped in a single well, the order must be split into layers for synthesis.  This identifies the layer the plate represents.', 'SCHEMA', N'dbo', 'TABLE', N'PLATE_SPEC_ASSEMBLY_DTL', 'COLUMN', N'LAYER'
GO
EXEC sp_addextendedproperty N'MS_Description', N'This ID is used to identify all of the component plates that need to be assembled into a single shipping plate.', 'SCHEMA', N'dbo', 'TABLE', N'PLATE_SPEC_ASSEMBLY_DTL', 'COLUMN', N'PLATE_ASSEMBLY_GROUP'
GO
EXEC sp_addextendedproperty N'MS_Description', N'This is the Primary Key. It receives its value from NEXT_KEY_VAL table using the PLATE_SPEC_ASSEMBLY_DTL_ID lookup value.', 'SCHEMA', N'dbo', 'TABLE', N'PLATE_SPEC_ASSEMBLY_DTL', 'COLUMN', N'PLATE_SPEC_ASSEMBLY_DTL_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'This is the Ref_ID of a plate used to specify a group of oligos to synthesize.', 'SCHEMA', N'dbo', 'TABLE', N'PLATE_SPEC_ASSEMBLY_DTL', 'COLUMN', N'REF_ID'
GO
