CREATE TABLE [dbo].[ORD_STATUS_AUDIT]
(
[ORD_STATUS_AUDIT_ID] [int] NOT NULL IDENTITY(1, 1),
[ORD_ID] [int] NOT NULL,
[CURR_STAT] [int] NOT NULL,
[WorkstationName] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_ORD_STATUS_AUDIT_WorkstationName] DEFAULT (host_name()),
[ApplicationName] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_ORD_STATUS_AUDIT_ApplicationName] DEFAULT (app_name()),
[LoginName] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_ORD_STATUS_AUDIT_LoginName] DEFAULT (user_name()),
[OperationType] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateDtm] [datetime] NULL CONSTRAINT [DF_ORD_STATUS_AUDIT_UpdateDtm] DEFAULT (getdate()),
[New_Curr_stat] [int] NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[ORD_STATUS_AUDIT] ADD CONSTRAINT [IX_ORD_STATUS_AUDIT_ID] UNIQUE NONCLUSTERED  ([ORD_STATUS_AUDIT_ID]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [ix_ORD_STATUS_AUDIT_OrdId] ON [dbo].[ORD_STATUS_AUDIT] ([ORD_ID]) ON [SALES_DATA]
GO
EXEC sp_addextendedproperty N'MS_Description', N'A temporary audit table capturing the status changed. Note: Inserts are not audited. Only deletes and updates. Connection info is at the time of the data change', 'SCHEMA', N'dbo', 'TABLE', N'ORD_STATUS_AUDIT', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'The application name that made the data change', 'SCHEMA', N'dbo', 'TABLE', N'ORD_STATUS_AUDIT', 'COLUMN', N'ApplicationName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Order Status before the change (lookup into ORD_STAT_TYPE table)', 'SCHEMA', N'dbo', 'TABLE', N'ORD_STATUS_AUDIT', 'COLUMN', N'CURR_STAT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The login name of the connection that made the data change', 'SCHEMA', N'dbo', 'TABLE', N'ORD_STATUS_AUDIT', 'COLUMN', N'LoginName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The operation type: delete or update', 'SCHEMA', N'dbo', 'TABLE', N'ORD_STATUS_AUDIT', 'COLUMN', N'OperationType'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The Primary Key Id of ORD table that was changed', 'SCHEMA', N'dbo', 'TABLE', N'ORD_STATUS_AUDIT', 'COLUMN', N'ORD_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary Key using AutoIncrement', 'SCHEMA', N'dbo', 'TABLE', N'ORD_STATUS_AUDIT', 'COLUMN', N'ORD_STATUS_AUDIT_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The date and time the record was changed', 'SCHEMA', N'dbo', 'TABLE', N'ORD_STATUS_AUDIT', 'COLUMN', N'UpdateDtm'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The Workstation of the connection that made the data change', 'SCHEMA', N'dbo', 'TABLE', N'ORD_STATUS_AUDIT', 'COLUMN', N'WorkstationName'
GO
