CREATE TABLE [dbo].[PARTY_BILL_ACCT]
(
[PARTY_BILL_ACCT_ID] [int] NOT NULL,
[PARTY_ID] [int] NOT NULL,
[BILL_ACCT_ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FROM_DTM] [datetime] NULL,
[THRU_DTM] [datetime] NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[PARTY_BILL_ACCT] ADD CONSTRAINT [PK_PARTY_BILL_ACCT] PRIMARY KEY CLUSTERED  ([PARTY_BILL_ACCT_ID]) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [ix_party_bill_acct_party_id] ON [dbo].[PARTY_BILL_ACCT] ([PARTY_ID]) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[PARTY_BILL_ACCT] WITH NOCHECK ADD CONSTRAINT [FK_PARTY_BILL_ACCT_PARTY] FOREIGN KEY ([PARTY_ID]) REFERENCES [dbo].[PARTY] ([PARTY_ID]) NOT FOR REPLICATION
GO
GRANT SELECT ON  [dbo].[PARTY_BILL_ACCT] TO [IDT_DATA_CLEANUP]
GRANT INSERT ON  [dbo].[PARTY_BILL_ACCT] TO [IDT_DATA_CLEANUP]
GRANT DELETE ON  [dbo].[PARTY_BILL_ACCT] TO [IDT_DATA_CLEANUP]
GRANT UPDATE ON  [dbo].[PARTY_BILL_ACCT] TO [IDT_DATA_CLEANUP]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Table linking parties to multiple billing account IDs', 'SCHEMA', N'dbo', 'TABLE', N'PARTY_BILL_ACCT', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'billing account customer ID', 'SCHEMA', N'dbo', 'TABLE', N'PARTY_BILL_ACCT', 'COLUMN', N'BILL_ACCT_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'active datetime', 'SCHEMA', N'dbo', 'TABLE', N'PARTY_BILL_ACCT', 'COLUMN', N'FROM_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key, gets its value from the Next_Key_Val table', 'SCHEMA', N'dbo', 'TABLE', N'PARTY_BILL_ACCT', 'COLUMN', N'PARTY_BILL_ACCT_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'foreign key to party table', 'SCHEMA', N'dbo', 'TABLE', N'PARTY_BILL_ACCT', 'COLUMN', N'PARTY_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'inactive datetime', 'SCHEMA', N'dbo', 'TABLE', N'PARTY_BILL_ACCT', 'COLUMN', N'THRU_DTM'
GO
