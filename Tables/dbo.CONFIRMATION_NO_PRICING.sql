CREATE TABLE [dbo].[CONFIRMATION_NO_PRICING]
(
[ORG_ID] [int] NOT NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[CONFIRMATION_NO_PRICING] ADD CONSTRAINT [PK_CONFIRMATION_NO_PRICING] PRIMARY KEY CLUSTERED  ([ORG_ID]) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[CONFIRMATION_NO_PRICING] WITH NOCHECK ADD CONSTRAINT [FK_CONFIRMATION_NO_PRICING_ORG] FOREIGN KEY ([ORG_ID]) REFERENCES [dbo].[ORG] ([PARTY_ID]) NOT FOR REPLICATION
GO
EXEC sp_addextendedproperty N'MS_Description', N'Holds Org''s that don''t want to see price information on confirmations.', 'SCHEMA', N'dbo', 'TABLE', N'CONFIRMATION_NO_PRICING', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Organization (Party_ID)', 'SCHEMA', N'dbo', 'TABLE', N'CONFIRMATION_NO_PRICING', 'COLUMN', N'ORG_ID'
GO
