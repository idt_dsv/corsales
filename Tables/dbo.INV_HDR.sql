CREATE TABLE [dbo].[INV_HDR]
(
[BATCH_ORD_ID] [int] NOT NULL,
[BATCH_ID] [int] NOT NULL,
[SALES_ORD_NBR] [int] NOT NULL,
[INV_NBR] [int] NULL,
[CURR_STAT] [int] NOT NULL CONSTRAINT [DF__INV_HDR__CURR_ST__5D95E53A] DEFAULT ((1)),
[BILL_ACCT_ID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ORG_NBR] [int] NULL,
[ACCT_NBR] [int] NULL,
[ENDUSER_NBR] [int] NULL,
[INV_TOTAL] [money] NULL,
[INV_LINES] [int] NULL,
[SHIP_DT] [datetime] NULL,
[ORD_DT] [datetime] NULL,
[INV_DT] [datetime] NULL,
[PAY_AUTH_NBR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PAYMENT_TYPE] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRICE_GRP_CD] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHIP_METH] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHIP_NM] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHIP_ADDR1] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHIP_ADDR2] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHIP_CITY] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHIP_ST] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHIP_ZIP] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHIP_ZIP4] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHIP_CNTRY] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHIP_PH] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BILL_NM] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BILL_ADDR1] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BILL_ADDR2] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BILL_CITY] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BILL_ST] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BILL_ZIP] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BILL_ZIP4] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BILL_CNTRY] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BILL_PH] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHIP_ENDUSER_NM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ORD_METH] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CURRENCY_ID] [int] NULL CONSTRAINT [DF__INV_HDR__CURRENC__5E8A0973] DEFAULT ((1)),
[EXCHANGE_RATE] [float] NULL CONSTRAINT [DF__INV_HDR__EXCHANG__5F7E2DAC] DEFAULT ((1)),
[BILL_FAX] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DELIVERY_METHOD_ID] [int] NULL,
[INV_TOTAL_NATURAL] [money] NULL,
[COMPANY_ID] [int] NULL,
[INCOTERM_CODE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CREATE_DTM] [datetime] NULL,
[ACTIVE] [bit] NULL,
[INV_REPORT_SETTING_ID] [int] NULL,
[TAX_RATE] [float] NULL,
[RemitToId] [int] NULL
) ON [SALES_DATA]
GO
GRANT SELECT ON  [dbo].[INV_HDR] TO [IDT-CORALVILLE\srv_SDSQLv01]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


 CREATE  TRIGGER [dbo].[trg_DEL_INV_HDR] ON [dbo].[INV_HDR] 
 FOR DELETE 
 NOT FOR REPLICATION 
 AS

 DELETE INV_LINE_ITEMS
 WHERE BATCH_ORD_ID IN (SELECT BATCH_ORD_ID FROM DELETED)
 DELETE INV_STAT_HIST
 WHERE BATCH_ORD_ID IN (SELECT BATCH_ORD_ID FROM DELETED)
 DELETE ORD_STAT_HIST
 WHERE ORD_ID IN (SELECT ORD_ID FROM DELETED 
    JOIN ORD ON DELETED.SALES_ORD_NBR = ORD.SALES_ORD_NBR) 
 AND ORD_STAT_TYPE_ID = 15
 
 UPDATE INV_HDR SET ACTIVE = 1
 WHERE BATCH_ORD_ID IN 
	( 
		SELECT TOP 1 BATCH_ORD_ID 
		FROM INV_HDR 
		WHERE SALES_ORD_NBR = (SELECT SALES_ORD_NBR FROM DELETED) 
		ORDER BY CREATE_DTM DESC
	)



GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 CREATE TRIGGER [dbo].[TRG_INS_HDR_STAT_HIST] ON [dbo].[INV_HDR] 
 FOR INSERT
 NOT FOR REPLICATION 
 AS

 INSERT INV_STAT_HIST
 (BATCH_ORD_ID, STAT_TYPE_ID, STAT_DT)
 SELECT BATCH_ORD_ID, 1,GETDATE()
 FROM INSERTED

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 CREATE TRIGGER [dbo].[TRG_INV_HDR_INV_NBR] ON [dbo].[INV_HDR] 
 FOR INSERT, UPDATE
 NOT FOR REPLICATION 
 AS

 --INSERTS AND UPDATES
 UPDATE ORD 
 SET INV_NBR = INSERTED.INV_NBR
 FROM ORD JOIN INSERTED ON INSERTED.SALES_ORD_NBR = ORD.SALES_ORD_NBR
 WHERE INSERTED.INV_NBR IS NOT NULL

 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 create  TRIGGER [dbo].[TRG_UPDATE_BATCH_TOTALS] ON [dbo].[INV_HDR] 
 FOR INSERT, UPDATE, DELETE 
 NOT FOR REPLICATION 
 AS

 
 UPDATE INV_BATCH
 SET TOTAL_AMT = (SELECT SUM(INV_TOTAL) FROM INV_HDR WHERE BATCH_ID = INSERTED.BATCH_ID),
           TOTAL_ORD = (SELECT COUNT(BATCH_ORD_ID) FROM INV_HDR WHERE BATCH_ID = INSERTED.BATCH_ID)
 FROM INV_BATCH JOIN INSERTED ON INV_BATCH.BATCH_ID = INSERTED.BATCH_ID
 UPDATE INV_BATCH
 SET TOTAL_AMT = (SELECT SUM(INV_TOTAL) FROM INV_HDR WHERE BATCH_ID = DELETED.BATCH_ID),
           TOTAL_ORD = (SELECT COUNT(BATCH_ORD_ID) FROM INV_HDR WHERE BATCH_ID = DELETED.BATCH_ID)
 FROM INV_BATCH JOIN DELETED ON INV_BATCH.BATCH_ID = DELETED.BATCH_ID

 
GO
ALTER TABLE [dbo].[INV_HDR] ADD CONSTRAINT [PK_BATCH_INV_HDR] PRIMARY KEY NONCLUSTERED  ([BATCH_ORD_ID]) ON [SALES_DATA]
GO
CREATE CLUSTERED INDEX [IX_INV_HDR_1] ON [dbo].[INV_HDR] ([BATCH_ID], [SALES_ORD_NBR]) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_INV_HDR_BILL_ACCT] ON [dbo].[INV_HDR] ([BILL_ACCT_ID]) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_INV_HDR_2] ON [dbo].[INV_HDR] ([INV_DT], [ORG_NBR]) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_INV_HDR_INVNBR] ON [dbo].[INV_HDR] ([INV_NBR]) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_InvHdr_InvNbr_InvDt] ON [dbo].[INV_HDR] ([INV_NBR], [INV_DT]) INCLUDE ([BATCH_ORD_ID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_INV_HDR] ON [dbo].[INV_HDR] ([SALES_ORD_NBR]) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[INV_HDR] WITH NOCHECK ADD CONSTRAINT [FK_INV_HDR_ORD] FOREIGN KEY ([SALES_ORD_NBR]) REFERENCES [dbo].[ORD] ([SALES_ORD_NBR]) NOT FOR REPLICATION
GO
EXEC sp_addextendedproperty N'MS_Description', N'Fax number to appear on invoice - can be null/empty
', 'SCHEMA', N'dbo', 'TABLE', N'INV_HDR', 'COLUMN', N'BILL_FAX'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Method of delivery for invoice (copied from value in ORD table) and is displayed on invoice
', 'SCHEMA', N'dbo', 'TABLE', N'INV_HDR', 'COLUMN', N'DELIVERY_METHOD_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Invoice total expressed in working (home) currency', 'SCHEMA', N'dbo', 'TABLE', N'INV_HDR', 'COLUMN', N'INV_TOTAL'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Invoice total expressed in customers currency', 'SCHEMA', N'dbo', 'TABLE', N'INV_HDR', 'COLUMN', N'INV_TOTAL_NATURAL'
GO
