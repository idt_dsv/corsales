CREATE TABLE [dbo].[SalesTerritoryManager]
(
[SalesTerritoryID] [int] NOT NULL,
[HR_EMPLOYEE_ID] [int] NOT NULL
) ON [SALES_DATA]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		MDW
-- Create date: 6/23/07
-- Description:	Constraint to validate HR_EMPLOYEE_ID
-- =============================================
CREATE TRIGGER [dbo].[SalesTerritoryManager_trg_insUpd] 
   ON  [dbo].[SalesTerritoryManager] 
   AFTER INSERT,UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  IF NOT exists (SELECT * FROM inserted i JOIN reference.dbo.employee  emp ON i.HR_EMPLOYEE_ID = emp.HR_EMPLOYEE_ID)
    RAISERROR('The inserted or updated HR_EMPLOYEE_ID is not in the Reference.dbo.employee table.', 16, 1)
    -- Insert statements for trigger here

END

GO
ALTER TABLE [dbo].[SalesTerritoryManager] ADD CONSTRAINT [PK_SalesTerritoryManager] PRIMARY KEY CLUSTERED  ([SalesTerritoryID], [HR_EMPLOYEE_ID]) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[SalesTerritoryManager] ADD CONSTRAINT [FK_SalesTerritoryManager_SalesTerritory] FOREIGN KEY ([SalesTerritoryID]) REFERENCES [dbo].[SalesTerritory] ([SalesTerritoryId])
GO
