CREATE TABLE [dbo].[OLIGO_CARD_TYPE]
(
[OLIGO_CARD_TYPE_ID] [int] NOT NULL,
[OLIGO_CARD_TYPE_DESC] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[OLIGO_CARD_TYPE] ADD CONSTRAINT [PK_OLIGO_CARD_TYPE] PRIMARY KEY CLUSTERED  ([OLIGO_CARD_TYPE_ID]) ON [SALES_DATA]
GO
GRANT SELECT ON  [dbo].[OLIGO_CARD_TYPE] TO [IDT_WebApps]
GRANT INSERT ON  [dbo].[OLIGO_CARD_TYPE] TO [IDT_WebApps]
GRANT DELETE ON  [dbo].[OLIGO_CARD_TYPE] TO [IDT_WebApps]
GRANT UPDATE ON  [dbo].[OLIGO_CARD_TYPE] TO [IDT_WebApps]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Contains the available types of Oligo Cards. i.e.; "Standard" is a pre-payment, PO_Management is a purchasing tool to limit amount charged by a PO', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_TYPE', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Description of the type of Card', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_TYPE', 'COLUMN', N'OLIGO_CARD_TYPE_DESC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK from Next_KEY_VAL', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_TYPE', 'COLUMN', N'OLIGO_CARD_TYPE_ID'
GO
