CREATE TABLE [dbo].[MFold_Calculation]
(
[Ref_ID] [int] NOT NULL,
[Delta_G] [numeric] (10, 2) NULL,
[Delta_S] [numeric] (10, 2) NULL,
[Delta_H] [numeric] (10, 2) NULL,
[Fold_TM] [numeric] (10, 2) NULL,
[Fold_Affects_Guarantees] [bit] NULL,
[MFold_Engine_Version_ID] [int] NOT NULL,
[MFold_Explanation_ID] [int] NOT NULL,
[Updated_By_Batch_Process] [bit] NULL,
[Updated_DTM] [datetime] NOT NULL,
[Debugging_Comment] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateYield] [bit] NULL,
[UpdatePurity] [bit] NULL,
[OD_OBLIGATION] [float] NULL,
[OD_MAX_OBLIGATION] [int] NULL,
[OD_MIN_SYNTH_REQ] [float] NULL,
[OD_MAX_SYNTH_REQ] [int] NULL,
[YIELD_GUAR_POINTS] [int] NULL,
[PURITY_GUAR_PCT] [float] NULL,
[PURITY_GUAR_EXPLANATION_ID] [int] NULL,
[PURITY_GUAR_POINTS] [int] NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[MFold_Calculation] ADD CONSTRAINT [MFold_Calculation_PK] PRIMARY KEY CLUSTERED  ([Ref_ID]) ON [SALES_DATA]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table is used to provide a list of engine and data file version combinations.', 'SCHEMA', N'dbo', 'TABLE', N'MFold_Calculation', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Hopefully never used, actual error messge returned from Yihe''s service.', 'SCHEMA', N'dbo', 'TABLE', N'MFold_Calculation', 'COLUMN', N'Debugging_Comment'
GO
EXEC sp_addextendedproperty N'MS_Description', N'This is the Gibb''s Free Energy (Delta G) data value calculated by MFold.', 'SCHEMA', N'dbo', 'TABLE', N'MFold_Calculation', 'COLUMN', N'Delta_G'
GO
EXEC sp_addextendedproperty N'MS_Description', N'This is the Delta H value calculated by MFold.', 'SCHEMA', N'dbo', 'TABLE', N'MFold_Calculation', 'COLUMN', N'Delta_H'
GO
EXEC sp_addextendedproperty N'MS_Description', N'This is the Delta S data value calculated by MFold.', 'SCHEMA', N'dbo', 'TABLE', N'MFold_Calculation', 'COLUMN', N'Delta_S'
GO
EXEC sp_addextendedproperty N'MS_Description', N'This specifies whether or not the values from MFold affect the yield or purity guarantee.', 'SCHEMA', N'dbo', 'TABLE', N'MFold_Calculation', 'COLUMN', N'Fold_Affects_Guarantees'
GO
EXEC sp_addextendedproperty N'MS_Description', N'This is the Melting temperature of the fold as calculated by MFold.', 'SCHEMA', N'dbo', 'TABLE', N'MFold_Calculation', 'COLUMN', N'Fold_TM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Reference to the MFold_Engine_Version table.  Referential Integrity is not enforced due to potential table size.', 'SCHEMA', N'dbo', 'TABLE', N'MFold_Calculation', 'COLUMN', N'MFold_Engine_Version_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Reference to the MFold_Explanation table.  Referential Integrity is not enforced due to potential table size.', 'SCHEMA', N'dbo', 'TABLE', N'MFold_Calculation', 'COLUMN', N'MFold_Explanation_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Temporary testing field to hold data that will be updated in oligo_spec when Trey, Aaron and Ben decide to go live with the MFold data.', 'SCHEMA', N'dbo', 'TABLE', N'MFold_Calculation', 'COLUMN', N'OD_MAX_OBLIGATION'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Temporary testing field to hold data that will be updated in oligo_spec when Trey, Aaron and Ben decide to go live with the MFold data.', 'SCHEMA', N'dbo', 'TABLE', N'MFold_Calculation', 'COLUMN', N'OD_MAX_SYNTH_REQ'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Temporary testing field to hold data that will be updated in oligo_spec when Trey, Aaron and Ben decide to go live with the MFold data.', 'SCHEMA', N'dbo', 'TABLE', N'MFold_Calculation', 'COLUMN', N'OD_MIN_SYNTH_REQ'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Temporary testing field to hold data that will be updated in oligo_spec when Trey, Aaron and Ben decide to go live with the MFold data.', 'SCHEMA', N'dbo', 'TABLE', N'MFold_Calculation', 'COLUMN', N'OD_OBLIGATION'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Temporary testing field to hold data that will be updated in oligo_spec when Trey, Aaron and Ben decide to go live with the MFold data.', 'SCHEMA', N'dbo', 'TABLE', N'MFold_Calculation', 'COLUMN', N'PURITY_GUAR_EXPLANATION_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Temporary testing field to hold data that will be updated in oligo_spec when Trey, Aaron and Ben decide to go live with the MFold data.', 'SCHEMA', N'dbo', 'TABLE', N'MFold_Calculation', 'COLUMN', N'PURITY_GUAR_PCT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Temporary testing field to hold data that will be updated in oligo_spec when Trey, Aaron and Ben decide to go live with the MFold data.', 'SCHEMA', N'dbo', 'TABLE', N'MFold_Calculation', 'COLUMN', N'PURITY_GUAR_POINTS'
GO
EXEC sp_addextendedproperty N'MS_Description', N'This is the Primary Key. Used to uniquely reference an oligo in the Oligo_Spec table.', 'SCHEMA', N'dbo', 'TABLE', N'MFold_Calculation', 'COLUMN', N'Ref_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Boolean value used to specify whether the record was entered by a batch process or directly from the ordering system.', 'SCHEMA', N'dbo', 'TABLE', N'MFold_Calculation', 'COLUMN', N'Updated_By_Batch_Process'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date and time of the last update to the specified oligo''s folding value.', 'SCHEMA', N'dbo', 'TABLE', N'MFold_Calculation', 'COLUMN', N'Updated_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Temporary testing field to hold data that will be updated in oligo_spec when Trey, Aaron and Ben decide to go live with the MFold data.', 'SCHEMA', N'dbo', 'TABLE', N'MFold_Calculation', 'COLUMN', N'UpdatePurity'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Temporary testing field to hold data that will be updated in oligo_spec when Trey, Aaron and Ben decide to go live with the MFold data.', 'SCHEMA', N'dbo', 'TABLE', N'MFold_Calculation', 'COLUMN', N'UpdateYield'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Temporary testing field to hold data that will be updated in oligo_spec when Trey, Aaron and Ben decide to go live with the MFold data.', 'SCHEMA', N'dbo', 'TABLE', N'MFold_Calculation', 'COLUMN', N'YIELD_GUAR_POINTS'
GO
