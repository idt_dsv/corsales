CREATE TABLE [worklist].[WorkItem]
(
[WorkItemId] [bigint] NOT NULL IDENTITY(1, 1),
[WorkListId] [int] NOT NULL,
[CreatedAt] [datetime] NOT NULL CONSTRAINT [DF__WorkItem__Create__4905A7FF] DEFAULT (getdate()),
[CreatedBy] [nvarchar] (110) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PayloadId] [int] NOT NULL,
[WorkItemTypeId] [int] NOT NULL
) ON [SALES_DATA]
GO
ALTER TABLE [worklist].[WorkItem] ADD CONSTRAINT [PK_worklist.WorkItem] PRIMARY KEY CLUSTERED  ([WorkItemId]) ON [SALES_DATA]
GO
ALTER TABLE [worklist].[WorkItem] ADD CONSTRAINT [FK_worklist.WorkItem_worklist.WorkItemType_WorkItemTypeId] FOREIGN KEY ([WorkItemTypeId]) REFERENCES [worklist].[WorkItemType] ([WorkItemTypeId])
GO
ALTER TABLE [worklist].[WorkItem] ADD CONSTRAINT [FK_worklist.WorkItem_worklist.WorkList_WorkListId] FOREIGN KEY ([WorkListId]) REFERENCES [worklist].[WorkList] ([WorkListId])
GO
