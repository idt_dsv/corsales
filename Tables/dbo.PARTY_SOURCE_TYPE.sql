CREATE TABLE [dbo].[PARTY_SOURCE_TYPE]
(
[PARTY_SOURCE_TYPE_ID] [int] NOT NULL,
[NAME] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DESCRIPTION] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[PARTY_SOURCE_TYPE] ADD CONSTRAINT [PK_PARTY_SOURCE_TYPE] PRIMARY KEY CLUSTERED  ([PARTY_SOURCE_TYPE_ID]) ON [SALES_DATA]
GO
GRANT SELECT ON  [dbo].[PARTY_SOURCE_TYPE] TO [IDT_DATA_CLEANUP]
GRANT INSERT ON  [dbo].[PARTY_SOURCE_TYPE] TO [IDT_DATA_CLEANUP]
GRANT DELETE ON  [dbo].[PARTY_SOURCE_TYPE] TO [IDT_DATA_CLEANUP]
GRANT UPDATE ON  [dbo].[PARTY_SOURCE_TYPE] TO [IDT_DATA_CLEANUP]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table defines the posoible sources for a party entry. (It was built for possible integrations with other companies).', 'SCHEMA', N'dbo', 'TABLE', N'PARTY_SOURCE_TYPE', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'The description for the party_source_type. This description should include any special handling and what it means to have a source of this type.', 'SCHEMA', N'dbo', 'TABLE', N'PARTY_SOURCE_TYPE', 'COLUMN', N'DESCRIPTION'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The name for the party_source_type', 'SCHEMA', N'dbo', 'TABLE', N'PARTY_SOURCE_TYPE', 'COLUMN', N'NAME'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The unique key for party_source_type. It is designed to be hand entered and therefore no value in the next key value table.', 'SCHEMA', N'dbo', 'TABLE', N'PARTY_SOURCE_TYPE', 'COLUMN', N'PARTY_SOURCE_TYPE_ID'
GO
