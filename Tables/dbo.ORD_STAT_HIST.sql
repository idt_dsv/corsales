CREATE TABLE [dbo].[ORD_STAT_HIST]
(
[ORD_ID] [int] NOT NULL,
[ORD_STAT_DT] [datetime] NOT NULL CONSTRAINT [DF__ORD_STAT___ORD_S__7B264821] DEFAULT (getdate()),
[ORD_STAT_TYPE_ID] [int] NOT NULL,
[USER_NM] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REASON] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [SALES_DATA]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

 CREATE TRIGGER [dbo].[DEL_ORD_STAT_HIST] ON [dbo].[ORD_STAT_HIST] 
 FOR DELETE 
 NOT FOR REPLICATION 
 AS

 
 UPDATE ORD
 SET CURR_STAT = SH1.ORD_STAT_TYPE_ID
	, LAST_EDIT_BY = coalesce(SH1.USER_NM,'') -- user name who made the status update, or the empty string if this is null.
	, LAST_EDIT_DT = SH1.ORD_STAT_DT
 FROM ORD JOIN DELETED ON ORD.ORD_ID = DELETED.ORD_ID 
 JOIN ORD_STAT_HIST AS SH1 ON DELETED.ORD_ID = SH1.ORD_ID
 WHERE SH1.ORD_STAT_DT =  (SELECT MAX(ORD_STAT_DT) 
 			FROM ORD_STAT_HIST WHERE ORD_ID = SH1.ORD_ID)

 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



 
 --Trigger as it stands now
 CREATE TRIGGER [dbo].[INS_ORD_STAT_HIST] ON [dbo].[ORD_STAT_HIST] 
 FOR INSERT, UPDATE
 /*
 Author: Bviering
 Date: Unknown
 Purpose:
 -Update Curr_Stat field on current status of Order
 
 Updates
 
 Author	Date	Comments
 ----------------------------------------
 MDW	???	Updated ref_nbr_Stat_hist trigger not to insert into ORD_STAT_HIST instead it will only run from nightly update Stored Procedure
 MDW	3/5/02	Added first statement in where clause.  This is to prevent an added shipment record after it had already been invoiced
 MDW 3/5/03 Added new stat type 21 to record changes in org_id of an order
 MAS	3/16/2004 Included stat type 22 as a status where we don't update the curr ord stat 
 MDW  3/08/06 changed 1st line of where statement to "WHERE INSERTED.ORD_STAT_TYPE_ID < 21" to exclude states that are for 
 MAS 4/25/07 changed < 21 to be specifically 21-23 because 24 and 25 should have ord updated
 */
 NOT FOR REPLICATION 
 AS

 
 UPDATE ORD
 SET CURR_STAT = SH1.ORD_STAT_TYPE_ID  --update ord with the most current ord_stat_hist record
	, LAST_EDIT_BY = coalesce(SH1.USER_NM,'') -- user name who made the status update, or the empty string if this is null.
	, LAST_EDIT_DT = SH1.ORD_STAT_DT
 FROM ORD JOIN INSERTED ON ORD.ORD_ID = INSERTED.ORD_ID 
   JOIN ORD_STAT_HIST AS SH1 ON INSERTED.ORD_ID = SH1.ORD_ID
 
 -- where clause comments. This is tricky. 
 -- If the ord curr_Stat is NOT IN (17,19) new status will update ord_curr_Stat. 
 -- If the ord curr_Stat is IN (17,19) it will additionally check the incoming status and make sure it is 18 or 19 before updating the ord.curr_Stat
 WHERE  (INSERTED.ORD_STAT_TYPE_ID not in (21, 22, 23)) -- currently excludes 21-23; because new states were causing issues they are cleanup entries and shouldn't affect the order status
   and (ord.curr_Stat not in (17,19) or INSERTED.ORD_STAT_TYPE_ID in (18,19)) --17 = invoiced, 18 = uninvoiced, 19 = re-invoiced, 21 is order's org_nbr changed
   and  SH1.ORD_STAT_DT =  (
       SELECT MAX(ORD_STAT_DT) 
 			FROM ORD_STAT_HIST 
       WHERE ORD_ID = SH1.ORD_ID
       )
 




GO
ALTER TABLE [dbo].[ORD_STAT_HIST] ADD CONSTRAINT [PK_ORD_STAT_HIST] PRIMARY KEY CLUSTERED  ([ORD_ID], [ORD_STAT_DT], [ORD_STAT_TYPE_ID]) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_ORD_STAT_HIST_1] ON [dbo].[ORD_STAT_HIST] ([ORD_STAT_DT], [ORD_STAT_TYPE_ID], [ORD_ID]) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[ORD_STAT_HIST] WITH NOCHECK ADD CONSTRAINT [FK_ORD_STAT_HIST_ORD] FOREIGN KEY ([ORD_ID]) REFERENCES [dbo].[ORD] ([ORD_ID]) NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[ORD_STAT_HIST] WITH NOCHECK ADD CONSTRAINT [FK_ORD_STAT_HIST_ORD_STAT_TYPE] FOREIGN KEY ([ORD_STAT_TYPE_ID]) REFERENCES [dbo].[ORD_STAT_TYPE] ([ORD_STAT_TYPE_ID]) NOT FOR REPLICATION
GO
GRANT SELECT ON  [dbo].[ORD_STAT_HIST] TO [IDT_DATA_CLEANUP]
GRANT INSERT ON  [dbo].[ORD_STAT_HIST] TO [IDT_DATA_CLEANUP]
GRANT DELETE ON  [dbo].[ORD_STAT_HIST] TO [IDT_DATA_CLEANUP]
GRANT UPDATE ON  [dbo].[ORD_STAT_HIST] TO [IDT_DATA_CLEANUP]
GO
