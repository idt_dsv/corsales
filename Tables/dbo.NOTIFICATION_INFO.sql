CREATE TABLE [dbo].[NOTIFICATION_INFO]
(
[NOTIFICATION_INFO_ID] [int] NOT NULL,
[CREATE_DTM] [datetime] NOT NULL,
[XML_SCHEMA_TYPE_ID] [int] NOT NULL,
[XML_DATA] [xml] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[NOTIFICATION_INFO] ADD CONSTRAINT [PK_NOTIFICATION_INFO] PRIMARY KEY CLUSTERED  ([NOTIFICATION_INFO_ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Notification Information Table', 'SCHEMA', N'dbo', 'TABLE', N'NOTIFICATION_INFO', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'This is the date and time the entry was created.', 'SCHEMA', N'dbo', 'TABLE', N'NOTIFICATION_INFO', 'COLUMN', N'CREATE_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'pk From Next_Key_Val', 'SCHEMA', N'dbo', 'TABLE', N'NOTIFICATION_INFO', 'COLUMN', N'NOTIFICATION_INFO_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'xml data for record.', 'SCHEMA', N'dbo', 'TABLE', N'NOTIFICATION_INFO', 'COLUMN', N'XML_DATA'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK EXTENDED_SPEC_TYPE', 'SCHEMA', N'dbo', 'TABLE', N'NOTIFICATION_INFO', 'COLUMN', N'XML_SCHEMA_TYPE_ID'
GO
