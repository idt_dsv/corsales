CREATE TABLE [dbo].[AGREEMENT]
(
[AGREEMENT_ID] [int] NOT NULL,
[YIELD_GUAR_AGREEMENT_ID] [int] NOT NULL,
[BEGIN_DT] [datetime] NULL,
[END_DT] [datetime] NULL,
[AGREEMENT_NM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ALLOW_DROP_SHIPMENT] [bit] NOT NULL CONSTRAINT [DF__AGREEMENT__ALLOW__5224328E] DEFAULT (CONVERT([bit],(0),0)),
[COMBINE_SHIPMENTS] [bit] NULL CONSTRAINT [DF__AGREEMENT__COMBI__531856C7] DEFAULT (CONVERT([bit],(0),0)),
[AGREEMENT_TEXT] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [SALES_DATA] TEXTIMAGE_ON [SALES_DATA]
GO
ALTER TABLE [dbo].[AGREEMENT] ADD CONSTRAINT [PK_AGREEMENT] PRIMARY KEY CLUSTERED  ([AGREEMENT_ID]) ON [SALES_DATA]
GO
EXEC sp_addextendedproperty N'MS_Description', N'unique ID used for internal reference', 'SCHEMA', N'dbo', 'TABLE', N'AGREEMENT', 'COLUMN', N'AGREEMENT_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The internal name of the agreement', 'SCHEMA', N'dbo', 'TABLE', N'AGREEMENT', 'COLUMN', N'AGREEMENT_NM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Stores the full text of the agreement
', 'SCHEMA', N'dbo', 'TABLE', N'AGREEMENT', 'COLUMN', N'AGREEMENT_TEXT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Specifies whether the packages on an order can be split and sent to multiple destinations', 'SCHEMA', N'dbo', 'TABLE', N'AGREEMENT', 'COLUMN', N'ALLOW_DROP_SHIPMENT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The date the agreement begins', 'SCHEMA', N'dbo', 'TABLE', N'AGREEMENT', 'COLUMN', N'BEGIN_DT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Specifies whether orders made under this agreement will have their packages combined in a single package when shipped
', 'SCHEMA', N'dbo', 'TABLE', N'AGREEMENT', 'COLUMN', N'COMBINE_SHIPMENTS'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Short notes to describe specifics of the agreement', 'SCHEMA', N'dbo', 'TABLE', N'AGREEMENT', 'COLUMN', N'Description'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The date the agreement ends. (9/9/9999 is considered a max date)', 'SCHEMA', N'dbo', 'TABLE', N'AGREEMENT', 'COLUMN', N'END_DT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The Yield Agreement specified for the Agreement. If not a special agreement use IDT_Standard_shipping Yield Guarantee Agreement', 'SCHEMA', N'dbo', 'TABLE', N'AGREEMENT', 'COLUMN', N'YIELD_GUAR_AGREEMENT_ID'
GO
