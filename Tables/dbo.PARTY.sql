CREATE TABLE [dbo].[PARTY]
(
[PARTY_ID] [int] NOT NULL,
[SHIP_GRP_ID] [int] NULL CONSTRAINT [DF__PARTY__SHIP_GRP___1332DBDC] DEFAULT ((14)),
[PRICE_GRP_ID] [int] NULL CONSTRAINT [DF__PARTY__PRICE_GRP__14270015] DEFAULT ((1)),
[SALES_TERR_ID] [int] NULL CONSTRAINT [DF__PARTY__SALES_TER__151B244E] DEFAULT ((9999)),
[BILL_CYCLE_ID] [int] NULL CONSTRAINT [DF__PARTY__BILL_CYCL__160F4887] DEFAULT ((0)),
[DEF_CARRIER_ID] [int] NOT NULL CONSTRAINT [DF__PARTY__DEF_CARRI__17036CC0] DEFAULT ((1)),
[CURR_STAT_TYPE] [int] NOT NULL CONSTRAINT [DF__PARTY__CURR_STAT__17F790F9] DEFAULT ((1)),
[CRDT_RATE_ID] [int] NULL CONSTRAINT [DF__PARTY__CRDT_RATE__18EBB532] DEFAULT ((1)),
[PARTY_SOURCE_TYPE_ID] [int] NULL CONSTRAINT [DF__PARTY__PARTY_SOU__19DFD96B] DEFAULT ((1)),
[BILL_ACCT_ID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SALES_TAX] [smallmoney] NULL CONSTRAINT [DF__PARTY__SALES_TAX__1AD3FDA4] DEFAULT ((0.00)),
[PARTY_SUB_TYPE] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CREATE_DT] [datetime] NULL CONSTRAINT [DF__PARTY__CREATE_DT__1BC821DD] DEFAULT (getdate()),
[CREATE_USER] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__PARTY__CREATE_US__1CBC4616] DEFAULT (suser_sname()),
[MODIFY_DT] [datetime] NULL,
[MODIFY_USER] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CURRENCY_ID] [int] NULL,
[SEND_PROMO_EMAIL] [bit] NOT NULL CONSTRAINT [DF__PARTY__SEND_PROM__1DB06A4F] DEFAULT (CONVERT([bit],(1),0)),
[PREF_BRANCH_LOC_ID] [int] NULL,
[IDT_GUID] [uniqueidentifier] NULL ROWGUIDCOL CONSTRAINT [Guid_Default] DEFAULT (newid())
) ON [SALES_DATA]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
 create  TRIGGER [dbo].[PARTY_INS] ON [dbo].[PARTY] 
 FOR INSERT
 /*
 AUTHOR: Mdewaard
 DATE: 5/3/02
 Comments:
 --Implements an audt for Sales Territories
 
 UPDATES
 Author	Date	Comment
 ---------------------------------------------
 MDEWAARD	5/9/02	Updated to include audit on Price groups
 MDEWAARD  4/8/03  Added Currency info
 */
 
 NOT FOR REPLICATION 
 AS

   insert party_sales_territory (party_id, Sales_terr_id, Begin_dt)
   select inserted.party_id, inserted.Sales_terr_id, getdate()
   from inserted
   where party_sub_type = 'ORG'
   
   insert PARTY_PRICE_GROUP (party_id, PRICE_GRP_ID, Begin_dt)
   select inserted.party_id, inserted.PRICE_GRP_ID, getdate()
   from inserted
 
   insert party_ship_Group (party_id, SHIP_Group_ID, Begin_dt)
   select inserted.party_id, inserted.SHIP_GRP_ID, getdate()
   from inserted
 
   insert PARTY_CURRENCY (party_id, CURRENCY_ID, Begin_dt)
   select inserted.party_id, inserted.CURRENCY_ID, getdate()
   from inserted
 
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
 CREATE  TRIGGER [dbo].[PARTY_UPD] ON [dbo].[PARTY] 
 FOR UPDATE
 /*
 AUTHOR: Mdewaard
 DATE: 5/3/02
 Comments:
 --Implements an audt for Sales Territories
 
 UPDATES
 Author	Date	Comment
 ---------------------------------------------
 MDEWAARD	5/9/02	Updated to include audit on Price groups
 MDEWAARD  4/8/03  Added Currency info
 */
 NOT FOR REPLICATION 
 AS

 declare @Date datetime
 set @Date  = getdate()
 
 --AUDIT SALES_TERRITORY
 
 
     update party_sales_territory
      set End_dt =  @Date
     from  party_sales_territory
      join inserted on inserted.party_id = party_sales_territory.party_id
     join deleted on deleted.party_id = Inserted.party_id
     where  inserted.party_sub_type = 'ORG'
       and party_sales_territory.party_id = inserted.party_id
       and party_sales_territory.begin_dt = (select max(pst.begin_dt) 
                                                     from party_sales_territory pst  
                                                     where pst.party_id =  party_sales_territory.party_id)
       and INSERTED.SALES_TERR_ID <> DELETED.SALES_TERR_ID
    
     insert party_sales_territory (party_id, Sales_terr_id, Begin_dt)
     select inserted.party_id, inserted.Sales_terr_id, @Date
     from inserted
     join deleted on deleted.party_id = Inserted.party_id
     where  inserted.party_sub_type = 'ORG'
       and INSERTED.SALES_TERR_ID <> DELETED.SALES_TERR_ID
   
   ---AUDIT PRICE_GRP 
   update PARTY_PRICE_GROUP
    set End_dt =  @Date
   from  PARTY_PRICE_GROUP
    join inserted on inserted.party_id = PARTY_PRICE_GROUP.party_id
     join deleted on deleted.party_id = Inserted.party_id
   where PARTY_PRICE_GROUP.party_id = inserted.party_id
     and PARTY_PRICE_GROUP.begin_dt = (select max(ppg.begin_dt) 
                                                     from PARTY_PRICE_GROUP ppg  
                                                     where ppg.party_id =  PARTY_PRICE_GROUP.party_id)
       and INSERTED.PRICE_GRP_ID <> DELETED.PRICE_GRP_ID
   
   insert PARTY_PRICE_GROUP (party_id, PRICE_Grp_ID, Begin_dt)
   select inserted.party_id, inserted.PRICE_GRP_ID, @Date
   from inserted
     join deleted on deleted.party_id = Inserted.party_id
   where  INSERTED.PRICE_GRP_ID <> DELETED.PRICE_GRP_ID
 
 
 --AUDIT Ship_grp
 
 
   update psg
    set End_dt =  @Date
   from  party_ship_Group psg
     join inserted on inserted.party_id = psg.party_id
     join deleted on deleted.party_id = Inserted.party_id
   where  psg.party_id = inserted.party_id
     and psg.begin_dt = (select max(psg2.begin_dt) 
                         from party_ship_Group psg2  
                         where psg2.party_id =  psg.party_id)
     and INSERTED.SHIP_GRP_ID <> DELETED.SHIP_GRP_ID
  
   insert party_ship_Group (party_id, SHIP_Group_ID, Begin_dt)
   select inserted.party_id, inserted.SHIP_GRP_ID, @Date
   from inserted
   join deleted on deleted.party_id = Inserted.party_id
   where INSERTED.SHIP_GRP_ID <> DELETED.SHIP_GRP_ID
 
 
   update pc
    set End_dt =  @Date
   from  party_CURRENCY pc
     join inserted on inserted.party_id = pc.party_id
     join deleted on deleted.party_id = Inserted.party_id
   where  pc.party_id = inserted.party_id
     and pc.begin_dt = (select max(pc2.begin_dt) 
                         from party_CURRENCY pc2  
                         where pc2.party_id =  pc.party_id)
     and INSERTED.CURRENCY_ID <> DELETED.CURRENCY_ID
  
   insert party_CURRENCY (party_id, CURRENCY_ID, Begin_dt)
   select inserted.party_id, inserted.CURRENCY_ID, @Date
   from inserted
   join deleted on deleted.party_id = Inserted.party_id
   where INSERTED.CURRENCY_ID <> DELETED.CURRENCY_ID
 
 
GO
ALTER TABLE [dbo].[PARTY] ADD CONSTRAINT [PK__PARTY__7CD98669] PRIMARY KEY NONCLUSTERED  ([PARTY_ID]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_PARTY_Bill_Acct] ON [dbo].[PARTY] ([BILL_ACCT_ID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [IX_PARTY] ON [dbo].[PARTY] ([SALES_TERR_ID]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[PARTY] WITH NOCHECK ADD CONSTRAINT [RefBILL_CYCLE79] FOREIGN KEY ([BILL_CYCLE_ID]) REFERENCES [dbo].[BILL_CYCLE] ([BILL_CYCLE_ID]) NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[PARTY] WITH NOCHECK ADD CONSTRAINT [RefDEF_CARRIER68] FOREIGN KEY ([DEF_CARRIER_ID]) REFERENCES [dbo].[DEF_CARRIER] ([DEF_CARRIER_ID]) NOT FOR REPLICATION
GO
GRANT SELECT ON  [dbo].[PARTY] TO [IDT_CORE_NOTES_APP]
GRANT SELECT ON  [dbo].[PARTY] TO [IDT_DATA_CLEANUP]
GRANT INSERT ON  [dbo].[PARTY] TO [IDT_DATA_CLEANUP]
GRANT DELETE ON  [dbo].[PARTY] TO [IDT_DATA_CLEANUP]
GRANT UPDATE ON  [dbo].[PARTY] TO [IDT_DATA_CLEANUP]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Preferred branch location for party''s orders.', 'SCHEMA', N'dbo', 'TABLE', N'PARTY', 'COLUMN', N'PREF_BRANCH_LOC_ID'
GO
