CREATE TABLE [dbo].[OLIGO_SPEC]
(
[REF_ID] [int] NOT NULL,
[BASES] [int] NULL,
[LENGTH] [int] NULL,
[PURIF_ID] [int] NULL,
[SYNTH_COL_ID] [int] NULL,
[EXT_COEFF] [float] NULL,
[GC] [float] NULL,
[MW_HYDRATED] [float] NULL,
[MW_ANHYDROUS] [float] NULL,
[TM] [float] NULL,
[OD_OBLIGATION] [float] NULL,
[OD_MAX_OBLIGATION] [int] NULL,
[OD_MIN_SYNTH_REQ] [float] NULL,
[OD_MAX_SYNTH_REQ] [int] NULL,
[YIELD_GUAR_TYPE] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YIELD_GUAR_POINTS] [int] NULL,
[PURITY_GUAR_PCT] [float] NULL,
[PURITY_GUAR_EXPLANATION_ID] [int] NULL,
[PURITY_GUAR_POINTS] [int] NULL,
[UNIT_SIZE] [float] NULL,
[OS_UPD_DTM] [datetime] NULL CONSTRAINT [DF__OLIGO_SPE__OS_UP__2739D489] DEFAULT (getdate()),
[SEQ] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SEQ_DESC2] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SEQ_DESC] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AGREEMENT_ID] [int] NULL,
[YIELD_GUAR_OVERRIDE] [int] NULL
) ON [SALES_DATA] TEXTIMAGE_ON [SALES_DATA]
GO
ALTER TABLE [dbo].[OLIGO_SPEC] ADD CONSTRAINT [PK_OLIGO_SPEC] PRIMARY KEY CLUSTERED  ([REF_ID]) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [ix_oligo_spec_1] ON [dbo].[OLIGO_SPEC] ([PURIF_ID]) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[OLIGO_SPEC] WITH NOCHECK ADD CONSTRAINT [FK_OLIGO_SPEC_LINE_ITEM_REF_NBR] FOREIGN KEY ([REF_ID]) REFERENCES [dbo].[LINE_ITEM_REF_NBR] ([REF_ID]) NOT FOR REPLICATION
GO
EXEC sp_addextendedproperty N'MS_Description', N'No longer valid. use line_item_ref_nbr.agreemnt_id instead', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_SPEC', 'COLUMN', N'AGREEMENT_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The maximum OD value possible for this oligo.', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_SPEC', 'COLUMN', N'OD_MAX_OBLIGATION'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The maximum OD value possible for this oligo after synthesis and before purification.', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_SPEC', 'COLUMN', N'OD_MAX_SYNTH_REQ'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The yield guarantee in OD''s after synthesis. i.e.; if PAGE purification the synth requirement is set higher.', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_SPEC', 'COLUMN', N'OD_MIN_SYNTH_REQ'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The Yield guarantee in OD''s to be shipped to the customer', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_SPEC', 'COLUMN', N'OD_OBLIGATION'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Purity_Guar_Explanation_ID is a link to a new purity guarantee table that has not yet been entered in the system.', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_SPEC', 'COLUMN', N'PURITY_GUAR_EXPLANATION_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Purity_Guar_Pct is the % of purity that we will guarantee for the specified oligo.', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_SPEC', 'COLUMN', N'PURITY_GUAR_PCT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Purity_Guar_Points is a measurement of how complex the oligo is, as defined by the purity guarantee system.

', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_SPEC', 'COLUMN', N'PURITY_GUAR_POINTS'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The oligo sequence (Using IDT format). If null it is using old Paradox sequence, that wasn''t compatible. Look in Sales Data Mart.', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_SPEC', 'COLUMN', N'SEQ'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The customers name for the sequence', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_SPEC', 'COLUMN', N'SEQ_DESC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Flag field used to specify whether or not the yeild guarantee fields have been overwritten.', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_SPEC', 'COLUMN', N'YIELD_GUAR_OVERRIDE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The number of points that help determine the yeild guarentee ', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_SPEC', 'COLUMN', N'YIELD_GUAR_POINTS'
GO
EXEC sp_addextendedproperty N'MS_Description', N'This is the type of the oligo. i.e.; DNA, RNA, etc.', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_SPEC', 'COLUMN', N'YIELD_GUAR_TYPE'
GO
