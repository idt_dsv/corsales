CREATE TABLE [dbo].[LINE_ITEM_PRICE_DETAIL]
(
[LINE_ITEM_PRICE_DETAIL_ID] [int] NOT NULL,
[LINE_ITEM_ID] [int] NOT NULL,
[WHOLESALE_STD_PRICE_HOME] [money] NULL,
[WHOLESALE_GROUP_PRICE_HOME] [money] NULL,
[WHOLESALE_QUOTE_PRICE_HOME] [money] NULL,
[RETAIL_STD_PRICE_HOME] [money] NULL,
[RETAIL_GROUP_PRICE_HOME] [money] NULL,
[RETAIL_QUOTE_PRICE_HOME] [money] NULL,
[WHOLESALE_STD_PRICE_NATURAL] [money] NULL,
[WHOLESALE_GROUP_PRICE_NATURAL] [money] NULL,
[WHOLESALE_QUOTE_PRICE_NATURAL] [money] NULL,
[RETAIL_STD_PRICE_NATURAL] [money] NULL,
[RETAIL_GROUP_PRICE_NATURAL] [money] NULL,
[RETAIL_QUOTE_PRICE_NATURAL] [money] NULL,
[WHOLESALE_CALC_PRICE_HOME] [money] NULL,
[WHOLESALE_CALC_PRICE_NATURAL] [money] NULL,
[WHOLESALE_OVER_PRICE_HOME] [money] NULL,
[WHOLESALE_OVER_PRICE_NATURAL] [money] NULL,
[RETAIL_CALC_PRICE_HOME] [money] NULL,
[RETAIL_CALC_PRICE_NATURAL] [money] NULL,
[RETAIL_OVER_PRICE_HOME] [money] NULL,
[RETAIL_OVER_PRICE_NATURAL] [money] NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[LINE_ITEM_PRICE_DETAIL] ADD CONSTRAINT [PK_LINE_ITEM_PRICE_DETAIL] PRIMARY KEY CLUSTERED  ([LINE_ITEM_PRICE_DETAIL_ID]) ON [SALES_DATA]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_LINE_ITEM_PRICE_DETAIL_LineItemId] ON [dbo].[LINE_ITEM_PRICE_DETAIL] ([LINE_ITEM_ID]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_DESCRIPTION', N'Table to hold additional pricing data to be used for debugging and reporting', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_PRICE_DETAIL', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Foreign Key to Ord_Line_Item', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_PRICE_DETAIL', 'COLUMN', N'LINE_ITEM_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Retail final calculated price in IDT home currency', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_PRICE_DETAIL', 'COLUMN', N'RETAIL_CALC_PRICE_HOME'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Retail final calculated price in customer''s currency', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_PRICE_DETAIL', 'COLUMN', N'RETAIL_CALC_PRICE_NATURAL'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Retail Group price in IDT home currency', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_PRICE_DETAIL', 'COLUMN', N'RETAIL_GROUP_PRICE_HOME'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Group retail price in customer''s currency', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_PRICE_DETAIL', 'COLUMN', N'RETAIL_GROUP_PRICE_NATURAL'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Retail overridden price in IDT home currency', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_PRICE_DETAIL', 'COLUMN', N'RETAIL_OVER_PRICE_HOME'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Retail overridden price in customer''s currency', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_PRICE_DETAIL', 'COLUMN', N'RETAIL_OVER_PRICE_NATURAL'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Retail quote price in IDT home currency', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_PRICE_DETAIL', 'COLUMN', N'RETAIL_QUOTE_PRICE_HOME'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Quote retail price in customer''s currency', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_PRICE_DETAIL', 'COLUMN', N'RETAIL_QUOTE_PRICE_NATURAL'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Standard Retail Price in IDT Home Currency', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_PRICE_DETAIL', 'COLUMN', N'RETAIL_STD_PRICE_HOME'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Standard retail price in customer''s currency', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_PRICE_DETAIL', 'COLUMN', N'RETAIL_STD_PRICE_NATURAL'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Wholesale final calculated price in IDT home currency', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_PRICE_DETAIL', 'COLUMN', N'WHOLESALE_CALC_PRICE_HOME'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Wholesale final calculated price in customer''s currency', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_PRICE_DETAIL', 'COLUMN', N'WHOLESALE_CALC_PRICE_NATURAL'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Group wholesale price in IDT home currency', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_PRICE_DETAIL', 'COLUMN', N'WHOLESALE_GROUP_PRICE_HOME'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Group wholesale price in customer''s currency', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_PRICE_DETAIL', 'COLUMN', N'WHOLESALE_GROUP_PRICE_NATURAL'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Wholesale overridden price in IDT home currency', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_PRICE_DETAIL', 'COLUMN', N'WHOLESALE_OVER_PRICE_HOME'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Wholesale overridden price in customer''s currency', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_PRICE_DETAIL', 'COLUMN', N'WHOLESALE_OVER_PRICE_NATURAL'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Quote wholesale price in IDT home currency', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_PRICE_DETAIL', 'COLUMN', N'WHOLESALE_QUOTE_PRICE_HOME'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Quote wholesale price in customer''s currency', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_PRICE_DETAIL', 'COLUMN', N'WHOLESALE_QUOTE_PRICE_NATURAL'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Standard wholesale price in IDT home currency', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_PRICE_DETAIL', 'COLUMN', N'WHOLESALE_STD_PRICE_HOME'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Standard wholesale price in customer''s currency', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_PRICE_DETAIL', 'COLUMN', N'WHOLESALE_STD_PRICE_NATURAL'
GO
