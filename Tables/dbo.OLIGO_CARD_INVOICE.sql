CREATE TABLE [dbo].[OLIGO_CARD_INVOICE]
(
[OLIGO_CARD_INVOICE_ID] [int] NOT NULL,
[OLIGO_CARD_ID] [int] NULL,
[INV_NBR] [int] NULL,
[PROD_ID] [int] NULL,
[CARD_AMOUNT] [money] NULL,
[IMPORT_STATUS] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[OLIGO_CARD_INVOICE] ADD CONSTRAINT [PK_OLIGO_CARD_INVOICE] PRIMARY KEY CLUSTERED  ([OLIGO_CARD_INVOICE_ID]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_OLIGO_CARD_INVOICE] ON [dbo].[OLIGO_CARD_INVOICE] ([INV_NBR]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_OLIGO_CARD_INVOICE_1] ON [dbo].[OLIGO_CARD_INVOICE] ([OLIGO_CARD_ID]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[OLIGO_CARD_INVOICE] WITH NOCHECK ADD CONSTRAINT [FK_OLIGO_CARD_INVOICE_OLIGO_CARD] FOREIGN KEY ([OLIGO_CARD_ID]) REFERENCES [dbo].[OLIGO_CARD] ([OLIGO_CARD_ID]) NOT FOR REPLICATION
GO
GRANT SELECT ON  [dbo].[OLIGO_CARD_INVOICE] TO [IDT_WebApps]
GRANT INSERT ON  [dbo].[OLIGO_CARD_INVOICE] TO [IDT_WebApps]
GRANT DELETE ON  [dbo].[OLIGO_CARD_INVOICE] TO [IDT_WebApps]
GRANT UPDATE ON  [dbo].[OLIGO_CARD_INVOICE] TO [IDT_WebApps]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Extension of Inv_Hdr table - for invoice information for an oligo card order (i.e. an order of an oligo card) to be imported into Epicor', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_INVOICE', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Amount of oligo card', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_INVOICE', 'COLUMN', N'CARD_AMOUNT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether or not it''s been imported into Epicor', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_INVOICE', 'COLUMN', N'IMPORT_STATUS'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Coliseum Invoice Number', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_INVOICE', 'COLUMN', N'INV_NBR'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Foreign key to OLIGO_CARD table', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_INVOICE', 'COLUMN', N'OLIGO_CARD_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary Key - get value from Next_Key_Val table.', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_INVOICE', 'COLUMN', N'OLIGO_CARD_INVOICE_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Product ID of oligo card - to puchase the Oligo Card or refill it.', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_INVOICE', 'COLUMN', N'PROD_ID'
GO
