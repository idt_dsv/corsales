CREATE TABLE [worklist].[WorkItemClaim]
(
[WorkItemId] [bigint] NOT NULL,
[ClaimedAt] [datetime] NOT NULL CONSTRAINT [DF__WorkItemC__Claim__4BE214AA] DEFAULT (getdate()),
[ClaimedBy] [nvarchar] (110) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [SALES_DATA]
GO
ALTER TABLE [worklist].[WorkItemClaim] ADD CONSTRAINT [PK_worklist.WorkItemClaim] PRIMARY KEY CLUSTERED  ([WorkItemId]) ON [SALES_DATA]
GO
ALTER TABLE [worklist].[WorkItemClaim] ADD CONSTRAINT [FK_worklist.WorkItemClaim_worklist.WorkItem_WorkItemId] FOREIGN KEY ([WorkItemId]) REFERENCES [worklist].[WorkItem] ([WorkItemId])
GO
