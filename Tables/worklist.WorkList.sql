CREATE TABLE [worklist].[WorkList]
(
[WorkListId] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [SALES_DATA]
GO
ALTER TABLE [worklist].[WorkList] ADD CONSTRAINT [PK_worklist.WorkList] PRIMARY KEY CLUSTERED  ([WorkListId]) ON [SALES_DATA]
GO
