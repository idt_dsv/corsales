CREATE TABLE [dbo].[PARTY_NOTE_TYPE]
(
[NOTE_TYPE_CD] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NOTE_TYPE_NM] [varbinary] (50) NOT NULL,
[NOTE_TYPE_DESC] [varbinary] (50) NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[PARTY_NOTE_TYPE] ADD CONSTRAINT [PK_PARTY_NOTE_TYPE] PRIMARY KEY NONCLUSTERED  ([NOTE_TYPE_CD]) ON [SALES_DATA]
GO
GRANT SELECT ON  [dbo].[PARTY_NOTE_TYPE] TO [IDT_DATA_CLEANUP]
GRANT INSERT ON  [dbo].[PARTY_NOTE_TYPE] TO [IDT_DATA_CLEANUP]
GRANT DELETE ON  [dbo].[PARTY_NOTE_TYPE] TO [IDT_DATA_CLEANUP]
GRANT UPDATE ON  [dbo].[PARTY_NOTE_TYPE] TO [IDT_DATA_CLEANUP]
GO
