CREATE TABLE [dbo].[ADDR_EXTENDED]
(
[ADDR_ID] [int] NOT NULL,
[PRIMARY_NUMBER] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRE_DIRECTIONAL] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STREET_NAME] [varchar] (28) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SUFFIX] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POST_DIRECTIONAL] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UNIT_DESIGNATOR] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UNIT_NUMBER] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LOT_NUMBER] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LACS] [int] NULL,
[DPC] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COUNTY_CODE] [int] NULL,
[COUNTY_NAME] [varchar] (28) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CASS_ADDR_TYPE] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VALIDATATION_SERVICE] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VALIDATATION_VERSION] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[ADDR_EXTENDED] ADD CONSTRAINT [PK_ADDR_Extended] PRIMARY KEY CLUSTERED  ([ADDR_ID]) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[ADDR_EXTENDED] WITH NOCHECK ADD CONSTRAINT [FK__ADDR_Exte__ADDR___72EB973C] FOREIGN KEY ([ADDR_ID]) REFERENCES [dbo].[ADDR] ([ADDR_ID]) NOT FOR REPLICATION
GO
GRANT SELECT ON  [dbo].[ADDR_EXTENDED] TO [IDT_DATA_CLEANUP]
GRANT INSERT ON  [dbo].[ADDR_EXTENDED] TO [IDT_DATA_CLEANUP]
GRANT DELETE ON  [dbo].[ADDR_EXTENDED] TO [IDT_DATA_CLEANUP]
GRANT UPDATE ON  [dbo].[ADDR_EXTENDED] TO [IDT_DATA_CLEANUP]
GO
EXEC sp_addextendedproperty N'MS_Description', N'S = Street record  P = Post Office box R = Rural Route or Highway Contract H = High-rise, Building, or Apartment F = Firm Record G = General Delivery M = Multi-Carrier Record', 'SCHEMA', N'dbo', 'TABLE', N'ADDR_EXTENDED', 'COLUMN', N'CASS_ADDR_TYPE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'County ID number', 'SCHEMA', N'dbo', 'TABLE', N'ADDR_EXTENDED', 'COLUMN', N'COUNTY_CODE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'County String Name', 'SCHEMA', N'dbo', 'TABLE', N'ADDR_EXTENDED', 'COLUMN', N'COUNTY_NAME'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Delivery Point Code + Check Digit', 'SCHEMA', N'dbo', 'TABLE', N'ADDR_EXTENDED', 'COLUMN', N'DPC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'911 location code of address', 'SCHEMA', N'dbo', 'TABLE', N'ADDR_EXTENDED', 'COLUMN', N'LACS'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Line of Travel on Carrier Route (Line of sight)', 'SCHEMA', N'dbo', 'TABLE', N'ADDR_EXTENDED', 'COLUMN', N'LOT_NUMBER'
GO
EXEC sp_addextendedproperty N'MS_Description', N'USPS Delivery Address Line - Field 5', 'SCHEMA', N'dbo', 'TABLE', N'ADDR_EXTENDED', 'COLUMN', N'POST_DIRECTIONAL'
GO
EXEC sp_addextendedproperty N'MS_Description', N'USPS Delivery Address Line - Field 2', 'SCHEMA', N'dbo', 'TABLE', N'ADDR_EXTENDED', 'COLUMN', N'PRE_DIRECTIONAL'
GO
EXEC sp_addextendedproperty N'MS_Description', N'USPS Delivery Address Line - Field 1', 'SCHEMA', N'dbo', 'TABLE', N'ADDR_EXTENDED', 'COLUMN', N'PRIMARY_NUMBER'
GO
EXEC sp_addextendedproperty N'MS_Description', N'USPS Delivery Address Line - Field 3', 'SCHEMA', N'dbo', 'TABLE', N'ADDR_EXTENDED', 'COLUMN', N'STREET_NAME'
GO
EXEC sp_addextendedproperty N'MS_Description', N'USPS Delivery Address Line - Field 4', 'SCHEMA', N'dbo', 'TABLE', N'ADDR_EXTENDED', 'COLUMN', N'SUFFIX'
GO
EXEC sp_addextendedproperty N'MS_Description', N'USPS Delivery Address Line - Field 6', 'SCHEMA', N'dbo', 'TABLE', N'ADDR_EXTENDED', 'COLUMN', N'UNIT_DESIGNATOR'
GO
EXEC sp_addextendedproperty N'MS_Description', N'USPS Delivery Address Line - Field 7', 'SCHEMA', N'dbo', 'TABLE', N'ADDR_EXTENDED', 'COLUMN', N'UNIT_NUMBER'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Product used for cleaning address', 'SCHEMA', N'dbo', 'TABLE', N'ADDR_EXTENDED', 'COLUMN', N'VALIDATATION_SERVICE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Version of Product used for cleaning address', 'SCHEMA', N'dbo', 'TABLE', N'ADDR_EXTENDED', 'COLUMN', N'VALIDATATION_VERSION'
GO
