CREATE TABLE [dbo].[CONFIRMATION]
(
[CON_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[ORD_ID] [int] NULL,
[TYPE_ID] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CON_DATE] [datetime] NULL CONSTRAINT [DF__CONFIRMAT__CON_D__208CD6FA] DEFAULT (getdate()),
[STATUS] [int] NULL CONSTRAINT [DF__CONFIRMAT__STATU__2180FB33] DEFAULT ((0)),
[RETRIES] [int] NULL CONSTRAINT [DF__CONFIRMAT__RETRI__22751F6C] DEFAULT ((0)),
[DEPT_ID] [int] NULL CONSTRAINT [DF__CONFIRMAT__DEPT___236943A5] DEFAULT ((0)),
[HOLD_ID] [int] NULL CONSTRAINT [DF__CONFIRMAT__HOLD___245D67DE] DEFAULT ((1)),
[DEST] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SUBJECT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MISC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ERR_MSG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USER_NM] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BODY] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SALES_ORD_NBR] [int] NULL,
[SENT_DTM] [datetime] NULL,
[OLIGO_CARD_ID] [int] NULL,
[NOTIFICATION_INFO_ID] [int] NULL,
[CLAIMED_DTM] [datetime] NULL,
[CLAIMED_WORKSTATION_ID] [int] NULL,
[ACTIVE_DTM] [datetime] NULL CONSTRAINT [DF__CONFIRMAT__ACT_D__208CD6FA] DEFAULT (getdate())
) ON [SALES_DATA] TEXTIMAGE_ON [SALES_DATA]
GO
ALTER TABLE [dbo].[CONFIRMATION] ADD CONSTRAINT [PK_CONFIRMATION] PRIMARY KEY NONCLUSTERED  ([CON_ID]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE CLUSTERED INDEX [IX_CONFIRMATION] ON [dbo].[CONFIRMATION] ([CON_DATE]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_CONFIRMATION_1] ON [dbo].[CONFIRMATION] ([ORD_ID]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_CONFIRMATION_2] ON [dbo].[CONFIRMATION] ([TYPE_ID], [HOLD_ID]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
GRANT SELECT ON  [dbo].[CONFIRMATION] TO [IDT_WebApps]
GRANT INSERT ON  [dbo].[CONFIRMATION] TO [IDT_WebApps]
GRANT UPDATE ON  [dbo].[CONFIRMATION] TO [IDT_WebApps]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Body of e-mail not for customer confirmation – inter-office only', 'SCHEMA', N'dbo', 'TABLE', N'CONFIRMATION', 'COLUMN', N'BODY'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DateTime entered', 'SCHEMA', N'dbo', 'TABLE', N'CONFIRMATION', 'COLUMN', N'CON_DATE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Not Used', 'SCHEMA', N'dbo', 'TABLE', N'CONFIRMATION', 'COLUMN', N'DEPT_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Fax number or E-mail address', 'SCHEMA', N'dbo', 'TABLE', N'CONFIRMATION', 'COLUMN', N'DEST'
GO
EXEC sp_addextendedproperty N'MS_Description', N'If e-mail or fax fails:  Recipient Not Found', 'SCHEMA', N'dbo', 'TABLE', N'CONFIRMATION', 'COLUMN', N'ERR_MSG'
GO
EXEC sp_addextendedproperty N'MS_Description', N'0 or 1 (1 for on-hold) updated when confirmation is type S and is being held in specialty before it can be produced', 'SCHEMA', N'dbo', 'TABLE', N'CONFIRMATION', 'COLUMN', N'HOLD_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Notes usually null', 'SCHEMA', N'dbo', 'TABLE', N'CONFIRMATION', 'COLUMN', N'MISC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Link to Oligo_Card table. It will only be folled when the Ologo Card confirmation is send to the customer via email with the pin number needed for the customer to use this card.', 'SCHEMA', N'dbo', 'TABLE', N'CONFIRMATION', 'COLUMN', N'OLIGO_CARD_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'link to ord.ord_id', 'SCHEMA', N'dbo', 'TABLE', N'CONFIRMATION', 'COLUMN', N'ORD_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'from 0 to 3 – if e-mail or fax does not go through the first time then it is retried', 'SCHEMA', N'dbo', 'TABLE', N'CONFIRMATION', 'COLUMN', N'RETRIES'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Link to Ord.Sales_Ord_Nbr', 'SCHEMA', N'dbo', 'TABLE', N'CONFIRMATION', 'COLUMN', N'SALES_ORD_NBR'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Actual Sent Date Time when the confirmation status is updated to 1', 'SCHEMA', N'dbo', 'TABLE', N'CONFIRMATION', 'COLUMN', N'SENT_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'0 or 1 (1 for sent) updated when actual confirmation goes out to customer or inter-office for P', 'SCHEMA', N'dbo', 'TABLE', N'CONFIRMATION', 'COLUMN', N'STATUS'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Used by website when auto-creating e-mails: either IDT New Account Confirmation or IDT- Login ID Request', 'SCHEMA', N'dbo', 'TABLE', N'CONFIRMATION', 'COLUMN', N'SUBJECT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'E:  Standard Email Confirmation
F:  Standard Fax Confirmation
S:  Specialty Oligo Information
G: General Message
P:  Purchase Order 
', 'SCHEMA', N'dbo', 'TABLE', N'CONFIRMATION', 'COLUMN', N'TYPE_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Person who created the order, po, or webmaster if auto-created', 'SCHEMA', N'dbo', 'TABLE', N'CONFIRMATION', 'COLUMN', N'USER_NM'
GO
