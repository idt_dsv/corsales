CREATE TABLE [dbo].[PARTY_CC_MBRSHP]
(
[PARTY_CC_MBRSHP_ID] [int] NOT NULL,
[CC_ID] [int] NOT NULL,
[PARTY_ID] [int] NOT NULL,
[IS_DEFAULT] [bit] NOT NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[PARTY_CC_MBRSHP] ADD CONSTRAINT [PK1] PRIMARY KEY CLUSTERED  ([PARTY_CC_MBRSHP_ID]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_PartyCCMbrshp_ccId] ON [dbo].[PARTY_CC_MBRSHP] ([CC_ID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PartyCcMbrshp_PartyId] ON [dbo].[PARTY_CC_MBRSHP] ([PARTY_ID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PARTY_CC_MBRSHP] WITH NOCHECK ADD CONSTRAINT [RefCC1] FOREIGN KEY ([CC_ID]) REFERENCES [dbo].[CC] ([CC_ID]) NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[PARTY_CC_MBRSHP] WITH NOCHECK ADD CONSTRAINT [RefPARTY2] FOREIGN KEY ([PARTY_ID]) REFERENCES [dbo].[PARTY] ([PARTY_ID]) NOT FOR REPLICATION
GO
GRANT SELECT ON  [dbo].[PARTY_CC_MBRSHP] TO [IDT_DATA_CLEANUP]
GRANT INSERT ON  [dbo].[PARTY_CC_MBRSHP] TO [IDT_DATA_CLEANUP]
GRANT DELETE ON  [dbo].[PARTY_CC_MBRSHP] TO [IDT_DATA_CLEANUP]
GRANT UPDATE ON  [dbo].[PARTY_CC_MBRSHP] TO [IDT_DATA_CLEANUP]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Stores the relationship between a party and the credit cards that the party used and/or was placed on file', 'SCHEMA', N'dbo', 'TABLE', N'PARTY_CC_MBRSHP', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'The foreign key to the credit card (cc table)', 'SCHEMA', N'dbo', 'TABLE', N'PARTY_CC_MBRSHP', 'COLUMN', N'CC_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Bit field indicating if this is the default entry for when a customer specifies to use the credit card on file', 'SCHEMA', N'dbo', 'TABLE', N'PARTY_CC_MBRSHP', 'COLUMN', N'IS_DEFAULT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'the Primary key from the next_key_val table', 'SCHEMA', N'dbo', 'TABLE', N'PARTY_CC_MBRSHP', 'COLUMN', N'PARTY_CC_MBRSHP_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The foreign key to the party table', 'SCHEMA', N'dbo', 'TABLE', N'PARTY_CC_MBRSHP', 'COLUMN', N'PARTY_ID'
GO
