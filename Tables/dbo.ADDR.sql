CREATE TABLE [dbo].[ADDR]
(
[ADDR_ID] [int] NOT NULL,
[CITY_ID] [int] NULL,
[CNTRY_ID] [int] NULL,
[ADDR1] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDR2] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDR3] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POST_CD] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CITY_NM] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATE_CD] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATE_NM] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CNTRY_NM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DIRECT] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSTAL_BARCODE] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PLUS4_ZIPDIGITS] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CARRIER_ROUTE] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VALIDATE_DT] [datetime] NULL,
[VALIDATE_STATUS] [int] NULL,
[GEOGRAPHIC_BOUNDARY_ID] [int] NULL,
[IDT_GUID] [uniqueidentifier] NULL ROWGUIDCOL CONSTRAINT [Guid_Default_Addr] DEFAULT (newid())
) ON [SALES_DATA] TEXTIMAGE_ON [SALES_DATA]
GO
ALTER TABLE [dbo].[ADDR] ADD CONSTRAINT [PK__ADDR__047AA831] PRIMARY KEY CLUSTERED  ([ADDR_ID]) ON [SALES_DATA]
GO
GRANT SELECT ON  [dbo].[ADDR] TO [IDT_DATA_CLEANUP]
GRANT INSERT ON  [dbo].[ADDR] TO [IDT_DATA_CLEANUP]
GRANT DELETE ON  [dbo].[ADDR] TO [IDT_DATA_CLEANUP]
GRANT UPDATE ON  [dbo].[ADDR] TO [IDT_DATA_CLEANUP]
GO
EXEC sp_addextendedproperty N'MS_Description', NULL, 'SCHEMA', N'dbo', 'TABLE', N'ADDR', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Carrier mail delivery route number', 'SCHEMA', N'dbo', 'TABLE', N'ADDR', 'COLUMN', N'CARRIER_ROUTE'
GO
EXEC sp_addextendedproperty N'MS_Description', NULL, 'SCHEMA', N'dbo', 'TABLE', N'ADDR', 'COLUMN', N'CITY_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Delivery Zone ID value from the geographic boundary list', 'SCHEMA', N'dbo', 'TABLE', N'ADDR', 'COLUMN', N'GEOGRAPHIC_BOUNDARY_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Extended Postal Code Digits', 'SCHEMA', N'dbo', 'TABLE', N'ADDR', 'COLUMN', N'PLUS4_ZIPDIGITS'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Postal Barcode', 'SCHEMA', N'dbo', 'TABLE', N'ADDR', 'COLUMN', N'POSTAL_BARCODE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date the Address was checked', 'SCHEMA', N'dbo', 'TABLE', N'ADDR', 'COLUMN', N'VALIDATE_DT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Null = No Attempt to CASS Format 0) = CASS Formatting Failed 1) = CASS Formatting Passed 2) = Manual override', 'SCHEMA', N'dbo', 'TABLE', N'ADDR', 'COLUMN', N'VALIDATE_STATUS'
GO
