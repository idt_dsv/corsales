CREATE TABLE [dbo].[ORD_COUPON]
(
[COUPON_ID] [int] NOT NULL,
[COUPON_NM] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[COUPON_PRE] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[COUPON_DESC] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [SALES_DATA] TEXTIMAGE_ON [SALES_DATA]
GO
ALTER TABLE [dbo].[ORD_COUPON] ADD CONSTRAINT [PK_ORD_COUPON] PRIMARY KEY NONCLUSTERED  ([COUPON_ID]) ON [SALES_DATA]
GO
GRANT SELECT ON  [dbo].[ORD_COUPON] TO [IDT_DATA_CLEANUP]
GRANT INSERT ON  [dbo].[ORD_COUPON] TO [IDT_DATA_CLEANUP]
GRANT DELETE ON  [dbo].[ORD_COUPON] TO [IDT_DATA_CLEANUP]
GRANT UPDATE ON  [dbo].[ORD_COUPON] TO [IDT_DATA_CLEANUP]
GO
