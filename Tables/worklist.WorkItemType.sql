CREATE TABLE [worklist].[WorkItemType]
(
[WorkItemTypeId] [int] NOT NULL,
[Name] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [SALES_DATA]
GO
ALTER TABLE [worklist].[WorkItemType] ADD CONSTRAINT [PK_worklist.WorkItemType] PRIMARY KEY CLUSTERED  ([WorkItemTypeId]) ON [SALES_DATA]
GO
