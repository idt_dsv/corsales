CREATE TABLE [worklist].[WorkListWorkSetLink]
(
[WorkListId] [int] NOT NULL,
[WorkSetId] [int] NOT NULL
) ON [SALES_DATA]
GO
ALTER TABLE [worklist].[WorkListWorkSetLink] ADD CONSTRAINT [PK_worklist.WorkListWorkSetLink] PRIMARY KEY CLUSTERED  ([WorkListId], [WorkSetId]) ON [SALES_DATA]
GO
ALTER TABLE [worklist].[WorkListWorkSetLink] ADD CONSTRAINT [FK_worklist.WorkListWorkSetLink_worklist.WorkList_WorkListId] FOREIGN KEY ([WorkListId]) REFERENCES [worklist].[WorkList] ([WorkListId])
GO
ALTER TABLE [worklist].[WorkListWorkSetLink] ADD CONSTRAINT [FK_worklist.WorkListWorkSetLink_worklist.WorkSet_WorkSetId] FOREIGN KEY ([WorkSetId]) REFERENCES [worklist].[WorkSet] ([WorkSetId])
GO
