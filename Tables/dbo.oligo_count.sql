CREATE TABLE [dbo].[oligo_count]
(
[Department] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Oligos] [int] NULL,
[Bases] [int] NULL,
[Ave_len] [int] NULL,
[Date_updated] [datetime] NULL
) ON [SALES_DATA]
GO
