CREATE TABLE [dbo].[ORD]
(
[ORD_ID] [int] NOT NULL,
[ORD_METH_ID] [int] NULL,
[ORG_ID] [int] NOT NULL,
[ACC_ID] [int] NULL,
[ENDUSER_ID] [int] NULL,
[SHIP_ADDR_ID] [int] NULL,
[BILL_ADDR_ID] [int] NULL,
[CURR_ID] [int] NULL CONSTRAINT [DF__ORD__CURR_ID__3587F3E0] DEFAULT ((1)),
[SALES_ORD_NBR] [int] NULL,
[CURR_STAT] [int] NOT NULL,
[PAY_METH_ID] [int] NULL,
[DROP_SHIP] [bit] NOT NULL CONSTRAINT [DF__ORD__DROP_SHIP__367C1819] DEFAULT ((0)),
[PART_SHIP] [bit] NOT NULL CONSTRAINT [DF__ORD__PART_SHIP__37703C52] DEFAULT ((1)),
[ORD_DATE] [datetime] NULL,
[ITEMS_ON_ORD] [int] NULL CONSTRAINT [DF__ORD__ITEMS_ON_OR__3864608B] DEFAULT ((0)),
[ORD_TOTAL] [money] NULL CONSTRAINT [DF__ORD__ORD_TOTAL__395884C4] DEFAULT ((0.00)),
[PAY_AUTH_NBR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CONTACT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FAX_EMAIL] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CONT_PH_NBR] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CREATE_DT] [datetime] NOT NULL CONSTRAINT [DF__ORD__CREATE_DT__3A4CA8FD] DEFAULT (getdate()),
[CREATE_BY] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__ORD__CREATE_BY__3B40CD36] DEFAULT (suser_sname()),
[LAST_EDIT_DT] [datetime] NULL,
[LAST_EDIT_BY] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INV_NBR] [int] NULL,
[TAX] [money] NULL,
[QUOTE_ID] [int] NULL,
[CURRENCY_ID] [int] NULL CONSTRAINT [DF__ORD__CURRENCY_ID__3C34F16F] DEFAULT ((1)),
[CURRENCY_EXCHANGE_RATE_ID] [int] NULL CONSTRAINT [DF__ORD__CURRENCY_EX__3D2915A8] DEFAULT ((1)),
[EXCHANGE_RATE] [float] NULL CONSTRAINT [DF__ORD__EXCHANGE_RA__3E1D39E1] DEFAULT ((1)),
[CC_ID] [int] NULL,
[BILL_ACCT_ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INV_ADDR_TYPE_ID] [int] NULL,
[OLIGO_CARD_ID] [int] NULL,
[INV_DELIVERY_METH_ID] [int] NULL,
[OLIGO_CARD_COUPON_ID] [int] NULL,
[RETAIL_CURRENCY_ID] [int] NULL
) ON [SALES_DATA]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[ORDTriggerUpdDel] ON [dbo].[ORD]
FOR UPDATE, DELETE 
NOT FOR REPLICATION
AS
BEGIN
  declare @OperationType char(3)
  if exists (Select * from inserted) 
    set @OperationType = 'Upd'
  else
    set @OperationType = 'Del'
  
  INSERT [dbo].[ORD_STATUS_AUDIT] ([ORD_ID], [CURR_STAT], New_Curr_stat, [OperationType])
  SELECT d1.[ORD_ID]
		, Old_Curr_stat = d1.[CURR_STAT]
		, New_Curr_stat = inserted.[CURR_STAT]
		, [OperationType] = @OperationType
  FROM deleted d1
  left join inserted on inserted.ord_id = d1.ord_id

END
GO
ALTER TABLE [dbo].[ORD] ADD CONSTRAINT [XPKORDER] PRIMARY KEY NONCLUSTERED  ([ORD_ID]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[ORD] ADD CONSTRAINT [IX_ORD_SALES_ORD_NBR] UNIQUE NONCLUSTERED  ([SALES_ORD_NBR]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_ORD_ACC_ID] ON [dbo].[ORD] ([ACC_ID]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ORD_2] ON [dbo].[ORD] ([ENDUSER_ID], [ORD_DATE]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [ix_Ord_enduserId] ON [dbo].[ORD] ([ENDUSER_ID], [SALES_ORD_NBR]) WITH (FILLFACTOR=80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ncix_ORD_ENDUSER_ID] ON [dbo].[ORD] ([ENDUSER_ID], [SALES_ORD_NBR]) INCLUDE ([ACC_ID], [CC_ID], [ORD_DATE], [ORD_ID]) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_ORD_INV_NBR] ON [dbo].[ORD] ([INV_NBR], [SALES_ORD_NBR]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [ix_ord_oligo_card_id] ON [dbo].[ORD] ([OLIGO_CARD_ID]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE CLUSTERED INDEX [IX_ORD_ORD_DATE] ON [dbo].[ORD] ([ORD_DATE]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_ORD_PARTY] ON [dbo].[ORD] ([ORG_ID], [ACC_ID], [ENDUSER_ID]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_ORD_PO] ON [dbo].[ORD] ([PAY_AUTH_NBR]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[ORD] WITH NOCHECK ADD CONSTRAINT [FK_ORD_CC] FOREIGN KEY ([CC_ID]) REFERENCES [dbo].[CC] ([CC_ID]) NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[ORD] WITH NOCHECK ADD CONSTRAINT [FK_ORD_INV_ADDR_TYPE] FOREIGN KEY ([INV_ADDR_TYPE_ID]) REFERENCES [dbo].[INV_ADDR_TYPE] ([INV_ADDR_TYPE_ID]) NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[ORD] WITH NOCHECK ADD CONSTRAINT [FK_ORD_OLIGO_CARD1] FOREIGN KEY ([OLIGO_CARD_COUPON_ID]) REFERENCES [dbo].[OLIGO_CARD] ([OLIGO_CARD_ID]) NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[ORD] WITH NOCHECK ADD CONSTRAINT [FK_ORD_OLIGO_CARD] FOREIGN KEY ([OLIGO_CARD_ID]) REFERENCES [dbo].[OLIGO_CARD] ([OLIGO_CARD_ID]) NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[ORD] NOCHECK CONSTRAINT [FK_ORD_INV_ADDR_TYPE]
GO
GRANT SELECT ON  [dbo].[ORD] TO [ARISTO_RL]
GRANT SELECT ON  [dbo].[ORD] TO [coliseum]
GRANT SELECT ON  [dbo].[ORD] TO [IDT_CORE_NOTES_APP]
GRANT SELECT ON  [dbo].[ORD] TO [IDT_DATA_CLEANUP]
GRANT INSERT ON  [dbo].[ORD] TO [IDT_DATA_CLEANUP]
GRANT DELETE ON  [dbo].[ORD] TO [IDT_DATA_CLEANUP]
GRANT UPDATE ON  [dbo].[ORD] TO [IDT_DATA_CLEANUP]
GO
EXEC sp_addextendedproperty N'MS_Description', N'customer order, header portion (ORD_LINE_ITEM holds line items, index via column ORD_ID)', 'SCHEMA', N'dbo', 'TABLE', N'ORD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'PARTY_ID of Customer''s Account', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'ACC_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Foreign key to epicor customer ID', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'BILL_ACCT_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'address to bill to (lookup into ADDR table)', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'BILL_ADDR_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Credit card id, linked to CC table', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'CC_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Contact phone number', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'CONT_PH_NBR'
GO
EXEC sp_addextendedproperty N'MS_Description', N'? {possibly obsolete?}', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'CONTACT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Who created in Coliseum', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'CREATE_BY'
GO
EXEC sp_addextendedproperty N'MS_Description', N'When created in Coliseum', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'CREATE_DT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'This field is not used - currency type (lookup into CURRENCIES table)', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'CURR_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Order Status -  (lookup into ORD_STAT_TYPE table)', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'CURR_STAT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The rate of exchange lookup', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'CURRENCY_EXCHANGE_RATE_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Currency payment is expected', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'CURRENCY_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Indicate if order can be shipped to multiple addresses', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'DROP_SHIP'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PARTY_ID of Customer', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'ENDUSER_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The rate of exchange', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'EXCHANGE_RATE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Send notification via email', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'FAX_EMAIL'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Foreign key to INV_ADDR_TYPE', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'INV_ADDR_TYPE_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Field for which delivery method to use for invoice of the order
Example: same as above
', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'INV_DELIVERY_METH_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'invoice number (? is this put in by Accounting)', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'INV_NBR'
GO
EXEC sp_addextendedproperty N'MS_Description', N'number of line items on order', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'ITEMS_ON_ORD'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Who last edited', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'LAST_EDIT_BY'
GO
EXEC sp_addextendedproperty N'MS_Description', N'When last edited', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'LAST_EDIT_DT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Key to oligo_card for coupons', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'OLIGO_CARD_COUPON_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Oligo Card ID that was used for order.', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'OLIGO_CARD_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'date&time order input into Coliseum', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'ORD_DATE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'unique key (internal use - does not appear at user level {I think})', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'ORD_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'"Order Type" - how order was received (web, email, etc); lookup ID into table ORD_METH', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'ORD_METH_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Total $ amount per order', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'ORD_TOTAL'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PARTY_ID of Customer''s Organization', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'ORG_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Indicates that order can be shipped as partial', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'PART_SHIP'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Payment method (e.g. PO#, credit card #)', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'PAY_AUTH_NBR'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Payment Method -  (lookup into ORD_PAY_METH table)', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'PAY_METH_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'ID of the currency used in the retail transaction for this order.  Relates to the currency table in Reference.', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'RETAIL_CURRENCY_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Coliseum "Sales Ord #"', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'SALES_ORD_NBR'
GO
EXEC sp_addextendedproperty N'MS_Description', N'address to ship to (lookup into ADDR table)', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'SHIP_ADDR_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'sales tax (if any)', 'SCHEMA', N'dbo', 'TABLE', N'ORD', 'COLUMN', N'TAX'
GO
