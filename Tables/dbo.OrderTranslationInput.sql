CREATE TABLE [dbo].[OrderTranslationInput]
(
[OrderTranslationInputId] [int] NOT NULL,
[UserId] [int] NOT NULL,
[ProcessDtm] [datetime] NOT NULL,
[FileType] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InputFileName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InputFileText] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [SALES_DATA] TEXTIMAGE_ON [SALES_DATA]
GO
ALTER TABLE [dbo].[OrderTranslationInput] ADD CONSTRAINT [PK_OrderTranslationInput] PRIMARY KEY CLUSTERED  ([OrderTranslationInputId]) ON [SALES_DATA]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Describes the input file used by the Custom Order Translator', 'SCHEMA', N'dbo', 'TABLE', N'OrderTranslationInput', NULL, NULL
GO
