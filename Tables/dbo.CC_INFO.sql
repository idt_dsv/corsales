CREATE TABLE [dbo].[CC_INFO]
(
[CC_INFO_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[BATCH_ORD_ID] [int] NOT NULL,
[CC_ACCOUNT_NAME] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CC_NBR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CC_EXPIRATION_DT] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[CC_INFO] ADD CONSTRAINT [PK_CC_INFO] PRIMARY KEY CLUSTERED  ([CC_INFO_ID]) ON [SALES_DATA]
GO
GRANT SELECT ON  [dbo].[CC_INFO] TO [IDT_WebApps]
GRANT INSERT ON  [dbo].[CC_INFO] TO [IDT_WebApps]
GRANT UPDATE ON  [dbo].[CC_INFO] TO [IDT_WebApps]
GRANT SELECT ON  [dbo].[CC_INFO] TO [IDT-CORALVILLE\IUSR_IDT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Credit Card Information', 'SCHEMA', N'dbo', 'TABLE', N'CC_INFO', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to INV_HDR table', 'SCHEMA', N'dbo', 'TABLE', N'CC_INFO', 'COLUMN', N'BATCH_ORD_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Optionally, the name that appears on the credit card ', 'SCHEMA', N'dbo', 'TABLE', N'CC_INFO', 'COLUMN', N'CC_ACCOUNT_NAME'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The credit card expiration date', 'SCHEMA', N'dbo', 'TABLE', N'CC_INFO', 'COLUMN', N'CC_EXPIRATION_DT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary Key (Dumb Key) Auto increment', 'SCHEMA', N'dbo', 'TABLE', N'CC_INFO', 'COLUMN', N'CC_INFO_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The credit card number', 'SCHEMA', N'dbo', 'TABLE', N'CC_INFO', 'COLUMN', N'CC_NBR'
GO
