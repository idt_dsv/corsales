CREATE TABLE [dbo].[CUST_CALL]
(
[CALL_ID] [int] NOT NULL,
[CALL_TYPE_ID] [int] NULL,
[CUST_CALL_CAT_ID] [int] NULL,
[SEVERITY_ID] [int] NULL,
[ENDUSER_ID] [int] NULL,
[ACCT_ID] [int] NULL,
[ORG_ID] [int] NULL,
[CUST_ATT_ID] [int] NULL,
[CUST_STATUS_ID] [int] NULL,
[DESC_TYPE_ID] [int] NULL,
[CALL_DT] [datetime] NULL,
[RESOL_DT] [datetime] NULL,
[CALL_SUMM] [varchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CALL_DESC] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RESOL_DESC] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USER_NAME] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [SALES_DATA] TEXTIMAGE_ON [SALES_DATA]
GO
ALTER TABLE [dbo].[CUST_CALL] ADD CONSTRAINT [PK_COMMENTS] PRIMARY KEY NONCLUSTERED  ([CALL_ID]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_CUST_CALL_3] ON [dbo].[CUST_CALL] ([CALL_DT]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [ix_CustCall_EnduserId] ON [dbo].[CUST_CALL] ([ENDUSER_ID]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_CUST_CALL_4] ON [dbo].[CUST_CALL] ([USER_NAME]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[CUST_CALL] WITH NOCHECK ADD CONSTRAINT [FK_CUST_CALL_CUST_CALL_TYPE] FOREIGN KEY ([CALL_TYPE_ID]) REFERENCES [dbo].[CUST_CALL_TYPE] ([CALL_TYPE_ID]) NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[CUST_CALL] WITH NOCHECK ADD CONSTRAINT [FK_CUST_CALL_CUST_ATTITUDE] FOREIGN KEY ([CUST_ATT_ID]) REFERENCES [dbo].[CUST_ATTITUDE] ([CUST_ATT_ID]) NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[CUST_CALL] WITH NOCHECK ADD CONSTRAINT [FK_CUST_CALL_CUST_CALL_CAT] FOREIGN KEY ([CUST_CALL_CAT_ID]) REFERENCES [dbo].[CUST_CALL_CAT] ([CUST_CALL_CAT_ID]) NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[CUST_CALL] WITH NOCHECK ADD CONSTRAINT [FK_CUST_CALL_CUST_STATUS] FOREIGN KEY ([CUST_STATUS_ID]) REFERENCES [dbo].[CUST_STATUS] ([CUST_STATUS_ID]) NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[CUST_CALL] WITH NOCHECK ADD CONSTRAINT [FK_CUST_CALL_CUST_CALL_DESC_TYPE] FOREIGN KEY ([DESC_TYPE_ID]) REFERENCES [dbo].[CUST_CALL_DESC_TYPE] ([DESC_TYPE_ID]) NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[CUST_CALL] WITH NOCHECK ADD CONSTRAINT [FK_CUST_CALL_PARTY] FOREIGN KEY ([ENDUSER_ID]) REFERENCES [dbo].[PARTY] ([PARTY_ID]) NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[CUST_CALL] WITH NOCHECK ADD CONSTRAINT [FK_CUST_CALL_CUST_CALL_SEV] FOREIGN KEY ([SEVERITY_ID]) REFERENCES [dbo].[CUST_CALL_SEV] ([SEVERITY_ID]) NOT FOR REPLICATION
GO
GRANT SELECT ON  [dbo].[CUST_CALL] TO [IDT_INTRANET_USER]
GO
