CREATE TABLE [dbo].[OLIGO_CARD]
(
[OLIGO_CARD_ID] [int] NOT NULL,
[OLIGO_CARD_NBR] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PIN_NBR] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BILL_ACCT_ID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ISSUE_DT] [datetime] NULL,
[ACTIVATE_DT] [datetime] NULL,
[ACTIVE] [bit] NULL,
[OLIGO_CARD_TYPE_ID] [int] NOT NULL CONSTRAINT [DF__OLIGO_CAR__OLIGO__498EEC8D] DEFAULT ((1)),
[CURRENCY_ID] [int] NULL CONSTRAINT [DF__OLIGO_CAR__CURRE__4A8310C6] DEFAULT ((1))
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[OLIGO_CARD] ADD CONSTRAINT [PK_OLIGO_CARD] PRIMARY KEY CLUSTERED  ([OLIGO_CARD_ID]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE UNIQUE NONCLUSTERED INDEX [ix_OLIGO_CARD_1] ON [dbo].[OLIGO_CARD] ([OLIGO_CARD_NBR]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[OLIGO_CARD] WITH NOCHECK ADD CONSTRAINT [FK_OLIGO_CARD_OLIGO_CARD_TYPE] FOREIGN KEY ([OLIGO_CARD_TYPE_ID]) REFERENCES [dbo].[OLIGO_CARD_TYPE] ([OLIGO_CARD_TYPE_ID]) NOT FOR REPLICATION
GO
GRANT INSERT ON  [dbo].[OLIGO_CARD] TO [IDT_ERP_APPS]
GRANT UPDATE ON  [dbo].[OLIGO_CARD] TO [IDT_ERP_APPS]
GRANT SELECT ON  [dbo].[OLIGO_CARD] TO [IDT_WebApps]
GRANT INSERT ON  [dbo].[OLIGO_CARD] TO [IDT_WebApps]
GRANT DELETE ON  [dbo].[OLIGO_CARD] TO [IDT_WebApps]
GRANT UPDATE ON  [dbo].[OLIGO_CARD] TO [IDT_WebApps]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Table that holds info for each OligoCard that is issued.', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date card was activated', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD', 'COLUMN', N'ACTIVATE_DT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether or not card is active', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD', 'COLUMN', N'ACTIVE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Epicor customer ID the card was ordered under', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD', 'COLUMN', N'BILL_ACCT_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Currency ID that accepted as payment', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD', 'COLUMN', N'CURRENCY_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'date card was issued', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD', 'COLUMN', N'ISSUE_DT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary Key', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD', 'COLUMN', N'OLIGO_CARD_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number printed on front of card', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD', 'COLUMN', N'OLIGO_CARD_NBR'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The type of card - FK-OLIGO_CARD_TYPE', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD', 'COLUMN', N'OLIGO_CARD_TYPE_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'4 digit pin associated with card', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD', 'COLUMN', N'PIN_NBR'
GO
