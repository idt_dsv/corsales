CREATE TABLE [dbo].[CUST_CALL_ACCESS_RIGHTS]
(
[NETWORK_LOGIN_NM] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SALES_REGION_ID] [int] NOT NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[CUST_CALL_ACCESS_RIGHTS] ADD CONSTRAINT [PK_CUST_SUPPORT_SALES_TERR] PRIMARY KEY CLUSTERED  ([NETWORK_LOGIN_NM]) ON [SALES_DATA]
GO
GRANT SELECT ON  [dbo].[CUST_CALL_ACCESS_RIGHTS] TO [IDT_INTRANET_USER]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Allows customer service representatives access to calls from distinct sales regions.', 'SCHEMA', N'dbo', 'TABLE', N'CUST_CALL_ACCESS_RIGHTS', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'login of IDT Employee', 'SCHEMA', N'dbo', 'TABLE', N'CUST_CALL_ACCESS_RIGHTS', 'COLUMN', N'NETWORK_LOGIN_NM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Determines access to sales territory. 99 has all access', 'SCHEMA', N'dbo', 'TABLE', N'CUST_CALL_ACCESS_RIGHTS', 'COLUMN', N'SALES_REGION_ID'
GO
