CREATE TABLE [dbo].[DUPLEX_OLIGO_SPEC]
(
[REF_ID] [int] NOT NULL,
[SENSE_REF_ID] [int] NOT NULL,
[ANTISENSE_REF_ID] [int] NOT NULL,
[COMPLEMENT_PCT] [float] NOT NULL,
[EXT_COEFF] [float] NULL,
[IS_COMPLEMENT] [bit] NOT NULL,
[MIN_SHIP_CONCENTRATION] [float] NOT NULL,
[PURIFICATION_INFO_ID] [int] NULL,
[SPEC_NAME] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[DUPLEX_OLIGO_SPEC] ADD CONSTRAINT [PK_DUPLEX_OLIGO_SPEC] PRIMARY KEY CLUSTERED  ([REF_ID]) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_DUPLEX_OLIGO_SPEC_antisense_ref_id] ON [dbo].[DUPLEX_OLIGO_SPEC] ([ANTISENSE_REF_ID]) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_DUPLEX_OLIGO_SPEC_sense_ref_id] ON [dbo].[DUPLEX_OLIGO_SPEC] ([SENSE_REF_ID]) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[DUPLEX_OLIGO_SPEC] WITH NOCHECK ADD CONSTRAINT [FK_DUPLEX_OLIGO_SPEC_LINE_ITEM_REF_NBR2] FOREIGN KEY ([ANTISENSE_REF_ID]) REFERENCES [dbo].[LINE_ITEM_REF_NBR] ([REF_ID]) NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[DUPLEX_OLIGO_SPEC] WITH NOCHECK ADD CONSTRAINT [FK_DUPLEX_OLIGO_SPEC_LINE_ITEM_REF_NBR] FOREIGN KEY ([REF_ID]) REFERENCES [dbo].[LINE_ITEM_REF_NBR] ([REF_ID]) NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[DUPLEX_OLIGO_SPEC] WITH NOCHECK ADD CONSTRAINT [FK_DUPLEX_OLIGO_SPEC_LINE_ITEM_REF_NBR1] FOREIGN KEY ([SENSE_REF_ID]) REFERENCES [dbo].[LINE_ITEM_REF_NBR] ([REF_ID]) NOT FOR REPLICATION
GO
EXEC sp_addextendedproperty N'MS_Description', N'his is an extension table to LINE_ITEM_REF_NBR and stores the specification for making a "duplexed" or "annealed" oligo. The duplex has links to the sense and anti-sense ref_ids that make up the duplexed oligo.', 'SCHEMA', N'dbo', 'TABLE', N'DUPLEX_OLIGO_SPEC', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK link to LINE_ITEM_REF_NBR.REF_ID. linked line must be an oligo spec', 'SCHEMA', N'dbo', 'TABLE', N'DUPLEX_OLIGO_SPEC', 'COLUMN', N'ANTISENSE_REF_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'0-100. length of longest complementary base-pair run / length of shorter oligo', 'SCHEMA', N'dbo', 'TABLE', N'DUPLEX_OLIGO_SPEC', 'COLUMN', N'COMPLEMENT_PCT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Don''t yet have a formula', 'SCHEMA', N'dbo', 'TABLE', N'DUPLEX_OLIGO_SPEC', 'COLUMN', N'EXT_COEFF'
GO
EXEC sp_addextendedproperty N'MS_Description', N'whether COMPLEMENT_PCNT = 100', 'SCHEMA', N'dbo', 'TABLE', N'DUPLEX_OLIGO_SPEC', 'COLUMN', N'IS_COMPLEMENT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'concentration below which oligo de-anneals at room temps', 'SCHEMA', N'dbo', 'TABLE', N'DUPLEX_OLIGO_SPEC', 'COLUMN', N'MIN_SHIP_CONCENTRATION'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK Link to PURIFICATION_INFO table', 'SCHEMA', N'dbo', 'TABLE', N'DUPLEX_OLIGO_SPEC', 'COLUMN', N'PURIFICATION_INFO_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key. This is an extension table to LINE_ITEM_REF_NBR', 'SCHEMA', N'dbo', 'TABLE', N'DUPLEX_OLIGO_SPEC', 'COLUMN', N'REF_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK link to LINE_ITEM_REF_NBR.REF_ID. linked line must be an oligo spec', 'SCHEMA', N'dbo', 'TABLE', N'DUPLEX_OLIGO_SPEC', 'COLUMN', N'SENSE_REF_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The name the customer assigned to the Duplex Oligo', 'SCHEMA', N'dbo', 'TABLE', N'DUPLEX_OLIGO_SPEC', 'COLUMN', N'SPEC_NAME'
GO
