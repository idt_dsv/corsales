CREATE TABLE [worklist].[WorkSet]
(
[WorkSetId] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [SALES_DATA]
GO
ALTER TABLE [worklist].[WorkSet] ADD CONSTRAINT [PK_worklist.WorkSet] PRIMARY KEY CLUSTERED  ([WorkSetId]) ON [SALES_DATA]
GO
