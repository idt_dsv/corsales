CREATE TABLE [dbo].[DO_NOT_CALL_REASON]
(
[DO_NOT_CALL_REASON_ID] [int] NOT NULL,
[REASON] [varchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[DO_NOT_CALL_REASON] ADD CONSTRAINT [PK_DO_NOT_CALL_REASON] PRIMARY KEY CLUSTERED  ([DO_NOT_CALL_REASON_ID]) ON [SALES_DATA]
GO
EXEC sp_addextendedproperty N'MS_Description', N'IDT Support Do Not Call Reasons.  Contains generic reasons not to call customers.', 'SCHEMA', N'dbo', 'TABLE', N'DO_NOT_CALL_REASON', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key', 'SCHEMA', N'dbo', 'TABLE', N'DO_NOT_CALL_REASON', 'COLUMN', N'DO_NOT_CALL_REASON_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Reason (often an item to ignore from Document ORE-003 - Oligos that go to Technical Support)', 'SCHEMA', N'dbo', 'TABLE', N'DO_NOT_CALL_REASON', 'COLUMN', N'REASON'
GO
