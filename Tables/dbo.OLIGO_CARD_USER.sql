CREATE TABLE [dbo].[OLIGO_CARD_USER]
(
[OLIGO_CARD_USER_ID] [int] NOT NULL,
[OLIGO_CARD_ID] [int] NOT NULL,
[WEB_ACCT_NBR] [int] NULL,
[ENDUSER_ID] [int] NULL,
[FROM_DT] [datetime] NULL,
[THRU_DT] [datetime] NULL,
[IS_OLIGO_CARD_OWNER] [bit] NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[OLIGO_CARD_USER] ADD CONSTRAINT [PK_OLIGO_CARD_USER] PRIMARY KEY CLUSTERED  ([OLIGO_CARD_USER_ID]) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[OLIGO_CARD_USER] WITH NOCHECK ADD CONSTRAINT [FK_OLIGO_CARD_USER_OLIGO_CARD] FOREIGN KEY ([OLIGO_CARD_ID]) REFERENCES [dbo].[OLIGO_CARD] ([OLIGO_CARD_ID]) NOT FOR REPLICATION
GO
GRANT SELECT ON  [dbo].[OLIGO_CARD_USER] TO [IDT_WebApps]
GRANT INSERT ON  [dbo].[OLIGO_CARD_USER] TO [IDT_WebApps]
GRANT DELETE ON  [dbo].[OLIGO_CARD_USER] TO [IDT_WebApps]
GRANT UPDATE ON  [dbo].[OLIGO_CARD_USER] TO [IDT_WebApps]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Table for list of people using a given Oligo Card
', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_USER', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Enduser ID of user', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_USER', 'COLUMN', N'ENDUSER_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date person was added to card''s user list', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_USER', 'COLUMN', N'FROM_DT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Whether or not the user is the original purchaser, or owner, of card', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_USER', 'COLUMN', N'IS_OLIGO_CARD_OWNER'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Foreign key to OLIGO_CARD table', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_USER', 'COLUMN', N'OLIGO_CARD_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary Key', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_USER', 'COLUMN', N'OLIGO_CARD_USER_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'inactivate date of that person for use of that card', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_USER', 'COLUMN', N'THRU_DT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Web acct nbr of user', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_USER', 'COLUMN', N'WEB_ACCT_NBR'
GO
