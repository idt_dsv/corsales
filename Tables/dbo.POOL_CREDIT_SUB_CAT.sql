CREATE TABLE [dbo].[POOL_CREDIT_SUB_CAT]
(
[POOL_CREDIT_SUB_CAT_ID] [int] NOT NULL,
[NAME] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DESCRIPTION] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[POOL_CREDIT_SUB_CAT] ADD CONSTRAINT [PK_POOL_CREDIT_SUB_CAT] PRIMARY KEY CLUSTERED  ([POOL_CREDIT_SUB_CAT_ID]) ON [SALES_DATA]
GO
EXEC sp_addextendedproperty N'MS_Description', N'List of subcategories for pool credits', 'SCHEMA', N'dbo', 'TABLE', N'POOL_CREDIT_SUB_CAT', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Description of subcategory', 'SCHEMA', N'dbo', 'TABLE', N'POOL_CREDIT_SUB_CAT', 'COLUMN', N'DESCRIPTION'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Name of subcategory', 'SCHEMA', N'dbo', 'TABLE', N'POOL_CREDIT_SUB_CAT', 'COLUMN', N'NAME'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PK', 'SCHEMA', N'dbo', 'TABLE', N'POOL_CREDIT_SUB_CAT', 'COLUMN', N'POOL_CREDIT_SUB_CAT_ID'
GO
