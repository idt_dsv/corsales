CREATE TABLE [dbo].[CC_TYPE]
(
[CC_TYPE_ID] [int] NOT NULL,
[NAME] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ACTIVE] [bit] NOT NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[CC_TYPE] ADD CONSTRAINT [PK4] PRIMARY KEY CLUSTERED  ([CC_TYPE_ID]) ON [SALES_DATA]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Stores the credit card types that IDT accepts.', 'SCHEMA', N'dbo', 'TABLE', N'CC_TYPE', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'A bit field indicating whether it is currently accepted', 'SCHEMA', N'dbo', 'TABLE', N'CC_TYPE', 'COLUMN', N'ACTIVE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The Primary Key. Loaded from the Next_Key_Val table', 'SCHEMA', N'dbo', 'TABLE', N'CC_TYPE', 'COLUMN', N'CC_TYPE_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The credit card name, like AMEX', 'SCHEMA', N'dbo', 'TABLE', N'CC_TYPE', 'COLUMN', N'NAME'
GO
