CREATE TABLE [admin].[IndexUsage]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[ObjectName] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndexName] [sys].[sysname] NULL,
[USER_SEEKS] [bigint] NOT NULL,
[USER_SCANS] [bigint] NOT NULL,
[USER_LOOKUPS] [bigint] NOT NULL,
[USER_UPDATES] [bigint] NOT NULL,
[IndexColumns] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IncludedFields] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is_primary_key] [bit] NULL,
[IS_Clustered] [int] NULL,
[Is_unique] [int] NULL,
[Index_create_date] [datetime] NULL,
[index_id] [int] NOT NULL,
[data_space_id] [int] NOT NULL,
[ignore_dup_key] [bit] NULL,
[fill_factor] [tinyint] NOT NULL,
[is_padded] [bit] NULL,
[is_disabled] [bit] NULL,
[is_hypothetical] [bit] NULL,
[allow_row_locks] [bit] NULL,
[allow_page_locks] [bit] NULL,
[CreateDate] [datetime] NULL CONSTRAINT [DF__IndexUsag__Creat__70BE939E] DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [admin].[IndexUsage] ADD CONSTRAINT [PK__IndexUsage__739B0049] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ncix_indexUsage_ObjectName_Index_Name] ON [admin].[IndexUsage] ([CreateDate], [ObjectName], [IndexName]) ON [PRIMARY]
GO
