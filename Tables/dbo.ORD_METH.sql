CREATE TABLE [dbo].[ORD_METH]
(
[ORD_METH_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[ORD_METH_CD] [char] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ORD_METH_DESC] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OE_ORDER] [int] NULL
) ON [SALES_DATA] TEXTIMAGE_ON [SALES_DATA]
GO
ALTER TABLE [dbo].[ORD_METH] ADD CONSTRAINT [XPKORDERTYPE] PRIMARY KEY CLUSTERED  ([ORD_METH_ID]) ON [SALES_DATA]
GO
GRANT SELECT ON  [dbo].[ORD_METH] TO [IDT_DATA_CLEANUP]
GRANT INSERT ON  [dbo].[ORD_METH] TO [IDT_DATA_CLEANUP]
GRANT DELETE ON  [dbo].[ORD_METH] TO [IDT_DATA_CLEANUP]
GRANT UPDATE ON  [dbo].[ORD_METH] TO [IDT_DATA_CLEANUP]
GO
