CREATE TABLE [dbo].[QUEUE_ITEM_NOTIFICATION]
(
[QUEUE_ITEM_ID] [int] NOT NULL,
[ENTERED_DTM] [datetime] NULL,
[ACTIVE_DTM] [datetime] NULL,
[BF_BOBJECT_CID] [int] NULL,
[BF_BOBJECT_OID] [int] NULL,
[QUEUE_DEFINITION_ID] [int] NULL,
[CLAIMED_WORKSTATION_ID] [int] NULL,
[CLAIMED_DTM] [datetime] NULL,
[COMPLETED_DTM] [datetime] NULL,
[PRIORITY] [int] NULL CONSTRAINT [QUEUE_ITEM_NOTIFICATION_Priority_Default] DEFAULT ((1)),
[ACTIVE] [int] NULL,
[APPLICATION_ID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[QUEUE_ITEM_NOTIFICATION] ADD CONSTRAINT [PK_QUEUE_ITEM_NOTIFICATION] PRIMARY KEY CLUSTERED  ([QUEUE_ITEM_ID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[QUEUE_ITEM_NOTIFICATION] TO [IDT-CORALVILLE\IUSR_IDT]
GRANT INSERT ON  [dbo].[QUEUE_ITEM_NOTIFICATION] TO [IDT-CORALVILLE\IUSR_IDT]
GRANT DELETE ON  [dbo].[QUEUE_ITEM_NOTIFICATION] TO [IDT-CORALVILLE\IUSR_IDT]
GRANT UPDATE ON  [dbo].[QUEUE_ITEM_NOTIFICATION] TO [IDT-CORALVILLE\IUSR_IDT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Holds items that are ready for notification.', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_NOTIFICATION', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'flag indicating if this is an active item or not.', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_NOTIFICATION', 'COLUMN', N'ACTIVE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date/time that the queue entry became/will become due for processing ', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_NOTIFICATION', 'COLUMN', N'ACTIVE_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'the ID  of the process that has claimed the queue item  ', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_NOTIFICATION', 'COLUMN', N'APPLICATION_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to BF_BOBJECT.BF_BOBJECT_ID to class Id of the object being queued - named like this for consistancy with event structures', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_NOTIFICATION', 'COLUMN', N'BF_BOBJECT_CID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'the instance id of the object being queued ', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_NOTIFICATION', 'COLUMN', N'BF_BOBJECT_OID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'when this item was last claimed', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_NOTIFICATION', 'COLUMN', N'CLAIMED_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to WORKSTATION.WORKSTATION_ID - the workstation that has claimed this queue item', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_NOTIFICATION', 'COLUMN', N'CLAIMED_WORKSTATION_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'when the item was marked as completed. This also serves as an indicator that the item is complete.', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_NOTIFICATION', 'COLUMN', N'COMPLETED_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date/time that the queue entry was created', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_NOTIFICATION', 'COLUMN', N'ENTERED_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'integer value indicating the priority of the item.', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_NOTIFICATION', 'COLUMN', N'PRIORITY'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to QUEUE_DEFINITION the queue event the object is waiting for', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_NOTIFICATION', 'COLUMN', N'QUEUE_DEFINITION_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary Key', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_NOTIFICATION', 'COLUMN', N'QUEUE_ITEM_ID'
GO
