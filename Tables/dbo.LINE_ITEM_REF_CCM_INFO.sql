CREATE TABLE [dbo].[LINE_ITEM_REF_CCM_INFO]
(
[REF_ID] [int] NOT NULL,
[PRODUCT_SPECIFICATION_VERSION_ID] [int] NOT NULL,
[SEQUENCE_SPECIFICATION_ID] [int] NOT NULL
) ON [SALES_DATA]
GO
EXEC sp_addextendedproperty N'MS_DESCRIPTION', N'Table to tie any given Ref ID to a CCM product and sequence.', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_REF_CCM_INFO', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Product Version ID from the CCM table, PRODUCT_SPECIFICATION_VERSION', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_REF_CCM_INFO', 'COLUMN', N'PRODUCT_SPECIFICATION_VERSION_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Ref ID. Primary Key.  What more can I say.', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_REF_CCM_INFO', 'COLUMN', N'REF_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Sequence specification ID from the CCM table, sequence_specification', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_REF_CCM_INFO', 'COLUMN', N'SEQUENCE_SPECIFICATION_ID'
GO
