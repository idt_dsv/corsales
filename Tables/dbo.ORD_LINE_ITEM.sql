CREATE TABLE [dbo].[ORD_LINE_ITEM]
(
[LINE_ITEM_ID] [int] NOT NULL,
[ORD_ID] [int] NOT NULL,
[QUOTE_ID] [int] NULL,
[LINE_ITEM_SUB_TYPE] [int] NOT NULL CONSTRAINT [DF__ORD_LINE___LINE___55F4C372] DEFAULT ((1)),
[QUANTITY] [int] NOT NULL,
[UNIT_PRICE] [money] NOT NULL,
[ON_INVOICE] [bit] NOT NULL CONSTRAINT [DF__ORD_LINE___ON_IN__56E8E7AB] DEFAULT ((1)),
[ON_PACK_LIST] [bit] NOT NULL CONSTRAINT [DF__ORD_LINE___ON_PA__57DD0BE4] DEFAULT ((1)),
[IS_SHIPPED] [bit] NOT NULL CONSTRAINT [DF__ORD_LINE___IS_SH__58D1301D] DEFAULT ((0)),
[EST_SHIP_DT] [datetime] NULL,
[LEV] [int] NOT NULL CONSTRAINT [DF__ORD_LINE_IT__LEV__59C55456] DEFAULT ((1)),
[POS] [int] NULL,
[PAR_LINE_ITEM] [int] NULL,
[LINE_ITEM_DESC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GUAR_SHIP_DT] [datetime] NULL,
[CALC_PRICE] [money] NULL,
[OVER_PRICE] [money] NULL,
[PRICE_TYPE] [int] NULL CONSTRAINT [DF__ORD_LINE___PRICE__5AB9788F] DEFAULT ((0)),
[OLI_PROD_ID] [int] NULL,
[RETAIL_PRICE] [money] NULL,
[UNIT_PRICE_NATURAL] [money] NULL,
[RETAIL_PRICE_NATURAL] [money] NULL,
[RETAIL_PRICE_TYPE] [int] NULL,
[DiscountAmount] [money] NULL,
[DiscountExplanation] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[ORD_LINE_ITEM] ADD CONSTRAINT [XPKORDERLINEITEM] PRIMARY KEY CLUSTERED  ([LINE_ITEM_ID]) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_ORD_LINE_ITEM_1] ON [dbo].[ORD_LINE_ITEM] ([OLI_PROD_ID]) ON [SALES_IND]
GO
CREATE NONCLUSTERED INDEX [ix_OrdLineItem_OrdId] ON [dbo].[ORD_LINE_ITEM] ([ORD_ID]) ON [SALES_IND]
GO
CREATE NONCLUSTERED INDEX [IDX_Par_Line_Item] ON [dbo].[ORD_LINE_ITEM] ([PAR_LINE_ITEM]) ON [SALES_IND]
GO
CREATE NONCLUSTERED INDEX [ncix_ORD_LINE_ITEM_PAR_LINE_ITEM] ON [dbo].[ORD_LINE_ITEM] ([PAR_LINE_ITEM], [OLI_PROD_ID]) INCLUDE ([LINE_ITEM_DESC], [LINE_ITEM_ID], [ON_PACK_LIST], [POS], [QUANTITY], [RETAIL_PRICE], [RETAIL_PRICE_NATURAL], [UNIT_PRICE], [UNIT_PRICE_NATURAL]) ON [SALES_IND]
GO
GRANT SELECT ON  [dbo].[ORD_LINE_ITEM] TO [IDT_DATA_CLEANUP]
GRANT INSERT ON  [dbo].[ORD_LINE_ITEM] TO [IDT_DATA_CLEANUP]
GRANT DELETE ON  [dbo].[ORD_LINE_ITEM] TO [IDT_DATA_CLEANUP]
GRANT UPDATE ON  [dbo].[ORD_LINE_ITEM] TO [IDT_DATA_CLEANUP]
GO
EXEC sp_addextendedproperty N'MS_Description', N'customer order, line item details (many-to-one with header in ORD, index via column ORD_ID)', 'SCHEMA', N'dbo', 'TABLE', N'ORD_LINE_ITEM', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'total calculated price in IDT home currency', 'SCHEMA', N'dbo', 'TABLE', N'ORD_LINE_ITEM', 'COLUMN', N'CALC_PRICE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Not used', 'SCHEMA', N'dbo', 'TABLE', N'ORD_LINE_ITEM', 'COLUMN', N'GUAR_SHIP_DT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'? {not used ?}', 'SCHEMA', N'dbo', 'TABLE', N'ORD_LINE_ITEM', 'COLUMN', N'IS_SHIPPED'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Used for ordering of line items', 'SCHEMA', N'dbo', 'TABLE', N'ORD_LINE_ITEM', 'COLUMN', N'LEV'
GO
EXEC sp_addextendedproperty N'MS_Description', N'brief description of item', 'SCHEMA', N'dbo', 'TABLE', N'ORD_LINE_ITEM', 'COLUMN', N'LINE_ITEM_DESC'
GO
EXEC sp_addextendedproperty N'MS_Description', N'unique key (internal use - does not appear at user level {I think})', 'SCHEMA', N'dbo', 'TABLE', N'ORD_LINE_ITEM', 'COLUMN', N'LINE_ITEM_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'?', 'SCHEMA', N'dbo', 'TABLE', N'ORD_LINE_ITEM', 'COLUMN', N'LINE_ITEM_SUB_TYPE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'PROD_ID of product', 'SCHEMA', N'dbo', 'TABLE', N'ORD_LINE_ITEM', 'COLUMN', N'OLI_PROD_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'key into table ORD', 'SCHEMA', N'dbo', 'TABLE', N'ORD_LINE_ITEM', 'COLUMN', N'ORD_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'User overridden price in IDT home currency', 'SCHEMA', N'dbo', 'TABLE', N'ORD_LINE_ITEM', 'COLUMN', N'OVER_PRICE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Line item ID of parent if applicable', 'SCHEMA', N'dbo', 'TABLE', N'ORD_LINE_ITEM', 'COLUMN', N'PAR_LINE_ITEM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Used for ordering of line items', 'SCHEMA', N'dbo', 'TABLE', N'ORD_LINE_ITEM', 'COLUMN', N'POS'
GO
EXEC sp_addextendedproperty N'MS_Description', N'0 = calculated price; 1 = manual override; 2 = quote based price', 'SCHEMA', N'dbo', 'TABLE', N'ORD_LINE_ITEM', 'COLUMN', N'PRICE_TYPE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'unit quantity', 'SCHEMA', N'dbo', 'TABLE', N'ORD_LINE_ITEM', 'COLUMN', N'QUANTITY'
GO
EXEC sp_addextendedproperty N'MS_Description', N'optional quote number (key into QUOTE table)', 'SCHEMA', N'dbo', 'TABLE', N'ORD_LINE_ITEM', 'COLUMN', N'QUOTE_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Field for displaying enduser price in IDT home currency', 'SCHEMA', N'dbo', 'TABLE', N'ORD_LINE_ITEM', 'COLUMN', N'RETAIL_PRICE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Final retail price in customer''s currency', 'SCHEMA', N'dbo', 'TABLE', N'ORD_LINE_ITEM', 'COLUMN', N'RETAIL_PRICE_NATURAL'
GO
EXEC sp_addextendedproperty N'MS_Description', N'0 = use calculated retail price, 1 = use overridden price (for retail_price) ', 'SCHEMA', N'dbo', 'TABLE', N'ORD_LINE_ITEM', 'COLUMN', N'RETAIL_PRICE_TYPE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'price per unit in IDT home currency', 'SCHEMA', N'dbo', 'TABLE', N'ORD_LINE_ITEM', 'COLUMN', N'UNIT_PRICE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Final unit price in customer''s currency', 'SCHEMA', N'dbo', 'TABLE', N'ORD_LINE_ITEM', 'COLUMN', N'UNIT_PRICE_NATURAL'
GO
