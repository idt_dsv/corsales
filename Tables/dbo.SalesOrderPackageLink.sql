CREATE TABLE [dbo].[SalesOrderPackageLink]
(
[SalesOrderPackageLinkId] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[SalesOrderNumber] [int] NOT NULL,
[PackageId] [int] NOT NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[SalesOrderPackageLink] ADD CONSTRAINT [PK__SalesOrd__602C010C3BABACE1] PRIMARY KEY CLUSTERED  ([SalesOrderPackageLinkId]) ON [SALES_DATA]
GO
