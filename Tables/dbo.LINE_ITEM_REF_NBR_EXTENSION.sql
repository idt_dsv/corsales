CREATE TABLE [dbo].[LINE_ITEM_REF_NBR_EXTENSION]
(
[REF_ID] [int] NOT NULL,
[SHIP_DATE_CALC_EXPLANATION_ID] [int] NOT NULL,
[GUAR_COMP_DT_MINIMUM_DAYS] [int] NOT NULL,
[GUAR_COMP_DT_MAXIMUM_DAYS] [int] NOT NULL,
[MFG_PRIORITY] [int] NULL,
[MFG_PRIORITY_EXPLANATION_ID] [int] NULL,
[DEPT_EXPLANATION_ID] [int] NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[LINE_ITEM_REF_NBR_EXTENSION] ADD CONSTRAINT [LINE_ITEM_REF_NBR_EXTENSION_PK] PRIMARY KEY CLUSTERED  ([REF_ID]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table is used to contain additional data related to the information in the Line_Item_Ref_Nbr table, but not needed in as many locations.', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_REF_NBR_EXTENSION', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Department', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_REF_NBR_EXTENSION', 'COLUMN', N'DEPT_EXPLANATION_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The maximum number of working days needed to create this item, according to our guarantee.', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_REF_NBR_EXTENSION', 'COLUMN', N'GUAR_COMP_DT_MAXIMUM_DAYS'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The minimum number of working days needed to create this item, according to our guarantee.', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_REF_NBR_EXTENSION', 'COLUMN', N'GUAR_COMP_DT_MINIMUM_DAYS'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Priority flag for lab applications.  Initially will be normal (1) or expedite(2)', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_REF_NBR_EXTENSION', 'COLUMN', N'MFG_PRIORITY'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Describes how mfg_priority was set.  Calculated(1) or Overridden(2)', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_REF_NBR_EXTENSION', 'COLUMN', N'MFG_PRIORITY_EXPLANATION_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Link to the Line_Item_Ref_Nbr table.', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_REF_NBR_EXTENSION', 'COLUMN', N'REF_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Link to the Ship_Date_Calc_Explanation table to specify how to interpret the Guar_Comp_Dt and associated fields', 'SCHEMA', N'dbo', 'TABLE', N'LINE_ITEM_REF_NBR_EXTENSION', 'COLUMN', N'SHIP_DATE_CALC_EXPLANATION_ID'
GO
