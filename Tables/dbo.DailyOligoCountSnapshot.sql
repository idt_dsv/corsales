CREATE TABLE [dbo].[DailyOligoCountSnapshot]
(
[SalesBranch] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MfgBranch] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Department] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Oligos] [int] NULL,
[Bases] [int] NULL,
[AveLen] [int] NULL,
[OrderCnt] [int] NULL,
[RecordType] [int] NULL,
[DateUpdated] [datetime] NULL
) ON [SALES_DATA]
GO
