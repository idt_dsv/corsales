CREATE TABLE [dbo].[CC_AUTHORIZATION]
(
[CC_AUTHORIZATION_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[BATCH_ORD_ID] [int] NOT NULL,
[TRANSACTION_ID] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RESULT_CD] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TRANSACTION_TYPE] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ENTRY_DTM] [datetime] NULL CONSTRAINT [DF__CC_AUTHOR__ENTRY__078C1F06] DEFAULT (getdate()),
[CC_AUTHORIZATION_VERSION] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__CC_AUTHOR__CC_AU__0880433F] DEFAULT ('VERISIGN vs 1'),
[EXCHANGE_RATE] [float] NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[CC_AUTHORIZATION] ADD CONSTRAINT [PK_CC_AUTHORIZATION] PRIMARY KEY NONCLUSTERED  ([CC_AUTHORIZATION_ID]) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[CC_AUTHORIZATION] ADD CONSTRAINT [IX_CC_AUTHORIZATION] UNIQUE CLUSTERED  ([BATCH_ORD_ID], [TRANSACTION_TYPE]) ON [SALES_DATA]
GO
GRANT SELECT ON  [dbo].[CC_AUTHORIZATION] TO [IDT_WebApps]
GRANT INSERT ON  [dbo].[CC_AUTHORIZATION] TO [IDT_WebApps]
GRANT DELETE ON  [dbo].[CC_AUTHORIZATION] TO [IDT_WebApps]
GRANT UPDATE ON  [dbo].[CC_AUTHORIZATION] TO [IDT_WebApps]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Credit Card authorization information for invoicing. 
NOTE: Normally will have two records. One for Credit card authorization, and one for credit card sale.', 'SCHEMA', N'dbo', 'TABLE', N'CC_AUTHORIZATION', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to INV_HDR table', 'SCHEMA', N'dbo', 'TABLE', N'CC_AUTHORIZATION', 'COLUMN', N'BATCH_ORD_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary Key (Dumb Key) Identity Field', 'SCHEMA', N'dbo', 'TABLE', N'CC_AUTHORIZATION', 'COLUMN', N'CC_AUTHORIZATION_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Vendor and version used', 'SCHEMA', N'dbo', 'TABLE', N'CC_AUTHORIZATION', 'COLUMN', N'CC_AUTHORIZATION_VERSION'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date transaction recorded', 'SCHEMA', N'dbo', 'TABLE', N'CC_AUTHORIZATION', 'COLUMN', N'ENTRY_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The exchange rate to multiply to the invoice amount to get the total in the designated currency', 'SCHEMA', N'dbo', 'TABLE', N'CC_AUTHORIZATION', 'COLUMN', N'EXCHANGE_RATE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Result Cd from VeriSign', 'SCHEMA', N'dbo', 'TABLE', N'CC_AUTHORIZATION', 'COLUMN', N'RESULT_CD'
GO
EXEC sp_addextendedproperty N'MS_Description', N'VeriSign Transaction_id', 'SCHEMA', N'dbo', 'TABLE', N'CC_AUTHORIZATION', 'COLUMN', N'TRANSACTION_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'VeriSign Transaction Type - ''A'' = Authorization, ''S'' = Sale', 'SCHEMA', N'dbo', 'TABLE', N'CC_AUTHORIZATION', 'COLUMN', N'TRANSACTION_TYPE'
GO
