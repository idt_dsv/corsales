CREATE TABLE [dbo].[PARTY_CONFIRM_ADDR]
(
[CONFIRM_ADDR_ID] [int] NOT NULL,
[PARTY_ID] [int] NOT NULL,
[CONFIRM_ADDR_TYPE_ID] [tinyint] NOT NULL,
[CONFIRM_ADDR] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[USED_LAST_TIME] [bit] NOT NULL,
[CONFIRM_ORDER] [bit] NOT NULL CONSTRAINT [DF_PARTY_CONFIRM_ADDR_CONFIRM_ORDER] DEFAULT ((1)),
[CONFIRM_SHIPMENT] [bit] NOT NULL CONSTRAINT [DF_PARTY_CONFIRM_ADDR_CONFIRM_SHIPMENT] DEFAULT ((0))
) ON [SALES_DATA]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Mdewaard
-- Create date: 6/4/07
-- Description:	Trigger to troubleshoot address changes. Requested by B Viering from CLIPS meeting
-- =============================================
CREATE TRIGGER [dbo].[trg_PartyConfirmAddr_InsUpdDel] 
   ON  [dbo].[PARTY_CONFIRM_ADDR] 
   AFTER INSERT,DELETE,UPDATE

NOT FOR REPLICATION

AS 
BEGIN
	  -- SET NOCOUNT ON added to prevent extra result sets from
	  -- interfering with SELECT statements.
	  SET NOCOUNT ON;
  DECLARE @record_type VARCHAR(15)
  IF EXISTS (SELECT * FROM inserted) AND EXISTS (SELECT * FROM deleted)
    SET @record_type = 'Updated'
  ELSE IF EXISTS (SELECT * FROM inserted)
    SET @record_type = 'Inserted'
  ELSE 
    SET @record_type = 'Deleted'


  IF @record_type IN ('Updated', 'deleted')
  INSERT dbo.PARTY_CONFIRM_ADDR_AUDIT
    (CONFIRM_ADDR_ID, PARTY_ID, CONFIRM_ADDR_TYPE_ID, CONFIRM_ADDR, USED_LAST_TIME, CONFIRM_ORDER, CONFIRM_SHIPMENT, Modified_DTM, Modified_Workstation, Modified_Username, Modified_Type)
    SELECT
      CONFIRM_ADDR_ID
    , PARTY_ID
    , CONFIRM_ADDR_TYPE_ID
    , CONFIRM_ADDR
    , USED_LAST_TIME
    , CONFIRM_ORDER
    , CONFIRM_SHIPMENT
    , Modified_DTM = GETDATE()
    , Modified_Workstation = HOST_NAME()
    , Modified_Username = USER_NAME()
    , Modified_type = @record_type
    FROM
        deleted
  ELSE 
    INSERT dbo.PARTY_CONFIRM_ADDR_AUDIT
      (CONFIRM_ADDR_ID, PARTY_ID, CONFIRM_ADDR_TYPE_ID, CONFIRM_ADDR, USED_LAST_TIME, CONFIRM_ORDER, CONFIRM_SHIPMENT, Modified_DTM, Modified_Workstation, Modified_Username, Modified_Type)
      SELECT
        CONFIRM_ADDR_ID
      , PARTY_ID
      , CONFIRM_ADDR_TYPE_ID
      , CONFIRM_ADDR
      , USED_LAST_TIME
      , CONFIRM_ORDER
      , CONFIRM_SHIPMENT
      , Modified_DTM = GETDATE()
      , Modified_Workstation = HOST_NAME()
      , Modified_Username = USER_NAME()
      , Modified_type = @record_type
      FROM
        inserted

END
GO
ALTER TABLE [dbo].[PARTY_CONFIRM_ADDR] ADD CONSTRAINT [PK_PARTY_CONFIRM_ADDR] PRIMARY KEY NONCLUSTERED  ([CONFIRM_ADDR_ID]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE CLUSTERED INDEX [IX_PARTY_CONFIRM_ADDR] ON [dbo].[PARTY_CONFIRM_ADDR] ([PARTY_ID]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
GRANT SELECT ON  [dbo].[PARTY_CONFIRM_ADDR] TO [IDT_DATA_CLEANUP]
GRANT INSERT ON  [dbo].[PARTY_CONFIRM_ADDR] TO [IDT_DATA_CLEANUP]
GRANT DELETE ON  [dbo].[PARTY_CONFIRM_ADDR] TO [IDT_DATA_CLEANUP]
GRANT UPDATE ON  [dbo].[PARTY_CONFIRM_ADDR] TO [IDT_DATA_CLEANUP]
GO
EXEC sp_addextendedproperty N'MS_Description', N'1 = yes - send order confirmation', 'SCHEMA', N'dbo', 'TABLE', N'PARTY_CONFIRM_ADDR', 'COLUMN', N'CONFIRM_ORDER'
GO
EXEC sp_addextendedproperty N'MS_Description', N'1 = yes - send shipment confirmation', 'SCHEMA', N'dbo', 'TABLE', N'PARTY_CONFIRM_ADDR', 'COLUMN', N'CONFIRM_SHIPMENT'
GO
