CREATE TABLE [dbo].[FAX_ORDER]
(
[FAX_ORDER_ID] [int] NOT NULL,
[FAX_FROM] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FAX_DTM] [datetime] NOT NULL,
[IS_RECEIVE_ERROR] [bit] NOT NULL CONSTRAINT [DF_FAX_ORDER_IS_RECEIVE_ERROR] DEFAULT ((0)),
[FAX_FILE_NAME] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IS_FILE_ARCHIVED] [bit] NOT NULL CONSTRAINT [DF_FAX_ORDER_IS_FILE_ARCHIVED] DEFAULT ((0)),
[SOURCE_FILE_NAME] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UNLOCK_STATUS] [int] NOT NULL CONSTRAINT [DF_FAX_ORDER_UNLOCK_STATUS] DEFAULT ((0)),
[UNLOCK_USER] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CREATE_DTM] [datetime] NOT NULL CONSTRAINT [DF_FAX_ORDER_CREATE_DTM] DEFAULT (getdate())
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[FAX_ORDER] ADD CONSTRAINT [PK_FAX_ORDER] PRIMARY KEY CLUSTERED  ([FAX_ORDER_ID]) WITH (FILLFACTOR=100) ON [SALES_DATA]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Orders received via fax.', 'SCHEMA', N'dbo', 'TABLE', N'FAX_ORDER', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Record creation date/time', 'SCHEMA', N'dbo', 'TABLE', N'FAX_ORDER', 'COLUMN', N'CREATE_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date/time the fax was received', 'SCHEMA', N'dbo', 'TABLE', N'FAX_ORDER', 'COLUMN', N'FAX_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'File name (no path) of the stored fax image', 'SCHEMA', N'dbo', 'TABLE', N'FAX_ORDER', 'COLUMN', N'FAX_FILE_NAME'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Sender of the fax (often just have a phone number)', 'SCHEMA', N'dbo', 'TABLE', N'FAX_ORDER', 'COLUMN', N'FAX_FROM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key', 'SCHEMA', N'dbo', 'TABLE', N'FAX_ORDER', 'COLUMN', N'FAX_ORDER_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'1 if stored fax image has been archived to another folder, 0 otherwise', 'SCHEMA', N'dbo', 'TABLE', N'FAX_ORDER', 'COLUMN', N'IS_FILE_ARCHIVED'
GO
EXEC sp_addextendedproperty N'MS_Description', N'1 if fax receive encountered an error (fax may be incomplete), 0 otherwise', 'SCHEMA', N'dbo', 'TABLE', N'FAX_ORDER', 'COLUMN', N'IS_RECEIVE_ERROR'
GO
EXEC sp_addextendedproperty N'MS_Description', N'File name of the original fax image, including path from the fax server base storage folder', 'SCHEMA', N'dbo', 'TABLE', N'FAX_ORDER', 'COLUMN', N'SOURCE_FILE_NAME'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Unlock status code - represents Order Entry step in the process', 'SCHEMA', N'dbo', 'TABLE', N'FAX_ORDER', 'COLUMN', N'UNLOCK_STATUS'
GO
EXEC sp_addextendedproperty N'MS_Description', N'User name (network/Active Directory) of last person to change unlock status', 'SCHEMA', N'dbo', 'TABLE', N'FAX_ORDER', 'COLUMN', N'UNLOCK_USER'
GO
