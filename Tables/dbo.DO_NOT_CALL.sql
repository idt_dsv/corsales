CREATE TABLE [dbo].[DO_NOT_CALL]
(
[DO_NOT_CALL_ID] [int] NOT NULL,
[PARTY_ID] [int] NOT NULL,
[DO_NOT_CALL_REASON_ID] [int] NOT NULL,
[CUSTOM_REASON] [varchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FROM_DTM] [datetime] NOT NULL CONSTRAINT [DF_DO_NOT_CALL_FROM_DTM] DEFAULT (getdate()),
[THRU_DTM] [datetime] NOT NULL CONSTRAINT [DF_DO_NOT_CALL_THRU_DTM] DEFAULT ('9999-09-09'),
[UPDATE_USER] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REVIEW_DTM] [datetime] NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[DO_NOT_CALL] ADD CONSTRAINT [PK_DO_NOT_CALL] PRIMARY KEY CLUSTERED  ([DO_NOT_CALL_ID]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_DO_NOT_CALL_PARTY_ID] ON [dbo].[DO_NOT_CALL] ([PARTY_ID]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[DO_NOT_CALL] WITH NOCHECK ADD CONSTRAINT [FK_DO_NOT_CALL_REASON] FOREIGN KEY ([DO_NOT_CALL_REASON_ID]) REFERENCES [dbo].[DO_NOT_CALL_REASON] ([DO_NOT_CALL_REASON_ID]) NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[DO_NOT_CALL] WITH NOCHECK ADD CONSTRAINT [FK_DO_NOT_CALL_PARTY] FOREIGN KEY ([PARTY_ID]) REFERENCES [dbo].[PARTY] ([PARTY_ID]) NOT FOR REPLICATION
GO
EXEC sp_addextendedproperty N'MS_Description', N'IDT Support Do Not Call List.  Contains customer requests that IDT Support not call them for specific reasons.  One customer may have multiple records (one per reason).', 'SCHEMA', N'dbo', 'TABLE', N'DO_NOT_CALL', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Customer-specific reason not to call', 'SCHEMA', N'dbo', 'TABLE', N'DO_NOT_CALL', 'COLUMN', N'CUSTOM_REASON'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key', 'SCHEMA', N'dbo', 'TABLE', N'DO_NOT_CALL', 'COLUMN', N'DO_NOT_CALL_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'DO_NOT_CALL_REASON table key', 'SCHEMA', N'dbo', 'TABLE', N'DO_NOT_CALL', 'COLUMN', N'DO_NOT_CALL_REASON_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Activation date of request', 'SCHEMA', N'dbo', 'TABLE', N'DO_NOT_CALL', 'COLUMN', N'FROM_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Party ID of customer (organization or person)', 'SCHEMA', N'dbo', 'TABLE', N'DO_NOT_CALL', 'COLUMN', N'PARTY_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'the DTM of the next review for the entry. ', 'SCHEMA', N'dbo', 'TABLE', N'DO_NOT_CALL', 'COLUMN', N'REVIEW_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Termination date of request', 'SCHEMA', N'dbo', 'TABLE', N'DO_NOT_CALL', 'COLUMN', N'THRU_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'User name (network/Active Directory) of last person to modify this record', 'SCHEMA', N'dbo', 'TABLE', N'DO_NOT_CALL', 'COLUMN', N'UPDATE_USER'
GO
