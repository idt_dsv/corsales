CREATE TABLE [dbo].[INV_ADDR_TYPE]
(
[INV_ADDR_TYPE_ID] [int] NOT NULL,
[NAME] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DESCRIPTION] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[INV_ADDR_TYPE] ADD CONSTRAINT [PK_INV_ADDR_TYPE] PRIMARY KEY CLUSTERED  ([INV_ADDR_TYPE_ID]) ON [SALES_DATA]
GO
EXEC sp_addextendedproperty N'MS_Description', N'A lookup list of different address types to be used for invoices', 'SCHEMA', N'dbo', 'TABLE', N'INV_ADDR_TYPE', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'description of address type', 'SCHEMA', N'dbo', 'TABLE', N'INV_ADDR_TYPE', 'COLUMN', N'DESCRIPTION'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Prim key', 'SCHEMA', N'dbo', 'TABLE', N'INV_ADDR_TYPE', 'COLUMN', N'INV_ADDR_TYPE_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Name of address type', 'SCHEMA', N'dbo', 'TABLE', N'INV_ADDR_TYPE', 'COLUMN', N'NAME'
GO
