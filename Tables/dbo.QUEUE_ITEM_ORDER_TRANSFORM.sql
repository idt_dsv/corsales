CREATE TABLE [dbo].[QUEUE_ITEM_ORDER_TRANSFORM]
(
[QUEUE_ITEM_ID] [int] NOT NULL,
[ENTERED_DTM] [datetime] NULL,
[ACTIVE_DTM] [datetime] NULL,
[BF_BOBJECT_CID] [int] NULL,
[BF_BOBJECT_OID] [int] NULL,
[QUEUE_DEFINITION_ID] [int] NULL,
[CLAIMED_WORKSTATION_ID] [int] NULL,
[CLAIMED_DTM] [datetime] NULL,
[COMPLETED_DTM] [datetime] NULL,
[PRIORITY] [int] NULL,
[ACTIVE] [int] NULL,
[APPLICATION_ID] [int] NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[QUEUE_ITEM_ORDER_TRANSFORM] ADD CONSTRAINT [PK_QUEUE_ITEM_ORDER_TRANSFORM] PRIMARY KEY CLUSTERED  ([QUEUE_ITEM_ID]) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [ix_QueueItemOrderTransform_BfBobjectCid] ON [dbo].[QUEUE_ITEM_ORDER_TRANSFORM] ([BF_BOBJECT_CID], [BF_BOBJECT_OID], [QUEUE_DEFINITION_ID], [CLAIMED_DTM], [COMPLETED_DTM], [ACTIVE]) INCLUDE ([ACTIVE_DTM], [APPLICATION_ID], [CLAIMED_WORKSTATION_ID], [ENTERED_DTM], [PRIORITY], [QUEUE_ITEM_ID]) ON [SALES_DATA]
GO
EXEC sp_addextendedproperty N'MS_Description', N'holds items waiting for an Order Transfer Event or compleated items.', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_ORDER_TRANSFORM', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'flag indicating if this is an active item or not.', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_ORDER_TRANSFORM', 'COLUMN', N'ACTIVE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date/time that the queue entry became/will become due for processing ', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_ORDER_TRANSFORM', 'COLUMN', N'ACTIVE_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'the ID  of the process that has claimed the queue item  ', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_ORDER_TRANSFORM', 'COLUMN', N'APPLICATION_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to BF_BOBJECT.BF_BOBJECT_ID to class Id of the object being queued - named like this for consistancy with event structures', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_ORDER_TRANSFORM', 'COLUMN', N'BF_BOBJECT_CID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'the instance id of the object being queued ', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_ORDER_TRANSFORM', 'COLUMN', N'BF_BOBJECT_OID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'when this item was last claimed', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_ORDER_TRANSFORM', 'COLUMN', N'CLAIMED_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to WORKSTATION.WORKSTATION_ID - the workstation that has claimed this queue item', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_ORDER_TRANSFORM', 'COLUMN', N'CLAIMED_WORKSTATION_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'when the item was marked as completed. This also serves as an indicator that the item is complete.', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_ORDER_TRANSFORM', 'COLUMN', N'COMPLETED_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date/time that the queue entry was created', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_ORDER_TRANSFORM', 'COLUMN', N'ENTERED_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'integer value indicating the priority of the item.', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_ORDER_TRANSFORM', 'COLUMN', N'PRIORITY'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to QUEUE_DEFINITION the queue event the object is waiting for', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_ORDER_TRANSFORM', 'COLUMN', N'QUEUE_DEFINITION_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary Key', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_ORDER_TRANSFORM', 'COLUMN', N'QUEUE_ITEM_ID'
GO
