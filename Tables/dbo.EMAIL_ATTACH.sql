CREATE TABLE [dbo].[EMAIL_ATTACH]
(
[Attach_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Msg_ID] [int] NOT NULL,
[Attach_Name] [varchar] (164) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Attach_Body] [varbinary] (8000) NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[EMAIL_ATTACH] ADD CONSTRAINT [PK_EMAIL_ATTACH] PRIMARY KEY CLUSTERED  ([Attach_ID]) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [ix_EMAIL_ATTACH] ON [dbo].[EMAIL_ATTACH] ([Msg_ID]) ON [SALES_DATA]
GO
