CREATE TABLE [dbo].[DO_NOT_CALL_REASON_RULE]
(
[DO_NOT_CALL_REASON_RULE_ID] [int] NOT NULL,
[DO_NOT_CALL_REASON_ID] [int] NOT NULL,
[DOES_APPLY_TO_ALL_RULES] [bit] NOT NULL,
[MANUAL_REASON] [bit] NOT NULL,
[RULE_SET_RULE_ID] [int] NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[DO_NOT_CALL_REASON_RULE] ADD CONSTRAINT [PK__DO_NOT_CALL_REAS__7E77B618] PRIMARY KEY CLUSTERED  ([DO_NOT_CALL_REASON_RULE_ID]) ON [SALES_DATA]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Association between a DNC reason and the rules not to call for. ', 'SCHEMA', N'dbo', 'TABLE', N'DO_NOT_CALL_REASON_RULE', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'fk to DO_NOT_CALL_REASON table ', 'SCHEMA', N'dbo', 'TABLE', N'DO_NOT_CALL_REASON_RULE', 'COLUMN', N'DO_NOT_CALL_REASON_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary Key', 'SCHEMA', N'dbo', 'TABLE', N'DO_NOT_CALL_REASON_RULE', 'COLUMN', N'DO_NOT_CALL_REASON_RULE_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'1 if the reason applies to all rules - these are rules like "never call" ', 'SCHEMA', N'dbo', 'TABLE', N'DO_NOT_CALL_REASON_RULE', 'COLUMN', N'DOES_APPLY_TO_ALL_RULES'
GO
EXEC sp_addextendedproperty N'MS_Description', N'1 if the reason needs to be determined by a person.', 'SCHEMA', N'dbo', 'TABLE', N'DO_NOT_CALL_REASON_RULE', 'COLUMN', N'MANUAL_REASON'
GO
EXEC sp_addextendedproperty N'MS_Description', N'fk to RULE_SET_RULE table. Will be null if this record applies to all rules. ', 'SCHEMA', N'dbo', 'TABLE', N'DO_NOT_CALL_REASON_RULE', 'COLUMN', N'RULE_SET_RULE_ID'
GO
