CREATE TABLE [dbo].[QUEUE_ITEM_SALES]
(
[QUEUE_ITEM_SALES_ID] [int] NOT NULL,
[ENTERED_DTM] [datetime] NULL,
[ACTIVE_DTM] [datetime] NULL,
[BF_BOBJECT_CID] [int] NULL,
[BF_BOBJECT_OID] [int] NULL,
[QUEUE_DEFINITION_ID] [int] NULL,
[CLAIMED_WORKSTATION_ID] [int] NULL,
[APPLICATION_ID] [int] NULL,
[CLAIMED_DTM] [datetime] NULL,
[COMPLETED_DTM] [datetime] NULL,
[PRIORITY] [int] NULL,
[ACTIVE] [int] NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[QUEUE_ITEM_SALES] ADD CONSTRAINT [PK__QUEUE_ITEM_SALES__3EC9A75C] PRIMARY KEY CLUSTERED  ([QUEUE_ITEM_SALES_ID]) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [ix_queue_item_sales] ON [dbo].[QUEUE_ITEM_SALES] ([BF_BOBJECT_OID]) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [ix_queue_item_sales_Load_queue] ON [dbo].[QUEUE_ITEM_SALES] ([QUEUE_DEFINITION_ID], [BF_BOBJECT_CID], [ACTIVE], [QUEUE_ITEM_SALES_ID], [BF_BOBJECT_OID], [CLAIMED_WORKSTATION_ID], [COMPLETED_DTM], [APPLICATION_ID]) ON [SALES_DATA]
GO
GRANT VIEW DEFINITION ON  [dbo].[QUEUE_ITEM_SALES] TO [IDT-CORALVILLE\IUSR_IDT]
GRANT SELECT ON  [dbo].[QUEUE_ITEM_SALES] TO [IDT-CORALVILLE\IUSR_IDT]
GRANT UPDATE ON  [dbo].[QUEUE_ITEM_SALES] TO [IDT-CORALVILLE\IUSR_IDT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Holds items waiting for an event or compleated items. Used primarely for order processing.', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_SALES', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Flag indicating if this is an active item or not.', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_SALES', 'COLUMN', N'ACTIVE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date/time that the queue entry became/will become due for processing ', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_SALES', 'COLUMN', N'ACTIVE_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The ID  of the process that has claimed the queue item  ', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_SALES', 'COLUMN', N'APPLICATION_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to BF_BOBJECT.BF_BOBJECT_ID to class Id of the object being queued - named like this for consistancy with event structures', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_SALES', 'COLUMN', N'BF_BOBJECT_CID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The instance id of the object being queued ', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_SALES', 'COLUMN', N'BF_BOBJECT_OID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'When this item was last claimed', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_SALES', 'COLUMN', N'CLAIMED_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to WORKSTATION.WORKSTATION_ID - the workstation that has claimed this queue item', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_SALES', 'COLUMN', N'CLAIMED_WORKSTATION_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'When the item was marked as completed. ', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_SALES', 'COLUMN', N'COMPLETED_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date/time that the queue entry was created', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_SALES', 'COLUMN', N'ENTERED_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Integer value indicating the priority of the item.', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_SALES', 'COLUMN', N'PRIORITY'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to QUEUE_DEFINITION the queue event the object is waiting for', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_SALES', 'COLUMN', N'QUEUE_DEFINITION_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary Key', 'SCHEMA', N'dbo', 'TABLE', N'QUEUE_ITEM_SALES', 'COLUMN', N'QUEUE_ITEM_SALES_ID'
GO
