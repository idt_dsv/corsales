CREATE TABLE [dbo].[POOL_CREDIT_USAGE_TYPE]
(
[POOL_CREDIT_USAGE_TYPE_ID] [int] NOT NULL,
[DESCRIPTION] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[POOL_CREDIT_USAGE_TYPE] ADD CONSTRAINT [PK_POOL_CREDIT_USAGE_TYPE] PRIMARY KEY CLUSTERED  ([POOL_CREDIT_USAGE_TYPE_ID]) ON [SALES_DATA]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Lookup table for usage types available for pool credits', 'SCHEMA', N'dbo', 'TABLE', N'POOL_CREDIT_USAGE_TYPE', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Description of usage type', 'SCHEMA', N'dbo', 'TABLE', N'POOL_CREDIT_USAGE_TYPE', 'COLUMN', N'DESCRIPTION'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary Key', 'SCHEMA', N'dbo', 'TABLE', N'POOL_CREDIT_USAGE_TYPE', 'COLUMN', N'POOL_CREDIT_USAGE_TYPE_ID'
GO
