CREATE TABLE [dbo].[OLIGO_CARD_JOB]
(
[OLIGO_CARD_JOB_ID] [int] NOT NULL IDENTITY(1, 1),
[OLIGO_CARD_ID] [int] NOT NULL,
[ACTIVE] [bit] NOT NULL,
[LOW_BALANCE_AMT] [money] NULL,
[EMAIL_ADDR] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FREQUENCY] [int] NULL CONSTRAINT [DF_OLIGO_CARD_JOB_FREQUENCY] DEFAULT ((1)),
[LAST_CHECK_DT] [datetime] NULL,
[SUCCESS_DT] [datetime] NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[OLIGO_CARD_JOB] ADD CONSTRAINT [PK_OLIGO_CARD_JOB] PRIMARY KEY CLUSTERED  ([OLIGO_CARD_JOB_ID]) ON [SALES_DATA]
GO
GRANT SELECT ON  [dbo].[OLIGO_CARD_JOB] TO [IDT_WebApps]
GRANT INSERT ON  [dbo].[OLIGO_CARD_JOB] TO [IDT_WebApps]
GRANT DELETE ON  [dbo].[OLIGO_CARD_JOB] TO [IDT_WebApps]
GRANT UPDATE ON  [dbo].[OLIGO_CARD_JOB] TO [IDT_WebApps]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Is the job active', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_JOB', 'COLUMN', N'ACTIVE'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Email address on record to send the job', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_JOB', 'COLUMN', N'EMAIL_ADDR'
GO
EXEC sp_addextendedproperty N'MS_Description', N'How often to Check. 1 = once a day, 2 once a week, 3 once a month', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_JOB', 'COLUMN', N'FREQUENCY'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Last check of active job', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_JOB', 'COLUMN', N'LAST_CHECK_DT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The low balance threshold for sending a email', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_JOB', 'COLUMN', N'LOW_BALANCE_AMT'
GO
EXEC sp_addextendedproperty N'MS_Description', N'ID of the oligocard', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_JOB', 'COLUMN', N'OLIGO_CARD_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Unique Dumb Key', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_JOB', 'COLUMN', N'OLIGO_CARD_JOB_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Marks if job was a success on the last check', 'SCHEMA', N'dbo', 'TABLE', N'OLIGO_CARD_JOB', 'COLUMN', N'SUCCESS_DT'
GO
