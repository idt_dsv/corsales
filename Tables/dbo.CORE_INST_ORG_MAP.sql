CREATE TABLE [dbo].[CORE_INST_ORG_MAP]
(
[CORE_INST_ID] [int] NOT NULL,
[ORG_ID] [int] NOT NULL,
[DEFAULT_ACC_ID] [int] NULL,
[BILL_ACCT_CD] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_MSO] [bit] NOT NULL CONSTRAINT [DF__CORE_INST__IS_MS__74AE54BC] DEFAULT ((1))
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[CORE_INST_ORG_MAP] ADD CONSTRAINT [PK_CORE_INST_ORG_MAP] PRIMARY KEY CLUSTERED  ([CORE_INST_ID], [ORG_ID]) ON [SALES_DATA]
GO
GRANT SELECT ON  [dbo].[CORE_INST_ORG_MAP] TO [IDT_DATA_CLEANUP]
GRANT INSERT ON  [dbo].[CORE_INST_ORG_MAP] TO [IDT_DATA_CLEANUP]
GRANT DELETE ON  [dbo].[CORE_INST_ORG_MAP] TO [IDT_DATA_CLEANUP]
GRANT UPDATE ON  [dbo].[CORE_INST_ORG_MAP] TO [IDT_DATA_CLEANUP]
GO
EXEC sp_addextendedproperty N'MS_Description', N'A mapping table between the Web Database Core Institutions that have a Web Portal and the Production Account info.', 'SCHEMA', N'dbo', 'TABLE', N'CORE_INST_ORG_MAP', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'The specified billing Account to be used for the ORG. It is a foreign Key to Epicor Billing Customer Account', 'SCHEMA', N'dbo', 'TABLE', N'CORE_INST_ORG_MAP', 'COLUMN', N'BILL_ACCT_CD'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The Core institution from the website. It is a foreign Key to Website CORE_INST table', 'SCHEMA', N'dbo', 'TABLE', N'CORE_INST_ORG_MAP', 'COLUMN', N'CORE_INST_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The default Account for the ORG. It is a foreign Key to ACCOUNT table''s party_id', 'SCHEMA', N'dbo', 'TABLE', N'CORE_INST_ORG_MAP', 'COLUMN', N'DEFAULT_ACC_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Does this core lab have consolidated shipments (MSO - multiple shipment organization).', 'SCHEMA', N'dbo', 'TABLE', N'CORE_INST_ORG_MAP', 'COLUMN', N'IS_MSO'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The party_id of the Organization. It is a foreign Key to ORG table', 'SCHEMA', N'dbo', 'TABLE', N'CORE_INST_ORG_MAP', 'COLUMN', N'ORG_ID'
GO
