CREATE TABLE [dbo].[PERSON]
(
[PARTY_ID] [int] NOT NULL,
[ENDUSER_NBR] [int] NULL,
[LAST_NM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FIRST_NM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MID_NM] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PER_TITLE] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SUFFIX] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SEX] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PREV_LAST_NM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EMPL_CUST] [int] NULL,
[ACC_ID] [int] NULL,
[ORG_ID] [int] NULL,
[BILL_CONFIG] [int] NULL,
[HR_EMPLOYEE_ID] [int] NULL,
[INV_ADDR_TYPE_ID] [int] NULL,
[INV_DELIVERY_METH_ID] [int] NULL,
[INV_REVIEW] [int] NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[PERSON] ADD CONSTRAINT [PK__PERSON__7908F585] PRIMARY KEY NONCLUSTERED  ([PARTY_ID]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_PERSON_1] ON [dbo].[PERSON] ([ACC_ID]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_PERSON_2] ON [dbo].[PERSON] ([ENDUSER_NBR]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE CLUSTERED INDEX [IX_PERSON] ON [dbo].[PERSON] ([LAST_NM], [FIRST_NM]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
CREATE NONCLUSTERED INDEX [IX_PERSON_3] ON [dbo].[PERSON] ([ORG_ID]) WITH (FILLFACTOR=80) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[PERSON] WITH NOCHECK ADD CONSTRAINT [FK_PERSON_INV_ADDR_TYPE] FOREIGN KEY ([INV_ADDR_TYPE_ID]) REFERENCES [dbo].[INV_ADDR_TYPE] ([INV_ADDR_TYPE_ID]) NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[PERSON] WITH NOCHECK ADD CONSTRAINT [FK_PERSON_PARTY] FOREIGN KEY ([PARTY_ID]) REFERENCES [dbo].[PARTY] ([PARTY_ID]) NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[PERSON] NOCHECK CONSTRAINT [FK_PERSON_INV_ADDR_TYPE]
GO
GRANT SELECT ON  [dbo].[PERSON] TO [IDT_DATA_CLEANUP]
GRANT INSERT ON  [dbo].[PERSON] TO [IDT_DATA_CLEANUP]
GRANT DELETE ON  [dbo].[PERSON] TO [IDT_DATA_CLEANUP]
GRANT UPDATE ON  [dbo].[PERSON] TO [IDT_DATA_CLEANUP]
GRANT SELECT ON  [dbo].[PERSON] TO [IDT_INTRANET_USER]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Information about customer.', 'SCHEMA', N'dbo', 'TABLE', N'PERSON', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Link to Account table', 'SCHEMA', N'dbo', 'TABLE', N'PERSON', 'COLUMN', N'ACC_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Explicit number that is assigned to customer', 'SCHEMA', N'dbo', 'TABLE', N'PERSON', 'COLUMN', N'ENDUSER_NBR'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Customer''s first name', 'SCHEMA', N'dbo', 'TABLE', N'PERSON', 'COLUMN', N'FIRST_NM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Link to HR system', 'SCHEMA', N'dbo', 'TABLE', N'PERSON', 'COLUMN', N'HR_EMPLOYEE_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Foreign key to INV_ADDR_TYPE table', 'SCHEMA', N'dbo', 'TABLE', N'PERSON', 'COLUMN', N'INV_ADDR_TYPE_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Field for preferred delivery method for invoices', 'SCHEMA', N'dbo', 'TABLE', N'PERSON', 'COLUMN', N'INV_DELIVERY_METH_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Flag for if person needs to be reviewed by invoicing', 'SCHEMA', N'dbo', 'TABLE', N'PERSON', 'COLUMN', N'INV_REVIEW'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Customer''s Last name', 'SCHEMA', N'dbo', 'TABLE', N'PERSON', 'COLUMN', N'LAST_NM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Middle initial', 'SCHEMA', N'dbo', 'TABLE', N'PERSON', 'COLUMN', N'MID_NM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Link to Org table', 'SCHEMA', N'dbo', 'TABLE', N'PERSON', 'COLUMN', N'ORG_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary key', 'SCHEMA', N'dbo', 'TABLE', N'PERSON', 'COLUMN', N'PARTY_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Ccustomer''s agenda', 'SCHEMA', N'dbo', 'TABLE', N'PERSON', 'COLUMN', N'SEX'
GO
