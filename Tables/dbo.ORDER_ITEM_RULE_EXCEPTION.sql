CREATE TABLE [dbo].[ORDER_ITEM_RULE_EXCEPTION]
(
[ORDER_ITEM_RULE_EXCEPTION_ID] [int] NOT NULL,
[ORD_ID] [int] NOT NULL,
[REF_ID] [int] NOT NULL,
[RULE_SET_RULE_ID] [int] NOT NULL,
[DNC_STATUS] [int] NOT NULL,
[FROM_DTM] [datetime] NOT NULL,
[THRU_DTM] [datetime] NULL
) ON [SALES_DATA]
GO
ALTER TABLE [dbo].[ORDER_ITEM_RULE_EXCEPTION] ADD CONSTRAINT [PK__ORDER_RULE__0618D7E0] PRIMARY KEY CLUSTERED  ([ORDER_ITEM_RULE_EXCEPTION_ID]) ON [SALES_DATA]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table stores the alerts that have been triggered on an order item (ref_id) based on the ordering rules.', 'SCHEMA', N'dbo', 'TABLE', N'ORDER_ITEM_RULE_EXCEPTION', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'The DNC status of the rule 0 = call, 1 = warning, 2 = Do not call', 'SCHEMA', N'dbo', 'TABLE', N'ORDER_ITEM_RULE_EXCEPTION', 'COLUMN', N'DNC_STATUS'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The Datetime the record was added', 'SCHEMA', N'dbo', 'TABLE', N'ORDER_ITEM_RULE_EXCEPTION', 'COLUMN', N'FROM_DTM'
GO
EXEC sp_addextendedproperty N'MS_Description', N'FK to the Ord table', 'SCHEMA', N'dbo', 'TABLE', N'ORDER_ITEM_RULE_EXCEPTION', 'COLUMN', N'ORD_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Primary Key', 'SCHEMA', N'dbo', 'TABLE', N'ORDER_ITEM_RULE_EXCEPTION', 'COLUMN', N'ORDER_ITEM_RULE_EXCEPTION_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'This is the line item (REF_ID) referenced. This only a logical FK to line_item_ref_nbr) because the Line Item may be deleted in the ordering system.', 'SCHEMA', N'dbo', 'TABLE', N'ORDER_ITEM_RULE_EXCEPTION', 'COLUMN', N'REF_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The rule that was flagged. ', 'SCHEMA', N'dbo', 'TABLE', N'ORDER_ITEM_RULE_EXCEPTION', 'COLUMN', N'RULE_SET_RULE_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The Datetime the record was no longer valid. There should be a date only when the order has changed and the alert for the rule no longer applied.', 'SCHEMA', N'dbo', 'TABLE', N'ORDER_ITEM_RULE_EXCEPTION', 'COLUMN', N'THRU_DTM'
GO
